 ``SQL/DATASET1/mysql76


	{% for item in data %}
	 
		(select '{{ item.V1_LABEL}}' AS V1_LABEL,trans_year 년도,section,channel,regname   , sum(issuecount) as issuecount , sum(recamount) as 'recamount',sum(recamount/issuecount) as AVGVALUE  
		from mw_tran WHERE 1=1   {{ item.V1}}  [[AND issuecount > ##issuecount##]]
		group by trans_year,section,channel,regname  limit 1 )
         {% if forloop.last == false %}
		    {{ item.UNION_STR}}
		{% endif %}
		
	{% endfor %}
	
``ENDSQL
``SQL/DATASET2/mysql76

   {% if DBMS == 'ORACLE' %}
	select 'ORACLE' as DBMSTYPE ,2 AS NUMM
   {%else%}
    select '{{ item.V1_LABEL}}' AS V1_LABEL,trans_year 년도,section,channel,regname   , sum(issuecount) as issuecount , sum(recamount) as 'recamount',sum(recamount/issuecount) as AVGVALUE  
		from mw_tran WHERE 1=1     
		group by trans_year,section,channel,regname  limit 1 
   {% endif %}
	
``ENDSQL
``HTML

  
   DATASET1  -> %%DATASET1%%.%%GROUPBY()VAR()STAT(SUM)OUTPUTTYPE(BASE_RESULTSET)%%
 
  
   DATASET2  -> %%DATASET2%%.%%GROUPBY()VAR()STAT(SUM)OUTPUTTYPE(BASE_RESULTSET)%%
``ENDHTML