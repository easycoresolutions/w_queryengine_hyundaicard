``SQL

 SELECT @BYNAME@, COUNT(*) AS COUNT FROM (SELECT CUST_NO FROM QUADMAX_WORK.@NODE_TABLE@) A 
 INNER JOIN QUADMAX.Z_CUST_INFO B ON A.CUST_NO = B.MBR_NO 
 GROUP BY @BYNAME@

``ENDSQL

``HTML


<div class="col col-12" id="container"></div> 
 
<script>
Highcharts.chart('container', {
  chart: {
    plotBackgroundColor: null,
    plotBorderWidth: null,
    plotShadow: false,
	 backgroundColor:'white' ,
    type: 'pie'
  },
    colors: ['#49919d', '#549f61', '#9dbc58', '#ced784', '#84c7bb', '#84a4c7', 
             '#a1aad4', '#c4c8cf', '#6AF9C4'],
 
  title: {
    text: '추출 대상고객 분포'
  },
  
  tooltip: {
    pointFormat: '{series.name}: <b>{point.percentage:.1f}%</b>'
  },
  plotOptions: {
    column: {
      allowPointSelect: true,
      cursor: 'pointer',
      dataLabels: {
        enabled: true,
        format: '<b>{point.name}</b>: {point.percentage:.1f} %',
        style: {
          color: (Highcharts.theme && Highcharts.theme.contrastTextColor) || 'black'
        }
      }
    }
  },
  series: [{
    name: '',
	label : '',
    colorByPoint: true,
    data: @@PIE@@
  }]
});

</script> 
``ENDHTML