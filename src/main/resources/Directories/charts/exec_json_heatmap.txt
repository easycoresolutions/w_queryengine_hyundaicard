 ``SQL
select trans_year , section,channel as CHANNEL_NAMNE,regname ,sum(issuecount) as issuecount , sum(recamount) as 'recamount1',sum(recamount/issuecount) as AVGVALUE  from mw_tran WHERE 1=1 [[AND issuecount > ##recamt##]] [ and Trans_year = @@year@@]   group by trans_year , regname , section ,channel  order by channel, section  ,trans_year , regname  

``ENDSQL
``HTML
<div class="header">
  <div class="container-fluid">
    <div class="row">
      <div class="col-lg-12">
        <div class="nav-top">
          <div class="media">
            <div class="media-body">
              <h4>Dashboard</h4>
              <h2>Lead Funnel Dashboard</h2>
              <p>As of Jan 3, 2019 3:21PM, Viewing as Alex </p>
            </div>
          </div>
        </div>
      </div>
    </div>
  </div>
</div>
<div class="nav-area">
  <div class="container-fluid ">
    <div class="row">
      <div class="col-lg-12">
        <div class="nav-btn-area">
          <div class="row">
            <div class="col-lg-5 col-md-5">
              <div class="nav-btn-group">
                <div class="btn-list">
                  <a href="#" class="btn-default">오늘</a>
                  <a href="#" class="btn-default">어제</a>
                  <a href="#" class="btn-default">지난1주</a>
                  <a href="#" class="btn-default">지난1달</a>
                  <a href="#" class="btn-default">지난분기</a>
                  <a href="#" class="btn-default">지난1년</a>
                </div>
            </div>
          </div>
          <div class="col-lg-3 col-md-3">
           <div class="btn-group btn-group-justified">
            <a href="#" class="btn btn-default"><i class="fa fa-calendar"></i> 14 Nov 2018 - 13 Nov 2019</a>
          </div> 
		             <select class="selectpicker" data-width="100%" placehoder ="aaa" multiple>
              
              <option value="1">조건1</option>
              <option value="2">조건2</option>
              <option value="3">조건3</option>
            </select>
        </div>
          <div class="col-lg-4 col-md-4">
           <div class="select-btn1">
            <select class="selectpicker" data-width="100%" placehoder ="aaa" multiple>
              
              <option value="1">조건1</option>
              <option value="2">조건2</option>
              <option value="3">조건3</option>
            </select>

            <select class="selectpicker" data-width="100%">
              <option value="">Accuracy: 100%</option>
              <option value="">Accuracy: 100%</option>
              <option value="">Accuracy: 100%</option>
              <option value="">Accuracy: 100%</option>
            </select>

            <select class="selectpicker" data-width="100%">
              <option value="" data-icon="fa fa-comment"> 7/7</option>
              <option value="" data-icon="fa fa-comment">7/7</option>
              <option value="" data-icon="fa fa-comment">7/7</option>
              <option value="" data-icon="fa fa-comment"> 7/7</option>
            </select>
          </div> 
        </div>
      </div>
    </div>
  </div>
</div>
</div>
</div>
<div class="container-fluid pb-60">
    <div class="row">

      <div class="col-lg-4 mb-10">
        <div class="chart-area">
          <div class="title-area mb-10">
            <div class="title">
              <h3>Top Campaigns</h3>
              <p>By Lead Generated</p>
            </div>
            <div class="set">
              <span class="dropdown-toggle" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false"><i class="fa fa-ellipsis-v"></i></span>
              <ul class="dropdown-menu right">
                <li><a href="#"><img src="assets/images/1.png" alt=""> Change Chart </a></li>
                <li><a href="#"><img src="assets/images/2.png" alt=""> Change Analysis Dimension</a></li>
                <li><a href="#"><img src="assets/images/3.png" alt=""> Change Analysis Measures</a></li>
              </ul>
            </div>
          </div>
        
        <div class="chart-field">
          <div class="chart" id="container1"></div>
          <div class="click-list">
            <h4>Which analysis view do you want?</h4>
            <ul>
              <li><a href="#">Campaign Details</a></li>
              <li><a href="#">Details By Channel</a></li>
              <li><a href="#">Details By Offer</a></li>
            </ul>
          </div>
        </div>
        </div>
      </div>


      <div class="col-lg-4 mb-10">
        <div class="chart-area">
          <div class="title-area mb-10">
            <div class="title">
              <h3>Expected Revenue by Marketing</h3>
              <p>By Lead Generated</p>
            </div>
            <div class="set">
              <span class="dropdown-toggle" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false"><i class="fa fa-ellipsis-v"></i></span>
              <ul class="dropdown-menu right">
                <li><a href="#"><img src="assets/images/1.png" alt=""> Change Chart </a></li>
                <li><a href="#"><img src="assets/images/2.png" alt=""> Change Analysis Dimension</a></li>
                <li><a href="#"><img src="assets/images/3.png" alt=""> Change Analysis Measures</a></li>
              </ul>
            </div>
          </div>
          <div class="chart-field">
            <div class="chart" id="container2" ></div>
            <div class="click-list">
              <h4>Which analysis view do you want?</h4>
              <ul>
                <li><a href="#">Campaign Details</a></li>
                <li><a href="#">Details By Channel</a></li>
                <li><a href="#">Details By Offer</a></li>
              </ul>
            </div>
          </div>
        </div>
      </div>
  
	 <div class="col-lg-4 mb-10">
        <div class="chart-area">
          <div class="title-area mb-10">
            <div class="title">
              <h3>Expected Revenue by Marketing</h3>
              <p>By Lead Generated</p>
            </div>
            <div class="set">
              <span class="dropdown-toggle" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false"><i class="fa fa-ellipsis-v"></i></span>
              <ul class="dropdown-menu right">
                <li><a href="#"><img src="assets/images/1.png" alt=""> Change Chart </a></li>
                <li><a href="#"><img src="assets/images/2.png" alt=""> Change Analysis Dimension</a></li>
                <li><a href="#"><img src="assets/images/3.png" alt=""> Change Analysis Measures</a></li>
              </ul>
            </div>
          </div>
          <div class="chart-field">
            <div class="chart" id="container3" ></div>
            <div class="click-list">
              <h4>Which analysis view do you want?</h4>
              <ul>
                <li><a href="#">Campaign Details</a></li>
                <li><a href="#">Details By Channel</a></li>
                <li><a href="#">Details By Offer</a></li>
              </ul>
            </div>
          </div>
        </div>
      </div>
    </div>
</div>
<script>
 Highcharts.chart('container1', {

    chart: {
        type: 'heatmap',
        marginTop: 40,
        marginBottom: 80,
        plotBorderWidth: 1
    },


    title: {
        text: 'Sales per employee per weekday'
    },

    xAxis: {
        categories:  %%GROUPBY(section)VAR(issuecount)STAT(SUM)OUTPUTTYPE(CATEGORY)%% 
    },

    yAxis: {
        categories:  %%GROUPBY(regname)VAR(issuecount)STAT(SUM)OUTPUTTYPE(CATEGORY)%% ,
        title: null,
        reversed: true
    },

 

    colorAxis: {
        min: 0,
        minColor: '#FFFFFF',
        maxColor: Highcharts.getOptions().colors[0]
    },

    legend: {
        align: 'right',
        layout: 'vertical',
        margin: 0,
        verticalAlign: 'top',
        y: 25,
        symbolHeight: 280
    },

    tooltip: {
        formatter: function () {
            return '<b>' + getPointCategoryName(this.point, 'x') + '</b> sold <br><b>' +
                this.point.value + '</b> items on <br><b>' + getPointCategoryName(this.point, 'y') + '</b>';
        }
    },

    series: [{
        name: 'Sales per employee',
        borderWidth: 1,
        data: 
%%GROUPBY(section,regname)VAR(issuecount,recamount1)STAT(SUM)OUTPUTTYPE(HEATMAP)%%
,
        dataLabels: {
            enabled: true,
            color: '#000000'
        }
    }],

    responsive: {
        rules: [{
            condition: {
                maxWidth: 500
            },
            chartOptions: {
                yAxis: {
                    labels: {
                        formatter: function () {
                            return this.value.charAt(0);
                        }
                    }
                }
            }
        }]
    }

});
 Highcharts.chart('container2', {

    chart: {
        type: 'heatmap',
        marginTop: 40,
        marginBottom: 80,
        plotBorderWidth: 1
    },


    title: {
        text: '성과분석'
    },

    xAxis: {
        categories:  %%GROUPBY(section)VAR(issuecount)STAT(SUM)OUTPUTTYPE(CATEGORY)%% 
    },

    yAxis: {
        categories:  %%GROUPBY(CHANNEL_NAMNE)VAR(issuecount)STAT(SUM)OUTPUTTYPE(CATEGORY)%% ,
        title: null,
        reversed: true
    },

 

    colorAxis: {
        min: 0,
        minColor: '#FFFFFF',
        maxColor: Highcharts.getOptions().colors[0]
    },

    legend: {
        align: 'right',
        layout: 'vertical',
        margin: 0,
        verticalAlign: 'top',
        y: 25,
        symbolHeight: 280
    },

    tooltip: {
        formatter: function () {
            return '<b>' + getPointCategoryName(this.point, 'x') + '</b> sold <br><b>' +
                this.point.value + '</b> items on <br><b>' + getPointCategoryName(this.point, 'y') + '</b>';
        }
    },

    series: [{
        name: 'Sales per employee',
		dataSorting: {
        enabled: false 
    },
        borderWidth: 1,
        data: 
%%GROUPBY(section,CHANNEL_NAMNE)VAR(issuecount,recamount1)STAT(SUM)OUTPUTTYPE(HEATMAP)%%
,
        dataLabels: {
            enabled: true,
            color: '#000000'
        }
    }],

    responsive: {
        rules: [{
            condition: {
                maxWidth: 500
            },
            chartOptions: {
                yAxis: {
                    labels: {
                        formatter: function () {
                            return this.value.charAt(0);
                        }
                    }
                }
            }
        }]
    }

});
  </script>
  %%GROUPBY(section,CHANNEL_NAMNE)VAR(issuecount,recamount1)STAT(SUM)OUTPUTTYPE(HEATMAP)%%
``ENDHTML