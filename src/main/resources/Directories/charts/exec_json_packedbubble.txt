 ``SQL
select regname,channel , sum(issuecount) as issuecount , sum(recamount) as 'recamount',sum(recamount/issuecount) as AVGVALUE  
from mw_tran WHERE 1=1  and issuecount > 1   
group by regname,channel 
order by regname,channel 
``ENDSQL
``HTML
 
<div class="container-fluid ">
    <div class="row">

      <div class="col-lg-4 ">
        <div class="chart-area">
          <div class="title-area ">
            <div class="title">
              <h3>Top Campaigns</h3>
              <p>By Lead Generated</p>
            </div>
            <div class="set">
              <span class="dropdown-toggle" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false"><i class="fa fa-ellipsis-v"></i></span>
              <ul class="dropdown-menu right">
                <li><a href="#"><img src="assets/images/1.png" alt=""> Change Chart </a></li>
                <li><a href="#"><img src="assets/images/2.png" alt=""> Change Analysis Dimension</a></li>
                <li><a href="#"><img src="assets/images/3.png" alt=""> Change Analysis Measures</a></li>
              </ul>
            </div>
          </div>
        
        <div class="chart-field">
          <div class="chart" id="container1"></div>
          <div class="click-list">
            <h4>Which analysis view do you want?</h4>
            <ul>
              <li><a href="#">Campaign Details</a></li>
              <li><a href="#">Details By Channel</a></li>
              <li><a href="#">Details By Offer</a></li>
            </ul>
          </div>
        </div>
        </div>
      </div>


      <div class="col-lg-4 ">
        <div class="chart-area">
          <div class="title-area ">
            <div class="title">
              <h3>Expected Revenue by Marketing</h3>
              <p>By Lead Generated</p>
            </div>
            <div class="set">
              <span class="dropdown-toggle" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false"><i class="fa fa-ellipsis-v"></i></span>
              <ul class="dropdown-menu right">
                <li><a href="#"><img src="assets/images/1.png" alt=""> Change Chart </a></li>
                <li><a href="#"><img src="assets/images/2.png" alt=""> Change Analysis Dimension</a></li>
                <li><a href="#"><img src="assets/images/3.png" alt=""> Change Analysis Measures</a></li>
              </ul>
            </div>
          </div>
          <div class="chart-field">
            <div class="chart" id="container2" ></div>
            <div class="click-list">
              <h4>Which analysis view do you want?</h4>
              <ul>
                <li><a href="#">Campaign Details</a></li>
                <li><a href="#">Details By Channel</a></li>
                <li><a href="#">Details By Offer</a></li>
              </ul>
            </div>
          </div>
        </div>
      </div>
  
	 <div class="col-lg-4 ">
        <div class="chart-area">
          <div class="title-area ">
            <div class="title">
              <h3>Expected Revenue by Marketing</h3>
              <p>By Lead Generated</p>
            </div>
            <div class="set">
              <span class="dropdown-toggle" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false"><i class="fa fa-ellipsis-v"></i></span>
              <ul class="dropdown-menu right">
                <li><a href="#"><img src="assets/images/1.png" alt=""> Change Chart </a></li>
                <li><a href="#"><img src="assets/images/2.png" alt=""> Change Analysis Dimension</a></li>
                <li><a href="#"><img src="assets/images/3.png" alt=""> Change Analysis Measures</a></li>
              </ul>
            </div>
          </div>
          <div class="chart-field">
            <div class="chart" id="container3" ></div>
            <div class="click-list">
              <h4>Which analysis view do you want?</h4>
              <ul>
                <li><a href="#">Campaign Details</a></li>
                <li><a href="#">Details By Channel</a></li>
                <li><a href="#">Details By Offer</a></li>
              </ul>
            </div>
          </div>
        </div>
      </div>
    </div>
</div>
 
<script>
 Highcharts.chart('container1', {
    chart: {
        type: 'packedbubble',
        height: '300px'
    },
    title: {
        text: 'Carbon emissions around the world (2014)'
    },
    tooltip: {
        useHTML: true,
        pointFormat: '<b>{point.name}:</b> {point.value}m CO<sub>2</sub>'
    },
    plotOptions: {
        packedbubble: {
            minSize: '30%',
            maxSize: '120%',
            zMin: 0,
            zMax: 1000,
            layoutAlgorithm: {
                splitSeries: false,
                gravitationalConstant: 0.02
            },
            dataLabels: {
                enabled: true,
                format: '{point.name}',
                filter: {
                    property: 'y',
                    operator: '>',
                    value: 250
                },
                style: {
                    color: 'black',
                    textOutline: 'none',
                    fontWeight: 'normal'
                }
            }
        }
    },
    series:   %%GROUPBY(channel,regname)VAR(issuecount)STAT(SUM)OUTPUTTYPE(PACKEDBUBBLE)%%

});
 Highcharts.chart('container2', {
    chart: {
        type: 'packedbubble',
        height: '300px'
    },
    title: {
        text: 'Carbon emissions around the world (2014)'
    },
    tooltip: {
        useHTML: true,
        pointFormat: '<b>{point.name}:</b> {point.value}m CO<sub>2</sub>'
    },
   plotOptions: {
        packedbubble: {
            minSize: '20%',
            maxSize: '100%',
            zMin: 0,
            zMax: 1000,
            layoutAlgorithm: {
                gravitationalConstant: 0.05,
                splitSeries: true,
                seriesInteraction: false,
                dragBetweenSeries: true,
                parentNodeLimit: true
            },
            dataLabels: {
                enabled: true,
                format: '{point.name}',
                filter: {
                    property: 'y',
                    operator: '>',
                    value: 250
                },
                style: {
                    color: 'black',
                    textOutline: 'none',
                    fontWeight: 'normal'
                }
            }
        }
    },
    series:   %%GROUPBY(channel,regname)VAR(issuecount)STAT(SUM)OUTPUTTYPE(PACKEDBUBBLE)%%

});
Highcharts.chart('container3', {

    chart: {
        type: 'bubble',
		 height: '300px'
    },

    legend: {
        enabled: true,
        align: 'left',
        layout: 'vertical',
        verticalAlign: 'top',
        itemMarginTop: 10,
        bubbleLegend: {
            enabled: true,
            borderWidth: 1,
            connectorDistance: 40,
            maxSize: 70,
            ranges: [{}, {}, { color: '#e4d354' }]
        }
    },

    plotOptions: {
        series: {
            maxSize: 70,
			dataLabels: {
                enabled: true,
                format: '{point.name}'
            }
        }
    },

    series: %%GROUPBY(channel,regname)VAR(issuecount,AVGVALUE,recamount)STAT(SUM)OUTPUTTYPE(BUBBLELEGEND)%%
});
  </script>
  %%GROUPBY(channel,regname)VAR(issuecount,AVGVALUE,recamount)STAT(SUM)OUTPUTTYPE(BUBBLELEGEND)%%
``ENDHTML