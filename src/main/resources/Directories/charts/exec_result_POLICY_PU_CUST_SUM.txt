``SQL/DATASET0/QUADMAX/templateObject
      SELECT CAMP_EXEC_NO, CHNL_SEND_YN
        FROM QUADMAX.Z_CHNL_SEND_REQ
       WHERE CAMP_ID = '@@CAMP_ID@@'
         AND CELL_NODE_ID = '@@NODE_ID@@'
         AND CAMP_EXEC_NO = (
                             SELECT MAX(CAMP_EXEC_NO) AS CAMP_EXEC_NO
                               FROM QUADMAX.Z_CHNL_SEND_REQ
                              WHERE CAMP_ID = '@@CAMP_ID@@'
                                AND CELL_NODE_ID = '@@NODE_ID@@'
                              GROUP BY CAMP_ID, CELL_NODE_ID
                            )
``ENDSQL

``SQL/DATASET1/QUADMAX
  {% if DATASET0.Input != null %}
    {% for item in DATASET0.Input limit:1 %}
      {% if item.CHNL_SEND_YN == "Y" %}
      {% assign chnlSednYn = "Y" %}
      SELECT B.LOOKUP_NAME AS FILTER_NM
           , COUNT(1) AS FILTER_CNT
        FROM QUADMAX.Z_CAMP_EXCLD_CUST A 
  INNER JOIN QUADMAX.T_LOOKUP_VALUES B
          ON B.LOOKUP_TYPE = 'FILTERING_POLICY_CMS'
         AND A.POLICY_CD = B.LOOKUP_CODE
         AND B.LANG = 'ko_KR'
       WHERE 1 = 1
         AND A.CAMP_ID = '@@CAMP_ID@@'
         AND A.CELL_NODE_ID = '@@NODE_ID@@'
         AND A.CAMP_EXEC_NO = '{{ item.CAMP_EXEC_NO }}'
    GROUP BY B.LOOKUP_NAME
      {% else %}
        {% assign chnlSednYn = "N" %}
      {% endif %}
    {% endfor %}
  {% else %}
    {% assign chnlSednYn = "N" %}
  {% endif %}

  {% if chnlSednYn != "Y" %}
      SELECT IFNULL(B.LOOKUP_NAME,'-') AS FILTER_NM
           , COUNT(1) AS FILTER_CNT
        FROM (
              SELECT POLICY_CD
                FROM QUADMAX_WORK.@@NODE_TABLE@@_E
             ) A
  INNER JOIN QUADMAX.T_LOOKUP_VALUES B
          ON B.LOOKUP_TYPE = 'FILTERING_POLICY_CMS'
         AND A.POLICY_CD = B.LOOKUP_CODE
         AND B.LANG = 'ko_KR'
    GROUP BY B.LOOKUP_NAME
  {% endif %}
``ENDSQL

``SQL/DATASET2/QUADMAX
      SELECT COUNT(1) AS CUST_SUM
  {% if DATASET0.Input != null %}
    {% for item in DATASET0.Input limit:1 %}
      {% if item.CHNL_SEND_YN == "Y" %}
      {% assign chnlSednYn = "Y" %}
        FROM QUADMAX.Z_CAMP_CELL_CUST A 
       WHERE 1 = 1
         AND A.CAMP_ID = '@@CAMP_ID@@'
         AND A.CELL_NODE_ID = '@@NODE_ID@@'
         AND A.CAMP_EXEC_NO = '{{ item.CAMP_EXEC_NO }}'
         AND A.CUST_TYPE_CD = 'T'
      {% else %}
        {% assign chnlSednYn = "N" %}
      {% endif %}
    {% endfor %}
  {% else %}
    {% assign chnlSednYn = "N" %}
  {% endif %}

  {% if chnlSednYn != "Y" %}
        FROM (
              SELECT CUST_ID
                FROM QUADMAX_WORK.@@NODE_TABLE@@
               WHERE CUST_TYPE_CD = 'T'
             ) A
  {% endif %}
``ENDSQL

``HTML
<div id="result-policy-chart"></div> 

<script>
  var datas = [ %%DATASET1%%.%%GROUPBY(FILTER_NM)VAR(FILTER_CNT)STAT(SUM)OUTPUTTYPE(PIE)%% ];
  if (datas.length != 0) {
  datas = datas[0];
  } else {
  datas = [{name:'제외고객 없음',y:0}]
  }
  
  var data = %%DATASET2%%.%%GROUPBY()VAR(CUST_SUM)STAT(SUM)OUTPUTTYPE(BASE_RESULTSET)%% ;
  $("#total_customers").text(comma(data.Input[0].CUST_SUM));
  
  Highcharts.chart('result-policy-chart', {
    chart: {
      plotBackgroundColor: null,
      plotBorderWidth: null,
      plotShadow: false,
      type: 'pie',
      {{ size }}
    },
    colors: [
    '#49919d', '#549f61', '#9dbc58', '#ced784',
    '#84c7bb', '#84a4c7', '#a1aad4', '#c4c8cf', '#6AF9C4'
  ],
  title: {
      text: ''
  },
  tooltip: {
    enabled: false
    /* 은호파트너 요청으로 tooltip 삭제 */
      // pointFormat: '<b>{point.percentage:.1f}%</b>'
  },
    accessibility: {
        point: {
            valueSuffix: '%'
        }
    },
    plotOptions: {
        pie: {
            allowPointSelect: true,
            cursor: 'pointer',
            dataLabels: {
                enabled: true,
                format: '<b>{point.name}</b>: {point.y} 건'
            },
      distance: 20,
      size: '50%'
        }
    },
    series: [{
        name: 'POLICY',
        colorByPoint: true,
        data: datas
    }]
});
</script> 
``ENDHTML