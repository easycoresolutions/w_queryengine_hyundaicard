
 ``SQL
 
select top 1 section,channel,sum(issuecount) as issuecount , sum(recamount) as 'recamount' from mw_tran WHERE 1=1 [[AND issuecount > ##recamt##]] [ and Trans_year = @@year@@]   group by section ,channel  order by channel, section  

``ENDSQL

``HTML
 
var pivotdata = @@JSONDATA@@
var Title = @@TITLE@@ ;
var Subtitme = @@SUBTITLE@@
var analysis = @@ANALYSIS@@  

	var pivot_parameter = 
	{
  "Layout": {
	"excelfile": "my_excel_filename",
    "rows": [ "AgeGroup", "CustType" ],
    "columns": [ "SalesYYYY" ],
    "fixFields":   @@ROWS@@  ,
    "thousand_delimeter": ",",
    "blank_text": "",
    "selectcolor": "#999999",
    "columntotal": {
      "show": false,
      "label": "Total"
    },
    "subtotal": {
      "show": true,
      "label": "Sub-Total"
    },
    "groundtotal": {
      "show": true,
      "label": "Ground Total"
    },
    "TableLayoutDisplay": true,
    "formatting": [
      {
        "field": "ContactCount",
        "type": "bar",
        "option": "#ff555a"
      },
      {
        "field": "ResponseCount",
        "type": "map",
        "option": "#ffff00"
      },
      {
        "field": "SalesAmtAvg",
        "type": "icons",
        "option": "5-arrows"
      },
      {
        "field": "Count",
        "type": "icons",
        "option": "3-stars"
      },
      {
        "field": "avg2",
        "type": "bar",
        "option": "#0099FF"
      }
    ],
    "themes": [
      {
        "name": "azure",
        "class": "azure-theme"
      },
      {
        "name": "brown",
        "class": "brown-theme"
      },
      {
        "name": "cosmo",
        "class": "cosmo-theme"
      },
      {
        "name": "eggplant",
        "class": "eggplant-theme"
      },
      {
        "name": "flat",
        "class": "flat-theme"
      },
      {
        "name": "granite",
        "class": "granite-theme"
      },
      {
        "name": "lime",
        "class": "lime-theme"
      },
      {
        "name": "navy",
        "class": "navy-theme"
      },
      {
        "name": "silver",
        "class": "silver-theme"
      },
      {
        "name": "yeti",
        "class": "yeti-theme"
      },
      {
        "name": "almond",
        "class": "almond-theme"
      }


    ],
    "set_theme": "azure",
    "slicer": { },
    "column_sorting": [
      {
        "fieldname": "CustType",
        "sorttype": "DES",
        "values": {
          "VVIP": 4,
          "VIP": 3,
          "General": 1,
          "Possible": 2
        }
      },
      {
        "fieldname": "AgeGroup",
        "sorttype": "ASC",
        "values": {
          "30-40": 3,
          "20-30": 2,
          "~20": 1,
          "40-50": 4,
          "50~": 5
        }
      },
      {
        "fieldname": "Gender",
        "sorttype": "ASC",
        "values": {
          "Male": 1,
          "Female": 2
        }
      }
    ],
    "values": [
      {
        "field": "SalesCount",
        "op": "sum",
        "precision": ""
      },
      {
        "field": "SalesAmt",
        "op": "sum",
        "precision": ""
      },

      {
        "field": "UnitPrice",
        "op": "",
        "precision": ""
      }

    ],
    "calculated": [
      {
        "fieldheader": "AvgSales/Item)",
        "calculatedname": "UnitPrice",
        "formula": "SalesAmt / SalesCount ",
        "precision": ".00",
        "surfix": "W"
      }
    ],
    "backGroundhighlight": [

    ],
    "foreGroundhighlight": [

    ],
    "percentage": [ ]
  }
} ; 

``ENDHTML
 