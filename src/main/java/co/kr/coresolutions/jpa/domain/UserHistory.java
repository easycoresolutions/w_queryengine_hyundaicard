//package co.kr.coresolutions.jpa.domain;
//
//
//import com.fasterxml.jackson.annotation.JsonInclude;
//import com.fasterxml.jackson.annotation.JsonProperty;
//import lombok.AllArgsConstructor;
//import lombok.Builder;
//import lombok.Data;
//import lombok.NoArgsConstructor;
//
//@Data
//@Builder
//@NoArgsConstructor
//@AllArgsConstructor
//@JsonInclude(JsonInclude.Include.NON_EMPTY)
//public class UserHistory {
//    @JsonProperty(value = "USERID")
//    private String userid;
//    @JsonProperty(value = "LOG_TYPE")
//    private String logType;
//    @JsonProperty(value = "LOG_DATE")
//    private String logDate;
//    @JsonProperty(value = "LOG_TIME")
//    private String logTime;
//    @JsonProperty(value = "PW_A")
//    private String pwa;
//    @JsonProperty(value = "PW_B")
//    private String pwb;
//}
