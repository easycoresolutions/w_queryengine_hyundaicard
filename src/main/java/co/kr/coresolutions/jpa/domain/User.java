package co.kr.coresolutions.jpa.domain;


import com.fasterxml.jackson.annotation.JsonInclude;
import com.fasterxml.jackson.annotation.JsonProperty;
import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.Data;
import lombok.NoArgsConstructor;

@Data
@Builder
@NoArgsConstructor
@AllArgsConstructor
@JsonInclude(JsonInclude.Include.NON_EMPTY)
public class User {
    @JsonProperty(value = "EMP_ID")
    private String userid;
    @JsonProperty(value = "EMP_NAME")
    private String username;
    @JsonProperty(value = "EMP_PW")
    private String userpw;
    //    @JsonProperty(value = "ACCESS_LEVEL")
//    private String accessLevel;
    @JsonProperty(value = "DEPT_ID")
    private String deptId;
    @JsonProperty(value = "EMP__TEL")
    private String userTel;
    @JsonProperty(value = "EMP__IP")
    private String userIp;
    @JsonProperty(value = "EMP__MOBILE")
    private String userMobile;
    @JsonProperty(value = "EMP__EMAIL")
    private String userEmail;
    @JsonProperty(value = "EMP__LANG")
    private String userLang;
    @JsonProperty(value = "REG_DATE")
    private String regDate;
    @JsonProperty(value = "REG_TIME")
    private String regTime;
    @JsonProperty(value = "EMP_KEY")
    private String userkey;
}
