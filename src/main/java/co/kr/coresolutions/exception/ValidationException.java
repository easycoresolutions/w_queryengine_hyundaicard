package co.kr.coresolutions.exception;

import lombok.Data;
import lombok.NoArgsConstructor;
import org.springframework.validation.Errors;


@Data
@NoArgsConstructor
public class ValidationException extends RuntimeException {
    private int code;
    private Errors errors;

    public ValidationException(int code, String message) {
        this(code, message, null);
    }

    public ValidationException(int code, String message, Errors errors) {
        super(message);
        this.code = code;
        this.errors = errors;
    }

}
