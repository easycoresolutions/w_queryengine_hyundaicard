package co.kr.coresolutions.enums;

public enum Extension {
    SCR(".scr"),
    LOG(".log");

    String type;

    Extension(String type) {
        this.type = type;
    }

    public String getName() {
        return type;
    }

}
