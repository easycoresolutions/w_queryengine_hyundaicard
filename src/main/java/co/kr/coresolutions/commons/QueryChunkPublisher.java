package co.kr.coresolutions.commons;

import com.google.common.collect.Lists;
import org.springframework.stereotype.Component;

import java.util.List;
import java.util.Map;
import java.util.Observable;
import java.util.Observer;
import java.util.concurrent.ConcurrentHashMap;

@Component
public class QueryChunkPublisher implements QueryObservable {
    private Map<Observer, List<Object>> observers = new ConcurrentHashMap<>();

    @Override
    public void attach(Observer observer, Object... args) {
        observers.put(observer, Lists.newArrayList(args));
    }

    @Override
    public void detach(Observer observer) {
        observers.remove(observer);
    }

    @Override
    public void update(Observable observable, Object arg) {
        observers.forEach((observer, objects) -> observer.update(observable, objects));
    }
}