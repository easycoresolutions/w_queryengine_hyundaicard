package co.kr.coresolutions.commons;

public interface ResponseCodes {
    int OK_RESPONSE = 200;

    int GENERAL_ERROR = 500;
    int NOT_FOUND_ERROR = 404;
    int DUPLICATED_ERROR = 302;
    int VALIDATION_ERROR = 13;
    int NOT_IMPLEMENTED = 18;
    int NOT_SUPPORTED = 19;

    int EXTERNAL_AUTH_NEEDED = 20;
    int REVOKING_ROLE_ERROR = 21;
    int ACCOUNT_LOCKED = 22;
    int ACCESS_DENIED = 23;

    int NULL_RESPONSE_ERROR = 300;
    int NOT_FOUND_RESPONSE_ERROR = 400;


}
