package co.kr.coresolutions.commons;


import javax.validation.Constraint;
import javax.validation.Payload;
import java.lang.annotation.ElementType;
import java.lang.annotation.Retention;
import java.lang.annotation.RetentionPolicy;
import java.lang.annotation.Target;

@Target({ElementType.TYPE})
@Retention(RetentionPolicy.RUNTIME)
@Constraint(validatedBy = {ConditionalOneExistValidator.class})
public @interface ConditionalOneExist {

    String message() default "should only one property exist";

    Class<?>[] groups() default {};

    Class<? extends Payload>[] payload() default {};

    String firstSelected();

    String secondSelected();

}