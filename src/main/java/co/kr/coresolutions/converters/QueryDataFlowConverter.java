package co.kr.coresolutions.converters;

import co.kr.coresolutions.model.dtos.HttpDto;
import org.mapstruct.Mapper;

@Mapper(componentModel = "spring")
public interface QueryDataFlowConverter {

    HttpDto convertToDto(String api, String method, String body);
}
