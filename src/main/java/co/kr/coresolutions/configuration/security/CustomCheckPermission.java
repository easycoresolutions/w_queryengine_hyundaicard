//package co.kr.coresolutions.configuration.security;
//
//import co.kr.coresolutions.service.QueryService;
//import co.kr.coresolutions.service.QueryServiceSession;
//import com.google.gson.JsonArray;
//import com.google.gson.JsonElement;
//import com.google.gson.JsonObject;
//import com.google.gson.JsonParser;
//import lombok.SneakyThrows;
//import org.springframework.beans.factory.annotation.Autowired;
//import org.springframework.security.core.context.SecurityContextHolder;
//import org.springframework.stereotype.Component;
//
//import java.util.Optional;
//import java.util.stream.StreamSupport;
//
//@Component
//public class CustomCheckPermission {
//    @Autowired
//    private QueryService queryService;
//    @Autowired
//    private QueryServiceSession queryServiceSession;
//
//    @SneakyThrows
//    public boolean hasAccess(String endpoint) {
//        final boolean[] isAdminRequested = {false};
//        SecurityContextHolder.getContext().getAuthentication().getAuthorities().stream().findFirst().ifPresent(o -> {
//            if (o.getAuthority().equalsIgnoreCase("ROLE_ADMIN")) {
//                isAdminRequested[0] = true;
//            }
//        });
//        final boolean[] returnPermission = {false};
//        final String[] resultSelect = {""};
//        final String[] connectionID = {""};
//        Optional.ofNullable(queryServiceSession.readFile())
//                .ifPresent(jsonNode -> {
//                    if (jsonNode.hasNonNull("LogonConnectionId") && jsonNode.get("LogonConnectionId").isTextual()) {
//                        connectionID[0] = jsonNode.get("LogonConnectionId").asText();
//                        resultSelect[0] = queryService
//                                .checkValidity("select * from T_API_CONFIG where ENDPOINT_NAME = '" + endpoint + "'",
//                                        connectionID[0], "", "", false);
//
//                        //in case endpoint doesn't found
//                        if (resultSelect[0].equals("[]")) {
//                            returnPermission[0] = true;
//                        }
//
//                        if (!(resultSelect[0].equalsIgnoreCase(connectionID[0]) || resultSelect[0].startsWith("Error") || resultSelect[0].startsWith("error") ||
//                                resultSelect[0].startsWith("maxRunningTimeOut") || resultSelect[0].startsWith("MaxRows"))) {
//                            try {
//                                JsonParser parser = new JsonParser();
//                                JsonElement jsonElement = parser.parse(resultSelect[0]);
//                                JsonArray jsonArray = jsonElement.getAsJsonArray();
//                                StreamSupport.stream(jsonArray.spliterator(), true).forEach(jsonElement1 -> {
//                                    JsonObject jsonObject = jsonElement1.getAsJsonObject();
//                                    if (jsonObject.has("ADMIN_ACCESS_LEVEL") && jsonObject.has("USER_ACCESS_LEVEL")) {
//                                        if (isAdminRequested[0]) {
//                                            String adminAccessLevel = jsonObject.get("ADMIN_ACCESS_LEVEL").getAsString();
//                                            if (adminAccessLevel.equalsIgnoreCase("1")) {
//                                                returnPermission[0] = true;
//                                            }
//                                        } else {
//                                            String userAccessLevel = jsonObject.get("USER_ACCESS_LEVEL").getAsString();
//                                            if (userAccessLevel.equalsIgnoreCase("0")) {
//                                                returnPermission[0] = true;
//                                            }
//                                        }
//                                    }
//                                });
//                            } catch (Exception e) {
//                                e.printStackTrace();
//                            }
//                        }
//
//                    }
//                });
//        return returnPermission[0];
//    }
//}
