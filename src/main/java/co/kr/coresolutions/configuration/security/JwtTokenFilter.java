package co.kr.coresolutions.configuration.security;

import co.kr.coresolutions.service.Constants;
import co.kr.coresolutions.util.ResettableStreamHttpServletRequest;
import com.fasterxml.jackson.databind.JsonNode;
import com.fasterxml.jackson.databind.ObjectMapper;
import lombok.RequiredArgsConstructor;
import org.apache.commons.io.IOUtils;
import org.springframework.jdbc.core.JdbcTemplate;
import org.springframework.jdbc.support.rowset.SqlRowSet;
import org.springframework.security.authentication.UsernamePasswordAuthenticationToken;
import org.springframework.security.core.context.SecurityContextHolder;
import org.springframework.security.core.userdetails.UserDetails;
import org.springframework.security.core.userdetails.UserDetailsService;
import org.springframework.security.core.userdetails.UsernameNotFoundException;
import org.springframework.security.web.authentication.WebAuthenticationDetailsSource;
import org.springframework.stereotype.Component;
import org.springframework.web.filter.OncePerRequestFilter;

import javax.servlet.FilterChain;
import javax.servlet.ServletException;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import java.io.IOException;
import java.util.List;
import java.util.Optional;
import java.util.Spliterators;
import java.util.UUID;
import java.util.stream.Collectors;
import java.util.stream.StreamSupport;

@Component
@RequiredArgsConstructor
public class JwtTokenFilter extends OncePerRequestFilter {
    private final UserDetailsService userDetailsService;
    private final JwtTokenProvider jwtTokenProvider;
    private final JdbcTemplate jdbcTemplate;
    private final ObjectMapper objectMapper;
    public static String apiName = "";
    private final Constants constants;

    @Override
    protected void doFilterInternal(HttpServletRequest request, HttpServletResponse response, FilterChain chain) throws ServletException, IOException {
        String path = request.getServletPath();
        JwtTokenFilter.apiName = path;
        if (path.startsWith("/command") || path.startsWith("/command2nd") || path.startsWith("/command3rd")) {
            String dirName = path.substring(1);
            Constants.commandDirName = dirName.contains("/") ? dirName.substring(0, path.indexOf("/")) : dirName;
        }

        final Optional[] optional = {Optional.empty()};
        ResettableStreamHttpServletRequest wrappedRequest = new ResettableStreamHttpServletRequest(request);
        String body = IOUtils.toString(wrappedRequest.getReader());
        wrappedRequest.resetInputStream();
        JsonNode jsonNodes = constants.getConfigFileAsJson();
        final boolean[] authorizationOption = {true};
        try {
            Optional.ofNullable(jsonNodes)
                        .ifPresent(jsonNode -> {
                            if (jsonNode.hasNonNull("authorizationOption") && jsonNode.get("authorizationOption").isBoolean()) {
                                authorizationOption[0] = jsonNode.get("authorizationOption").asBoolean();
                                if (!authorizationOption[0]) {
                                    optional[0] = Optional.of(UUID.randomUUID().toString());
                                }
                            }
                            if (!path.startsWith("/auth/") && authorizationOption[0]) {
                                if (jsonNode.hasNonNull("auth_ignored") && jsonNode.get("auth_ignored").isArray()) {
                                    List<String> list = StreamSupport
                                            .stream(jsonNode.get("auth_ignored").spliterator(), false).map(JsonNode::asText).collect(Collectors.toList());
                                    if ("POST".equalsIgnoreCase(request.getMethod()) || "PUT".equalsIgnoreCase(request.getMethod()) || "DELETE".equalsIgnoreCase(request.getMethod())) {
                                        try {
                                            JsonNode nodeBody = objectMapper.readValue(body, JsonNode.class);
                                            optional[0] = StreamSupport
                                                    .stream(Spliterators.spliteratorUnknownSize(nodeBody.fieldNames(), 0), false)
                                                    .filter(s -> s.equalsIgnoreCase("userid"))
                                                    .filter(s -> list.contains(nodeBody.get(s).asText())).map(s -> nodeBody.get(s).asText()).findFirst();
                                    } catch (IOException e) {
                                        e.printStackTrace();
                                    }
                                }
                                }
                            }
                        });
            } catch (Exception e) {
                e.printStackTrace();
            }

        String requestHeader = request.getHeader("Authorization");
        if (!authorizationOption[0]) {
            requestHeader = null;
        }

        if (optional[0].isPresent() && (requestHeader == null || requestHeader.isEmpty())) {
            requestHeader = "Bearer " + jwtTokenProvider.generateInternalToken(optional[0].get().toString());
        }

        String username = null;
        String authToken = null;
        if (requestHeader != null && requestHeader.startsWith("Bearer ")) {
            authToken = requestHeader.substring(7);
            try {
                username = jwtTokenProvider.getUsername(authToken);
            } catch (RuntimeException e) {
                SecurityContextHolder.clearContext();
                response.sendError(HttpServletResponse.SC_UNAUTHORIZED, e.getMessage());
                return;
            }
        }

        if (authToken != null) {
            SqlRowSet sqlRowSet = jdbcTemplate.queryForRowSet("select tbl.userId from Tokens_BackListing tbl where tbl.token = '" + authToken + "'");
            if (sqlRowSet.next()) {
                SecurityContextHolder.clearContext();
                response.sendError(HttpServletResponse.SC_UNAUTHORIZED, "token is revoked, please sign in");
            }
        }

        if (username != null && SecurityContextHolder.getContext().getAuthentication() == null) {
            UserDetails userDetails;
            try {
                userDetails = userDetailsService.loadUserByUsername(username);
            } catch (UsernameNotFoundException e) {
                SecurityContextHolder.clearContext();
                response.sendError(HttpServletResponse.SC_UNAUTHORIZED, e.getMessage());
                return;
            }
            if (jwtTokenProvider.validateToken(authToken)) {
                UsernamePasswordAuthenticationToken authentication = new UsernamePasswordAuthenticationToken(userDetails, null, userDetails.getAuthorities());
                authentication.setDetails(new WebAuthenticationDetailsSource().buildDetails(wrappedRequest));
                SecurityContextHolder.getContext().setAuthentication(authentication);
/*
                if (!path.equalsIgnoreCase("/auth/login")) {
                    boolean hasAccess = customCheckPermission.hasAccess(path.substring(1));
                    if (!hasAccess) {
                        SecurityContextHolder.clearContext();
                        response.sendError(HttpServletResponse.SC_UNAUTHORIZED, "you are not authorized to make this request");
                        return;
                    }
                }
*/
            }
        }


        chain.doFilter(wrappedRequest, response);
    }
}


