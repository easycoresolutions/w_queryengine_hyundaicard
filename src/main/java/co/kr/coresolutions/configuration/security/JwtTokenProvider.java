package co.kr.coresolutions.configuration.security;

import co.kr.coresolutions.jpa.domain.Role;
import co.kr.coresolutions.jpa.domain.User;
import co.kr.coresolutions.service.QueryService;
import co.kr.coresolutions.service.QueryServiceSession;
import com.fasterxml.jackson.databind.JsonNode;
import com.fasterxml.jackson.databind.ObjectMapper;
import io.jsonwebtoken.Claims;
import io.jsonwebtoken.Header;
import io.jsonwebtoken.JwsHeader;
import io.jsonwebtoken.JwtException;
import io.jsonwebtoken.Jwts;
import io.jsonwebtoken.SignatureAlgorithm;
import lombok.RequiredArgsConstructor;
import org.springframework.stereotype.Component;
import org.springframework.web.context.annotation.RequestScope;

import javax.annotation.PostConstruct;
import java.io.IOException;
import java.util.Arrays;
import java.util.Base64;
import java.util.Date;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.Optional;

@Component
@RequiredArgsConstructor
@RequestScope
public class JwtTokenProvider {
    private final QueryServiceSession queryServiceSession;
    private final ObjectMapper objectMapper;
    private final QueryService queryService;
    private String secretKey;
    private Long expireDate;
    private JsonNode jsonNodeConfig;

    @PostConstruct
    protected void init() {
        jsonNodeConfig = queryServiceSession.readFile();
        Optional.ofNullable(jsonNodeConfig)
                .ifPresent(jsonNode -> {
                    if (jsonNode.hasNonNull("CompanyKey") && jsonNode.get("CompanyKey").isTextual()
                            && jsonNode.hasNonNull("Expired") && jsonNode.get("Expired").isInt()) {
                        secretKey = Base64.getEncoder().encodeToString(jsonNode.get("CompanyKey").toString().getBytes());
                        expireDate = jsonNode.get("Expired").asLong() * 60 * 1000;
                    }
                });
    }

    public String generateToken(String username) throws IOException {

        Date now = new Date();
        Date validDate = new Date(now.getTime() + expireDate);

        Map<String, Object> header = new HashMap<>();
        header.put(Header.TYPE, Header.JWT_TYPE);
        header.put(JwsHeader.ALGORITHM, SignatureAlgorithm.HS256);

        final String[] resultSelect = {""};
        final String[] connectionID = {""};
        Optional.ofNullable(jsonNodeConfig)
                .ifPresent(jsonNode -> {
                    if (jsonNode.hasNonNull("LogonConnectionId") && jsonNode.get("LogonConnectionId").isTextual()) {
                        connectionID[0] = jsonNode.get("LogonConnectionId").asText();

                        resultSelect[0] = queryService
                                .checkValidity("SELECT * FROM " + queryService.getInfoConnection(connectionID[0]).getSCHEME() + "T_EMP WHERE EMP_ID = '" + username + "'",
                                        connectionID[0], "", "", false);
                    }
                });

        Claims claims = Jwts.claims().setSubject(username);
        if (!(resultSelect[0].equalsIgnoreCase(connectionID[0]) || resultSelect[0].startsWith("Error") || resultSelect[0].startsWith("error"))) {
            List<User> users = Arrays.asList(objectMapper.readValue(resultSelect[0], User[].class));
            users.stream().findFirst().ifPresent(user -> {
                claims.put("InternalAccessLevel", Role.ROLE_ADMIN.getName());
                claims.put("userid", user.getUserid());
                claims.put("username", username);
            });
        }
        return Jwts.builder()
                .setClaims(claims)
                .setIssuedAt(now)
                .setExpiration(validDate)
                .setHeader(header)
                .signWith(SignatureAlgorithm.HS256, secretKey)
                .compact();
    }

    public String generateInternalToken(String username) {

        Date now = new Date();
        Date validDate = new Date(now.getTime() + expireDate);

        Map<String, Object> header = new HashMap<>();
        header.put(Header.TYPE, Header.JWT_TYPE);
        header.put(JwsHeader.ALGORITHM, SignatureAlgorithm.HS256);

        Claims claims = Jwts.claims().setSubject(username);

        return Jwts.builder()
                .setClaims(claims)
                .setIssuedAt(now)
                .setExpiration(validDate)
                .setHeader(header)
                .signWith(SignatureAlgorithm.HS256, secretKey)
                .compact();
    }

    public String getUsername(String token) {
        return Jwts.parser().setSigningKey(secretKey).parseClaimsJws(token).getBody().getSubject();
    }

    public boolean validateToken(String token) {
        try {
            Jwts.parser().setSigningKey(secretKey).parseClaimsJws(token);
            return true;
        } catch (JwtException | IllegalArgumentException e) {
            return false;
        }
    }

}
