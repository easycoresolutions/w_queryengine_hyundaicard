package co.kr.coresolutions.configuration;

import javax.servlet.ServletContextEvent;
import javax.servlet.ServletContextListener;
import javax.servlet.annotation.WebListener;
import java.io.IOException;
import java.lang.management.ManagementFactory;
import java.util.Set;

@WebListener
public class CustomServletContextListener implements ServletContextListener {
    private static String OS = System.getProperty("os.name").toLowerCase();

    private static boolean isUnix() {
        return (OS.contains("nix") || OS.contains("nux") || OS.indexOf("aix") > 0);
    }

    @Override
    public void contextInitialized(ServletContextEvent sce) {
        //not needed
    }

    @Override
    public void contextDestroyed(ServletContextEvent context) {
        runKillCommand(Long.valueOf(ManagementFactory.getRuntimeMXBean().getName().split("@")[0]));
        System.exit(0);
    }

    public void runKillCommand(Long pid) {
//        Set<Thread> threadSet = Thread.getAllStackTraces().keySet();
//        threadSet.stream().filter(Thread::isAlive).forEach(Thread::stop);
        try {
            Process p;
            if (isUnix()) {
                p = Runtime.getRuntime().exec(new String[]{"bash", "-c", "kill -9 " + pid});
            } else {
                p = Runtime.getRuntime().exec(new String[]{"cmd.exe", "-c", "taskkill /pid " + pid + "/f"});
            }
            if (p.isAlive()) {
                p.destroy();
            }
        } catch (IOException e) {
            e.printStackTrace();
        }
    }
}