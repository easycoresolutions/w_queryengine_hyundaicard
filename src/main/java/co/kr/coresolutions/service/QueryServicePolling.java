package co.kr.coresolutions.service;

import co.kr.coresolutions.commons.Chunk;
import co.kr.coresolutions.dao.NativeQuery;
import co.kr.coresolutions.facades.IQuerySocketFacade;
import co.kr.coresolutions.facades.Impl.QueryPollingFacade;
import co.kr.coresolutions.facades.Impl.QueryTargetFacade;
import co.kr.coresolutions.model.Activemq;
import co.kr.coresolutions.model.Connection;
import co.kr.coresolutions.model.Rabbitmq;
import co.kr.coresolutions.model.Tibco;
import co.kr.coresolutions.model.dtos.SocketBodyDto;
import co.kr.coresolutions.model.dtos.TargetDto;
import co.kr.coresolutions.util.QueryPollingHelper;

import com.fasterxml.jackson.databind.JsonNode;
import com.fasterxml.jackson.databind.ObjectMapper;
import com.fasterxml.jackson.databind.node.ArrayNode;
import com.google.common.base.Joiner;
import com.rabbitmq.client.AMQP;
import com.rabbitmq.client.Address;
import com.rabbitmq.client.Channel;
import com.rabbitmq.client.ConnectionFactory;
import com.tibco.tibjms.TibjmsConnectionFactory;
import com.tibco.tibjms.TibjmsQueueConnectionFactory;
import com.tibco.tibjms.TibjmsTopicConnectionFactory;
import com.tibco.tibjms.admin.TibjmsAdmin;
import com.tibco.tibjms.admin.TibjmsAdminException;
import lombok.RequiredArgsConstructor;

import org.apache.activemq.ActiveMQConnectionFactory;
import org.apache.commons.lang3.StringUtils;
import org.apache.kafka.clients.admin.AdminClient;
import org.apache.kafka.clients.admin.KafkaAdminClient;
import org.apache.kafka.clients.producer.KafkaProducer;
import org.apache.kafka.clients.producer.Producer;
import org.apache.kafka.clients.producer.ProducerRecord;
import org.json.JSONArray;
import org.json.JSONObject;
import org.springframework.scheduling.TaskScheduler;
import org.springframework.scheduling.support.CronTrigger;
import org.springframework.stereotype.Service;

import javax.jms.JMSException;
import javax.jms.QueueConnection;
import javax.jms.QueueSender;
import javax.jms.QueueSession;
import javax.jms.TopicConnection;
import javax.jms.TopicPublisher;
import javax.jms.TopicSession;
import java.io.IOException;
import java.math.BigInteger;
import java.nio.file.Files;
import java.nio.file.Paths;
import java.nio.file.StandardOpenOption;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Comparator;
import java.util.Date;
import java.util.HashMap;
import java.util.HashSet;
import java.util.Iterator;
import java.util.List;
import java.util.Map;
import java.util.Observable;
import java.util.Observer;
import java.util.Optional;
import java.util.Properties;
import java.util.Set;
import java.util.concurrent.ExecutionException;
import java.util.concurrent.ScheduledFuture;
import java.util.stream.Collectors;
import java.util.stream.StreamSupport;

@Service
@RequiredArgsConstructor
public class QueryServicePolling implements Observer {
    private final NativeQuery nativeQuery;
    private final QueryService queryService;
    private final QueryPollingHelper pollingHelper;
    private static final Map<String, String> latestOutputFileNameMap = new HashMap<>();
    private final ObjectMapper objectMapper;
    private static final Map<String, String> latestdpIDTimeValueNMap = new HashMap<>();
    private static final Map<String, String> latestdpIDNameValueNMap = new HashMap<>();
    private static final Map<String, Object> latestdpIDScheduleObjectMap = new HashMap<>();
    private final IQuerySocketFacade querySocketFacade;
    private final TaskScheduler taskScheduler;
    private final Constants constants;

    public void push(Object jsonOrList, String outConnId, String format) throws IOException {
        Properties properties = pollingHelper.getProperties(queryService.getConnectionFile(outConnId));
        Producer producer = new KafkaProducer<>(properties);
        pollingHelper
                .getOutData(jsonOrList, format)
                .forEach(oneLine -> producer.send(new ProducerRecord<String, String>(properties.getProperty("topic"), oneLine)));
        QueryPollingFacade.success[0] = true;
        producer.close();
    }

    public void pushWithTibco(Object jsonOrList, String outConnId, String format) throws IOException, JMSException, TibjmsAdminException {
            Tibco tibco = objectMapper.readValue(queryService.getConnectionFile(outConnId), Tibco.class);
            String destination = tibco.getDestination();
            if (destination.equalsIgnoreCase("queue")) {
                TibjmsAdmin tibjmsAdmin = new TibjmsAdmin(tibco.getIp() + ":" + tibco.getPort(), tibco.getId(), tibco.getPw());
                tibjmsAdmin.setCommandTimeout(5000);
                System.out.println("tibcoAdmin ConnectionId\t" + tibjmsAdmin.getConnectionId());
                TibjmsConnectionFactory factory = new TibjmsQueueConnectionFactory(tibco.getIp() + ":" + tibco.getPort());
                QueueConnection connection = ((TibjmsQueueConnectionFactory) factory).createQueueConnection(tibco.getId(), tibco.getPw());
                QueueSession session = connection.createQueueSession(false, javax.jms.Session.AUTO_ACKNOWLEDGE);
                javax.jms.Queue queue = session.createQueue(tibco.getQueue());
                QueueSender sender = session.createSender(queue);
                javax.jms.TextMessage message = session.createTextMessage();
                pollingHelper.getOutData(jsonOrList, format).forEach(oneLine -> {
                    try {
                        message.clearBody();
                        message.setText(oneLine);
                        sender.send(message);
                        QueryPollingFacade.success[0] = true;
                    } catch (JMSException e) {
                        QueryPollingFacade.message[0] = e.getMessage();
                        QueryPollingFacade.success[0] = false;
                    }
                });
                tibjmsAdmin.close();
                connection.close();
            } else if (destination.equalsIgnoreCase("topic")) {
                TibjmsConnectionFactory factory = new TibjmsTopicConnectionFactory(tibco.getIp() + ":" + tibco.getPort());
                TibjmsAdmin tibjmsAdmin = new TibjmsAdmin(tibco.getIp() + ":" + tibco.getPort(), tibco.getId(), tibco.getPw());
                tibjmsAdmin.setCommandTimeout(5000);
                System.out.println("tibcoAdmin ConnectionId\t" + tibjmsAdmin.getConnectionId());
                TopicConnection connection = ((TibjmsTopicConnectionFactory) factory).createTopicConnection(tibco.getId(), tibco.getPw());
                TopicSession session = connection.createTopicSession(false, javax.jms.Session.AUTO_ACKNOWLEDGE);
                javax.jms.Topic topic = session.createTopic(tibco.getTopic());
                TopicPublisher publisher = session.createPublisher(topic);
                javax.jms.TextMessage message = session.createTextMessage();
                pollingHelper.getOutData(jsonOrList, format).forEach(oneLine -> {
                    try {
                        message.clearBody();
                        message.setText(oneLine);
                        publisher.publish(message);
                        QueryPollingFacade.success[0] = true;
                    } catch (JMSException e) {
                        QueryPollingFacade.message[0] = e.getMessage();
                        QueryPollingFacade.success[0] = false;
                    }
                });
                tibjmsAdmin.close();
                connection.close();
            }
    }

    public void rabbitmqSend(Object jsonOrList, String outConnId, String format) throws Exception {
        Rabbitmq rabbitmq = objectMapper.readValue(queryService.getConnectionFile(outConnId), Rabbitmq.class);
        String destination = rabbitmq.getDestination();
        String mqQueue = rabbitmq.getQueue();
        String mqExchangeQueue = rabbitmq.getExchangeQueue();

        if (destination.equalsIgnoreCase("queue")) {
            ConnectionFactory factory = new ConnectionFactory();
            String[] mqIpList = rabbitmq.getIp().split(",");
    	    Address[] addrList = null;
    	    if (mqIpList.length == 2) {
    	    	addrList = new Address[] {new Address(mqIpList[0], Integer.parseInt(rabbitmq.getPort())), new Address(mqIpList[1], Integer.parseInt(rabbitmq.getPort()))};
    	    } else {
    	    	addrList = new Address[] {new Address(mqIpList[0], Integer.parseInt(rabbitmq.getPort()))};
    	    }
            // factory.setHost(rabbitmq.getIp());
            // factory.setPort(Integer.parseInt(rabbitmq.getPort()));
            factory.setUsername(rabbitmq.getId());
            factory.setPassword(rabbitmq.getPw());
            try (com.rabbitmq.client.Connection connection = factory.newConnection(addrList); Channel channel = connection.createChannel()) {
                if (mqQueue != null && !mqQueue.equals("") && mqExchangeQueue != null && !mqExchangeQueue.equals("")) {
                    channel.exchangeDeclare(mqExchangeQueue, "direct", true, false, false, null);
                    channel.queueBind(mqQueue, mqExchangeQueue, "");
                }
                AMQP.BasicProperties.Builder props = new AMQP.BasicProperties.Builder().contentType("text/plain").deliveryMode(2);
                pollingHelper.getOutData(jsonOrList, format).forEach(oneLine -> {
                    try {
                        channel.basicPublish(mqExchangeQueue, "", props.build(), oneLine.getBytes());
                        QueryPollingFacade.success[0] = true;
                    } catch (IOException e) {
                        QueryPollingFacade.message[0] = e.getMessage();
                        QueryPollingFacade.success[0] = false;
                    }
                });
            }
        } else {
            QueryPollingFacade.message[0] = "rabbitmq destination check failed..";
            QueryPollingFacade.success[0] = false;
        }
    }

    public void activemqSend(Object jsonOrList, String outConnId, String format) throws Exception {
        Activemq activemq = objectMapper.readValue(queryService.getConnectionFile(outConnId), Activemq.class);
        String destination = activemq.getDestination();
        String mqQueue = activemq.getQueue();
        // activemqUrl = "tcp://localhost:61616"
        String activemqUrl = activemq.getIp() + ":" + activemq.getPort();
        if (destination.equalsIgnoreCase("queue")) {
            ActiveMQConnectionFactory connectionFactory = new ActiveMQConnectionFactory();
            connectionFactory.setBrokerURL(activemqUrl);
            connectionFactory.setUserName(activemq.getId());
            connectionFactory.setPassword(activemq.getPw());
            QueueConnection connection = connectionFactory.createQueueConnection();
            QueueSession session = connection.createQueueSession(false, javax.jms.Session.AUTO_ACKNOWLEDGE);
            javax.jms.Queue queue = session.createQueue(mqQueue);
            QueueSender sender = session.createSender(queue);
            javax.jms.TextMessage message = session.createTextMessage();
            pollingHelper.getOutData(jsonOrList, format).stream().forEachOrdered(oneLine -> {
                try {
                    message.clearBody();
                    message.setText(oneLine);
                    sender.send(message);
                    QueryPollingFacade.success[0] = true;
                } catch (JMSException e) {
                    QueryPollingFacade.message[0] = e.getMessage();
                    QueryPollingFacade.success[0] = false;
                }
            });
            sender.close();
            session.close();
            connection.close();
        } else {
            QueryPollingFacade.message[0] = "activemq destination check failed..";
            QueryPollingFacade.success[0] = false;
        }
    }   
    

    public Boolean insert(JsonNode jsonNode, String connectionId, String tableName, Map<String, String> dataTypes, Boolean isTarget, String dbms) {
        Iterator<JsonNode> iterator = jsonNode.iterator();
        ArrayNode arrayNode = objectMapper.getNodeFactory().arrayNode();
        List<String> queries = new ArrayList<>();
        int counter = 0;

        while (iterator.hasNext()) {
            arrayNode.add(iterator.next());
            counter++;
            if (counter > 0 && counter % 5000 == 0) {
                queries.add(getStructureInsertQuery(arrayNode, connectionId, tableName, dataTypes, isTarget, dbms));
                arrayNode = objectMapper.getNodeFactory().arrayNode();
            }
        }

        if (counter > 0 && counter % 5000 != 0) {
            queries.add(getStructureInsertQuery(arrayNode, connectionId, tableName, dataTypes, isTarget, dbms));
        }

        if (!queries.isEmpty()) {
            String resultValidity = nativeQuery.executeBatchQuery(connectionId, queries);
            if (isTarget) {
                QueryPollingFacade.message[0] = resultValidity;
            }
            return !resultValidity.startsWith("error");
        }
        return false;
    }

    public Boolean truncateInsert(JsonNode jsonNode, String connectionId, String tableName, Map<String, String> dataTypes, Boolean isTarget, String dbms) {
    
        String truncQuery = nativeQuery.executeQuery(connectionId, "TRUNCATE TABLE " + tableName);
        if (!truncQuery.isEmpty()) {
            QueryPollingFacade.message[0] = truncQuery;
            return false;
        }
        return insert(jsonNode, connectionId, tableName, dataTypes, isTarget, dbms);
    }


    private String getStructureInsertQuery(JsonNode jsonNode, String connectionId, String tableName, Map<String, String> dataTypes, Boolean isTarget, String dbms) {
        Map<String, Object> columns;
        if (dataTypes == null) {
            columns = nativeQuery.getColumns(connectionId, tableName);
            if (columns.containsKey("datatypes")) {
                dataTypes = (Map<String, String>) columns.get("datatypes");
                if (!dataTypes.isEmpty()) {
                    return pollingHelper.getInsertSqlQuery(dataTypes, jsonNode, tableName, dbms);
                }
            }
        } else {
            return pollingHelper.getInsertSqlQuery(dataTypes, jsonNode, tableName, dbms);
        }
        if (!isTarget) {
            QueryPollingFacade.message[0] = "can't get keys and types from the table\t" + tableName + "\t as it maybe missing";
        }
        return "";
    }

    public Boolean update(String dpSqlField, JsonNode jsonNode, String connectionId, String tableName, String dbTableKey, String dbms) {
        Map<String, Object> columns = nativeQuery.getColumns(connectionId, tableName);
        final String[] type = {""};
        if (columns.containsKey("datatypes")) {
            Map<String, String> dataTypes = (Map<String, String>) columns.get("datatypes");
            if (!dataTypes.isEmpty()) {
                dataTypes.entrySet().stream().filter(object -> object.getKey().toUpperCase().equals(dbTableKey)).findAny().ifPresent(ob -> type[0] = ob.getValue());
            }
            Set<String> valuesToDelete = getValuesOFKeyForDeletion(jsonNode, dbTableKey, type[0]);
            if (valuesToDelete.isEmpty()) {
                QueryPollingFacade.message[0] = "DB_TABLE_KEY\t" + dbTableKey + "\t doesn't exist in target table\t" + tableName;
                return false;
            }
            JsonNode node = getUniqueMaxRecords(jsonNode, dpSqlField,
                    valuesToDelete.stream().map(s -> s.replace("'", "")).collect(Collectors.toList()), dbTableKey);
            String resultQuery = nativeQuery.executeQuery(connectionId, "DELETE FROM " + tableName + " WHERE " + dbTableKey + " IN (" + Joiner.on(",").join(valuesToDelete) + ")");
                QueryPollingFacade.message[0] = resultQuery;
                if (resultQuery.isEmpty()) {
                    return insert(node, connectionId, tableName, dataTypes, false, dbms);
                }
        }
        return false;
    }

    private JsonNode getUniqueMaxRecords(JsonNode jsonNode, String primaryKey, List<String> dbTableKeyValues, String dbTableKey) {
        List list = new ArrayList<>();
        StreamSupport.stream(jsonNode.spliterator(), false)
                .collect(Collectors.toCollection(ArrayList::new))
                .stream()
                .filter(node -> node.hasNonNull(primaryKey.toUpperCase()) && node.hasNonNull(dbTableKey) && dbTableKeyValues.contains(node.get(dbTableKey).asText()))
                .collect(Collectors.groupingBy(node -> node.get(dbTableKey).asText()))
                .forEach((key, value) -> value.stream()
                        .max(Comparator.comparing(jsonNodeObject -> Integer.parseInt(jsonNodeObject.get(primaryKey).toString())))
                        .ifPresent(list::add));
        return objectMapper.valueToTree(list);
    }

    private Set<String> getValuesOFKeyForDeletion(JsonNode jsonNode, String key, String type) {
        Set<String> values = new HashSet<>();
        StreamSupport.stream(jsonNode.spliterator(), false).forEach(node -> {
            JSONObject jsonObject = new JSONObject(node.toString());
            if (jsonObject.has(key)) {
                values.add(type.contains("char") ? "'" + jsonObject.get(key).toString() + "'" : jsonObject.get(key).toString());
            }
        });
        return values;
    }

    public void logToDB(String connectionID, Connection infoConnection, String dpID, String logCD, String logMSG, String logData) {
        Thread thread = new Thread(() -> {
            try {
                queryService.checkValidityUpdateClauses("INSERT INTO " + infoConnection.getSCHEME() +
                        "Z_LOG (PROGRAM_ID, LOG_KIND, LOG_CD, LOG_MSG, LOG_DATA, LOAD_DTTM) VALUES ('" + dpID + "', 'D', '" + logCD + "', '"
                        + Optional.ofNullable(logMSG).orElse("").replaceAll("'", "") + "', '"
                        + Optional.ofNullable(logData).orElse("").replaceAll("'", "") + "', '"
                        + new SimpleDateFormat("yyyyMMddHHmmss").format(new Date()) + "')", connectionID);
            } catch (Exception e) {
                queryService.addLogging("POST", "dbpolling triggered (ID = " + dpID + " ), fail when inserting data to z_log\t" + e.getMessage());
            }
        });
        thread.setDaemon(true);
        thread.start();
    }

    //    @Cacheable(value = "maxSeqNumber", cacheManager = "cacheManagerCaffeine")
    public BigInteger getMaxSeqNumber(JsonNode jsonNode, String dbSqlField) {
        return StreamSupport.stream(jsonNode.spliterator(), false).map(o -> {
            JSONObject jsonObject = new JSONObject(o.toString());
            if (jsonObject.has(dbSqlField)) {
                return new BigInteger(jsonObject.get(dbSqlField).toString());
            } else {
                return new BigInteger("0");
            }
        }).max((a, b) -> a.compareTo(b) == -1 ? -1 : 1 ).orElse(new BigInteger("0"));
    }

    public void initDBPollingFile(String dbPollingDir, String dpID, String dbTableName, String dpFileRepeat) {
        CronTrigger cronTrigger = null;
        String format = "yyyyMMdd";
        if (dpFileRepeat.matches("\\d+M")) {
            cronTrigger = new CronTrigger("0 */" + dpFileRepeat.substring(0, dpFileRepeat.indexOf("M")) + " * ? * *");
            format = "yyyyMMddHHmm";
        } else if (dpFileRepeat.equalsIgnoreCase("H")) {
            cronTrigger = new CronTrigger("0 0 * ? * *");
            format = "yyyyMMddHH";
        } else if (dpFileRepeat.equalsIgnoreCase("D")) {
            cronTrigger = new CronTrigger("0 0 0 * * ?");
            format = "yyyyMMdd";
        }
        if (!latestdpIDTimeValueNMap.containsKey(dpID)) {
            latestdpIDTimeValueNMap.put(dpID, dpFileRepeat);
        }
        if (cronTrigger != null) {
            String finalFormat = format;

            //at first time
            if (!latestOutputFileNameMap.containsKey(dpID)) {
                executeNow(dbPollingDir, dpID, dbTableName, finalFormat, true);
                latestdpIDScheduleObjectMap.put(dpID, taskScheduler.schedule(() -> executeNow(dbPollingDir, dpID, dbTableName, finalFormat, false), cronTrigger));
                latestdpIDNameValueNMap.put(dpID, dbTableName);
            }
            //second time
            else {
                executeNow(dbPollingDir, dpID, dbTableName, finalFormat, true);
                //time|name changed
                if (!latestdpIDTimeValueNMap.get(dpID).equalsIgnoreCase(dpFileRepeat) || !latestdpIDNameValueNMap.get(dpID).equalsIgnoreCase(dbTableName)) {
                    ScheduledFuture oldScheduleFuture = (ScheduledFuture) latestdpIDScheduleObjectMap.get(dpID);
                    oldScheduleFuture.cancel(true);
                    latestdpIDNameValueNMap.put(dpID, dbTableName);
                    latestdpIDTimeValueNMap.put(dpID, dpFileRepeat);
                    latestdpIDScheduleObjectMap.put(dpID, taskScheduler.schedule(() -> executeNow(dbPollingDir, dpID, dbTableName, finalFormat, false), cronTrigger));
                }
            }
        }
    }

    private void executeNow(String dbPollingDir, String dpID, String outputName, String format, boolean create) {
        latestOutputFileNameMap.put(dpID, outputName + "_" + new SimpleDateFormat(format).format(new Date()) + ".txt");
        try {
            if (create) {
                if (!Files.isDirectory(Paths.get(dbPollingDir))) {
                    Files.createDirectory(Paths.get(dbPollingDir));
                }
                if (!Paths.get(dbPollingDir + latestOutputFileNameMap.get(dpID)).toFile().exists()) {
                    Files.createFile(Paths.get(dbPollingDir + latestOutputFileNameMap.get(dpID)));
                }
            }
        } catch (IOException e) {
            QueryPollingFacade.success[0] = false;
            QueryPollingFacade.message[0] = e.getMessage();
        }
    }

    public void saveToFile(Integer dpOutSeq, String dpID, String dbTableName, String dpFileRepeat, List<String> list) {
        try {
            String dbPollingDir = constants.getDBPollingDir();
            initDBPollingFile(dbPollingDir, dpID, dbTableName, dpFileRepeat);
            if (latestOutputFileNameMap.containsKey(dpID)) {
                long size = Files.size(Paths.get(dbPollingDir + latestOutputFileNameMap.get(dpID)));
                if (size > 0) {
                    list = list.subList(1, list.size());
                }
                Files.write(Paths.get(dbPollingDir + latestOutputFileNameMap.get(dpID)), StringUtils.join(list, "\n").concat("\n").getBytes(),
                        StandardOpenOption.CREATE, StandardOpenOption.APPEND);
                QueryPollingFacade.success[0] = true;
            }
        } catch (Exception e) {
            QueryPollingFacade.dpOutSeq.add(dpOutSeq);
            QueryPollingFacade.success[0] = false;
            QueryPollingFacade.message[0] = e.getMessage();
        }
    }

    public void logToDBTriggered(String lastSeqProc, String lastSeqProcPlusMax, Integer dpOutSeq, Boolean success, String outMessage, String connectionID, String dpID, Connection infoConnection) {
        Thread thread = new Thread(() -> {
            try {
                queryService.checkValidityUpdateClauses("INSERT INTO " + infoConnection.getSCHEME() +
                        "T_DBPOLLING_TRIGGERED (DP_ID, DP_OUT_SEQ, DP_STATUS, DP_LAST_SEQ_FROM, DP_LAST_SEQ_TO, DP_MESSAGE, LOAD_DTTM) VALUES ('" + dpID + "', "
                        + dpOutSeq + ", '"
                        + (success ? "success" : "fail") + "', "
                        + (new BigInteger(lastSeqProc).add(BigInteger.ONE)) + ", "
                        + (new BigInteger(lastSeqProcPlusMax)) + ", '"
                        + (success ? "success" : outMessage.replaceAll("'", "")) + "', '"
                        + new SimpleDateFormat("yyyyMMddHHmmss").format(new Date()) + "')", connectionID);
            } catch (Exception e) {
                queryService.addLogging("POST", "dbpolling triggered (ID = " + dpID + " ), fail when inserting data to T_DBPOLLING_TRIGGERED\t" + e.getMessage());
            }
        });
        thread.setDaemon(true);
        thread.start();
    }

    public void recreateTable(String connectionId, String tableName, String dmlSql) {
        nativeQuery.executeQuery(connectionId, "DROP TABLE IF EXISTS " + tableName);
        nativeQuery.executeQuery(connectionId, dmlSql);
    }

    @Override
    public void update(Observable observable, Object arg) {
        List arrayList = (ArrayList) arg;
        if (arrayList.get(0) instanceof TargetDto) {
            TargetDto targetDto = (TargetDto) arrayList.get(0);
            String connectionID = (String) arrayList.get(1);
            String tableName = (String) arrayList.get(2);
            Map<String, String> dataTypes = (Map<String, String>) arrayList.get(3);
            Boolean isTarget = (Boolean) arrayList.get(4);
            String dbms = (String) arrayList.get(5);
            String partMessage = (String) arrayList.get(6);
            Chunk chunk = (Chunk) observable;
            String tID = targetDto.getParameters().getTId();
            JsonNode jsonNode;
            try {
                jsonNode = objectMapper.readTree(chunk.getContent());
            } catch (IOException e) {
                querySocketFacade.send(SocketBodyDto.builder().userId(targetDto.getUserID()).key("TARGETLIST").message(partMessage + e.getMessage()).build());
                queryService.addLogging("POST", partMessage + e.getMessage());
                QueryTargetFacade.message[0] = "fail due\t" + e.getMessage();
                return;
            }

            JSONArray outJsonArray = new JSONArray();
            StreamSupport.stream(jsonNode.spliterator(), false).forEach(node -> {
                JSONObject jsonObject = new JSONObject();
                node.fieldNames().forEachRemaining(oneKey -> {
                    jsonObject.put("CUST_ID", node.get(oneKey).asText());
                    jsonObject.put("T_ID", tID);
                });
                outJsonArray.put(jsonObject);
            });

            insert(objectMapper.valueToTree(outJsonArray.toList()), connectionID, tableName, dataTypes, isTarget, dbms);
        }
    }
}
