package co.kr.coresolutions.service;

import co.kr.coresolutions.commons.ResponseCodes;
import co.kr.coresolutions.commons.ResponseDto;
import co.kr.coresolutions.model.dtos.SessionDto;
import com.fasterxml.jackson.databind.JsonNode;
import lombok.RequiredArgsConstructor;
import lombok.extern.slf4j.Slf4j;
import net.sf.ehcache.Ehcache;
import net.sf.ehcache.Element;
import org.springframework.cache.Cache;
import org.springframework.cache.CacheManager;
import org.springframework.scheduling.annotation.Scheduled;
import org.springframework.stereotype.Service;
import org.springframework.web.context.annotation.RequestScope;

import java.time.LocalDateTime;
import java.time.format.DateTimeFormatter;
import java.util.Optional;

@Service
@RequiredArgsConstructor
@Slf4j
@RequestScope
public class QueryServiceSession {
    private final Constants constants;
    private static final String SESSION_CHECK = "session_check";
    private static final String PATTERN = "yyyyMMdd:HH:mm:ss";
    private final CacheManager cacheManager;

    @Scheduled(fixedRate = 60000)
    public void removeExpiredCache() {
        Optional.ofNullable(readFile())
                .ifPresent(this::examineExpirationCache);
    }

    private void examineExpirationCache(JsonNode jsonNode) {
        JsonNode sessionDurationTimeNode = jsonNode.get("SessionDurationTime");
        if (sessionDurationTimeNode != null && sessionDurationTimeNode.isNumber()) {
            int sessionDurationTime = sessionDurationTimeNode.asInt();
            Cache cache = cacheManager.getCache("cacheSession");
            Object nativeCache = cache.getNativeCache();
            if (nativeCache instanceof Ehcache) {
                Ehcache ehcache = (Ehcache) nativeCache;
                if (sessionDurationTime <= 0) {
                    ehcache.removeAll();
                } else {
                    DateTimeFormatter dateTimeFormatter = DateTimeFormatter.ofPattern(PATTERN);
                    ehcache.getKeys().stream()
                            .map(oneKey -> ehcache.get(oneKey).getObjectValue())
                            .filter(o -> {
                                SessionDto sessionDto = (SessionDto) o;
                                return (LocalDateTime.parse(sessionDto.getLastUpdatedTime(), dateTimeFormatter)
                                        .isAfter(LocalDateTime.parse(sessionDto.getOpenedTime(), dateTimeFormatter).plusMinutes(sessionDurationTime)) ||
                                        LocalDateTime.now()
                                                .isAfter(LocalDateTime.parse(sessionDto.getLastUpdatedTime(), dateTimeFormatter).plusMinutes(sessionDurationTime)));
                            })
                            .forEach(o -> ehcache.remove(((SessionDto) o).getOwnerId()));
                }
            }
        }
    }

    public JsonNode readFile() {
        return constants.getConfigFileAsJson();
    }

    public ResponseDto updateSession(String ownerId, JsonNode sessionIdNode) {
        final ResponseDto[] responseDto = {ResponseDto.ok()};
        Optional.ofNullable(readFile())
                .ifPresent(jsonNode -> {
                    if (jsonNode.hasNonNull(SESSION_CHECK)) {
                        if (jsonNode.get(SESSION_CHECK).isBoolean()) {
                            if (jsonNode.get(SESSION_CHECK).asBoolean()) {
                                if (sessionIdNode == null) {
                                    responseDto[0] = ResponseDto.builder().errorCode(ResponseCodes.GENERAL_ERROR).message("session_id required and should be as String").build();
                                } else if (!sessionIdNode.isTextual()) {
                                    responseDto[0] = ResponseDto.builder().errorCode(ResponseCodes.GENERAL_ERROR).message("session_id required and should be as String").build();
                                } else {
                                    responseDto[0] = checkSession(ownerId, sessionIdNode.asText());
                                }
                            } else {
                                responseDto[0] = ResponseDto.ok();
                            }
                        } else {
                            responseDto[0] = ResponseDto.ERROR_PARSE;
                        }
                    }
                });
        return responseDto[0];
    }

    private ResponseDto checkSession(String ownerId, String sessionId) {
        Cache cache = cacheManager.getCache("cacheSession");
        Object nativeCache = cache.getNativeCache();
        if (nativeCache instanceof Ehcache) {
            Ehcache ehcache = (Ehcache) nativeCache;
            if (ehcache.isKeyInCache(ownerId)) {
                SessionDto sessionDto = (SessionDto) ehcache.get(ownerId).getObjectValue();
                if (sessionDto != null) {
                    if (!sessionId.equals(sessionDto.getSessionId())) {
                        return ResponseDto.builder().errorCode(ResponseCodes.GENERAL_ERROR).message("session_id doesn't match with the existing in cache!").build();
                    }
                    sessionDto.setLastUpdatedTime(LocalDateTime.now().format(DateTimeFormatter.ofPattern(PATTERN)));
                    ehcache.put(new Element(ownerId, sessionDto));
                    return ResponseDto.ok();
                }
            }
        }
        return ResponseDto.builder().errorCode(ResponseCodes.GENERAL_ERROR).message("User-session doesn't exist for a user, Please create a user-session!").build();
    }
}
