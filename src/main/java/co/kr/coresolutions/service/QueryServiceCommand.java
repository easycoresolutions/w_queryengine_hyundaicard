package co.kr.coresolutions.service;

import java.io.File;
import java.io.IOException;
import java.lang.reflect.Field;
import java.nio.file.Files;
import java.nio.file.Paths;
import java.util.ArrayList;
import java.util.List;
import java.util.Map;
import java.util.Objects;
import java.util.concurrent.ConcurrentHashMap;
import java.util.stream.Collectors;

import javax.annotation.PostConstruct;

import org.json.JSONArray;
import org.springframework.stereotype.Service;

import com.fasterxml.jackson.databind.JsonNode;
import com.fasterxml.jackson.databind.node.ObjectNode;
import com.sun.jna.Pointer;
import com.sun.jna.platform.win32.Kernel32;
import com.sun.jna.platform.win32.WinNT;

import co.kr.coresolutions.model.Command;
import co.kr.coresolutions.model.dtos.CommandTemplate;
import lombok.RequiredArgsConstructor;
import net.sf.ehcache.Cache;
import net.sf.ehcache.CacheManager;
import net.sf.ehcache.Element;

@Service
@RequiredArgsConstructor
public class QueryServiceCommand {
	private static final String OS = System.getProperty("os.name").toLowerCase();
    private final Constants constants;
    private String commandDir;
    private String scriptEllaDir;
    public static volatile Map<String, Command.CommandDetails> cacheCommand = new ConcurrentHashMap<>();
    private final QueryService queryService;

    @PostConstruct
    public void init() {
        scriptEllaDir = constants.getScriptEllaDir();
    }

    public boolean isCommandFileExists(String userId, String fileName) {
        commandDir = constants.getCommandDir();
        return Files.exists(Paths.get(commandDir + userId + File.separator + fileName));
    }

    public String formatCommand(String userId, String command, ObjectNode parameter) throws IOException {
        commandDir = constants.getCommandDir();
        final String[] content = {new String(Files.readAllBytes(Paths.get(commandDir + userId + File.separator + command)))};

        if (content[0].length() > 0 && parameter != null && parameter.size() > 0) {

            parameter.fields().forEachRemaining(nodeEntry -> {

                if (content[0].contains(nodeEntry.getKey())) {
                    content[0] = content[0].replace(nodeEntry.getKey(), nodeEntry.getValue().asText());
                }
            });
        }

        return content[0];
    }

    public String getFormattedCommandEtl(String directory, String xml, CommandTemplate commandTemplate) throws IOException {
        final String[] content = {new String(Files.readAllBytes(Paths.get(scriptEllaDir + directory + File.separator + xml)))};
        JsonNode prompts = commandTemplate.getPrompts();
        if (content[0].length() > 0 && prompts != null && prompts.size() > 0) {
            prompts.fields().forEachRemaining(nodeEntry -> {
                if (content[0].contains(nodeEntry.getKey())) {
                    content[0] = content[0].replace(nodeEntry.getKey(), nodeEntry.getValue().asText());
                }
            });
        }
        return content[0];
    }

    public void saveCacheCommand(String owner, Command.CommandDetails commandDetails) {
        cacheCommand.put(owner, commandDetails);
    }

    public List<Command.CommandDetails> getCacheCommands() {
        CacheManager cm = CacheManager.getInstance();
        Cache cache = cm.getCache("cacheCommand");
        List<Command.CommandDetails> commandDetails = new ArrayList<>();
        if (cache != null) {
            List<String> cacheKeys = cache.getKeys();
            commandDetails = cacheKeys.stream().map(oneKey -> {
                Element element = cache.get(oneKey);
                if (element != null) {
                    return (Command.CommandDetails) element.getObjectValue();
                }
                return null;
            }).collect(Collectors.toList());
        }
        return commandDetails.stream().filter(Objects::nonNull).collect(Collectors.toList());
    }

    public Command.CommandDetails getCacheCommandByKey(String key) {
        CacheManager cm = CacheManager.getInstance();
        Cache cache = cm.getCache("cacheCommand");
        if (cache != null) {
            Element element = cache.get(key);
            if (element != null) {
                return (Command.CommandDetails) element.getObjectValue();
            }
        }
        return null;
    }


    public Process getCacheCommandProcess(String commandID) {
        CacheManager cm = CacheManager.getInstance();
        Cache cache = cm.getCache("cacheCommandProcess");
        if (cache != null) {
            Element element = cache.get(commandID);
            if (element != null) {
                return (Process) element.getObjectValue();
            }
        }
        return null;
    }
    
    public Object getCacheCommandObject(String commandID) {
        CacheManager cm = CacheManager.getInstance();
        Cache cache = cm.getCache("cacheCommandProcess");
        if (cache != null) {
            Element element = cache.get(commandID);
            if (element != null) {
                return element.getObjectValue();
            }
        }
        return null;
    }

    public boolean removeCacheCommand(String commandID) {
        cacheCommand.remove(commandID);
        return !cacheCommand.containsKey(commandID);
    }

    public void stopCommand(String commandID) {
        //Optional.ofNullable(getCacheCommandProcess(commandID)).ifPresent(Process::destroy);
    	Object command = QueryService.sessionProcess.get(commandID);
        if(null != command) {
            if(command instanceof Process) {
                if(null != command)
                ((Process) command).destroy();
            } else if(command instanceof Thread) {
                if(null != command)
                ((Thread) command).interrupt();
            }
        }
    }

    public String getFormattedCommandEtlFromDBMS(String connectionID, String projectID, String scriptID, CommandTemplate commandTemplate) {
        final String[] content = {""};
        String resultQuery = queryService.checkValidity("SELECT SCRIPT_CONTENT FROM T_ETL_SCRIPT WHERE ETL_ID='" + projectID + "' AND SCRIPT_ID='" + scriptID + "'",
                connectionID, "", "cache", false);
        if (!(resultQuery.equalsIgnoreCase("[]") || resultQuery.startsWith("Error")
                || resultQuery.startsWith("error") || resultQuery.startsWith("maxRunningTimeOut") || resultQuery.startsWith("MaxRows"))) {
            content[0] = new JSONArray(resultQuery).getJSONObject(0).get("SCRIPT_CONTENT").toString();
            JsonNode prompts = commandTemplate.getPrompts();
            if (content[0].length() > 0 && prompts != null && prompts.size() > 0) {
                prompts.fields().forEachRemaining(nodeEntry -> {
                    if (content[0].contains(nodeEntry.getKey())) {
                        content[0] = content[0].replace(nodeEntry.getKey(), nodeEntry.getValue().asText());
                    }
                });
            }
        }
        return content[0];
    }
    
    public static boolean isUnix() {
        return (OS.indexOf("nix") >= 0 || OS.indexOf("nux") >= 0 || OS.indexOf("aix") >= 0);
    }
    
    public static long getProcessID(Process p) {
        long result = -1;
        try {
            //for windows
            if (p.getClass().getName().equals("java.lang.Win32Process") ||
                    p.getClass().getName().equals("java.lang.ProcessImpl")) {
                Field f = p.getClass().getDeclaredField("handle");
                f.setAccessible(true);
                long handl = f.getLong(p);
                Kernel32 kernel = Kernel32.INSTANCE;
                WinNT.HANDLE hand = new WinNT.HANDLE();
                hand.setPointer(Pointer.createConstant(handl));
                result = kernel.GetProcessId(hand);
                f.setAccessible(false);
            }
            //for unix based operating systems
            if (p.getClass().getName().equals("java.lang.UNIXProcess")) {
                Field f = p.getClass().getDeclaredField("pid");
                f.setAccessible(true);
                result = f.getLong(p);
                f.setAccessible(false);
            }
        } catch (Exception ex) {
            result = -1;
        }
        return result;
    }
}
