package co.kr.coresolutions.service;

import co.kr.coresolutions.dao.OrderedJSONObject;
import com.google.gson.JsonArray;
import com.google.gson.JsonElement;
import com.google.gson.JsonParser;
import lombok.RequiredArgsConstructor;
import org.json.CDL;
import org.json.JSONArray;
import org.springframework.stereotype.Service;

import java.util.Arrays;
import java.util.stream.Collectors;

@Service
@RequiredArgsConstructor
public class QueryServiceCSV {
    public static int countRecords = 0;

    public String structureCSV(String resultValidity) {
        JSONArray secondArray = new JSONArray();
        formatToCsv(resultValidity, secondArray);
        return CDL.toString(secondArray);
    }

    private synchronized void formatToCsv(String resultValidity, JSONArray secondArray) {
        JsonParser parser = new JsonParser();
        JsonElement jsonElement = parser.parse(resultValidity);
        JsonArray jsonArray = jsonElement.getAsJsonArray();
        jsonArray.iterator().forEachRemaining(e -> {
            OrderedJSONObject temp = new OrderedJSONObject(e.toString());
            OrderedJSONObject temp2 = new OrderedJSONObject(temp, Arrays.stream(OrderedJSONObject.getNames(temp))
                    .filter(s -> (s.length() > 0))
                    .collect(Collectors.toList())
                    .toArray(new String[OrderedJSONObject.getNames(temp).length]));
            temp2.keySet().forEach(s -> {
                if (temp2.get(s).toString().equalsIgnoreCase("null")) {
                    temp2.put(s, "");
                }
            });
            secondArray.put(temp2);
        });
        QueryServiceCSV.countRecords = jsonArray.size();
    }

    public String getCsvWithHeader(String resultValidity, Boolean header) {
        JSONArray secondArray = new JSONArray();
        formatToCsv(resultValidity, secondArray);
        if (header) {
            return CDL.toString(secondArray);
        } else {
            return CDL.toString(secondArray.getJSONObject(0).names(), secondArray);
        }
    }
}
