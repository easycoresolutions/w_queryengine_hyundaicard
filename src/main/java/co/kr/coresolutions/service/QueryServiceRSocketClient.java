package co.kr.coresolutions.service;

import co.kr.coresolutions.commons.CustomResponseDto;
import co.kr.coresolutions.model.Connection;
import co.kr.coresolutions.model.MessageBodySocket;
import co.kr.coresolutions.model.dtos.SocketDto;
import co.kr.coresolutions.model.dtos.SocketLoginDto;
import com.fasterxml.jackson.databind.ObjectMapper;
import io.rsocket.SocketAcceptor;
import io.rsocket.frame.decoder.PayloadDecoder;
import io.rsocket.metadata.WellKnownMimeType;
import lombok.RequiredArgsConstructor;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.messaging.rsocket.RSocketRequester;
import org.springframework.messaging.rsocket.RSocketStrategies;
import org.springframework.security.rsocket.metadata.UsernamePasswordMetadata;
import org.springframework.stereotype.Service;
import org.springframework.util.MimeType;
import org.springframework.util.MimeTypeUtils;
import reactor.core.Disposable;

import java.nio.file.Files;
import java.nio.file.Paths;
import java.util.HashMap;
import java.util.Map;
import java.util.concurrent.ConcurrentHashMap;

@Service
@RequiredArgsConstructor
public class QueryServiceRSocketClient {
    private final Logger log = LoggerFactory.getLogger(QueryServiceRSocketClient.class);
    //message/x.rsocket.authentication.v0
    private static final MimeType rSocketAUTHV0 = MimeTypeUtils.parseMimeType(WellKnownMimeType.MESSAGE_RSOCKET_AUTHENTICATION.getString());
    public static Map<String, Object> sessionRSocketRequester = new HashMap<>();
    public static Map<String, String> sessionRSocketRunningUsers = new ConcurrentHashMap<>();
    private final RSocketStrategies rSocketStrategies;
    private final ObjectMapper objectMapper;
    private final SocketAcceptor socketAcceptor;

    public RSocketRequester createCommonRSocketRequester(String api, String host, String userID, String password, int port, String owner) {
        return RSocketRequester.builder()
                .setupRoute("r/" + api)
                .setupData(owner)
                .setupMetadata(new UsernamePasswordMetadata(userID, password), rSocketAUTHV0)
                .rsocketStrategies(rSocketStrategies)
                .rsocketConnector(connector -> connector.acceptor(socketAcceptor).payloadDecoder(PayloadDecoder.ZERO_COPY))
                .connectTcp(host, port)
                .block();
    }

    void login(String host, String userID, String password, int port, String owner) {
        //connect to QuerySocketServer
        RSocketRequester rsocketRequester = createCommonRSocketRequester("login", host, userID, password, port, owner);

        if (rsocketRequester != null) {
            Disposable disposable = rsocketRequester.rsocket()
                    .onClose()
                    .doOnSuccess(aVoid -> log.info("Connection initiated\t"))
                    .doOnError(error -> {
                        QueryServiceRSocketClient.sessionRSocketRequester.remove(owner);
                        QueryServiceRSocketClient.sessionRSocketRunningUsers.remove(owner);
                        log.warn("Connection CLOSED " + error.getMessage());
                    })
                    .doFinally(consumer -> {
                        QueryServiceRSocketClient.sessionRSocketRequester.remove(owner);
                        log.info("Client DISCONNECTED " + consumer.name());
                    })
                    .subscribe();
            if (!disposable.isDisposed()) {
                QueryServiceRSocketClient.sessionRSocketRequester.put(owner, rsocketRequester);
            }
        }
    }

    public Object requestResponse(String api, Connection connection, SocketDto socketDto) {
        String owner = socketDto.getOwner();
        try {
            if (api.equalsIgnoreCase("closeSession")) {
                if (!QueryServiceRSocketClient.sessionRSocketRequester.containsKey(owner)) {
                    return CustomResponseDto.builder().message("owner {" + owner + "} is not connected!").build();
                }
                closeRSession(connection.getHost(), connection.getID(), connection.getPW(), Integer.parseInt(connection.getPort()), owner);
                return CustomResponseDto.builder().Success(true).message("closed").build();
            } else if (api.equalsIgnoreCase("openSession")) {
                if (!QueryServiceRSocketClient.sessionRSocketRequester.containsKey(owner)) {
                    login(connection.getHost(), connection.getID(), connection.getPW(), Integer.parseInt(connection.getPort()), owner);
                    return CustomResponseDto.builder().Success(true).message("connected").build();
                }
                return CustomResponseDto.builder().Success(true).message("already connected!").build();
            } else if (api.equalsIgnoreCase("listUsersRunning")) {
                return objectMapper.valueToTree(QueryServiceRSocketClient.sessionRSocketRunningUsers);
            }

            if (QueryServiceRSocketClient.sessionRSocketRequester.containsKey(owner)) {
                RSocketRequester rSocketRequester = getRSocketRequesterForUser(owner);
                if (rSocketRequester != null) {
                    MessageBodySocket messageBodySocket = MessageBodySocket.builder().owner(owner).build();
                    if (socketDto.getFile() != null) {
                        messageBodySocket.setScript(new String(Files.readAllBytes(Paths.get(socketDto.getFile()))));
                    } else {
                        messageBodySocket.setScript(socketDto.getScript());
                    }

                    String responseEvaluation = rSocketRequester
                            .route("r/" + api)
                            .data(objectMapper.writeValueAsString(messageBodySocket))
                            .retrieveMono(String.class)
                            .block();

                    if (socketDto.getFormat() != null && socketDto.getFormat().equalsIgnoreCase("text")) {
                        return responseEvaluation;
                    }

                    return CustomResponseDto
                            .builder()
                            .Success(true)
                            .message(responseEvaluation)
                            .build();
                }
            }
        } catch (Exception e) {
            return CustomResponseDto.builder().message("error: " + e.getMessage()).build();
        }
        return CustomResponseDto.builder().message("owner {" + owner + "} is not connected!").build();
    }

    private void closeRSession(String host, String userID, String password, int port, String owner) {
        createCommonRSocketRequester("close", host, userID, password, port, owner);
        QueryServiceRSocketClient.sessionRSocketRequester.remove(owner);
    }

    private RSocketRequester getRSocketRequesterForUser(String userID) {
        if (QueryServiceRSocketClient.sessionRSocketRequester.containsKey(userID)) {
            RSocketRequester rSocketRequester = (RSocketRequester) QueryServiceRSocketClient.sessionRSocketRequester.get(userID);
            if (rSocketRequester != null && !rSocketRequester.rsocket().isDisposed()) {
                return rSocketRequester;
            }
        }
        return null;
    }

    public CustomResponseDto fireAndForget(String api, Connection connection, SocketLoginDto socketLoginDto, String owner) {
        if (api.equalsIgnoreCase("deleteRunningUser")) {
            if (!connection.getPW().equals(socketLoginDto.getPassword())) {
                return CustomResponseDto.builder().message("password {" + socketLoginDto.getPassword() + "} is not matched!").build();
            }
            if (!connection.getID().equals(socketLoginDto.getUserID())) {
                return CustomResponseDto.builder().message("userid {" + owner + "} is not matched!").build();
            }

            RSocketRequester rSocketRequester = getRSocketRequesterForUser(owner);
            if (rSocketRequester == null) {
                return CustomResponseDto.builder().message("userid {" + owner + "} is not connected!").build();
            }

            createCommonRSocketRequester("deleteRunningUser",
                    connection.getHost(), connection.getID(), connection.getPW(), Integer.parseInt(connection.getPort()), owner);

            return CustomResponseDto.builder().message("").Success(true).build();
        }

        return CustomResponseDto.ok();
    }
}

