package co.kr.coresolutions.service;

import co.kr.coresolutions.model.StoryLayout;
import com.fasterxml.jackson.databind.ObjectMapper;
import com.google.gson.JsonArray;
import com.google.gson.JsonElement;
import com.google.gson.JsonParser;
import lombok.RequiredArgsConstructor;
import org.json.JSONArray;
import org.json.JSONObject;
import org.springframework.stereotype.Service;

import javax.annotation.PostConstruct;
import java.io.File;
import java.io.FileReader;
import java.io.IOException;
import java.nio.charset.StandardCharsets;
import java.nio.file.Files;
import java.nio.file.Path;
import java.nio.file.Paths;
import java.nio.file.StandardOpenOption;
import java.util.Arrays;
import java.util.stream.Collectors;
import java.util.stream.Stream;

@Service
@RequiredArgsConstructor
public class QueryServiceStoryLayout {
    private boolean indexExists = false;
    private final Constants constants;
    private final ObjectMapper objectMapper;
    private String _storylayoutQueryDir;
    private boolean ownerExists = false;
    private boolean fail = false;

    @PostConstruct
    public void init() {
        _storylayoutQueryDir = constants.getStoryLayoutQueryDir();
    }


    public String saveStoryLayout(String owner, String storylayoutid, StoryLayout storyLayout) throws IOException {
        indexExists = false;
        ownerExists = false;
        if (!Files.isDirectory(Paths.get(_storylayoutQueryDir)))
            Files.createDirectory(Paths.get(_storylayoutQueryDir));

        fail = false;
        try (Stream<Path> paths = Files.walk(Paths.get(_storylayoutQueryDir))) {
            paths
                    .filter(Files::isDirectory)
                    .forEach(path -> {
                        try (Stream<Path> path1 = Files.walk(Paths.get(_storylayoutQueryDir + File.separator + owner))) {
                            ownerExists = true;
                            path1
                                    .filter(Files::isRegularFile)
                                    .forEach(path2 -> {
                                        if (path2.getFileName().toString().equalsIgnoreCase("index.txt")) {
                                            indexExists = true;
                                        }
                                    });
                        } catch (IOException e1) {
                        }

                    });

        } catch (Exception e) {
        }


        if (fail) return "fail";
        Path ownerPath = Paths.get(_storylayoutQueryDir + File.separator + owner);

        if (!ownerExists) {
            ownerExists = true;
            if (!Files.isDirectory(Paths.get(_storylayoutQueryDir + File.separator + owner)))
                ownerPath = Files.createDirectory(ownerPath);
        }


        if (ownerExists) {
            JSONArray jsonArray = null;
            byte[] STORYlAYOUT = objectMapper.writeValueAsBytes(storyLayout);
            JSONObject jsonObject1 = new JSONObject(new String(STORYlAYOUT));
            JSONObject StoryLAYOUTWithoutJsonField = new JSONObject(jsonObject1, Arrays.asList(JSONObject.getNames(jsonObject1)).stream().filter(s ->
                    !s.equals("storylayoutjson")
            ).collect(Collectors.toList()).toArray(new String[JSONObject.getNames(jsonObject1).length]));

            Files.write(Paths.get(ownerPath + File.separator + storylayoutid + ".txt"), STORYlAYOUT,
                    StandardOpenOption.CREATE, StandardOpenOption.TRUNCATE_EXISTING);


            if (!indexExists) {
                jsonArray = new JSONArray();
                jsonArray.put(StoryLAYOUTWithoutJsonField);
                Files.write(Paths.get(ownerPath + File.separator + "index.txt"),
                        jsonArray.toString().getBytes(StandardCharsets.UTF_8), StandardOpenOption.CREATE);
            } else {
                JsonParser parser = new JsonParser();
                JsonElement jsonElement = parser.parse(new FileReader(String.valueOf(Paths.get(ownerPath + File.separator + "index.txt"))));
                JsonArray _jsonArraytemp = jsonElement.getAsJsonArray();
                JSONArray returnArray = new JSONArray();

                _jsonArraytemp.iterator().forEachRemaining(e -> {
                    JSONObject _temp = new JSONObject(e.toString());
                    if (_temp.has("storylayoutID")) {
                        if (!_temp.get("storylayoutID").toString().equalsIgnoreCase(storylayoutid) && storyLayout.getStorylayoutID().equalsIgnoreCase(storylayoutid))
                            returnArray.put(_temp);
                    }
                });

                returnArray.put(StoryLAYOUTWithoutJsonField);
                Files.write(Paths.get(ownerPath + File.separator + "index.txt"), returnArray.toString().getBytes(StandardCharsets.UTF_8),
                        StandardOpenOption.CREATE, StandardOpenOption.TRUNCATE_EXISTING);

            }
        }
        return "success";
    }

    public String getStoryLayouts(String owner) throws IOException {
        if (!Files.isDirectory(Paths.get(_storylayoutQueryDir)))
            Files.createDirectory(Paths.get(_storylayoutQueryDir));

        try {
            return new String(Files.readAllBytes(Paths.get(_storylayoutQueryDir + File.separator + owner + File.separator + "index.txt")), StandardCharsets.UTF_8);
        } catch (IOException e1) {
            return "fail";
        }
    }

    public String getStoryLayout(String owner, String layoutqueryid) throws IOException {
        if (!Files.isDirectory(Paths.get(_storylayoutQueryDir)))
            Files.createDirectory(Paths.get(_storylayoutQueryDir));

        if (!Files.isDirectory(Paths.get(_storylayoutQueryDir + File.separator + owner))) {
            return "failowner";
        } else {
            try {
                return new String(Files.readAllBytes(Paths.get(_storylayoutQueryDir + File.separator + owner + File.separator + layoutqueryid + ".txt")), StandardCharsets.UTF_8);
            } catch (IOException e) {
                return "failstorylayoutid";
            }
        }
    }

    public boolean deleteStoryLayout(String owner, String storylayoutid) throws IOException {
        if (!Files.isDirectory(Paths.get(_storylayoutQueryDir)))
            Files.createDirectory(Paths.get(_storylayoutQueryDir));

        if (!Files.isDirectory(Paths.get(_storylayoutQueryDir + File.separator + owner))) {
            return false;
        } else {
            try {
                JsonParser parser = new JsonParser();
                JsonElement jsonElement = parser.parse(new FileReader(String.valueOf(Paths.get(_storylayoutQueryDir + File.separator + owner + File.separator + "index.txt"))));
                JsonArray _jsonArraytemp = jsonElement.getAsJsonArray();

                JSONArray returnArray = new JSONArray();
                _jsonArraytemp.iterator().forEachRemaining(e -> {
                    JSONObject _temp = new JSONObject(e.toString());
                    if (!_temp.get("storylayoutID").toString().equalsIgnoreCase(storylayoutid))
                        returnArray.put(_temp);
                });

                try {
                    Files.write(Paths.get(_storylayoutQueryDir + File.separator + owner + File.separator + "index.txt"), returnArray.toString().getBytes(StandardCharsets.UTF_8),
                            StandardOpenOption.CREATE, StandardOpenOption.TRUNCATE_EXISTING);
                } catch (IOException e1) {
                }
                return Files.deleteIfExists(Paths.get(_storylayoutQueryDir + File.separator + owner + File.separator + storylayoutid + ".txt"));
            } catch (IOException e) {
                return false;
            }
        }
    }

}
