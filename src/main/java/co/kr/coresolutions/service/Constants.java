package co.kr.coresolutions.service;

import com.fasterxml.jackson.databind.JsonNode;
import com.fasterxml.jackson.databind.ObjectMapper;
import com.ibatis.common.resources.Resources;
import lombok.RequiredArgsConstructor;
import org.springframework.stereotype.Component;

import java.io.File;
import java.io.FileInputStream;
import java.io.InputStream;
import java.io.Reader;
import java.text.SimpleDateFormat;
import java.util.Properties;

@Component
@RequiredArgsConstructor
public class Constants {
    public static String commandDirName = "command";
    // public static final String quadMaxConnectionID = "quadmax";
    public static final String quadMaxConnectionID = "QUADMAX";
    public static SimpleDateFormat simpleDateFormat = new SimpleDateFormat("yyyyMMddHHmmss");
    public String pathListQueues = "/api/queues";
    public String pathListExchanges = "/api/exchanges";
    private final ObjectMapper objectMapper;

    public String getConnectionDir() {
        return getRootDir() + "connection_info" + File.separator;
    }

    public String getJarsDir() {
        return getRootDir() + "jars" + File.separator;
    }

    public String getDBPollingDir() {
        return getRootDir() + "dbpolling" + File.separator;
    }

    public String getReferenceDir() {
        return getRootDir() + "reference" + File.separator;
    }

    public JsonNode getConfigFileAsJson() {
        try(
//            InputStream inputStream = Resources.getResourceAsStream("Directories" + File.separator + "system" + File.separator + "config.txt");
            InputStream inputStream = new FileInputStream(System.getProperty("catalina.base") + File.separator + "webapps" + File.separator + "dataqueryConfig" + File.separator + "config.txt");
        ) {
            return objectMapper.readValue(inputStream, JsonNode.class);
        } catch (Exception e) {
            return null;
        }
    }

    public String getRootDir() {
        try (
            InputStream inputStream = new FileInputStream(System.getProperty("catalina.base") + File.separator + "webapps" + File.separator + "dataqueryConfig" + File.separator + "config.txt");
            //            InputStream inputStream = Resources.getResourceAsStream("Directories" + File.separator + "system" + File.separator + "config.txt");
        ) {
            JsonNode jsonNode = objectMapper.readValue(inputStream, JsonNode.class);
            if (jsonNode.has("DirectoriesDir")) {
                return jsonNode.get("DirectoriesDir").asText().replaceAll("/", File.separator);
            } else {
                throw new NullPointerException("Cannot find property DirectoriesDir in the resource config.txt file");
            }
        } catch (Exception e) {
            throw new NullPointerException("Cannot read correctly file config.txt");
        }
    }

    public String getBusinessQueryDir() {
        return getRootDir() + "business_query" + File.separator;
    }

    public String getLogDir() {
        try (
            InputStream inputStream = new FileInputStream(System.getProperty("catalina.base") + File.separator + "webapps" + File.separator + "dataqueryConfig" + File.separator + "config.txt")
            //            InputStream inputStream = Resources.getResourceAsStream("Directories" + File.separator + "system" + File.separator + "config.txt");
        ) {
            JsonNode jsonNode = objectMapper.readValue(inputStream, JsonNode.class);
            if (jsonNode.has("logDir")) {
                return jsonNode.get("logDir").asText().replaceAll("/", File.separator);
            } else {
                throw new NullPointerException("Cannot find property logDir in the resource config.txt file");
            }
        } catch (Exception e) {
            throw new NullPointerException("Cannot read correctly file config.txt");
        }
    }

    public String getCommandDir() {
        return getRootDir() + commandDirName + File.separator;
    }

    public String getScriptEllaDir() {
        return getRootDir() + "scriptella" + File.separator;
    }

    public String getUnloadDir() {
        return getRootDir() + "unload" + File.separator;
    }

    public String getLayoutQueryDir() {
        return getRootDir() + "querylayout" + File.separator;
    }

    public String getProjectQueryDir() {
        return getRootDir() + "project" + File.separator;
    }

    public String getHtmlLayoutQueryDir() {
        return getRootDir() + "htmllayout" + File.separator;
    }

    public String getContentsLayoutQueryDir() {
        return getRootDir() + "contentslayout" + File.separator;
    }

    public String getStoryLayoutQueryDir() {
        return getRootDir() + "storylayout" + File.separator;
    }

    public String getRfmLayoutQueryDir() {
        return getRootDir() + "rfmlayout" + File.separator;
    }

    public String getFileQueryDir() {
        return getRootDir() + "file" + File.separator;
    }

    public String getRModelQueryDir() {
        return getRootDir() + "model" + File.separator;
    }

    public String getRActivationQueryDir() {
        return getRootDir() + "activated" + File.separator;
    }

    public String getSystemQueryDir() {
        return getRootDir() + "system" + File.separator;
    }

    public String getBatchQueryDir() {
        return getRootDir() + "batchreport" + File.separator;
    }

    public String getSQLiteQueryDir() {
        return getRootDir() + "sqlite" + File.separator;
    }

    boolean getIsCache() {
        String isCache = "";
        String resource = "config" + File.separator + "config.properties";
        Properties properties = new Properties();
        try {
            Reader reader = Resources.getResourceAsReader(resource);
            properties.load(reader);

            isCache = properties.getProperty("IS_CACHE", "false");
        } catch (Exception e) {
        }
        return isCache.equalsIgnoreCase("true") || isCache.equalsIgnoreCase("y");
    }

    public String getDataFlowDir() {
        return getRootDir() + "dataflow" + File.separator;

    }

    public String getSurveyProjectQueryDir() {
        return getRootDir() + "surveyproject" + File.separator;
    }

    public String getSurveyResultQueryDir() {
        return getRootDir() + "surveyresult" + File.separator;
    }

    public String getContainerProjectQueryDir() {
        return getRootDir() + "containerproject" + File.separator;
    }

    public String getMainCommandDir() {
        return getRootDir() + "command" + File.separator;
    }

    public String getMustacheDir() {
        return getRootDir() + "mustache" + File.separator;
    }

    public String getExcelDir() {
        return getRootDir() + "excel" + File.separator;
    }

    public String getLiquidDir() {
        return getRootDir() + "liquid" + File.separator;
    }

    public String getNitriteDir() {
        return getRootDir() + "nitrite" + File.separator;
    }

    public String getGADir() {
        return getRootDir() + "GA" + File.separator;
    }

    public String getFieldMappingDir() {
        return getConnectionDir() + "field_mapping" + File.separator;
    }

    //used only for POST /config api
    public String getConfigDir() {
        return System.getProperty("catalina.base") + File.separator + "webapps" + File.separator + "dataqueryConfig" + File.separator;
//        return getSystemQueryDir();
    }
}
