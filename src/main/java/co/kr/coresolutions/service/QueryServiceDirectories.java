package co.kr.coresolutions.service;

import lombok.RequiredArgsConstructor;
import org.springframework.stereotype.Service;

import javax.annotation.PostConstruct;
import java.io.IOException;
import java.nio.charset.StandardCharsets;
import java.nio.file.Files;
import java.nio.file.Path;
import java.nio.file.Paths;
import java.util.concurrent.atomic.AtomicBoolean;
import java.util.concurrent.atomic.AtomicReference;
import java.util.stream.Stream;

@Service
@RequiredArgsConstructor
public class QueryServiceDirectories {
    private final Constants constants;
    private String _rootDir;

    @PostConstruct
    public void init() {
        _rootDir = constants.getRootDir();
    }

    public String getFile(String directory_name, String filename) {
        AtomicReference<String> contentFile = new AtomicReference<>("");
        AtomicBoolean fileExists = new AtomicBoolean(false);
        try (Stream<Path> paths = Files.walk(Paths.get(_rootDir + directory_name))) {
            paths
                    .filter(Files::isRegularFile)
                    .forEach(path1 -> {
                        if (path1.getFileName().toString().equalsIgnoreCase(filename)) {
                            fileExists.set(true);
                            try {
                                contentFile.set(new String(Files.readAllBytes(path1), StandardCharsets.UTF_8));

                            } catch (IOException e) {
                                e.printStackTrace();
                            }
                        }
                    });
        } catch (IOException e) {
            e.printStackTrace();
            return "fail";
        }

        if (!fileExists.get()) return "fail";
        return contentFile.get();
    }
}
