package co.kr.coresolutions.service;

import co.kr.coresolutions.dao.NativeQuery;
import co.kr.coresolutions.model.SqliteFileRepo;
import co.kr.coresolutions.model.TableLoad;
import co.kr.coresolutions.util.Util;
import lombok.RequiredArgsConstructor;
import org.json.JSONArray;
import org.springframework.stereotype.Service;

import javax.annotation.PostConstruct;

import com.fasterxml.jackson.databind.ObjectMapper;

import java.io.BufferedReader;
import java.io.File;
import java.io.IOException;
import java.nio.charset.StandardCharsets;
import java.nio.file.Files;
import java.nio.file.Path;
import java.nio.file.Paths;
import java.nio.file.StandardOpenOption;
import java.nio.file.attribute.BasicFileAttributes;
import java.sql.SQLException;
import java.text.SimpleDateFormat;
import java.time.ZoneId;
import java.util.Arrays;
import java.util.Date;
import java.util.UUID;
import java.util.stream.Stream;

@Service
@RequiredArgsConstructor
public class QueryServiceSQLite {
    public static String DBFILE = "mydbfile.db";
    public static final String fileRepo = "mydbfile.txt";
    private final NativeQuery nativeQuery;
    private final RunCommands runCommands;
    private final ObjectMapper objectMapper;
    public static String SQLITE_USERID = "mss";
    private final Constants constants;
    public static final SimpleDateFormat SIMPLE_DATE_FORMAT = new SimpleDateFormat("yyyyMMddHHmmss");
    private String _SQLiteQueryDir;
    private String _fileQueryDir;
    private String _unloadQueryDir;

    @PostConstruct
    public void init() {
        _SQLiteQueryDir = constants.getSQLiteQueryDir();
        _fileQueryDir = constants.getFileQueryDir();
        _unloadQueryDir = constants.getUnloadDir();
    }

    public boolean isDBFileNameExists(String userID, String dbFileName) {
        return Paths.get(_SQLiteQueryDir + userID + File.separator + dbFileName + ".db").toFile().exists();
    }

    public String createDBFile(String connectionId, String userID, String dbFileName) {
        try {
            Files.createDirectories(Paths.get(_SQLiteQueryDir + userID));
            nativeQuery
                    .createSQLiteDB(connectionId, "jdbc:sqlite:" + File.separator + _SQLiteQueryDir, userID + File.separator, dbFileName + ".db");
        } catch (IOException | SQLException | ClassNotFoundException e) {
            e.printStackTrace();
            return "error : {" + e.getMessage() + "}";
        }
        return "success";
    }

    public boolean deleteDbFile(String userid, String dbFileName) {
        return Paths.get(_SQLiteQueryDir + userid + File.separator + dbFileName + ".db").toFile().delete();
    }

    public boolean deleteTableFromDBfile(String connectionId, String userID, String dbFileName, String tableName) {
        try {
            Files.createDirectories(Paths.get(_SQLiteQueryDir));
            if (Paths.get(_SQLiteQueryDir + userID + File.separator + dbFileName + ".db").toFile().exists()) {
                nativeQuery
                        .deleteTableSQLite(connectionId, "jdbc:sqlite:" + File.separator + _SQLiteQueryDir, userID + File.separator + dbFileName + ".db", tableName);
                return true;
            }
        } catch (IOException | SQLException | ClassNotFoundException e) {
            e.printStackTrace();
            return false;
        }
        return false;
    }

    public String getDBFile(String userID, String dbFileName) {
        return "jdbc:sqlite:" + _SQLiteQueryDir + userID + File.separator + dbFileName + ".db";
    }

    public String getDBFiles(String userid) {
        if (Paths.get(_SQLiteQueryDir + userid).toFile().exists()) {
            JSONArray myArray = new JSONArray();
            try (Stream<Path> paths = Files.walk(Paths.get(_SQLiteQueryDir + userid))) {
                paths
                        .filter(Files::isRegularFile)
                        .forEach(path1 -> {
                            if (path1.getFileName().toString().endsWith(".db")) {
                                org.json.JSONObject _temp = new org.json.JSONObject();
                                _temp.put("db_file_name", path1.getFileName().toString());
                                try {
                                    _temp.put("db_file_size", Files.size(path1));
                                    _temp.put("created_date", Files.readAttributes(path1, BasicFileAttributes.class).creationTime()
                                            .toInstant()
                                            .atZone(ZoneId.systemDefault())
                                            .toLocalDateTime());
                                    _temp.put("modified_date", Files.readAttributes(path1, BasicFileAttributes.class).lastModifiedTime()
                                            .toInstant()
                                            .atZone(ZoneId.systemDefault())
                                            .toLocalDateTime());
                                } catch (IOException e) {
                                }
                                myArray.put(_temp);
                            }
                        });
            } catch (IOException e) {
                return "error :{" + e.getMessage() + "}";
            }
            return myArray.toString();

        }
        return "error :{" + userid + " directory doesn't exist}";
    }

    public String isFileNameExists(String userID, String input_directory, String file_name) {
        try {
            if (!(Arrays.asList(new String[]{"unload", "file"}).contains(input_directory))) {
                return "error : {input_directory " + input_directory + " not valid}";
            }

            if (input_directory.equals("file")) {
                if (!Paths.get(_fileQueryDir + userID + File.separator + file_name).toFile().exists()) {
                    return "error : {file_name " + file_name + " not valid}";
                } else
                    return new String(Files.readAllBytes(Paths.get(_fileQueryDir + userID + File.separator + file_name)), StandardCharsets.UTF_8);
            } else {
                if (!Paths.get(_unloadQueryDir + userID + File.separator + file_name).toFile().exists()) {
                    return "error : {file_name " + file_name + " not valid}";
                } else
                    return new String(Files.readAllBytes(Paths.get(_unloadQueryDir + userID + File.separator + file_name)), StandardCharsets.UTF_8);
            }

        } catch (IOException e) {
            return "error : { " + e.getMessage() + "}";
        }

    }

    public String tableLoad(TableLoad tableLoad) {
        String metaFile = Util.replaceAll(tableLoad.getFile_name(), ".csv", "_meta.txt");
        String unloadDir = Util.replaceAll(_unloadQueryDir, "/", "//");
        unloadDir = Util.replaceAll(unloadDir, "\\", "\\\\");
        String fileDir = Util.replaceAll(_fileQueryDir, "/", "//");
        fileDir = Util.replaceAll(fileDir, "\\", "\\\\");
        String commandSqlite;
        String tableMeta = "";
        String delMeta = "";
        Path pathMeta = null;
        if (tableLoad.getInput_directory().equalsIgnoreCase("file")
                && Files.exists(Paths.get(_fileQueryDir + tableLoad.getUserid() + File.separator + metaFile))) {
            pathMeta = Paths.get(_fileQueryDir + tableLoad.getUserid() + File.separator + metaFile);
        } else if (tableLoad.getInput_directory().equalsIgnoreCase("unload")
                && Files.exists(Paths.get(_unloadQueryDir + tableLoad.getUserid() + File.separator + metaFile))) {
            pathMeta = Paths.get(_unloadQueryDir + tableLoad.getUserid() + File.separator + metaFile);
        }

        if (pathMeta != null) {
            tableMeta = " \"CREATE TABLE " + tableLoad.getTable_name() + "(";
            delMeta = " \"DELETE FROM " + tableLoad.getTable_name() + " WHERE ";
            try (BufferedReader reader = Files.newBufferedReader(pathMeta, StandardCharsets.UTF_8)) {
                String lineStr;
                String fieldStr = "";
                String delStr = "";
                int cnt = 0;
                while ((lineStr = reader.readLine()) != null) {
                    String[] fieldInfo = lineStr.split("\r\t");
                    if (cnt != 0) {
                        if (cnt != 1) {
                            fieldStr += ", ";
                            delStr += " and ";
                        }
                        for (int i = 0; i < fieldInfo.length; ++i) {
                            String[] fields = fieldInfo[i].split(",");
                            if (fields.length <= 1) {
                                fields = fieldInfo[i].split("\t");
                            }
                            if (i != 0) {
                                fieldStr += ", ";
                                delStr += " and ";
                            }
                            if (fields[1].toLowerCase().contains("char")
                                    || fields[1].toLowerCase().contains("text")
                                    || fields[1].toLowerCase().contains("clob")) {
                                fieldStr += "'" + fields[0] + "' TEXT";
                            } else if (fields[1].toLowerCase().contains("blob")) {
                                fieldStr += "'" + fields[0] + "' BLOB";
                            } else if (fields[1].toLowerCase().contains("REAL")
                                    || fields[1].toLowerCase().contains("DOUBLE")
                                    || fields[1].toLowerCase().contains("float")) {
                                fieldStr += "'" + fields[0] + "' REAL";
                            } else if (fields[1].toLowerCase().contains("NUMERIC")
                                    || fields[1].toLowerCase().contains("DECIMAL")
                                    || fields[1].toLowerCase().contains("BOOLEAN")
                                    || fields[1].toLowerCase().contains("DATE")
                                    || fields[1].toLowerCase().contains("DATETIME")) {
                                fieldStr += "'" + fields[0] + "' NUMERIC";
                            } else {
                                fieldStr += "'" + fields[0] + "' INTEGER";
                            }
                            delStr += fields[0] + " = '" + fields[0] + "'";
                        }
                    }
                    cnt++;
                }
                tableMeta += fieldStr + ");\"";
                delMeta += delStr + "\"";
            } catch (IOException e) {
                return "Error : " + e.getMessage();
            }

        }

        tableMeta = Util.replaceAll(tableMeta, "'", "");
        String tempFileContent = "drop table if exists " + tableLoad.getTable_name() + ";" +
                "\n" + tableMeta.replace("\"", "") + ";" +
                "\n.mode csv\n" +
                ".import " +
                (tableLoad.getInput_directory().equals("unload") ? unloadDir : fileDir) + (tableLoad.getUserid() + File.separator + tableLoad.getFile_name() +
                " " + tableLoad.getTable_name() + "\n"
                + delMeta.replace("\"", "") + ";");
        Path path;
        try {
            path = Files.write(Paths.get(_fileQueryDir + tableLoad.getTable_name() + "_" + UUID.randomUUID().toString() + "_" +
                            SIMPLE_DATE_FORMAT.format(new Date()) + ".txt"),
                    tempFileContent.getBytes(), StandardOpenOption.CREATE_NEW, StandardOpenOption.TRUNCATE_EXISTING);
            if (RunCommands.isUnix()) {
                commandSqlite = "sqlite3 " + (_SQLiteQueryDir + tableLoad.getUserid() + File.separator + tableLoad.getDb_file().concat(".db")) +
                        " '.read " + path.toFile().getAbsolutePath() + "'";
            } else {
                commandSqlite = _SQLiteQueryDir + "sqlite3.exe " + (_SQLiteQueryDir + tableLoad.getUserid() + File.separator + tableLoad.getDb_file().concat(".db")) +
                        " '.read " + path.toFile().getAbsolutePath() + "'";
            }
        } catch (IOException e) {
            return "Error: " + e.getMessage();
        }

        String resultExecution = runCommands.runStandaloneCommand(commandSqlite);
        try {
            Files.deleteIfExists(path);
        } catch (IOException e) {
            return "Error: " + e.getMessage();
        }
        return resultExecution;
    }

    public String getDBFileRepo(String userID) {
        try {
            Path path = Paths.get(_SQLiteQueryDir + userID + File.separator + fileRepo);
            if (!path.toFile().exists()) {
                return fileRepo + " doesn't exist!";
            }

            final String[] dbFileRepo = {""};
            Stream
                    .of(objectMapper.readValue(new String(Files.readAllBytes(path)), SqliteFileRepo[].class))
                    .filter(SqliteFileRepo::isActive)
                    .findAny()
                    .ifPresent(sqliteFileRepo -> dbFileRepo[0] = sqliteFileRepo.getDbFile());
            if (dbFileRepo[0].isEmpty()) {
                return "There is no active dbfile defined!";
            } else {
                return dbFileRepo[0];
            }
        } catch (IOException e) {
            return "error : {" + e.getMessage() + "}";
        }
    }


}