package co.kr.coresolutions.service;

import org.hibernate.HibernateException;
import org.hibernate.SessionFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.scheduling.annotation.Async;
import org.springframework.stereotype.Service;

import java.sql.SQLException;
import java.sql.Statement;

@Service("asyncTaskService")
public class AsyncTaskService {

    @Autowired
    QueryService queryService;

    private String errMsg = null;

    public String getErrMsg() {
        return errMsg;
    }

    private void setErrMsg(String val) {
        this.errMsg = val;
    }

    @Async("executor")
    public void setTimeout(SessionFactory sessionFactory, String queryid, int timeoutSec) {
//        System.out.println("==============>>>>>>>>>>>> THREAD START [" + queryid + "]");
        boolean isExcute = true;
        Thread.currentThread().setName(queryid);
        long startTimeSec = (System.currentTimeMillis()) / 1000;
        setErrMsg(null);

        while (isExcute) {
            try {
                Thread.sleep(1000);

                if (queryService.isRunStop(queryid)) {
                    break;
                }
                if ((System.currentTimeMillis()) / 1000 - startTimeSec > timeoutSec) {
                    queryService.setRunStop(queryid);
                    if (sessionFactory != null && sessionFactory.isOpen()) {
                        sessionFactory.getCurrentSession().close();
                    }

//                    System.out.println(queryid + " / timeout !!!");
                    this.setErrMsg(queryid + " / query timeout!!!");
                    isExcute = false;
                    QueryService.sessionMap.remove(queryid);
                    return;
                }
                QueryService.sessionMap.get(queryid).put("runningTime", ((System.currentTimeMillis()) / 1000 - startTimeSec));
//                System.out.println(queryid + " / running time : " + ((System.currentTimeMillis()) / 1000 - startTimeSec));
            } catch (InterruptedException e) {
                // TODO Auto-generated catch block
                e.printStackTrace();
            } catch (HibernateException e1) {
                e1.printStackTrace();
            }
        }

        QueryService.sessionMap.remove(queryid);
//        System.out.println("==============>>>>>>>>>>>> THREAD END [" + queryid + "]");
    }

    @Async("executor")
    public void setTimeoutJDBC(Statement st, String queryid, int timeoutSec) throws SQLException {
//        System.out.println("==============>>>>>>>>>>>> JDBC THREAD START [" + queryid + "]");
        boolean isExcute = true;
        Thread.currentThread().setName(queryid);
        long startTimeSec = (System.currentTimeMillis()) / 1000;
        setErrMsg(null);
        while (isExcute) {
            try {
                Thread.sleep(1000);
                if (queryService.isRunStop(queryid)) {
                    break;
                }
                if ((System.currentTimeMillis()) / 1000 - startTimeSec > timeoutSec) {
                    queryService.setRunStop(queryid);
                    if (st != null) {
                        if (!st.isClosed()) {
                            st.cancel();
                        }
                    }

//                    System.out.println(queryid + " / timeout !!!");
                    this.setErrMsg(queryid + " / query timeout!!!");
                    isExcute = false;
                    QueryService.sessionMap.remove(queryid);
                    return;
                }
                QueryService.sessionMap.get(queryid).put("runningTime", ((System.currentTimeMillis()) / 1000 - startTimeSec));
//                System.out.println(queryid + " / running time : " + ((System.currentTimeMillis()) / 1000 - startTimeSec));
            } catch (InterruptedException e) {
                // TODO Auto-generated catch block
                e.printStackTrace();
            } catch (HibernateException e1) {
                e1.printStackTrace();
            }
        }

        //System.out.println("[" + queryid + "]remove before : " + queryService.sessionMap.keySet().toString());
        QueryService.sessionMap.remove(queryid);

//        System.out.println("==============>>>>>>>>>>>> THREAD END [" + queryid + "]");
        //System.out.println("[" + queryid + "]remove after : " + queryService.sessionMap.keySet().toString());
    }

    @Async("executor")
    public void stopQuery(SessionFactory sessionFactory, String queryid) {
//        System.out.println("==============>>>>>>>>>>>> Stop Query Thread [" + queryid + "]");
        if (sessionFactory != null && sessionFactory.isOpen()) {
            sessionFactory.close();
        }
        queryService.setRunStop(queryid);
//        System.out.println(queryid + " / stop Query !!!");
        this.setErrMsg(queryid + " / stop Query !!!");
//        System.out.println("==============>>>>>>>>>>>> Stop Query THREAD Success [" + queryid + "]");
    }

    @Async("executor")
    public void stopQueryJDBC(Statement stmt, String queryid) {
//        System.out.println("==============>>>>>>>>>>>> Stop Query JDBC Thread [" + queryid + "]");
        try {
            if (stmt != null && !stmt.isClosed()) {
                stmt.cancel();
                stmt.close();
            }
        } catch (SQLException e) {
            // TODO Auto-generated catch block
        }

        queryService.setRunStop(queryid);
//        System.out.println(queryid + " / stop Query !!!");
        this.setErrMsg(queryid + " / stop Query !!!");
//        System.out.println("==============>>>>>>>>>>>> Stop Query THREAD Success [" + queryid + "]");

    }
}
