package co.kr.coresolutions.service;

import co.kr.coresolutions.commons.CustomResponseDto;
import co.kr.coresolutions.model.DateConverter;
import co.kr.coresolutions.model.dtos.DelayDto;
import org.json.JSONObject;
import org.springframework.stereotype.Service;

import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.time.LocalDate;
import java.time.LocalDateTime;
import java.time.LocalTime;
import java.time.ZoneId;
import java.time.format.DateTimeFormatter;
import java.util.Date;

@Service
public class QueryServiceDate {
    public String generateCronExpression(Date date) throws ParseException {
        SimpleDateFormat dateFormat = new SimpleDateFormat("yyyyMMddHHmmss");
        Date simpleDateFormat = new SimpleDateFormat("yyyyMMddHHmmss").parse(dateFormat.format(date));
        DateConverter calHelper = new DateConverter(simpleDateFormat);
        String cron = calHelper.getSECONDS() + " " + calHelper.getMinutes() + " " + calHelper.getHours() + " " + calHelper.getDaysOfMonth() + " " + calHelper.getMonths() + " " +
                calHelper.getDAYSOFWEEK() + " " + calHelper.getYears();
        return cron;
    }

    public CustomResponseDto convertDays(DelayDto delayDto) {
        int days;
        int minutes;
        JSONObject jsonObject = new JSONObject();
        try {
            days = Integer.parseInt(delayDto.getDelayDay());
            minutes = Integer.parseInt(delayDto.getDelayTime());
            Date date = Date.from(LocalDateTime.now().plusDays(days).plusMinutes(minutes).atZone(ZoneId.systemDefault()).toInstant());
            SimpleDateFormat formatDate = new SimpleDateFormat("yyyyMMdd");
            SimpleDateFormat formatTime = new SimpleDateFormat("HHmmss");
            jsonObject.put("date", formatDate.format(date));
            jsonObject.put("time", formatTime.format(date));
            jsonObject.put("schedule", generateCronExpression(date));
            return CustomResponseDto.builder().Success(true).DetailedMessage(jsonObject.toString()).build();
        } catch (Exception e) {
            return CustomResponseDto.builder().ErrorCode(1).ErrorMessage(e.getMessage()).build();
        }
    }

    public CustomResponseDto convertFixTime(DelayDto delayDto) {
        int days;
        JSONObject jsonObject = new JSONObject();
        try {
            days = Integer.parseInt(delayDto.getDelayDay());
            LocalDateTime localDateTime = LocalDateTime.of(LocalDate.now(), LocalTime.parse(delayDto.getDelayTime(), DateTimeFormatter.ofPattern("HHmmss")));
            Date date = Date.from(localDateTime.plusDays(days).atZone(ZoneId.systemDefault()).toInstant());
            SimpleDateFormat formatDate = new SimpleDateFormat("yyyyMMdd");
            jsonObject.put("date", formatDate.format(date));
            jsonObject.put("time", delayDto.getDelayTime());
            jsonObject.put("schedule", generateCronExpression(date));
            return CustomResponseDto.builder().Success(true).DetailedMessage(jsonObject.toString()).build();
        } catch (Exception e) {
            return CustomResponseDto.builder().ErrorCode(1).ErrorMessage(e.getMessage()).build();
        }
    }

    public CustomResponseDto convertFixDateAndTime(DelayDto delayDto) {
        JSONObject jsonObject = new JSONObject();
        try {
            String delayTime = delayDto.getDelayTime();
            if (delayTime.length() == 5) {
                delayTime = "0".concat(delayTime);
            }
            LocalDateTime localDateTime = LocalDateTime.of(LocalDate.parse(delayDto.getDelayDay(), DateTimeFormatter.ofPattern("yyyyMMdd")),
                    LocalTime.parse(delayTime, DateTimeFormatter.ofPattern("HHmmss")));
            Date date = Date.from(localDateTime.atZone(ZoneId.systemDefault()).toInstant());
            jsonObject.put("date", delayDto.getDelayDay());
            jsonObject.put("time", delayTime);
            jsonObject.put("schedule", generateCronExpression(date));
            return CustomResponseDto.builder().Success(true).DetailedMessage(jsonObject.toString()).build();
        } catch (Exception e) {
            return CustomResponseDto.builder().ErrorCode(1).ErrorMessage(e.getMessage()).build();
        }
    }
}
