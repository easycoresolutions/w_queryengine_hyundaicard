package co.kr.coresolutions.service;

import co.kr.coresolutions.commons.CustomResponseDto;
import co.kr.coresolutions.model.dtos.FileSystemDto;
import lombok.RequiredArgsConstructor;
import lombok.SneakyThrows;
import org.json.JSONArray;
import org.json.JSONObject;
import org.springframework.stereotype.Service;

import java.io.File;
import java.io.IOException;
import java.nio.charset.StandardCharsets;
import java.nio.file.Files;
import java.nio.file.Path;
import java.nio.file.Paths;
import java.nio.file.StandardOpenOption;
import java.nio.file.attribute.BasicFileAttributes;
import java.time.ZoneId;
import java.util.stream.Stream;

@Service
@RequiredArgsConstructor
public class QueryServiceFileSystem {
    private final QueryService queryService;
    private final Constants constants;

    public CustomResponseDto getAllFilesAndDirectories(String directory) {
        JSONArray jsonArray = new JSONArray();
        try (Stream<Path> paths = Files.walk(Paths.get(directory.equalsIgnoreCase(".") ? constants.getRootDir() : constants.getRootDir() + directory), 1)) {
            paths
                    .forEach(path -> {
                        JSONObject jsonObject = new JSONObject();
                        jsonObject.put("name", path.getFileName().toString());
                        try {
                            jsonObject.put("created", Files.readAttributes(path, BasicFileAttributes.class).creationTime()
                                    .toInstant()
                                    .atZone(ZoneId.systemDefault())
                                    .toLocalDateTime());
                        } catch (IOException e) {
                        }
                        if (path.toFile().isDirectory()) {
                            jsonObject.put("type", "D");

                        } else {
                            jsonObject.put("type", "F");
                        }
                        jsonArray.put(jsonObject);
                    });
        } catch (Exception e) {
            queryService.addLogging("POST", "filesystem/files directory is not found. - " + directory + "\n");
            return CustomResponseDto.builder().ErrorCode(110).ErrorMessage("directory is not found. - " + directory).build();
        }
        queryService.addLogging("POST", "filesystem/files success\n");
        return CustomResponseDto.builder().DetailedMessage(jsonArray.toString()).build();
    }

    public CustomResponseDto getContentFile(String directory, String filename) {
        String targetDir = directory.equalsIgnoreCase(".") ? constants.getRootDir() : constants.getRootDir() + directory + File.separator;
        if (!Paths.get(targetDir).toFile().exists()) {
            queryService.addLogging("POST", "filesystem/contents directory is not found. - " + directory + "\n");
            return CustomResponseDto.builder().ErrorCode(110).ErrorMessage("directory is not found. - " + directory).build();
        }
        try {
            return CustomResponseDto.builder().DetailedMessage(new String(Files.readAllBytes(Paths.get(targetDir + filename)))).build();
        } catch (IOException e) {
            return CustomResponseDto.builder().ErrorCode(111).ErrorMessage("File name not found - " + filename).build();
        }
    }

    @SneakyThrows
    public CustomResponseDto write(FileSystemDto fileSystemDto) {
        String targetDir = fileSystemDto.getDirectory().equalsIgnoreCase(".") ? constants.getRootDir() : constants.getRootDir() + fileSystemDto.getDirectory() + File.separator;
        if (!Paths.get(targetDir).toFile().isDirectory()) {
            queryService.addLogging("POST", "filesystem/write directory is not found. - " + fileSystemDto.getDirectory() + "\n");
            return CustomResponseDto.builder().ErrorCode(110).ErrorMessage("directory is not found. - " + fileSystemDto.getDirectory()).build();
        }

        if (Paths.get(targetDir + fileSystemDto.getFilename()).toFile().exists()) {
            if (fileSystemDto.isOverwrite()) {
                queryService.addLogging("POST", "filesystem/write overwrite existing file - " + fileSystemDto.getDirectory() + "/" + fileSystemDto.getFilename() + "\n");
                Files.write(Paths.get(targetDir + fileSystemDto.getFilename()), fileSystemDto.getContents().getBytes(StandardCharsets.UTF_8),
                        StandardOpenOption.CREATE, StandardOpenOption.TRUNCATE_EXISTING);
                return CustomResponseDto.ok();
            } else {
                queryService.addLogging("POST", "File already exist  - " + fileSystemDto.getDirectory() + "/" + fileSystemDto.getFilename() + "\n");
                return CustomResponseDto.builder().ErrorCode(112).ErrorMessage("File already exist  - " + fileSystemDto.getDirectory() + "/" + fileSystemDto.getFilename()).build();
            }
        } else {
            Files.write(Paths.get(targetDir + fileSystemDto.getFilename()), fileSystemDto.getContents().getBytes(StandardCharsets.UTF_8),
                    StandardOpenOption.CREATE);
            return CustomResponseDto.ok();
        }
    }

    public CustomResponseDto delete(FileSystemDto fileSystemDto) {
        queryService.addLogging("DELETE", "filesystem/write triggered");
        String targetDir = fileSystemDto.getDirectory().equalsIgnoreCase(".") ? constants.getRootDir() : constants.getRootDir() + fileSystemDto.getDirectory() + File.separator;
        try {
            return CustomResponseDto.builder().Success(Files.deleteIfExists(Paths.get(targetDir + fileSystemDto.getFilename()))).build();
        } catch (IOException e) {
            queryService.addLogging("DELETE", "filesystem/write error\t" + e.getMessage());
            return CustomResponseDto.builder().ErrorCode(110).ErrorMessage("Error delete file\t" + e.getMessage()).build();
        }
    }
}
