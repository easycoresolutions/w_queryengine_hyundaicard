package co.kr.coresolutions.service;

import co.kr.coresolutions.commons.ResponseDto;
import co.kr.coresolutions.enums.Extension;
import lombok.RequiredArgsConstructor;
import lombok.SneakyThrows;
import org.springframework.stereotype.Component;

import javax.annotation.PostConstruct;
import java.io.File;
import java.nio.file.Files;
import java.nio.file.Paths;
import java.nio.file.StandardOpenOption;

@Component
@RequiredArgsConstructor
public class QueryServiceDataFlow {
    private final Constants constants;
    private String dataFlowDir;

    @PostConstruct
    public void init() {
        dataFlowDir = constants.getDataFlowDir();
    }

    @SneakyThrows
    public void log(String userId, String fileName, String logName, String startDate, ResponseDto responseDto) {
        String stringBuilder =
                logName + "\t" + startDate + System.lineSeparator() +
                        logName + "\t" + responseDto.getEndDateExecution() + System.lineSeparator() +
                        logName + "\t" + (responseDto.getErrorCode() == 0 ? responseDto.getSuccessCode() : responseDto.getErrorCode() + " - " + responseDto.getMessage()) + System.lineSeparator();

        Files.write(Paths.get(dataFlowDir + userId + File.separator + fileName + Extension.LOG.getName()), stringBuilder.getBytes(),
                StandardOpenOption.CREATE, StandardOpenOption.APPEND);
    }
}
