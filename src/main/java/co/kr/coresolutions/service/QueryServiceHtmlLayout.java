package co.kr.coresolutions.service;

import co.kr.coresolutions.model.HtmlLayout;
import com.fasterxml.jackson.databind.ObjectMapper;
import com.google.gson.JsonArray;
import com.google.gson.JsonElement;
import com.google.gson.JsonParser;
import lombok.RequiredArgsConstructor;
import org.json.JSONArray;
import org.json.JSONObject;
import org.springframework.stereotype.Service;

import javax.annotation.PostConstruct;
import java.io.File;
import java.io.FileReader;
import java.io.IOException;
import java.nio.charset.StandardCharsets;
import java.nio.file.Files;
import java.nio.file.Path;
import java.nio.file.Paths;
import java.nio.file.StandardOpenOption;
import java.util.Arrays;
import java.util.stream.Collectors;
import java.util.stream.Stream;

@Service
@RequiredArgsConstructor
public class QueryServiceHtmlLayout {
    private boolean indexExists = false;
    private final Constants constants;
    private final ObjectMapper objectMapper;
    private String _htmllayoutQueryDir;
    private boolean ownerExists = false;
    private boolean fail = false;

    @PostConstruct
    public void init() {
        _htmllayoutQueryDir = constants.getHtmlLayoutQueryDir();
    }

    public String saveHtmlLayout(String owner, String htmllayoutid, HtmlLayout htmlLayout) throws IOException {
        indexExists = false;
        ownerExists = false;
        if (!Files.isDirectory(Paths.get(_htmllayoutQueryDir)))
            Files.createDirectory(Paths.get(_htmllayoutQueryDir));

        fail = false;
        try (Stream<Path> paths = Files.walk(Paths.get(_htmllayoutQueryDir))) {
            paths
                    .filter(Files::isDirectory)
                    .forEach(path -> {
                        try (Stream<Path> path1 = Files.walk(Paths.get(_htmllayoutQueryDir + File.separator + owner))) {
                            ownerExists = true;
                            path1
                                    .filter(Files::isRegularFile)
                                    .forEach(path2 -> {
                                        if (path2.getFileName().toString().equalsIgnoreCase("index.txt")) {
                                            indexExists = true;
                                        }
                                    });
                        } catch (IOException e1) {
                            e1.printStackTrace();
                        }

                    });

        } catch (Exception e) {
            e.printStackTrace();
        }


        if (fail) return "fail";
        Path ownerPath = Paths.get(_htmllayoutQueryDir + File.separator + owner);

        if (!ownerExists) {
            ownerExists = true;
            if (!Files.isDirectory(Paths.get(_htmllayoutQueryDir + File.separator + owner)))
                ownerPath = Files.createDirectory(ownerPath);
        }

        //if(!Files.isDirectory(Paths.get(_htmllayoutQueryDir+File.separator+owner+File.separator+"layouts")))
        // Files.createDirectory(Paths.get(_htmllayoutQueryDir+File.separator+owner+File.separator+"layouts"));


        if (ownerExists) {
            JSONArray jsonArray = null;
            byte[] HTMLlAYOUT = objectMapper.writeValueAsBytes(htmlLayout);
            JSONObject jsonObject1 = new JSONObject(new String(HTMLlAYOUT));
            JSONObject HtmlLAYOUTWithoutJsonField = new JSONObject(jsonObject1, Arrays.asList(JSONObject.getNames(jsonObject1)).stream().filter(s ->
                    !s.equals("htmllayoutjson")
            ).collect(Collectors.toList()).toArray(new String[JSONObject.getNames(jsonObject1).length]));

            Files.write(Paths.get(ownerPath + File.separator + htmllayoutid + ".txt"), HTMLlAYOUT,
                    StandardOpenOption.CREATE, StandardOpenOption.TRUNCATE_EXISTING);


            if (!indexExists) {
                jsonArray = new JSONArray();
                jsonArray.put(HtmlLAYOUTWithoutJsonField);
                Files.write(Paths.get(ownerPath + File.separator + "index.txt"),
                        jsonArray.toString().getBytes(StandardCharsets.UTF_8), StandardOpenOption.CREATE);
            } else {
                JsonParser parser = new JsonParser();
                JsonElement jsonElement = parser.parse(new FileReader(String.valueOf(Paths.get(ownerPath + File.separator + "index.txt"))));
                JsonArray _jsonArraytemp = jsonElement.getAsJsonArray();
                JSONArray returnArray = new JSONArray();

                _jsonArraytemp.iterator().forEachRemaining(e -> {
                    org.json.JSONObject _temp = new org.json.JSONObject(e.toString());
                    if (_temp.has("htmllayoutID")) {
                        if (!_temp.get("htmllayoutID").toString().equalsIgnoreCase(htmllayoutid) && htmlLayout.getHtmllayoutID().equalsIgnoreCase(htmllayoutid))
                            returnArray.put(_temp);
                    }
                });

                returnArray.put(HtmlLAYOUTWithoutJsonField);
                Files.write(Paths.get(ownerPath + File.separator + "index.txt"), returnArray.toString().getBytes(StandardCharsets.UTF_8),
                        StandardOpenOption.CREATE, StandardOpenOption.TRUNCATE_EXISTING);

            }

        }

        return "success";
    }

    public String getHtmlLayouts(String owner) throws IOException {
        if (!Files.isDirectory(Paths.get(_htmllayoutQueryDir)))
            Files.createDirectory(Paths.get(_htmllayoutQueryDir));

        try {
            return new String(Files.readAllBytes(Paths.get(_htmllayoutQueryDir + File.separator + owner + File.separator + "index.txt")), StandardCharsets.UTF_8);
        } catch (IOException e1) {
            e1.printStackTrace();
            return "fail";
        }
    }

    public String getHtmlLayout(String owner, String layoutqueryid) throws IOException {
        if (!Files.isDirectory(Paths.get(_htmllayoutQueryDir)))
            Files.createDirectory(Paths.get(_htmllayoutQueryDir));

        if (!Files.isDirectory(Paths.get(_htmllayoutQueryDir + File.separator + owner))) {
            return "failowner";
        } else {
            try {
                return new String(Files.readAllBytes(Paths.get(_htmllayoutQueryDir + File.separator + owner + File.separator + layoutqueryid + ".txt")), StandardCharsets.UTF_8);
            } catch (IOException e) {
                e.printStackTrace();
                return "failhtmllayoutid";
            }
        }
    }

    public boolean deleteHtmlLayout(String owner, String htmllayoutid) throws IOException {
        if (!Files.isDirectory(Paths.get(_htmllayoutQueryDir)))
            Files.createDirectory(Paths.get(_htmllayoutQueryDir));

        if (!Files.isDirectory(Paths.get(_htmllayoutQueryDir + File.separator + owner))) {
            return false;
        } else {
            try {
                JsonParser parser = new JsonParser();
                JsonElement jsonElement = parser.parse(new FileReader(String.valueOf(Paths.get(_htmllayoutQueryDir + File.separator + owner + File.separator + "index.txt"))));
                JsonArray _jsonArraytemp = jsonElement.getAsJsonArray();

                JSONArray returnArray = new JSONArray();
                _jsonArraytemp.iterator().forEachRemaining(e -> {
                    org.json.JSONObject _temp = new org.json.JSONObject(e.toString());
                    if (!_temp.get("htmllayoutID").toString().equalsIgnoreCase(htmllayoutid))
                        returnArray.put(_temp);
                });

                try {
                    Files.write(Paths.get(_htmllayoutQueryDir + File.separator + owner + File.separator + "index.txt"), returnArray.toString().getBytes(StandardCharsets.UTF_8),
                            StandardOpenOption.CREATE, StandardOpenOption.TRUNCATE_EXISTING);
                } catch (IOException e1) {
                    e1.printStackTrace();
                }
                return Files.deleteIfExists(Paths.get(_htmllayoutQueryDir + File.separator + owner + File.separator + htmllayoutid + ".txt"));
            } catch (IOException e) {
                e.printStackTrace();
                return false;
            }
        }
    }

}
