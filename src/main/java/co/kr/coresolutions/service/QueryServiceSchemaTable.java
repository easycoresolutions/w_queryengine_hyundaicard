package co.kr.coresolutions.service;

import co.kr.coresolutions.commons.CustomResponseDto;
import co.kr.coresolutions.model.dtos.SchemaDto;
import com.google.common.collect.Lists;
import com.google.gson.JsonElement;
import com.google.gson.JsonObject;
import com.google.gson.JsonParser;
import lombok.RequiredArgsConstructor;
import org.json.JSONObject;
import org.springframework.stereotype.Service;

import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Date;
import java.util.List;
import java.util.UUID;

@Service
@RequiredArgsConstructor
public class QueryServiceSchemaTable {
    private final QueryService queryService;

    private void populateSchemaVersionAndCreated(String connectionID, String[] schemaVersion, String[] created, String resultSelect) {
        if (!(resultSelect.equalsIgnoreCase(connectionID) || resultSelect.startsWith("Error") || resultSelect.startsWith("error"))) {
            JsonParser parser = new JsonParser();
            JsonElement jsonElement = parser.parse(resultSelect);
            if (jsonElement.isJsonArray() && jsonElement.getAsJsonArray().size() > 0) {
                JsonObject jsonObject = jsonElement.getAsJsonArray().get(0).getAsJsonObject();
                if (jsonObject.has("SCHEMA_VERSION")) {
                    schemaVersion[0] = jsonObject.get("SCHEMA_VERSION").getAsString();
                }
                if (jsonObject.has("CREATED")) {
                    created[0] = jsonObject.get("CREATED").getAsString();
                }
            }
        }
    }

    public CustomResponseDto insertForMain(List<String> listRandomIds, String connectionID, String mainTable, String schemaID, SchemaDto schemaDto) {
        final String[] schemaVersion = {"0"};
        final String[] created = {null};
        String updated = new SimpleDateFormat("yyyyMMdd:HHmmss").format(new Date());
        String resultValidity = queryService
                .checkValidity("SELECT SCHEMA_VERSION, CREATED FROM " + mainTable + " WHERE SCHEMA_ID = '" + schemaID + "'",
                        connectionID, "", "", false);
        if (!(resultValidity.equalsIgnoreCase(connectionID) || resultValidity.startsWith("Error") || resultValidity.startsWith("error"))) {
            populateSchemaVersionAndCreated(connectionID, schemaVersion, created, resultValidity);
        } else {
            return CustomResponseDto.builder().message("fail due error is\t" + resultValidity).build();
        }

        String randomDeleteMain = UUID.randomUUID().toString();
        resultValidity = queryService
                .updateClausesWithRollback(Lists.newArrayList("DELETE FROM " + mainTable + " WHERE SCHEMA_ID = '" + schemaID + "'"), connectionID,
                        randomDeleteMain);
        if (!resultValidity.startsWith("success")) {
            return CustomResponseDto.builder().message("fail due error is\t" + resultValidity).build();
        }
        listRandomIds.add(randomDeleteMain);

        if (created[0] == null) {
            created[0] = updated;
        }

        String randomInsertMain = UUID.randomUUID().toString();
        String resultInsert = queryService
                .updateClausesWithRollback(Lists.newArrayList("INSERT INTO " + mainTable + "(SCHEMA_ID, SCHEMA_NAME, SCHEMA_SEQ, SCHEMA_DESC, " +
                                "SCHEMA_VERSION, SCHEMA_JSON, OWNER, CREATED, UPDATED, SHARED, DEL_F) VALUES ('" + schemaID + "', '" + schemaDto.getSchemaName() + "', "
                                + schemaDto.getSchemaSeq() + ", '" + schemaDto.getSchemaDesc() + "', " + (Integer.parseInt(schemaVersion[0]) + 1) + ", '"
                                + schemaDto.getSchemaJson().toString()
                                .replace("\\", "\\\\")
                                .replace("/", "\\/")
                                .replace("'", "''") + "', '" + schemaDto.getOwner() + "', '" + created[0] + "', '" + updated + "', " +
                                "'" + schemaDto.getShared() + "', '" + schemaDto.getDelF() + "')"), connectionID,
                        randomDeleteMain + "same" + randomInsertMain);
        if (!resultInsert.startsWith("success")) {
            return CustomResponseDto.builder().message("fail due " + resultInsert).build();
        }
        listRandomIds.add(randomInsertMain);
        return CustomResponseDto.ok();
    }

    public CustomResponseDto deleteAndInsertAuxiliaryTable(String tableName, List<String> authIDS, List<String> listRandomIds, String schemaID, String connectionID) {
        CustomResponseDto customResponseDto = CustomResponseDto.builder().build();
        //delete if exist and insert in auxiliary tables
        String randomDeleteAuxiliary = UUID.randomUUID().toString();
        String resultDelete = queryService
                .updateClausesWithRollback(
                        Lists.newArrayList("DELETE FROM " + tableName + " WHERE SCHEMA_ID = '" + schemaID + "'"), connectionID, randomDeleteAuxiliary);
        if (!resultDelete.startsWith("success")) {
            queryService.rollBack(listRandomIds);
            customResponseDto.setMessage(resultDelete);
            customResponseDto.setSuccess(false);
            return customResponseDto;
        }
        listRandomIds.add(randomDeleteAuxiliary);

        String randomInsertAuxiliary = UUID.randomUUID().toString();

        StringBuilder stringBuilderKeys = new StringBuilder();
        StringBuilder stringBuilderValues = new StringBuilder();
        List<String> queries = new ArrayList<>();
        authIDS.forEach(authID -> {
            JSONObject jsonObject = new JSONObject();
            jsonObject.put("SCHEMA_ID", schemaID);
            jsonObject.put("AUTH_ID", authID);
            jsonObject.keySet().forEach(oneKey -> {
                stringBuilderKeys.append(oneKey).append(",");
                stringBuilderValues.append("'").append(jsonObject.get(oneKey).toString().replace("'", "''")).append("',");
            });
            queries.add("insert into " + tableName
                    + "(" + stringBuilderKeys.deleteCharAt(stringBuilderKeys.length() - 1).toString() + ") values ("
                    + stringBuilderValues.deleteCharAt(stringBuilderValues.length() - 1).toString() + ")");
            stringBuilderKeys.delete(0, stringBuilderKeys.length());
            stringBuilderValues.delete(0, stringBuilderValues.length());
        });

        String resultInsert = queryService.updateClausesWithRollback(queries, connectionID, randomDeleteAuxiliary + "same" + randomInsertAuxiliary);
        if (resultInsert.startsWith("success")) {
            listRandomIds.add(randomInsertAuxiliary);
        }
        return CustomResponseDto.builder().message(resultInsert).Success(resultInsert.startsWith("success")).build();
    }
}
