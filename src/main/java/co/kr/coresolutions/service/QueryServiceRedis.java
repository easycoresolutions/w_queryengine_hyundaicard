package co.kr.coresolutions.service;

import co.kr.coresolutions.model.Connection;
import com.fasterxml.jackson.databind.JsonNode;
import com.fasterxml.jackson.databind.node.ObjectNode;
import lombok.RequiredArgsConstructor;
import org.json.JSONArray;
import org.json.JSONObject;
import org.springframework.stereotype.Service;
import redis.clients.jedis.Jedis;
import redis.clients.jedis.JedisShardInfo;
import redis.clients.jedis.Pipeline;

import javax.annotation.PostConstruct;
import java.io.BufferedReader;
import java.io.FileReader;
import java.io.IOException;
import java.nio.charset.StandardCharsets;
import java.nio.file.Files;
import java.nio.file.Paths;
import java.util.HashMap;
import java.util.LinkedList;
import java.util.List;
import java.util.Map;
import java.util.Set;
import java.util.regex.Matcher;
import java.util.regex.Pattern;
import java.util.stream.Collectors;

@Service
@RequiredArgsConstructor
public class QueryServiceRedis {
    private final QueryService queryService;
    private final QueryServiceJSON queryServiceJSON;
    private final Constants constants;
    private String _unloadQueryDir;

    @PostConstruct
    public void init() {
        _unloadQueryDir = constants.getUnloadDir();
    }

    public Jedis connectToRedis(String connectionId) {
        Connection connection = queryService.getInfoConnection(connectionId);
        String redisHost = connection.getHost();
        String redisPort = connection.getPort();
        String password = connection.getPW();
        JedisShardInfo sharedInfo = new JedisShardInfo(redisHost, redisPort);
        sharedInfo.setPassword(password);
        Jedis jedis = new Jedis(sharedInfo);
            jedis.connect();
            if (jedis.isConnected()) {
                return jedis;
            }
        return null;
    }

    public void disconnect(Jedis jedis) {
        if (jedis != null) {
            if (jedis.isConnected())
                jedis.disconnect();
            jedis.close();
        }
    }

    public String saveString(Jedis jedis, ObjectNode jsonBody) {
        jedis.set(jsonBody.get("KEY").asText(), jsonBody.get("VALUE").asText());
        return "";
    }

    public String saveList(Jedis jedis, ObjectNode jsonBody) {
        List<String> list = new LinkedList<>();
        jsonBody.get("LIST").elements().forEachRemaining(jsonNode -> {
            list.add(jsonNode.asText());
        });
        try {
            jedis.del(jsonBody.get("KEY").asText());
            jedis.rpush(jsonBody.get("KEY").asText(), list.toArray(new String[list.size()]));
        } catch (Exception e) {
            e.printStackTrace();
            return e.getMessage();
        }
        return "";
    }

    public String saveHash(Jedis jedis, ObjectNode jsonBody) {
        String key = jsonBody.get("KEY").asText();
        JsonNode jsonObject = jsonBody.get("HASH").deepCopy();
        Map<String, String> map = new HashMap<>();
        jsonObject.fieldNames().forEachRemaining(s -> {
            map.put(s, jsonObject.get(s).asText());
        });
        jedis.hmset(key, map);
        return "";
    }

    public String saveFileHash(Jedis jedis, ObjectNode jsonBody) throws IOException {
        String format = (jsonBody.get("format").asText()).equalsIgnoreCase("csv") ? "csv" : "txt";
        String fileName = jsonBody.get("filename").asText();
        String keyname = jsonBody.get("KEY_NAME").asText();
        String contentFile = new String(Files.readAllBytes(Paths.get(_unloadQueryDir + fileName + "." + format)), StandardCharsets.UTF_8);
        if (contentFile.length() > 0) {
            JSONArray contentAsJson = null;
            if (format.equalsIgnoreCase("csv"))
                contentAsJson = new org.json.JSONArray(queryServiceJSON.structureJsonFromCSV(contentFile));
            else
                contentAsJson = new org.json.JSONArray(queryServiceJSON.structureJsonFromTAB(contentFile));

            if (contentAsJson.length() > 0) {
                if (contentAsJson.getJSONObject(0).has(keyname)) {
                    Pipeline pipeline = jedis.pipelined();
                    contentAsJson.forEach(o -> {
                        JSONObject jsonObject = new JSONObject(o.toString());
                        Map<String, String> hash = new HashMap<>();
                        jsonObject.keys().forEachRemaining(key -> {
                            if (!key.equalsIgnoreCase(keyname)) {
                                hash.put(key, jsonObject.get(key).toString());
                            }
                        });
                        pipeline.hmset(jsonObject.get(keyname).toString(), hash);
                    });
                    pipeline.sync();
                } else return "error : {the keyname " + keyname + " doesn't exists in the file}";

            } else return "error : {can't structure csv to json}";
            //pipeline.sadd()

        } else return "error : {" + fileName + "." + format + " is empty}";
        //pipeline.sadd("")
        return "";
    }

    public String saveFileString(Jedis jedis, ObjectNode jsonBody) throws IOException {
        String format = (jsonBody.get("format").asText()).equalsIgnoreCase("csv") ? "csv" : "txt";
        String fileName = jsonBody.get("filename").asText();
        String keyname = "";
        String value = "";
        String contentFile = new String(Files.readAllBytes(Paths.get(_unloadQueryDir + fileName + "." + format)), StandardCharsets.UTF_8);
        if (contentFile.length() > 0) {
            JSONArray contentAsJson = null;
            BufferedReader bufferedReader = new BufferedReader(new FileReader(_unloadQueryDir + fileName + "." + format));
            String line;
            int counter = 0;
            if (format.equalsIgnoreCase("csv")) {
                while ((line = bufferedReader.readLine()) != null) {
                    Pattern p = Pattern.compile("(([^\"][^,]*)|\"([^\"]*)\"),?");
                    Matcher m = p.matcher(line);
                    while (m.find()) {
                        counter++;
                        if (counter > 2)
                            return "error : {the filename " + fileName + " doesn't have valid key/value structure}";
                        if (m.group(2) != null)
                            if (keyname.length() == 0)
                                keyname = m.group(2).replace("\n", "");
                            else value = m.group(2).replace("\n", "");
                        else if (m.group(3) != null)
                            if (keyname.length() == 0)
                                keyname = m.group(3).replace("\n", "");
                            else value = m.group(3).replace("\n", "");
                    }
                    break;
                }
                if (counter != 2)
                    return "error : {the filename " + fileName + " doesn't have valid key/value structure}";
                contentAsJson = new org.json.JSONArray(queryServiceJSON.structureJsonFromCSV(contentFile));
            } else {
                String[] cols = null;
                while ((line = bufferedReader.readLine()) != null) {
                    cols = line.split("\t");
                    if (cols.length != 2)
                        return "error : {the filename " + fileName + " doesn't have valid key/value structure}";
                    break;
                }
                keyname = cols[0];
                value = cols[1];
                contentAsJson = new org.json.JSONArray(queryServiceJSON.structureJsonFromTAB(contentFile));
            }
            if (contentAsJson.length() > 0) {
                Pipeline pipeline = jedis.pipelined();
                String finalKeyname = keyname;
                String finalValue = value;
                contentAsJson.forEach(o -> {
                    JSONObject jsonObject = new JSONObject(o.toString());
                    pipeline.set(jsonObject.get(finalKeyname.trim()).toString(), jsonObject.get(finalValue.trim()).toString());
                });
                pipeline.sync();
            } else return "error : {can't structure csv to json}";

        } else return "error : {" + fileName + "." + format + " is empty}";

        return "";
    }

    public JSONArray readFileHash(Jedis jedis, ObjectNode jsonBody) {
        JSONArray jsonArray = new JSONArray();
        String key = jsonBody.get("KEY").asText();
        List<String> listFields = new LinkedList<>();

        jsonBody.get("HASH").elements().forEachRemaining(jsonNode -> {
            listFields.add(jsonNode.asText());
        });

        jedis.keys(key).stream().forEachOrdered(s -> {
            //jedis.smembers(s).forEach(System.out::println);
            JSONObject jsonObject = new JSONObject();

            jedis.hgetAll(s).entrySet().stream().forEachOrdered(stringStringEntry -> {
                if (listFields.contains(stringStringEntry.getKey()))
                    jsonObject.put(stringStringEntry.getKey(), stringStringEntry.getValue());
            });
            if (jsonObject.length() > 0)
                jsonArray.put(jsonObject);
        });
        return jsonArray;
    }

    public String getStringCache(Jedis jedis, String key) {
        return jedis.get(key) != null ? jedis.get(key) : "";
    }

    public List<String> getListCache(Jedis jedis, String key) {
        return jedis.lrange(key, 0, -1);
    }

    public JSONObject getHash(Jedis jedis, String key) {
        JSONObject jsonObject = new JSONObject();
        jedis.hgetAll(key).entrySet().stream().forEachOrdered(stringStringEntry -> {
            jsonObject.put(stringStringEntry.getKey(), stringStringEntry.getValue());
        });
        return jsonObject;
    }

    public String saveResultSQLToHash(Jedis jedis, String structuredJson, String key_name) {

        org.json.JSONObject jsonObjectGlobal = new org.json.JSONObject(structuredJson);
        if (!jsonObjectGlobal.has("Input")) {
            return "Result SQL query can't be structured to valid JSON";
        }
        final String[] finalKey_name = {key_name};

        JSONArray jsonArrayInput = jsonObjectGlobal.getJSONArray("Input");
        if (jsonArrayInput.length() == 0) return "Result SQL query is Empty";

        JSONObject jsonObjectFirst = jsonArrayInput.getJSONObject(0);
        Set<String> keys = jsonObjectFirst.keySet().stream().map(s -> {
            if (s.equalsIgnoreCase(key_name)) finalKey_name[0] = s;
            return s.toLowerCase();
        }).collect(Collectors.toSet());

        if (!keys.contains(key_name.toLowerCase()))
            return "Result SQL query doesn't have key_name\t" + key_name + "\tSo we can't save the result as redis HASH";

        if (jsonObjectFirst.length() == 1) return "Result SQL query contain only the key_name " + key_name +
                " ,So we are unable to save empty hash";
        Pipeline pipeline = jedis.pipelined();

        jsonArrayInput.forEach(o -> {
            org.json.JSONObject jsonObject = new org.json.JSONObject(o.toString());
            Map<String, String> hash = new HashMap<>();
            jsonObject.keys().forEachRemaining(key -> {
                if (!key.equalsIgnoreCase(key_name)) {
                    hash.put(key, jsonObject.get(key).toString());
                }
            });

            pipeline.hmset(jsonObject.get(finalKey_name[0]).toString(), hash);
        });
        pipeline.sync();
        return "";
    }

    public String saveResultSQLToString(Jedis jedis, String structuredJson, String key_name) {
        org.json.JSONObject jsonObjectGlobal = new org.json.JSONObject(structuredJson);
        if (!jsonObjectGlobal.has("Input")) {
            return "Result SQL query can't be structured to valid JSON";
        }
        final String[] finalKey_name = {key_name};

        JSONArray jsonArrayInput = jsonObjectGlobal.getJSONArray("Input");
        if (jsonArrayInput.length() == 0) return "Result SQL query is Empty";

        JSONObject jsonObjectFirst = jsonArrayInput.getJSONObject(0);
        Set<String> keys = jsonObjectFirst.keySet().stream().map(s -> {
            if (s.equalsIgnoreCase(key_name)) finalKey_name[0] = s;
            return s.toLowerCase();
        }).collect(Collectors.toSet());

        if (keys.size() < 2)
            return "Result SQL query doesn't Contain valid key value structure";

        if (!keys.contains(key_name.toLowerCase()))
            return "Result SQL query doesn't have key_name\t" + key_name + "\tSo we can't save the result as redis HASH";

        if (jsonObjectFirst.length() == 1) return "Result SQL query contain only the key_name " + key_name +
                " ,So we are unable to save empty hash";
        Pipeline pipeline = jedis.pipelined();

        jsonArrayInput.forEach(o -> {
            org.json.JSONObject jsonObject = new org.json.JSONObject(o.toString());
            jsonObject.keys().forEachRemaining(key -> {
                if (!key.equalsIgnoreCase(key_name)) {
                    pipeline.set(jsonObject.get(finalKey_name[0]).toString(), jsonObject.get(key).toString());
                }
            });

        });
        pipeline.sync();
        return "";
    }


}
