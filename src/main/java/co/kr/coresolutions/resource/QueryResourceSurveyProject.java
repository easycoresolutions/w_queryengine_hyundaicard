package co.kr.coresolutions.resource;

import co.kr.coresolutions.commons.ResponseDto;
import co.kr.coresolutions.facades.IQuerySurveyProjectFacade;
import co.kr.coresolutions.model.dtos.SurveyProjectDto;
import lombok.RequiredArgsConstructor;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.DeleteMapping;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RestController;

import javax.validation.Valid;

@RestController
@RequiredArgsConstructor
public class QueryResourceSurveyProject {
    private final IQuerySurveyProjectFacade querySurveyProjectFacade;

    @PostMapping(value = "/savesurveyproject/{userid}/{projectid}", consumes = "*/*;charset=UTF-8")
    public ResponseEntity<Object> saveSurveyProject(@Valid @RequestBody SurveyProjectDto surveyProjectDto,
                                                    @PathVariable(value = "userid") String userID,
                                                    @PathVariable(value = "projectid") String projectID) {
        ResponseDto responseDto = querySurveyProjectFacade.saveSurveyProject(surveyProjectDto, userID, projectID);
        return ResponseEntity.status(responseDto.getErrorCode() == 0 ? responseDto.getSuccessCode() : responseDto.getErrorCode()).header("Content-Type", "application/json; charset=utf-8").body(responseDto.getMessage());
    }

    @DeleteMapping("/deletesurveyproject/{userid}/{projectid}")
    public ResponseEntity<Object> deleteSurveyProject(@PathVariable(value = "userid") String userID,
                                                      @PathVariable(value = "projectid") String projectID) {
        ResponseDto responseDto = querySurveyProjectFacade.deleteSurveyProject(userID, projectID);
        return ResponseEntity.status(responseDto.getErrorCode() == 0 ? responseDto.getSuccessCode() : responseDto.getErrorCode()).header("Content-Type", "application/json; charset=utf-8").body(responseDto.getMessage());
    }

    @GetMapping("/getsurveyproject/{userid}/{projectid}")
    public ResponseEntity<Object> getSurveyProject(@PathVariable(value = "userid") String userID,
                                                   @PathVariable(value = "projectid") String projectID) {
        ResponseDto responseDto = querySurveyProjectFacade.getSurveyProject(userID, projectID);
        return ResponseEntity.status(responseDto.getErrorCode() == 0 ? responseDto.getSuccessCode() : responseDto.getErrorCode()).header("Content-Type", "application/json; charset=utf-8").body(responseDto.getMessage());
    }

    @GetMapping("/getsurveyprojects/{userid}")
    public ResponseEntity<Object> getSurveyProjects(@PathVariable(value = "userid") String userID) {
        ResponseDto responseDto = querySurveyProjectFacade.getSurveyProjects(userID);
        return ResponseEntity.status(responseDto.getErrorCode() == 0 ? responseDto.getSuccessCode() : responseDto.getErrorCode()).header("Content-Type", "application/json; charset=utf-8").body(responseDto.getMessage());
    }
}
