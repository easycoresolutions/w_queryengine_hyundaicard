package co.kr.coresolutions.resource;

import co.kr.coresolutions.facades.IQueryTargetFacade;
import co.kr.coresolutions.model.dtos.Target2Dto;
import co.kr.coresolutions.model.dtos.TargetDto;
import lombok.RequiredArgsConstructor;
import lombok.extern.slf4j.Slf4j;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RestController;

import javax.validation.Valid;

@RestController
@RequiredArgsConstructor
@Slf4j
public class QueryResourceTarget {
    private final IQueryTargetFacade queryTargetFacade;

    @PostMapping("target/targetlist")
    public ResponseEntity targetList(@Valid @RequestBody TargetDto targetDto) {
        return ResponseEntity.ok(queryTargetFacade.targetList(targetDto));
    }

    @PostMapping("targetlist/upload")
    public ResponseEntity upload(@Valid @RequestBody Target2Dto target2Dto) {
        return ResponseEntity.ok(queryTargetFacade.upload(target2Dto));
    }
}
