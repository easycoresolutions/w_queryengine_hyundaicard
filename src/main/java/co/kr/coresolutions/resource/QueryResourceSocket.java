package co.kr.coresolutions.resource;

import co.kr.coresolutions.commons.CustomResponseDto;
import co.kr.coresolutions.facades.IQuerySocketFacade;
import co.kr.coresolutions.model.dtos.SocketBodyDto;
import lombok.RequiredArgsConstructor;
import lombok.extern.slf4j.Slf4j;
import org.springframework.http.ResponseEntity;
import org.springframework.security.access.annotation.Secured;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RestController;

import javax.validation.Valid;

@RestController
@RequiredArgsConstructor
@Slf4j
@Secured("IS_AUTHENTICATED_FULLY")
public class QueryResourceSocket {
    private final IQuerySocketFacade querySocketFacade;

    @PostMapping("socket/send")
    public ResponseEntity<CustomResponseDto> sendSocket(@Valid @RequestBody SocketBodyDto socketBody) {
        querySocketFacade.send(socketBody);
        return ResponseEntity.ok(CustomResponseDto.ok());
    }
}