package co.kr.coresolutions.resource;

import co.kr.coresolutions.commons.ResponseCodes;
import co.kr.coresolutions.commons.ResponseDto;
import co.kr.coresolutions.facades.IQuerySessionFacade;
import co.kr.coresolutions.model.dtos.SessionDto;
import lombok.RequiredArgsConstructor;
import lombok.SneakyThrows;
import lombok.extern.slf4j.Slf4j;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.DeleteMapping;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import javax.validation.Valid;

@RestController
@RequiredArgsConstructor
@RequestMapping(consumes = "*/*;charset=UTF-8")
@Slf4j
public class QueryResourceSession {

    private final IQuerySessionFacade querySessionFacade;

    @PostMapping("/opensession")
    @SneakyThrows
    public ResponseEntity openSession(@Valid @RequestBody SessionDto sessionDto) {
//        System.out.println("call Api openSession !!!");
        SessionDto response = querySessionFacade.saveOrUpdateSession(sessionDto.getOwnerId());
        return ResponseEntity.status(ResponseCodes.OK_RESPONSE).body(response.getSessionId());
    }

    @GetMapping(value = "/listsessions", produces = "application/json;charset=UTF-8")
    @SneakyThrows
    public ResponseEntity listSessions() {
//        System.out.println("Call Api listSessions !!!");
        ResponseDto responseDto;
        responseDto = querySessionFacade.listSessions();
        return ResponseEntity.status(responseDto.getErrorCode() == 0 ? responseDto.getSuccessCode() : responseDto.getErrorCode())
                .body(responseDto.getMessage());
    }

    @DeleteMapping("/removesession/{ownerId}")
    @SneakyThrows
    public ResponseEntity<ResponseDto> deleteSession(@PathVariable String ownerId) {
//        System.out.println("call Api deleteSession !!! ");
        ResponseDto responseDto = querySessionFacade.deleteSession(ownerId);
        return ResponseEntity.status(responseDto.getSuccessCode() == HttpStatus.OK.value() ? responseDto.getSuccessCode() : responseDto.getErrorCode()).body(responseDto);
    }
}
