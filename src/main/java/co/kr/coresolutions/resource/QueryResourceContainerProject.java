package co.kr.coresolutions.resource;

import co.kr.coresolutions.commons.ResponseDto;
import co.kr.coresolutions.facades.IQueryContainerProjectFacade;
import co.kr.coresolutions.model.dtos.ContainerProjectDto;
import lombok.RequiredArgsConstructor;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.DeleteMapping;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RestController;

import javax.validation.Valid;

@RestController
@RequiredArgsConstructor
public class QueryResourceContainerProject {
    private final IQueryContainerProjectFacade queryContainerProjectFacade;

    @PostMapping("/savecontainerproject/{userid}/{projectid}")
    public ResponseEntity<Object> saveContainerProject(@Valid @RequestBody ContainerProjectDto containerProjectDto,
                                                       @PathVariable(value = "userid") String userID,
                                                       @PathVariable(value = "projectid") String projectID) {
        ResponseDto responseDto = queryContainerProjectFacade.saveContainerProject(containerProjectDto, userID, projectID);
        return ResponseEntity.status(responseDto.getErrorCode() == 0 ? responseDto.getSuccessCode() : responseDto.getErrorCode()).header("Content-Type", "application/json; charset=utf-8").body(responseDto.getMessage());
    }

    @DeleteMapping("/deletecontainerproject/{userid}/{projectid}")
    public ResponseEntity<Object> deleteContainerProject(@PathVariable(value = "userid") String userID,
                                                         @PathVariable(value = "projectid") String projectID) {
        ResponseDto responseDto = queryContainerProjectFacade.deleteContainerProject(userID, projectID);
        return ResponseEntity.status(responseDto.getErrorCode() == 0 ? responseDto.getSuccessCode() : responseDto.getErrorCode()).header("Content-Type", "application/json; charset=utf-8").body(responseDto.getMessage());
    }

    @GetMapping("/getcontainerproject/{userid}/{projectid}")
    public ResponseEntity<Object> getContainerProject(@PathVariable(value = "userid") String userID,
                                                      @PathVariable(value = "projectid") String projectID) {
        ResponseDto responseDto = queryContainerProjectFacade.getContainerProject(userID, projectID);
        return ResponseEntity.status(responseDto.getErrorCode() == 0 ? responseDto.getSuccessCode() : responseDto.getErrorCode()).header("Content-Type", "application/json; charset=utf-8").body(responseDto.getMessage());
    }

    @GetMapping("/getcontainerprojects/{userid}")
    public ResponseEntity<Object> getContainerProjects(@PathVariable(value = "userid") String userID) {
        ResponseDto responseDto = queryContainerProjectFacade.getContainerProjects(userID);
        return ResponseEntity.status(responseDto.getErrorCode() == 0 ? responseDto.getSuccessCode() : responseDto.getErrorCode()).header("Content-Type", "application/json; charset=utf-8").body(responseDto.getMessage());
    }
}
