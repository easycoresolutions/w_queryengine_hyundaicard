package co.kr.coresolutions.resource;

import co.kr.coresolutions.facades.IQueryNitriteFacade;
import com.fasterxml.jackson.databind.node.ObjectNode;
import lombok.RequiredArgsConstructor;
import lombok.extern.slf4j.Slf4j;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.DeleteMapping;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.PutMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

@RestController
@RequiredArgsConstructor
@RequestMapping(value = QueryResourceNitrite.V1_NITRITE_PATH)
@Slf4j
public class QueryResourceNitrite {
    static final String V1_NITRITE_PATH = "/nitrite/";
    private final IQueryNitriteFacade queryNitriteFacade;

    @PostMapping("{filename}/{collectionName}/{doc_id}")
    public ResponseEntity save(@PathVariable("filename") String fileName, @PathVariable("collectionName") String collectionName, @PathVariable("doc_id") String docID, @RequestBody ObjectNode json) {
        return ResponseEntity.ok(queryNitriteFacade.save(fileName, collectionName, docID, json));
    }

    @DeleteMapping("{filename}/{collectionName}/{doc_id}")
    public ResponseEntity delete(@PathVariable("filename") String fileName, @PathVariable("collectionName") String collectionName, @PathVariable("doc_id") String docID) {
        return ResponseEntity.ok(queryNitriteFacade.delete(fileName, collectionName, docID));
    }

    @PutMapping("{filename}/{collectionName}/{doc_id}")
    public ResponseEntity update(@PathVariable("filename") String fileName, @PathVariable("collectionName") String collectionName,
                                 @PathVariable("doc_id") String docID, @RequestBody ObjectNode json) {
        return ResponseEntity.ok(queryNitriteFacade.update(fileName, collectionName, docID, json));
    }

    @GetMapping("{filename}/collections")
    public ResponseEntity get(@PathVariable("filename") String fileName) {
        return ResponseEntity.ok(queryNitriteFacade.get(fileName));
    }

    @PostMapping("find/{filename}/{collection}/docs")
    public ResponseEntity getIf(@PathVariable("filename") String fileName, @PathVariable("collection") String collectionName, @RequestBody ObjectNode json) {
        return ResponseEntity.ok(queryNitriteFacade.getIf(fileName, collectionName, json));
    }

    @GetMapping("{filename}/{collectionName}/{doc_id}")
    public ResponseEntity getByDocID(@PathVariable("filename") String fileName, @PathVariable("collectionName") String collectionName, @PathVariable("doc_id") String docID) {
        return ResponseEntity.ok(queryNitriteFacade.getByDocID(fileName, collectionName, docID));
    }

    @PostMapping("index/{filename}/{collectionName}")
    public ResponseEntity saveIndexes(@PathVariable("filename") String fileName, @PathVariable("collectionName") String collectionName, @RequestBody ObjectNode json) {
        return ResponseEntity.ok(queryNitriteFacade.saveIndexes(fileName, collectionName, json));
    }

    @PostMapping("indexrebuild/{filename}/{collectionName}/{field_name}")
    public ResponseEntity rebuildIndex(@PathVariable("filename") String fileName, @PathVariable("collectionName") String collectionName,
                                       @PathVariable("field_name") String fieldName) {
        return ResponseEntity.ok(queryNitriteFacade.rebuildIndex(fileName, collectionName, fieldName));
    }

    @DeleteMapping("index/{filename}/{collectionName}/{field_name}")
    public ResponseEntity deleteIndex(@PathVariable("filename") String fileName, @PathVariable("collectionName") String collectionName, @PathVariable("field_name") String fieldName) {
        return ResponseEntity.ok(queryNitriteFacade.deleteIndex(fileName, collectionName, fieldName));
    }

}
