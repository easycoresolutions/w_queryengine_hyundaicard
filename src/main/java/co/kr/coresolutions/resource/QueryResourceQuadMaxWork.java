package co.kr.coresolutions.resource;

import co.kr.coresolutions.commons.CustomResponseDto;
import co.kr.coresolutions.facades.IQueryQuadMaxWorkFacade;
import co.kr.coresolutions.model.dtos.QuadMaxWorkDto;
import lombok.RequiredArgsConstructor;
import lombok.extern.slf4j.Slf4j;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.DeleteMapping;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RestController;

import javax.validation.Valid;

@RestController
@RequiredArgsConstructor
@Slf4j
public class QueryResourceQuadMaxWork {
    private final IQueryQuadMaxWorkFacade queryQuadMaxWorkFacade;

    @GetMapping(value = "{connectionid}/{tablename}", produces = "application/json; charset=utf-8")
    public ResponseEntity getCountRows(@PathVariable(value = "connectionid") String connectionID,
                                       @PathVariable(value = "tablename") String tableName) {
        CustomResponseDto customResponseDto = queryQuadMaxWorkFacade.countRows(connectionID, tableName);
        return ResponseEntity.ok(customResponseDto.getSuccess() ? customResponseDto : customResponseDto.getMessage());
    }

    @DeleteMapping(value = "{connectionid}/{tablename}", produces = "application/json; charset=utf-8")
    public ResponseEntity<CustomResponseDto> dropTable(@PathVariable(value = "connectionid") String connectionID,
                                                       @PathVariable(value = "tablename") String tableName) {
        CustomResponseDto customResponseDto = queryQuadMaxWorkFacade.dropTable(connectionID, tableName);
        return ResponseEntity.status(customResponseDto.getSuccess() ? HttpStatus.OK : HttpStatus.BAD_REQUEST).body(customResponseDto);
    }

    @PostMapping(value = "{connectionid}/getcount", produces = "application/json; charset=utf-8")
    public ResponseEntity postCountRows(@PathVariable(value = "connectionid") String connectionID,
                                        @Valid @RequestBody QuadMaxWorkDto quadMaxWorkDto) {
        CustomResponseDto customResponseDto = (CustomResponseDto) queryQuadMaxWorkFacade.countRows(connectionID, quadMaxWorkDto);
        return ResponseEntity.ok(customResponseDto.getSuccess() ? customResponseDto.getMessage() : customResponseDto);
    }
    
    // postCountRows 메소드의  SQL 인젝션 이슈 fix 버전 
    @PostMapping(value = "{connectionid}/getSubmitCount", produces = "application/json; charset=utf-8")
    public ResponseEntity postSubmitCountRows(@PathVariable(value = "connectionid") String connectionID,
                                        @Valid @RequestBody QuadMaxWorkDto quadMaxWorkDto) {
        CustomResponseDto customResponseDto = (CustomResponseDto) queryQuadMaxWorkFacade.submitCountRows(connectionID, quadMaxWorkDto);
        return ResponseEntity.ok(customResponseDto.getSuccess() ? customResponseDto.getMessage() : customResponseDto);
    }

}
