package co.kr.coresolutions.resource;

import co.kr.coresolutions.facades.IQueryGoogleFacade;
import co.kr.coresolutions.model.dtos.GoogleTemplateDTO;
import lombok.RequiredArgsConstructor;
import lombok.extern.slf4j.Slf4j;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RestController;

import javax.validation.Valid;

@RestController
@RequiredArgsConstructor
@Slf4j
public class QueryResourceGoogle {
    private final IQueryGoogleFacade queryGoogleFacade;

    @PostMapping(value = "googlesheet/{connectionid}")
    public ResponseEntity convertSheet(@PathVariable("connectionid") String connectionID,
                                       @Valid @RequestBody GoogleTemplateDTO googleTemplateDTO) {
        return ResponseEntity.ok(queryGoogleFacade.getAndConvertSheet(connectionID, googleTemplateDTO));
    }
}
