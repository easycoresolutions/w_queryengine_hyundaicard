package co.kr.coresolutions.resource;

import co.kr.coresolutions.commons.CustomResponseDto;
import co.kr.coresolutions.facades.IQueryDateFacade;
import co.kr.coresolutions.model.dtos.CriteriaDto;
import co.kr.coresolutions.model.dtos.DelayDto;
import lombok.RequiredArgsConstructor;
import lombok.extern.slf4j.Slf4j;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RestController;

import javax.validation.Valid;

@RestController
@RequiredArgsConstructor
@Slf4j
public class QueryResourceDate {
    private final IQueryDateFacade queryDateFacade;

    @PostMapping("validate/date/{connectionid}/{holiday_tablename}/{yyyymmdd}")
    public ResponseEntity<CustomResponseDto> validate(@PathVariable("connectionid") String connectionID,
                                                      @PathVariable("holiday_tablename") String holidayTableName,
                                                      @PathVariable("yyyymmdd") String yyyyMMdd,
                                                      @Valid @RequestBody CriteriaDto criteriaDto) {
        return ResponseEntity.ok(queryDateFacade.validate(connectionID, holidayTableName, yyyyMMdd, criteriaDto));
    }

    @PostMapping("calculate/date")
    public ResponseEntity calculate(@Valid @RequestBody DelayDto delayDto) {
        CustomResponseDto customResponseDto = queryDateFacade.calculate(delayDto);
        return ResponseEntity.status(customResponseDto.getErrorCode() != null ? HttpStatus.BAD_REQUEST : HttpStatus.OK).body(
                customResponseDto.getErrorCode() != null ? customResponseDto : customResponseDto.getDetailedMessage());
    }

}
