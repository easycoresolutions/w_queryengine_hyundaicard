package co.kr.coresolutions.resource;

import co.kr.coresolutions.commons.CustomResponseDto;
import co.kr.coresolutions.facades.IQueryLazyCommandFacade;
import co.kr.coresolutions.model.dtos.LazyCommandDto;
import lombok.RequiredArgsConstructor;
import lombok.extern.slf4j.Slf4j;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.DeleteMapping;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import javax.validation.Valid;

@RestController
@RequiredArgsConstructor
@Slf4j
@RequestMapping(value = QueryResourceLazyCommand.V1_LAZY_COMMAND_PATH, consumes = "*/*;charset=UTF-8")
public class QueryResourceLazyCommand {
    static final String V1_LAZY_COMMAND_PATH = "/lazycommand/";
    private final IQueryLazyCommandFacade queryLazyCommandFacade;

    @PostMapping("send")
    public ResponseEntity<CustomResponseDto> send(@Valid @RequestBody LazyCommandDto lazyCommandDto) {
        CustomResponseDto customResponseDto = queryLazyCommandFacade.send(lazyCommandDto);
        return ResponseEntity.status(customResponseDto.getErrorCode() != null ? HttpStatus.BAD_REQUEST : HttpStatus.OK).body(customResponseDto);
    }

    @GetMapping(value = "{queuename}")
    public ResponseEntity<String> getAll(@PathVariable("queuename") String queueName) {
        return ResponseEntity.ok(queryLazyCommandFacade.getAll(queueName).toString());
    }

    @DeleteMapping("{queuename}/{commandid}")
    public ResponseEntity<CustomResponseDto> remove(@PathVariable("queuename") String queueName,
                                                    @PathVariable("commandid") String commandID) {
        CustomResponseDto customResponseDto = queryLazyCommandFacade.remove(queueName, commandID);
        return ResponseEntity.status(customResponseDto.getErrorCode() != null ? HttpStatus.BAD_REQUEST : HttpStatus.OK).body(customResponseDto);
    }
}
