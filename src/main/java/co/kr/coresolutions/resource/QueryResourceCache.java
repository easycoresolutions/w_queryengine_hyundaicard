package co.kr.coresolutions.resource;

import co.kr.coresolutions.service.QueryServiceCache;
import lombok.RequiredArgsConstructor;
import org.springframework.context.annotation.Scope;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RestController;

@RestController
@Scope(value = "request")
@RequiredArgsConstructor
public class QueryResourceCache {
    private final QueryServiceCache queryServiceCache;

    @PostMapping(value = "/reference")
    public ResponseEntity refresh() {
        queryServiceCache.refresh();
        return ResponseEntity.ok("Cache reference updated successfully");
    }
}
