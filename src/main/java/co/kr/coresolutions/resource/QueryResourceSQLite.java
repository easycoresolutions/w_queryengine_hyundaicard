package co.kr.coresolutions.resource;

import co.kr.coresolutions.model.Message;
import co.kr.coresolutions.model.TableLoad;
import co.kr.coresolutions.service.QueryService;
import co.kr.coresolutions.service.QueryServiceCSV;
import co.kr.coresolutions.service.QueryServiceSQLite;
import com.fasterxml.jackson.databind.node.ObjectNode;
import lombok.RequiredArgsConstructor;
import org.springframework.context.annotation.Scope;
import org.springframework.http.HttpHeaders;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.validation.BindingResult;
import org.springframework.web.bind.annotation.DeleteMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RestController;
import org.springframework.web.util.UriComponentsBuilder;

import javax.validation.Valid;
import java.io.IOException;
import java.nio.charset.StandardCharsets;

@RestController
@Scope(value = "request")
@RequiredArgsConstructor
public class QueryResourceSQLite {
    private final QueryService queryService;
    private final QueryServiceCSV queryServiceCSV;
    private final QueryServiceSQLite queryServiceSQLite;

    @PostMapping(value = "/sqlite/createdb_file", consumes = "*/*;charset=UTF-8")
    public ResponseEntity<Object> createdb_file(@RequestBody ObjectNode jsonBody,
                                                UriComponentsBuilder _ucBuilder, BindingResult bindingResult) {
        HttpHeaders __headers = new HttpHeaders();
        __headers.setLocation(_ucBuilder.path("/createdb_file").buildAndExpand().toUri());
        __headers.add("Content-Type", "application/json; charset=utf-8");

        Message _message = Message.getInstance();
        if (!jsonBody.has("userid") || !jsonBody.has("db_file") || bindingResult.hasErrors()) {
            _message.setMessage("createdb_file : Some errors , Please check ur json content!" + "\n");
            queryService.addLogging("POST", _message.getMessage());
            return new ResponseEntity<Object>(_message.getMessage(), __headers, HttpStatus.BAD_REQUEST);
        }

        String userID = jsonBody.get("userid").asText();
        String dbFileName = jsonBody.get("db_file").asText();

        boolean isDBFileName = queryServiceSQLite.isDBFileNameExists(userID, dbFileName);
        if (isDBFileName) {
            _message.setMessage("db-file " + dbFileName + " already exist.");
            queryService.addLogging("POST", _message.getMessage() + "\n");
            return new ResponseEntity<Object>(_message.getMessage(), __headers, HttpStatus.BAD_REQUEST);
        }
        String result = queryServiceSQLite.createDBFile("mysqlite", userID, dbFileName);
        if (result.startsWith("error")) {
            _message.setMessage(new String(result.getBytes(), StandardCharsets.UTF_8));
            queryService.addLogging("POST", _message.getMessage() + "\n");
            return new ResponseEntity<Object>(_message.getMessage(), __headers, HttpStatus.NOT_IMPLEMENTED);
        }

        String log = "createdb_file successfully";
        queryService.addLogging("POST", log + "\n");
        return new ResponseEntity<Object>(log, __headers, HttpStatus.OK);
    }

    @DeleteMapping(value = "/sqlite/{userid}/{db_file_name}", consumes = "*/*;charset=UTF-8")
    public ResponseEntity<Object> deleteDBfile(@PathVariable(value = "db_file_name") String db_file_name,
                                               @PathVariable(value = "userid") String userid,
                                               UriComponentsBuilder _ucBuilder) {
        HttpHeaders __headers = new HttpHeaders();
        __headers.setLocation(_ucBuilder.path("/sqlite/" + userid + "/" + db_file_name).buildAndExpand().toUri());
        __headers.add("Content-Type", "text/plain; charset=utf-8");

        boolean resultDelete = queryServiceSQLite.deleteDbFile(userid, db_file_name);

        if (!resultDelete) {
            queryService.addLogging("DELETE", db_file_name + " doesn't exist.\n");
            return new ResponseEntity<Object>(db_file_name + " doesn't exist.", __headers, HttpStatus.NOT_IMPLEMENTED);
        }

        String log = "deleteDBfile: " + db_file_name;
        queryService.addLogging("DELETE", log);
        return new ResponseEntity<Object>(db_file_name + " Deleted successfully", __headers, HttpStatus.OK);
    }

    @DeleteMapping(value = "/sqlite/{userid}/{db_file_name}/{table_name}", consumes = "*/*;charset=UTF-8")
    public ResponseEntity<Object> deleteTableFromDBfile(@PathVariable(value = "db_file_name") String db_file_name,
                                                        @PathVariable(value = "userid") String userid,
                                                        @PathVariable(value = "table_name") String table_name,
                                                        UriComponentsBuilder _ucBuilder) {
        HttpHeaders __headers = new HttpHeaders();
        __headers.setLocation(_ucBuilder.path("/sqlite/" + userid + "/" + db_file_name + "/" + table_name).buildAndExpand().toUri());
        __headers.add("Content-Type", "text/plain; charset=utf-8");

        boolean resultDelete = queryServiceSQLite.deleteTableFromDBfile("mysqlite", userid, db_file_name, table_name);

        if (!resultDelete) {
            queryService.addLogging("DELETE", db_file_name + " doesn't exist.\n");
            return new ResponseEntity<Object>(db_file_name + " doesn't exist.", __headers, HttpStatus.NOT_IMPLEMENTED);
        }

        String log = "deleteTable " + table_name + " From DB_file: " + db_file_name;
        queryService.addLogging("DELETE", log);
        return new ResponseEntity<Object>(table_name + " Deleted successfully", __headers, HttpStatus.OK);
    }


    @PostMapping(value = "/sqlite/tableload", consumes = "*/*;charset=UTF-8")
    public ResponseEntity tableLoad(@Valid @RequestBody TableLoad tableLoad) throws IOException {
        Message _message = Message.getInstance();

        boolean isDBFileName = queryServiceSQLite.isDBFileNameExists(tableLoad.getUserid(), tableLoad.getDb_file());
        if (!isDBFileName) {
            _message.setMessage("db-file " + tableLoad.getDb_file() + " doesn't exist.");
            queryService.addLogging("POST", _message.getMessage());
            return ResponseEntity.status(HttpStatus.BAD_REQUEST).header("Content-Type", "application/json; charset=utf-8").body(_message.getMessage());
        }

        String contentFile = queryServiceSQLite.isFileNameExists(tableLoad.getUserid(), tableLoad.getInput_directory(), tableLoad.getFile_name());
        if (contentFile.startsWith("error") || contentFile.length() == 0) {
            _message.setMessage("file_name " + tableLoad.getFile_name() + " doesn't exist.");
            queryService.addLogging("POST", _message.getMessage());
            return ResponseEntity.status(HttpStatus.BAD_REQUEST).header("Content-Type", "application/json; charset=utf-8").body(_message.getMessage());
        }

        String resultUnload = queryServiceSQLite.tableLoad(tableLoad);
        if (resultUnload.startsWith("Error") || resultUnload.startsWith("sqlite3: Error")) {
            _message.setMessage(new String(resultUnload.getBytes(StandardCharsets.UTF_8)));
            queryService.addLogging("POST", _message.getMessage());
            return ResponseEntity.status(HttpStatus.BAD_REQUEST).header("Content-Type", "application/json; charset=utf-8").body(_message.getMessage());
        }

        String log = "tableload successfully";
        queryService.addLogging("POST", log);
        return ResponseEntity.ok(log);
    }

    @RequestMapping(path = "/sqlite/{userID}/db_files", method = RequestMethod.GET)
    public ResponseEntity<Object> getDBFiles(@PathVariable("userID") String userid,
                                             UriComponentsBuilder _ucBuilder) {
        HttpHeaders __headers = new HttpHeaders();
        __headers.add("Content-Type", "text/plain; charset=utf-8");
        __headers.setLocation(_ucBuilder.path("/sqlite/" + userid + "/db_files").buildAndExpand().toUri());
        String dbFiles = queryServiceSQLite.getDBFiles(userid);

        if (dbFiles.startsWith("error")) {
            queryService.addLogging("GET", dbFiles + "\n");
            return new ResponseEntity<Object>(dbFiles, __headers, HttpStatus.NOT_IMPLEMENTED);
        }

        String resultdbFiles = "";
        if (dbFiles.length() > 0)
            resultdbFiles = new String(queryServiceCSV.structureCSV(dbFiles).getBytes(), StandardCharsets.UTF_8);
        String log = " getDBFiles called for userid " + userid + "\n";
        queryService.addLogging("GET", log);
        return new ResponseEntity<Object>(resultdbFiles, __headers, HttpStatus.OK);

    }
}
