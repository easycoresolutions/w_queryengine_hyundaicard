package co.kr.coresolutions.resource;


import co.kr.coresolutions.model.Message;
import co.kr.coresolutions.service.QueryService;
import co.kr.coresolutions.service.QueryServiceDirectories;
import lombok.RequiredArgsConstructor;
import org.springframework.context.annotation.Scope;
import org.springframework.http.HttpHeaders;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RestController;
import org.springframework.web.util.UriComponentsBuilder;

@RestController
@Scope(value = "request")
@RequiredArgsConstructor
public class QueryResourceDirectories {
    private final QueryService queryService;
    private final QueryServiceDirectories queryServiceDirectories;

    @RequestMapping(value = {"/getdata/{directory_name}/{filename}"}, method = RequestMethod.GET)
    public ResponseEntity<Object> getdata(UriComponentsBuilder _ucBuilder,
                                          @PathVariable(value = "directory_name") String directory_name,
                                          @PathVariable(value = "filename") String filename) {
        HttpHeaders __headers = new HttpHeaders();
        Message _message = Message.getInstance();
        __headers.add("Content-Type", "application/text; charset=utf-8");

        __headers.setLocation(_ucBuilder.path("/getdata/" + directory_name + "/" + filename).buildAndExpand().toUri());

        String resultGetFile = queryServiceDirectories.getFile(directory_name, filename);
        if (resultGetFile.equalsIgnoreCase("fail")) {
            _message.setMessage(filename + " doesn't exist in the directory " + directory_name);
            queryService.addLogging("GET", _message.getMessage() + "\n");
            return new ResponseEntity<Object>(_message.getMessage(), __headers, HttpStatus.NOT_FOUND);
        }

        String log = "getdata called success\n";
        queryService.addLogging("GET", log);
        return new ResponseEntity<Object>(resultGetFile, __headers, HttpStatus.OK);
    }


}
