package co.kr.coresolutions.resource;

import co.kr.coresolutions.facades.IQueryCommandFacade;
import co.kr.coresolutions.facades.IQueryETLFacade;
import co.kr.coresolutions.facades.IQueryLiquidFacade;
import co.kr.coresolutions.facades.IQueryMustacheFacade;
import co.kr.coresolutions.facades.IQuerySSBIFacade;
import co.kr.coresolutions.model.Connection;
import co.kr.coresolutions.model.Message;
import co.kr.coresolutions.model.Query;
import co.kr.coresolutions.model.dtos.CommandTemplate;
import co.kr.coresolutions.model.dtos.ETLTemplateDTO;
import co.kr.coresolutions.model.dtos.LiquidTemplateDTO;
import co.kr.coresolutions.model.dtos.LiquidTemplateQueryDTO;
import co.kr.coresolutions.model.dtos.MustacheTemplateDTO;
import co.kr.coresolutions.model.dtos.SSBIDto;
import co.kr.coresolutions.service.QueryService;
import co.kr.coresolutions.service.QueryServiceFiles;
import com.google.gson.Gson;
import lombok.RequiredArgsConstructor;
import org.json.JSONObject;
import org.springframework.http.HttpHeaders;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.validation.BindingResult;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RestController;
import org.springframework.web.util.UriComponentsBuilder;

import javax.servlet.http.HttpServletResponse;
import javax.validation.Valid;
import java.io.IOException;
import java.nio.charset.StandardCharsets;

@RestController
@RequiredArgsConstructor
public class QueryResourceOthers {
    private final QueryService queryService;
    private final QueryServiceFiles queryServiceFile;
    private final IQueryCommandFacade queryCommandFacade;
    private final IQuerySSBIFacade querySSBIFacade;
    private final IQueryMustacheFacade queryMustacheFacade;
    private final IQueryLiquidFacade queryLiquidFacade;
    private final IQueryETLFacade queryETLFacade;

    @GetMapping("status")
    public ResponseEntity isAlive() {
        JSONObject jsonObject = new JSONObject();
        jsonObject.put("status", "alive");
        return ResponseEntity.ok(jsonObject.toString());
    }

    @RequestMapping(value = "/business_query/{queryID}", method = RequestMethod.POST, consumes = "*/*;charset=UTF-8")
    public ResponseEntity<Object> SaveQueryFile(@Valid @RequestBody Query json, @PathVariable(value = "queryID") String queryID,
                                                UriComponentsBuilder _ucBuilder, BindingResult result) throws Exception {
        HttpHeaders __headers = new HttpHeaders();
        Message _message = Message.getInstance();
        __headers.setLocation(_ucBuilder.path("/business_query/" + queryID).buildAndExpand().toUri());

        if (result.hasErrors()) {
            _message.setMessage("SaveQueryFile : Some errors , Please check ur json content!" + "\n");
            queryService.addLogging("POST", _message.getMessage());
            return new ResponseEntity<Object>("config : Some errors , Please check ur json content!", __headers, HttpStatus.BAD_REQUEST);
        }

        queryServiceFile.SaveOrUpdateQueryFile(queryID, json.getSql());
        String log = "SaveQueryFile: Query file has been uploaded successfully\n";
        _message.setMessage(log);
        queryService.addLogging("POST", log);
        __headers.add("Content-Type", "application/json; charset=utf-8");
        return new ResponseEntity<Object>(_message, __headers, HttpStatus.CREATED);
    }

    @RequestMapping(value = {"/business_query/{queryID}"}, method = RequestMethod.GET)
    public ResponseEntity<Object> getQueryFile(UriComponentsBuilder _ucBuilder,
                                               @PathVariable(value = "queryID") String queryID) throws IOException {
        HttpHeaders __headers = new HttpHeaders();
        Message _message = Message.getInstance();
        __headers.setLocation(_ucBuilder.path("/business_query/" + queryID).buildAndExpand().toUri());

        boolean isQueryFileExists = queryServiceFile.isQueryFileExist(queryID);

        if (!isQueryFileExists) {
            _message.setMessage(queryID + " (information) doesn't exist.");
            queryService.addLogging("GET", queryID + " (information) doesn't exist." + "\n");
            return new ResponseEntity<Object>(_message, __headers, HttpStatus.UNAUTHORIZED);
        }

        String query = queryService.getFormattedString(queryServiceFile.fetchQueryFromFile(queryID));

        __headers.add("Content-Type", "application/json; charset=utf-8");
        String log = "getQueryFile called" + "\n";
        queryService.addLogging("GET", log);
        JSONObject response = new JSONObject();
        response.put("sql", query);
        return new ResponseEntity<Object>(response.toString(), __headers, HttpStatus.OK);
    }

    @RequestMapping(value = "/business_query/{queryID}", method = RequestMethod.DELETE, consumes = "*/*;charset=UTF-8")
    public ResponseEntity<Object> deleteQueryFile(@PathVariable(value = "queryID") String queryID,
                                                  UriComponentsBuilder _ucBuilder) throws Exception {
        HttpHeaders __headers = new HttpHeaders();
        __headers.setLocation(_ucBuilder.path("/business_query/" + queryID).buildAndExpand().toUri());
        boolean isQueryFileExists = queryServiceFile.isQueryFileExist(queryID);

        if (!isQueryFileExists) {
            queryService.addLogging("DELETE", queryID + " (information) doesn't exist." + "\n");
            return new ResponseEntity<Object>(queryID + " (information) doesn't exist." + "\n", __headers, HttpStatus.UNAUTHORIZED);
        }
        queryServiceFile.deleteQueryFile(queryID);
        String log = "deleteQueryFile: file " + queryID + " has been Deleted successfully";
        queryService.addLogging("DELETE", log);
        __headers.add("Content-Type", "application/json; charset=utf-8");
        return new ResponseEntity<Object>(log, __headers, HttpStatus.OK);
    }

    @RequestMapping(value = {"/list_business_querys"}, method = RequestMethod.GET)
    public ResponseEntity<Object> getQueryFiles(UriComponentsBuilder _ucBuilder) throws IOException {
        HttpHeaders __headers = new HttpHeaders();
        __headers.setLocation(_ucBuilder.path("/list_business_querys").buildAndExpand().toUri());

        String resultGetQueryFiles = queryServiceFile.getFiles(true, false, "");
        __headers.add("Content-Type", "application/json; charset=utf-8");
        String log = "getQueryFiles called\n";
        queryService.addLogging("GET", log);
        return new ResponseEntity<Object>(new String(resultGetQueryFiles.getBytes(), StandardCharsets.UTF_8), __headers, HttpStatus.OK);

    }

    /****************************************************************************************************/


    @RequestMapping(value = {"/connection_info/{connectionID}"}, method = RequestMethod.GET)
    public ResponseEntity<Object> getConnectionFile(UriComponentsBuilder _ucBuilder,
                                                    @PathVariable(value = "connectionID") String connectionID) throws IOException {
        HttpHeaders __headers = new HttpHeaders();
        Message _message = Message.getInstance();
        __headers.setLocation(_ucBuilder.path("/connection_info/" + connectionID).buildAndExpand().toUri());

        boolean isConnectionFileExists = queryServiceFile.isConnectionIdFileExists(connectionID);

        if (!isConnectionFileExists) {
            _message.setMessage(connectionID + " (information) doesn't exist.");
            queryService.addLogging("GET", connectionID + " (information) doesn't exist." + "\n");
            return new ResponseEntity<Object>(_message, __headers, HttpStatus.UNAUTHORIZED);
        }

        String connectionFile = queryService.getConnectionFile(connectionID);
        __headers.add("Content-Type", "application/json; charset=utf-8");
        String log = "getConnectionFile called" + "\n";
        queryService.addLogging("GET", log);
        return new ResponseEntity<Object>(connectionFile, __headers, HttpStatus.OK);
    }

    @RequestMapping(value = "/connection_info/{connectionID}", method = RequestMethod.POST, consumes = "*/*;charset=UTF-8")
    public ResponseEntity<Object> SaveConnectionFile(@Valid @RequestBody Connection connection,
                                                     @PathVariable(value = "connectionID") String connectionID,
                                                     UriComponentsBuilder _ucBuilder, BindingResult result) throws Exception {
        HttpHeaders __headers = new HttpHeaders();
        Message _message = Message.getInstance();
        __headers.setLocation(_ucBuilder.path("/connection_info/" + connectionID).buildAndExpand().toUri());

        if (result.hasErrors()) {
            _message.setMessage("SaveConnectionFile : Some errors , Please check ur json content!" + "\n");
            queryService.addLogging("POST", _message.getMessage());
            return new ResponseEntity<Object>("config : Some errors , Please check ur json content!", __headers, HttpStatus.BAD_REQUEST);
        }
        Gson gson = new Gson();
        String json = gson.toJson(connection);
        queryServiceFile.SaveOrUpdateConnectionFile(connectionID, json);
        String log = "SaveConnectionFile: Connection file has been uploaded successfully\n";
        _message.setMessage(log);
        queryService.addLogging("POST", log);
        __headers.add("Content-Type", "application/json; charset=utf-8");
        return new ResponseEntity<Object>(_message, __headers, HttpStatus.CREATED);
    }

    @RequestMapping(value = "/connection_info/{connectionID}", method = RequestMethod.DELETE, consumes = "*/*;charset=UTF-8")
    public ResponseEntity<Object> deleteConnectionFile(@PathVariable(value = "connectionID") String connectionID,
                                                       UriComponentsBuilder _ucBuilder) throws Exception {
        HttpHeaders __headers = new HttpHeaders();
        __headers.setLocation(_ucBuilder.path("/connection_info/" + connectionID).buildAndExpand().toUri());
        boolean isConnectionIdFileExists = queryServiceFile.isConnectionIdFileExists(connectionID);

        if (!isConnectionIdFileExists) {
            queryService.addLogging("DELETE", connectionID + " (information) doesn't exist." + "\n");
            return new ResponseEntity<Object>(connectionID + " (information) doesn't exist." + "\n", __headers, HttpStatus.UNAUTHORIZED);
        }
        queryServiceFile.deleteConnectionFile(connectionID);
        String log = "deleteConnectionFile: file " + connectionID + " has been Deleted successfully";
        queryService.addLogging("DELETE", log);
        __headers.add("Content-Type", "application/json; charset=utf-8");
        return new ResponseEntity<Object>(log, __headers, HttpStatus.OK);
    }


    @RequestMapping(value = {"/list_connections"}, method = RequestMethod.GET)
    public ResponseEntity<Object> getConnectionFiles(UriComponentsBuilder _ucBuilder) throws IOException {
        HttpHeaders __headers = new HttpHeaders();
        __headers.setLocation(_ucBuilder.path("/list_connections").buildAndExpand().toUri());

        String resultGetQueryFiles = queryServiceFile.getFiles(false, true, "");
        __headers.add("Content-Type", "application/json; charset=utf-8");
        String log = "getConnectionFiles called\n";
        queryService.addLogging("GET", log);
        return new ResponseEntity<Object>(resultGetQueryFiles, __headers, HttpStatus.OK);
    }

    @PostMapping("scriptella/{directory}/{xml}")
    public ResponseEntity<Object> executeScriptElla(@PathVariable("directory") String directory,
                                                    @PathVariable("xml") String xml,
                                                    @Valid @RequestBody CommandTemplate commandTemplate,
                                                    HttpServletResponse response) {
        return ResponseEntity.ok(queryCommandFacade.executeETL(directory, xml, commandTemplate, response));
    }

    @PostMapping("scriptella/dbtable/{connectionid}/{project_id}/{script_id}")
    public ResponseEntity<Object> executeScriptEllaFromDBMS(@PathVariable(value = "connectionid") String connectionID,
                                                            @PathVariable(value = "project_id") String projectID,
                                                            @PathVariable(value = "script_id") String scriptID,
                                                            @Valid @RequestBody CommandTemplate commandTemplate,
                                                            HttpServletResponse response) {
        return ResponseEntity.ok(queryCommandFacade.executeETLFromDBMS(connectionID, projectID, scriptID, commandTemplate, response));
    }

    @PostMapping(value = "etl_sql/{connectionID}/{projectID}/{scriptID}", produces = "application/json; charset=utf-8")
    public ResponseEntity<Object> exchange(@PathVariable("connectionID") String connectionID,
                                           @PathVariable("projectID") String projectID,
                                           @PathVariable("scriptID") String scriptID,
                                           @Valid @RequestBody ETLTemplateDTO etlTemplateDTO) throws IOException {
        return ResponseEntity.ok(queryETLFacade.exchange(connectionID, projectID, scriptID, etlTemplateDTO.getOwner(), etlTemplateDTO.getCommandID()));
    }

    @PostMapping(value = "template_engine/mustache")
    public ResponseEntity templateMustache(@Valid @RequestBody MustacheTemplateDTO mustacheTemplateDTO) {
        return ResponseEntity.ok(queryMustacheFacade.convert(mustacheTemplateDTO));
    }

    @PostMapping(value = "template_engine/liquid")
    public ResponseEntity templateLiquid(@Valid @RequestBody MustacheTemplateDTO mustacheTemplateDTO) {
        return ResponseEntity.ok(queryLiquidFacade.convertTemplate(mustacheTemplateDTO));
    }

    @PostMapping(value = "template_engine/liquid/customerid")
    public ResponseEntity templateLiquidCustomerID(@Valid @RequestBody LiquidTemplateDTO liquidTemplateDTO) {
        return ResponseEntity.ok(queryLiquidFacade.convertLiquidTemplate(liquidTemplateDTO));
    }

    @PostMapping(value = "template_engine/liquid/query")
    public ResponseEntity templateLiquidQuery(@Valid @RequestBody LiquidTemplateQueryDTO liquidTemplateQueryDTO) {
        return ResponseEntity.ok(queryLiquidFacade.convertLiquidTemplateQuery(liquidTemplateQueryDTO));
    }

    @PostMapping(value = "ssbisql/{ssbi_id}/{owner}")
    public ResponseEntity ssbisql(@Valid @RequestBody SSBIDto ssbiDto,
                                  @PathVariable("ssbi_id") String ssbiID,
                                  @PathVariable("owner") String owner) {
        return ResponseEntity.ok(querySSBIFacade.logToDB(ssbiDto, owner, ssbiID));
    }
}
