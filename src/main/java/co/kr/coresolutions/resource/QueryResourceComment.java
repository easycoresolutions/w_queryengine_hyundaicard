package co.kr.coresolutions.resource;

import co.kr.coresolutions.commons.CustomResponseDto;
import co.kr.coresolutions.facades.IQueryCommentFacade;
import co.kr.coresolutions.model.dtos.CommentDto;
import lombok.RequiredArgsConstructor;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.DeleteMapping;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import javax.servlet.http.HttpServletRequest;
import javax.validation.Valid;

@RestController
@RequiredArgsConstructor
@RequestMapping(value = QueryResourceComment.V1_COMMENT_PATH)
public class QueryResourceComment {
    static final String V1_COMMENT_PATH = "/commenttable/";
    private final IQueryCommentFacade queryCommentFacade;

    @PostMapping(value = "{connectionid}/{projectid}")
    public ResponseEntity addComment(@Valid @RequestBody CommentDto commentDto,
                                     @PathVariable(value = "connectionid") String connectionID,
                                     @PathVariable(value = "projectid") String projectID,
                                     HttpServletRequest httpServletRequest) {
        CustomResponseDto customResponseDto = (CustomResponseDto) queryCommentFacade.addComment(connectionID, projectID, commentDto);
        return ResponseEntity.status(customResponseDto.getErrorCode() != null ? HttpStatus.BAD_REQUEST : HttpStatus.OK).body(customResponseDto);
    }

    @GetMapping(value = "{connectionid}/{projectid}")
    public ResponseEntity listComments(@PathVariable(value = "connectionid") String connectionID,
                                       @PathVariable(value = "projectid") String projectID) {
        return ResponseEntity.ok(queryCommentFacade.listComments(connectionID, projectID));
    }

    @DeleteMapping(value = "{connectionid}/{projectid}")
    public ResponseEntity removeComment(@PathVariable(value = "connectionid") String connectionID,
                                        @PathVariable(value = "projectid") String projectID) {
        return ResponseEntity.ok(queryCommentFacade.deleteComment(connectionID, projectID));
    }
}
