package co.kr.coresolutions.resource;

import co.kr.coresolutions.commons.CustomResponseDto;
import co.kr.coresolutions.facades.IQueryDimensionFacade;
import com.fasterxml.jackson.databind.JsonNode;
import lombok.RequiredArgsConstructor;
import lombok.extern.slf4j.Slf4j;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RestController;

import javax.validation.Valid;

@RestController
@RequiredArgsConstructor
@Slf4j
public class QueryResourceDimension {
    private final IQueryDimensionFacade queryDimensionFacade;

    @GetMapping("{connectionid}/dimension/{lang_type}/{dimension_name}")
    public ResponseEntity queryWithDimension(@PathVariable(value = "connectionid") String connectionID,
                                             @PathVariable(value = "lang_type") String langType,
                                             @PathVariable(value = "dimension_name") String dimensionName) {
        CustomResponseDto customResponseDto = queryDimensionFacade.queryWithDimension(connectionID, langType, dimensionName, "", null);
        return ResponseEntity.status(customResponseDto.getErrorCode() != null ? HttpStatus.BAD_REQUEST : HttpStatus.OK)
                .header("Content-Type", "application/json; charset=utf-8").body(customResponseDto.getErrorCode() != null ? customResponseDto : customResponseDto.getDetailedMessage());
    }

    @PostMapping("{connectionid}/dimension/{lang_type}/{dimension_name}")
    public ResponseEntity postWithDimension(@PathVariable(value = "connectionid") String connectionID,
                                            @PathVariable(value = "lang_type") String langType,
                                            @PathVariable(value = "dimension_name") String dimensionName,
                                            @RequestBody @Valid JsonNode node) {
        CustomResponseDto customResponseDto = queryDimensionFacade.queryWithDimension(connectionID, langType, dimensionName, "", node);
        return ResponseEntity.status(customResponseDto.getErrorCode() != null ? HttpStatus.BAD_REQUEST : HttpStatus.OK)
                .header("Content-Type", "application/json; charset=utf-8").body(customResponseDto.getErrorCode() != null ? customResponseDto : customResponseDto.getDetailedMessage());
    }
}
