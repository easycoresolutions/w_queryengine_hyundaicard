package co.kr.coresolutions.resource;

import co.kr.coresolutions.commons.ResponseDto;
import co.kr.coresolutions.facades.IQueryControlFacade;
import co.kr.coresolutions.model.dtos.ExcelTemplateDto;
import co.kr.coresolutions.model.dtos.FileControlDto;
import lombok.RequiredArgsConstructor;
import lombok.SneakyThrows;
import lombok.extern.slf4j.Slf4j;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.DeleteMapping;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import javax.servlet.http.HttpServletResponse;
import javax.validation.Valid;
import java.util.Objects;

@RestController
@RequiredArgsConstructor
@RequestMapping(consumes = "*/*;charset=UTF-8")
@Slf4j
public class QueryResourceControl {

    private final IQueryControlFacade queryControlFacade;

    @PostMapping("/createfile")
    @SneakyThrows
    public ResponseEntity<ResponseDto> createFile(@Valid @RequestBody FileControlDto fileControlDto) {
//        System.out.println("call Api createFile !!!");
        ResponseDto responseDto;
        try {
            Objects.requireNonNull(fileControlDto.getFile());
            Objects.requireNonNull(fileControlDto.getControlFilename());
            responseDto = queryControlFacade.createFile(fileControlDto);
        } catch (RuntimeException e) {
            responseDto = ResponseDto.NotValidRequest();
        }
        return ResponseEntity.status(responseDto.getErrorCode() == 0 ? responseDto.getSuccessCode() : responseDto.getErrorCode()).body(responseDto);
    }

    @DeleteMapping("/deletedir")
    @SneakyThrows
    public ResponseEntity<ResponseDto> deleteDir(@Valid @RequestBody FileControlDto fileControlDto) {
//        System.out.println("call Api deletedir !!! ");
        ResponseDto responseDto = queryControlFacade.deleteDir(fileControlDto);
        return ResponseEntity.status(responseDto.getSuccessCode() == HttpStatus.OK.value() ? responseDto.getSuccessCode() : responseDto.getErrorCode()).body(responseDto);
    }

    @PostMapping("/reportsubmit")
    @SneakyThrows
    public ResponseEntity reportSubmit(@Valid @RequestBody FileControlDto fileControlDto, HttpServletResponse response) {
//        System.out.println("Call Api reportsubmit !!!");
        ResponseDto responseDto;
        try {
            Objects.requireNonNull(fileControlDto.getControlFilename());
            responseDto = queryControlFacade.reportSubmit(fileControlDto, response);
        } catch (RuntimeException e) {
            responseDto = ResponseDto.NotValidRequest();
        }
        return ResponseEntity.status(responseDto.getErrorCode() == 0 ? responseDto.getSuccessCode() : responseDto.getErrorCode()).body(responseDto.getMessage());
    }

    @PostMapping("reports/excel")
    public ResponseEntity reportExcel(@Valid @RequestBody ExcelTemplateDto excelTemplateDto) {
        return ResponseEntity.ok(queryControlFacade.reportExcel(excelTemplateDto));
    }
}
