package co.kr.coresolutions.resource;

import co.kr.coresolutions.commons.CustomResponseDto;
import co.kr.coresolutions.facades.IQueryEbmFacade;
import com.fasterxml.jackson.databind.JsonNode;
import lombok.RequiredArgsConstructor;
import lombok.extern.slf4j.Slf4j;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RestController;

@RestController
@RequiredArgsConstructor
@Slf4j
public class QueryResourceEbm {
    private final IQueryEbmFacade queryEbmFacade;

    @PostMapping(value = "ebm_rulecondition/{connectionid}", consumes = "*/*;charset=UTF-8")
    public ResponseEntity getEbm(@PathVariable("connectionid") String connectionID,
                                 @RequestBody JsonNode jsonObject) {
        CustomResponseDto customResponseDto = queryEbmFacade.BizCommon(connectionID, jsonObject, true);
        return ResponseEntity.status(customResponseDto.getErrorCode() != null ? HttpStatus.BAD_REQUEST : HttpStatus.OK)
                .body(customResponseDto.getErrorCode() != null ? customResponseDto : customResponseDto.getDetailedMessage().getBytes());
    }

    @PostMapping(value = "biz_rulecondition/{connectionid}", consumes = "*/*;charset=UTF-8")
    public ResponseEntity getBiz(@PathVariable("connectionid") String connectionID,
                                 @RequestBody JsonNode jsonObject) {
        CustomResponseDto customResponseDto = queryEbmFacade.BizCommon(connectionID, jsonObject, false);
        return ResponseEntity.status(customResponseDto.getErrorCode() != null ? HttpStatus.BAD_REQUEST : HttpStatus.OK)
                .body(customResponseDto.getErrorCode() != null ? customResponseDto : customResponseDto.getDetailedMessage().getBytes());
    }
}
