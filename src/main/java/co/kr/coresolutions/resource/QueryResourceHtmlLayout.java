package co.kr.coresolutions.resource;

import co.kr.coresolutions.model.HtmlLayout;
import co.kr.coresolutions.service.QueryService;
import co.kr.coresolutions.service.QueryServiceHtmlLayout;
import lombok.RequiredArgsConstructor;
import org.springframework.context.annotation.Scope;
import org.springframework.http.HttpHeaders;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.validation.BindingResult;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RestController;
import org.springframework.web.util.UriComponentsBuilder;

import javax.validation.Valid;
import java.io.IOException;
import java.nio.charset.StandardCharsets;

@RestController
@Scope(value = "request")
@RequiredArgsConstructor
public class QueryResourceHtmlLayout {
    private final QueryService queryService;
    private final QueryServiceHtmlLayout queryServiceHtmlLayout;

    @RequestMapping(value = "/deletehtmllayout/{ownerid}/{htmllayoutid}", method = RequestMethod.DELETE, consumes = "*/*;charset=UTF-8")
    public ResponseEntity<Object> deleteHtmlLayout(@PathVariable(value = "ownerid") String owner,
                                                   @PathVariable(value = "htmllayoutid") String htmllayoutid,
                                                   UriComponentsBuilder _ucBuilder) throws Exception {
        HttpHeaders __headers = new HttpHeaders();
        __headers.setLocation(_ucBuilder.path("/deletehtmllayout/" + owner + "/" + htmllayoutid).buildAndExpand().toUri());

        boolean resultDelete = queryServiceHtmlLayout.deleteHtmlLayout(owner, htmllayoutid);

        if (!resultDelete) {
            queryService.addLogging("DELETE", htmllayoutid + " doesn't exist.\n");
            return new ResponseEntity<Object>(htmllayoutid + " doesn't exist.", __headers, HttpStatus.CONFLICT);
        }

        String log = "deletehtmllayout: " + htmllayoutid;
        queryService.addLogging("DELETE", log);
        __headers.add("Content-Type", "application/json; charset=utf-8");
        return new ResponseEntity<Object>(htmllayoutid + " Deleted successfully", __headers, HttpStatus.OK);
    }


    @RequestMapping(value = {"/gethtmllayouts/{owner}"}, method = RequestMethod.GET)
    public ResponseEntity<Object> gethtmllayouts(@PathVariable String owner,
                                                 UriComponentsBuilder _ucBuilder) throws IOException {
        HttpHeaders __headers = new HttpHeaders();
        __headers.setLocation(_ucBuilder.path("/gethtmllayouts/" + owner).buildAndExpand().toUri());

        String resultgetHtmlLayouts = queryServiceHtmlLayout.getHtmlLayouts(owner);

        if (resultgetHtmlLayouts.equalsIgnoreCase("fail")) {
            queryService.addLogging("GET", owner + " doesn't exist.\n");
            return new ResponseEntity<Object>(owner + " doesn't exist.", __headers, HttpStatus.GONE);
        }
        __headers.add("Content-Type", "application/json; charset=utf-8");
        String log = " gethtmllayouts called " + owner + "\n";
        queryService.addLogging("GET", log);

        return new ResponseEntity<Object>(new String(resultgetHtmlLayouts.getBytes(), StandardCharsets.UTF_8), __headers, HttpStatus.OK);

    }

    @RequestMapping(value = {"/gethtmllayout/{ownerid}/{htmllayoutid}"}, method = RequestMethod.GET)
    public ResponseEntity<Object> getHtmlLayout(@PathVariable(value = "ownerid") String owner,
                                                @PathVariable(value = "htmllayoutid") String htmllayoutid,
                                                UriComponentsBuilder _ucBuilder) throws IOException {
        HttpHeaders __headers = new HttpHeaders();
        __headers.setLocation(_ucBuilder.path("/gethtmllayout/" + owner + "/" + htmllayoutid).buildAndExpand().toUri());

        String resultgetHtmlLayout = queryServiceHtmlLayout.getHtmlLayout(owner, htmllayoutid);
        if (resultgetHtmlLayout.equalsIgnoreCase("failhtmllayoutid")) {
            queryService.addLogging("GET", htmllayoutid + " doesn't exist.\n");
            return new ResponseEntity<Object>(htmllayoutid + " doesn't exist.", __headers, HttpStatus.CONFLICT);
        } else if (resultgetHtmlLayout.equalsIgnoreCase("failowner")) {
            queryService.addLogging("GET", owner + " directory doesn't exist.\n");
            return new ResponseEntity<Object>(owner + " doesn't exist.", __headers, HttpStatus.GONE);
        }
        String log = " getHtml called " + owner + " , " + htmllayoutid + "\n";
        queryService.addLogging("GET", log);
        __headers.add("Content-Type", "application/json; charset=utf-8");
        return new ResponseEntity<Object>(new String(resultgetHtmlLayout.getBytes(), StandardCharsets.UTF_8), __headers, HttpStatus.OK);

    }


    @RequestMapping(value = "/savehtmllayout/{owner}/{htmllayoutid}", method = RequestMethod.POST, consumes = "*/*;charset=UTF-8")
    public ResponseEntity<Object> savehtmllayout(@Valid @RequestBody HtmlLayout _htmlLayout,
                                                 @PathVariable(value = "owner") String owner,
                                                 @PathVariable(value = "htmllayoutid") String htmllayoutid,
                                                 UriComponentsBuilder _ucBuilder, BindingResult result) throws Exception {
        HttpHeaders __headers = new HttpHeaders();
        __headers.setLocation(_ucBuilder.path("/savehtmllayout/" + owner + "/" + htmllayoutid).buildAndExpand().toUri());

        if (result.hasErrors()) {
            queryService.addLogging("POST", "Savehtmllayout : Some errors , Please check ur json content!" + "\n");
            return new ResponseEntity<Object>("Savehtmllayout : Some errors , Please check ur json content!", __headers, HttpStatus.BAD_REQUEST);
        }

        String resultsaveLayout = queryServiceHtmlLayout.saveHtmlLayout(owner, htmllayoutid, _htmlLayout);
        if (resultsaveLayout.equalsIgnoreCase("fail")) {
            queryService.addLogging("POST", "can't create the html layout " + htmllayoutid + " for the owner " + owner + "\n");
            return new ResponseEntity<Object>("can't create the html layout " + htmllayoutid + " for the owner " + owner, __headers, HttpStatus.BANDWIDTH_LIMIT_EXCEEDED);
        }

        String log = htmllayoutid + " html layout uploaded successfully\n";
        queryService.addLogging("POST", log);
        __headers.add("Content-Type", "application/json; charset=utf-8");
        return new ResponseEntity<Object>(htmllayoutid + " html layout uploaded successfully", __headers, HttpStatus.OK);

    }

}
