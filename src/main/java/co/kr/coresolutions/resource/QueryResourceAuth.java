package co.kr.coresolutions.resource;

import co.kr.coresolutions.commons.CustomResponseDto;
import co.kr.coresolutions.facades.IQueryAuthFacade;
import co.kr.coresolutions.model.dtos.LoginDto;
import lombok.RequiredArgsConstructor;
import lombok.SneakyThrows;
import lombok.extern.slf4j.Slf4j;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import javax.servlet.http.HttpServletRequest;
import javax.validation.Valid;

@RestController
@RequiredArgsConstructor
@RequestMapping(value = QueryResourceAuth.V1_AUTH_PATH)
@Slf4j
public class QueryResourceAuth {
    static final String V1_AUTH_PATH = "/auth/";
    private final IQueryAuthFacade queryAuthFacade;

    @PostMapping("login")
    public ResponseEntity<CustomResponseDto> login(@Valid @RequestBody LoginDto loginDto) {
        return ResponseEntity.ok(queryAuthFacade.login(loginDto));
    }

/*
    @PostMapping("user/Add")
    @SneakyThrows
    public ResponseEntity<CustomResponseDto> signUp(@Valid @RequestBody LoginDto loginDto) {
        CustomResponseDto customResponseDto = queryAuthFacade.addUser(loginDto);
        return ResponseEntity.status(customResponseDto.getErrorCode() != null ? HttpStatus.BAD_REQUEST : HttpStatus.OK).body(customResponseDto);
    }
*/

    @PostMapping("user/logout")
    @SneakyThrows
    public ResponseEntity<CustomResponseDto> logout(HttpServletRequest request) {
        CustomResponseDto customResponseDto = queryAuthFacade.logout(request);
        return ResponseEntity.status(customResponseDto.getErrorCode() != null ? HttpStatus.BAD_REQUEST : HttpStatus.OK).body(customResponseDto);
    }

/*
    @DeleteMapping("user/remove")
    @SneakyThrows
    public ResponseEntity<CustomResponseDto> remove(@Valid @RequestBody LoginDto loginDto) {
        CustomResponseDto customResponseDto = queryAuthFacade.removeUser(loginDto);
        return ResponseEntity.status(customResponseDto.getErrorCode() != null ? HttpStatus.BAD_REQUEST : HttpStatus.OK).body(customResponseDto);
    }

    @PutMapping("user/ChangePw")
    @SneakyThrows
    public ResponseEntity<CustomResponseDto> changePassword(@Valid @RequestBody LoginDto loginDto) {
        CustomResponseDto customResponseDto = queryAuthFacade.changePwUser(loginDto.getUserId(), loginDto.getPassword(), loginDto.getNewPassword());
        return ResponseEntity.status(customResponseDto.getErrorCode() != null ? HttpStatus.BAD_REQUEST : HttpStatus.OK).body(customResponseDto);
    }

    @PutMapping("user/initpw")
    @SneakyThrows
    public ResponseEntity<CustomResponseDto> initPassword(@RequestBody JsonNode jsonNode) {
        CustomResponseDto customResponseDto = queryAuthFacade.initPassword(jsonNode);
        return ResponseEntity.status(customResponseDto.getErrorCode() != null ? HttpStatus.BAD_REQUEST : HttpStatus.OK).body(customResponseDto);
    }

*/



    @GetMapping("blacklist")
    @SneakyThrows
    public ResponseEntity<CustomResponseDto> getBackList() {
        CustomResponseDto customResponseDto = queryAuthFacade.getBlackList();
        return ResponseEntity.status(customResponseDto.getErrorCode() != null ? HttpStatus.BAD_REQUEST : HttpStatus.OK).body(customResponseDto);
    }


}
