package co.kr.coresolutions.resource;

import co.kr.coresolutions.commons.ResponseCodes;
import co.kr.coresolutions.commons.ResponseDto;
import co.kr.coresolutions.facades.IQueryDataFlowFacade;
import co.kr.coresolutions.model.dtos.FileBatchDto;
import com.fasterxml.jackson.databind.node.ObjectNode;
import io.swagger.annotations.ApiOperation;
import lombok.RequiredArgsConstructor;
import lombok.SneakyThrows;
import lombok.extern.slf4j.Slf4j;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.DeleteMapping;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import javax.validation.Valid;

@RestController
@RequiredArgsConstructor
@RequestMapping(value = QueryResourceDataFlow.V1_DATA_FLOW_PATH)
@Slf4j
public class QueryResourceDataFlow {

    static final String V1_DATA_FLOW_PATH = "/dataflow";

    private final IQueryDataFlowFacade queryDataFlowFacade;

    @PostMapping("/{userId}/scriptupload")
    @ApiOperation(value = "upload file for later execution of data flow batch")
    @SneakyThrows
    public ResponseEntity<ResponseDto> fileUpload(@PathVariable("userId") String userId,
                                                  @Valid @RequestBody FileBatchDto fileBatchDto) {

        userId = "2134";

        ResponseDto responseDto = queryDataFlowFacade.fileBatchUpload(userId, fileBatchDto);
        return ResponseEntity.status(responseDto.getErrorCode() == 0 ? responseDto.getSuccessCode() : responseDto.getErrorCode()).body(responseDto);
    }

    @DeleteMapping("/{userId}/scriptdelete/{fileName}")
    @ApiOperation(value = "delete file from data flow dir")
    @SneakyThrows
    public ResponseEntity<ResponseDto> fileDelete(@PathVariable("userId") String userId,
                                                  @PathVariable("fileName") String fileName) {

        ResponseDto responseDto = queryDataFlowFacade.fileDelete(userId, fileName);
        return ResponseEntity.status(responseDto.getSuccessCode() == HttpStatus.OK.value() ? responseDto.getSuccessCode() : responseDto.getErrorCode()).build();
    }

    @PostMapping("/{userId}/submit/{fileName}")
    @ApiOperation(value = "submit script batch from file resides in dataFlowDir/userId")
    @SneakyThrows
    public ResponseEntity submit(@RequestBody(required = false) ObjectNode nodes,
                                 @PathVariable("userId") String userId,
                                 @PathVariable("fileName") String fileName) {

        ResponseDto responseDto = queryDataFlowFacade.submit(userId, fileName);
        return ResponseEntity.status(responseDto.getErrorCode() == 0 ? responseDto.getSuccessCode() : responseDto.getErrorCode()).body(responseDto.getMessage());
    }

    @GetMapping("/{userId}/scriptview/{fileName}")
    @ApiOperation(value = "fetch file content")
    @SneakyThrows
    public ResponseEntity findOne(@PathVariable("userId") String userId,
                                  @PathVariable("fileName") String fileName) {

        String response = queryDataFlowFacade.findOne(userId, fileName);
        return ResponseEntity.status(response.startsWith("file read error") ? ResponseCodes.NOT_FOUND_ERROR : ResponseCodes.OK_RESPONSE)
                .body(response);
    }

    @GetMapping("/{userId}/logview/{fileName}")
    @ApiOperation(value = "fetch log content")
    @SneakyThrows
    public ResponseEntity findOneLog(@PathVariable("userId") String userId,
                                     @PathVariable("fileName") String fileName) {

        String response = queryDataFlowFacade.findOneLog(userId, fileName);
        return ResponseEntity.status(response.startsWith("file read error") ? ResponseCodes.NOT_FOUND_ERROR : ResponseCodes.OK_RESPONSE)
                .body(response);
    }
}
