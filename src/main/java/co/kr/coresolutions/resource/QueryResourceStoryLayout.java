package co.kr.coresolutions.resource;

import co.kr.coresolutions.model.StoryLayout;
import co.kr.coresolutions.service.QueryService;
import co.kr.coresolutions.service.QueryServiceStoryLayout;
import lombok.RequiredArgsConstructor;
import org.springframework.context.annotation.Scope;
import org.springframework.http.HttpHeaders;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.validation.BindingResult;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RestController;
import org.springframework.web.util.UriComponentsBuilder;

import javax.validation.Valid;
import java.io.IOException;
import java.nio.charset.StandardCharsets;

@RestController
@Scope(value = "request")
@RequiredArgsConstructor
public class QueryResourceStoryLayout {
    private final QueryService queryService;
    private final QueryServiceStoryLayout queryServiceStoryLayout;

    @RequestMapping(value = "/deletestorylayout/{ownerid}/{storylayoutid}", method = RequestMethod.DELETE, consumes = "*/*;charset=UTF-8")
    public ResponseEntity<Object> deleteStoryLayout(@PathVariable(value = "ownerid") String owner,
                                                    @PathVariable(value = "storylayoutid") String storylayoutid,
                                                    UriComponentsBuilder _ucBuilder) throws Exception {
        HttpHeaders __headers = new HttpHeaders();
        __headers.setLocation(_ucBuilder.path("/deletestorylayout/" + owner + "/" + storylayoutid).buildAndExpand().toUri());

        boolean resultDelete = queryServiceStoryLayout.deleteStoryLayout(owner, storylayoutid);

        if (!resultDelete) {
            queryService.addLogging("DELETE", storylayoutid + " doesn't exist.\n");
            return new ResponseEntity<Object>(storylayoutid + " doesn't exist.", __headers, HttpStatus.CONFLICT);
        }

        String log = "deleteStoryLayout: " + storylayoutid;
        queryService.addLogging("DELETE", log);
        __headers.add("Content-Type", "application/json; charset=utf-8");
        return new ResponseEntity<Object>(storylayoutid + " Deleted successfully", __headers, HttpStatus.OK);
    }


    @RequestMapping(value = {"/getstorylayouts/{owner}"}, method = RequestMethod.GET)
    public ResponseEntity<Object> getStorylayouts(@PathVariable String owner,
                                                  UriComponentsBuilder _ucBuilder) throws IOException {
        HttpHeaders __headers = new HttpHeaders();
        __headers.setLocation(_ucBuilder.path("/getstorylayouts/" + owner).buildAndExpand().toUri());

        String resultgetStoryLayouts = queryServiceStoryLayout.getStoryLayouts(owner);

        if (resultgetStoryLayouts.equalsIgnoreCase("fail")) {
            queryService.addLogging("GET", owner + " doesn't exist.\n");
            return new ResponseEntity<Object>(owner + " doesn't exist.", __headers, HttpStatus.GONE);
        }
        __headers.add("Content-Type", "application/json; charset=utf-8");
        String log = " getStorylayouts called " + owner + "\n";
        queryService.addLogging("GET", log);
        return new ResponseEntity<Object>(new String(resultgetStoryLayouts.getBytes(), StandardCharsets.UTF_8), __headers, HttpStatus.OK);
    }

    @RequestMapping(value = {"/getstorylayout/{ownerid}/{storylayoutid}"}, method = RequestMethod.GET)
    public ResponseEntity<Object> getStoryLayout(@PathVariable(value = "ownerid") String owner,
                                                 @PathVariable(value = "storylayoutid") String storylayoutid,
                                                 UriComponentsBuilder _ucBuilder) throws IOException {
        HttpHeaders __headers = new HttpHeaders();
        __headers.setLocation(_ucBuilder.path("/getstorylayout/" + owner + "/" + storylayoutid).buildAndExpand().toUri());

        String resultgetStoryLayout = queryServiceStoryLayout.getStoryLayout(owner, storylayoutid);
        if (resultgetStoryLayout.equalsIgnoreCase("failstorylayoutid")) {
            queryService.addLogging("GET", storylayoutid + " doesn't exist.\n");
            return new ResponseEntity<Object>(storylayoutid + " doesn't exist.", __headers, HttpStatus.CONFLICT);
        } else if (resultgetStoryLayout.equalsIgnoreCase("failowner")) {
            queryService.addLogging("GET", owner + " directory doesn't exist.\n");
            return new ResponseEntity<Object>(owner + " doesn't exist.", __headers, HttpStatus.GONE);
        }
        String log = " getStoryLayout called owner " + owner + " , storylayoutid " + storylayoutid + "\n";
        queryService.addLogging("GET", log);
        __headers.add("Content-Type", "application/json; charset=utf-8");
        return new ResponseEntity<Object>(new String(resultgetStoryLayout.getBytes(), StandardCharsets.UTF_8), __headers, HttpStatus.OK);
    }


    @RequestMapping(value = "/savestorylayout/{owner}/{storylayoutid}", method = RequestMethod.POST, consumes = "*/*;charset=UTF-8")
    public ResponseEntity<Object> saveContensLayout(@Valid @RequestBody StoryLayout _storyLayout,
                                                    @PathVariable(value = "owner") String owner,
                                                    @PathVariable(value = "storylayoutid") String storylayoutid,
                                                    UriComponentsBuilder _ucBuilder, BindingResult result) throws Exception {
        HttpHeaders __headers = new HttpHeaders();
        __headers.setLocation(_ucBuilder.path("/savestorylayout/" + owner + "/" + storylayoutid).buildAndExpand().toUri());

        if (result.hasErrors()) {
            queryService.addLogging("POST", "SaveStorylayout : Some errors , Please check ur json content!" + "\n");
            return new ResponseEntity<Object>("SaveStorylayout : Some errors , Please check ur json content!", __headers, HttpStatus.BAD_REQUEST);
        }

        String resultsaveLayout = queryServiceStoryLayout.saveStoryLayout(owner, storylayoutid, _storyLayout);
        if (resultsaveLayout.equalsIgnoreCase("fail")) {
            queryService.addLogging("POST", "can't create the story layout " + storylayoutid + " for the owner " + owner + "\n");
            return new ResponseEntity<Object>("can't create the story layout " + storylayoutid + " for the owner " + owner, __headers, HttpStatus.BANDWIDTH_LIMIT_EXCEEDED);
        }

        String log = storylayoutid + " story layout uploaded successfully\n";
        queryService.addLogging("POST", log);
        __headers.add("Content-Type", "application/json; charset=utf-8");
        return new ResponseEntity<Object>(storylayoutid + " story layout uploaded successfully", __headers, HttpStatus.OK);

    }

}
