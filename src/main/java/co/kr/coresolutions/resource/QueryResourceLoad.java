package co.kr.coresolutions.resource;

import co.kr.coresolutions.facades.IQueryLoadFacade;
import co.kr.coresolutions.model.UnLoad;
import lombok.RequiredArgsConstructor;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.DeleteMapping;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RestController;

import javax.validation.Valid;

@RestController
@RequiredArgsConstructor
public class QueryResourceLoad {
    private final IQueryLoadFacade queryLoadFacade;

    @PostMapping(value = "/unload/{connectionid}", consumes = "*/*;charset=UTF-8")
    public ResponseEntity unload(@Valid @RequestBody UnLoad unLoad,
                                 @PathVariable("connectionid") String connectionID) {
        return ResponseEntity.ok(queryLoadFacade.unload(unLoad, connectionID));
    }

    //reserved to clickhouse dbms
    @PostMapping("/unload2/{connectionid}")
    public ResponseEntity unloadClickHouse(@Valid @RequestBody UnLoad unLoad,
                                           @PathVariable("connectionid") String connectionID) {
        return ResponseEntity.ok(queryLoadFacade.unloadClickHouse(unLoad, connectionID));
    }

    @GetMapping(value = {"unload2/monitor"})
    public ResponseEntity monitor() {
        return ResponseEntity.ok(queryLoadFacade.monitor());
    }

    @DeleteMapping(value = {"unload2/monitor/{unload2_id}"})
    public ResponseEntity deleteCommand(@PathVariable("unload2_id") String unloadID) {
        return ResponseEntity.ok(queryLoadFacade.deleteUnload(unloadID));
    }

}
