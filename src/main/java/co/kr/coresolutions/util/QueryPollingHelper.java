package co.kr.coresolutions.util;

import co.kr.coresolutions.model.dtos.KafkaDto;
import co.kr.coresolutions.service.KafkaConstants;
import com.fasterxml.jackson.databind.JsonNode;
import com.fasterxml.jackson.databind.ObjectMapper;
import lombok.RequiredArgsConstructor;
import org.apache.kafka.clients.consumer.ConsumerConfig;
import org.apache.kafka.common.serialization.StringSerializer;
import org.json.JSONObject;
import org.springframework.cache.annotation.Cacheable;
import org.springframework.stereotype.Component;

import java.util.Arrays;
import java.util.List;
import java.util.Map;
import java.util.Properties;
import java.util.stream.Collectors;
import java.util.stream.StreamSupport;

@Component
@RequiredArgsConstructor
public class QueryPollingHelper {
    private final ObjectMapper objectMapper;

    //    @Cacheable(value = "insertSqlQuery", cacheManager = "cacheManagerCaffeine")
    public String getInsertSqlQuery(Map<String, String> dataTypes, JsonNode jsonNode, String targetTable, String dbms) {
        StringBuilder stringBuilder = new StringBuilder();
        StringBuilder globalValues = new StringBuilder();
        StreamSupport.stream(jsonNode.spliterator(), false).forEach(node -> {
            JSONObject jsonObject = new JSONObject(node.toString());
            stringBuilder.append("(");
            dataTypes.forEach((key, value) -> {
                key = key.toUpperCase();
                if (jsonObject.has(key)) {
                    String objectValue = jsonObject.get(key).toString();
                    if (!value.toLowerCase().contains("char")) {
                        stringBuilder.append(objectValue).append(",");
                    } else {
                        if (objectValue.toLowerCase().equals("null")) {
                            stringBuilder.append(objectValue).append(",");
                        } else {
                            objectValue = objectValue.replaceAll("'", "''");
                            stringBuilder.append("'").append(objectValue).append("',");
                        }
                    }
                } else {
                    stringBuilder.append("null").append(",");
                }
            });
            globalValues.append(stringBuilder.substring(0, stringBuilder.length() - 1)).append("),,");
            stringBuilder.delete(0, stringBuilder.length());
        });

        stringBuilder.delete(0, stringBuilder.length());
        stringBuilder.append("(");
        dataTypes.keySet().forEach(s -> stringBuilder.append(s).append(","));
        String finalNames = (stringBuilder.substring(0, stringBuilder.length() - 1) + ")").toUpperCase();
        if (dbms.equalsIgnoreCase("ORACLE")) {
            return "INSERT ALL\n" + (Arrays.stream(globalValues.substring(0, globalValues.length() - 2).split(",,"))
                    .map(s1 -> " INTO " + targetTable + finalNames + " VALUES " + s1)
                    .collect(Collectors.joining("\n"))
            ) + " \nSELECT COUNT(*) FROM " + targetTable;
        } else {
            return "INSERT INTO " + targetTable + finalNames + " VALUES " + globalValues.substring(0, globalValues.length() - 2).replace(",,", ",") + ";";
        }
    }

    //    @Cacheable(value = "getOutData", cacheManager = "cacheManagerCaffeine")
    public List<String> getOutData(Object jsonOrList, String format) {
        if (format.equalsIgnoreCase("csv")) {
            return (List<String>) jsonOrList;
        } else {
            return StreamSupport.stream(((JsonNode) jsonOrList).spliterator(), false).map(JsonNode::toString).collect(Collectors.toList());
        }
    }

    @Cacheable(value = "getProperties", cacheManager = "cacheManagerCaffeine")
    public Properties getProperties(String fileContent) {
        try {
            KafkaDto kafkaDto = objectMapper.readValue(fileContent, KafkaDto.class);
            Properties properties = new Properties();
            properties.put(ConsumerConfig.BOOTSTRAP_SERVERS_CONFIG, kafkaDto.getIp());
            properties.put(ConsumerConfig.GROUP_ID_CONFIG, KafkaConstants.GROUP_ID_CONFIG);
            properties.put("key.serializer", StringSerializer.class);
            properties.put("value.serializer", StringSerializer.class);
            properties.put("topic", kafkaDto.getTopic());
            properties.put("batch.size", 10000);
            properties.put("retries", 0);
            properties.put("max.block.ms", 10);
            properties.put("delivery.timeout.ms", 3000);
            properties.put("connections.max.idle.ms", 10000);
            properties.put("request.timeout.ms", 5000);
            return properties;
        } catch (Exception e) {
            e.printStackTrace();
        }
        return null;
    }
}
