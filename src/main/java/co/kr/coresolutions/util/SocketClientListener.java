package co.kr.coresolutions.util;

import co.kr.coresolutions.enums.StatusConnection;
import co.kr.coresolutions.service.QueryServicePythonSocketClient;
import co.kr.coresolutions.service.QueryServiceRSocketClient;
import lombok.extern.slf4j.Slf4j;
import org.springframework.messaging.handler.annotation.MessageMapping;
import org.springframework.messaging.handler.annotation.Payload;
import org.springframework.stereotype.Component;

import java.util.Arrays;
import java.util.List;

@Slf4j
@Component
public class SocketClientListener {
    @MessageMapping("r/status")
    public void rStatusUpdate(@Payload String details) {
        List<String> payload = Arrays.asList(details.split("\n"));
        StatusConnection statusConnection = StatusConnection.valueOf(payload.get(1));
        if (!statusConnection.name().equals(StatusConnection.OPEN.name())) {
            QueryServiceRSocketClient.sessionRSocketRequester.remove(payload.get(0));
        }
    }

    @MessageMapping("r/userSituation")
    public void rRunningUsers(@Payload String userID) {
        QueryServiceRSocketClient.sessionRSocketRunningUsers.remove(userID);
    }

    @MessageMapping("python/status")
    public void pyStatusUpdate(@Payload String details) {
        List<String> payload = Arrays.asList(details.split("\n"));
        StatusConnection statusConnection = StatusConnection.valueOf(payload.get(1));
        if (!statusConnection.name().equals(StatusConnection.OPEN.name())) {
            QueryServicePythonSocketClient.sessionPythonSocketRequester.remove(payload.get(0));
        }
    }

    @MessageMapping("python/userSituation")
    public void pyRunningUsers(@Payload String userID) {
        QueryServicePythonSocketClient.sessionPythonSocketRunningUsers.remove(userID);
    }

}

