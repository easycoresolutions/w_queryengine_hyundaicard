package co.kr.coresolutions.util;

import org.springframework.security.crypto.password.PasswordEncoder;

import javax.crypto.Cipher;
import javax.crypto.SecretKey;
import javax.crypto.spec.IvParameterSpec;
import javax.crypto.spec.SecretKeySpec;
import java.nio.charset.StandardCharsets;

public class AES256Cipher implements PasswordEncoder {
    private final static String secretKey = "SecRetKey:EasyCore-Solutions-Inc"; //32bit
    private static String IV = secretKey.substring(0, 16); //16bit

    @Override
    public String encode(CharSequence charSequence) {
        String str = charSequence.toString();
        if (str == null) {
            return str;
        }
        byte[] keyData = secretKey.getBytes();
        IV = secretKey.substring(0, 16);

        SecretKey secureKey = new SecretKeySpec(keyData, "AES");

        Cipher c;
        try {
            c = Cipher.getInstance("AES/CBC/PKCS5Padding");
            c.init(Cipher.ENCRYPT_MODE, secureKey, new IvParameterSpec(IV.getBytes()));
            byte[] encrypted = c.doFinal(str.getBytes(StandardCharsets.UTF_8));
            return new String(java.util.Base64.getEncoder().encode(encrypted));
        } catch (Exception e) {
            return null;
        }
    }

    @Override
    public boolean matches(CharSequence charSequence, String str) {
        if (str == null) {
            return false;
        }
        byte[] keyData = secretKey.getBytes();
        IV = secretKey.substring(0, 16);
        SecretKey secureKey = new SecretKeySpec(keyData, "AES");
        try {
            Cipher c = Cipher.getInstance("AES/CBC/PKCS5Padding");
            c.init(Cipher.DECRYPT_MODE, secureKey, new IvParameterSpec(IV.getBytes(StandardCharsets.UTF_8)));
            byte[] byteStr = java.util.Base64.getDecoder().decode(str);
            return new String(c.doFinal(byteStr), StandardCharsets.UTF_8).equals(charSequence.toString());
        } catch (Exception e) {
            return false;
        }
    }

    public String decode(String pwEncoded) {
        try {
            if (pwEncoded == null) {
                return "";
            }
            byte[] keyData = secretKey.getBytes();
            IV = secretKey.substring(0, 16);
            SecretKey secureKey = new SecretKeySpec(keyData, "AES");
            Cipher cipher = Cipher.getInstance("AES/CBC/PKCS5Padding");
            cipher.init(Cipher.DECRYPT_MODE, secureKey, new IvParameterSpec(IV.getBytes(StandardCharsets.UTF_8)));
            byte[] byteStr = java.util.Base64.getDecoder().decode(pwEncoded);
            return new String(cipher.doFinal(byteStr), StandardCharsets.UTF_8);
        } catch (Exception e) {
            return "";
        }
    }

/*
	public static void main(String[] args) {
		try {
			if (args.length == 1) {
				System.out.println(args[0] + " -> " + AES256Cipher.AES_Encode(args[0]));
			}
			if (args.length == 2) {
				if (args[0].equalsIgnoreCase("SHA")) {
					System.out.println(args[1] + " -> " + AES256Cipher.SHA_Encode(args[1]));
				} else {
					System.out.println(args[1] + " <- " + AES256Cipher.AES_Decode(args[1]));
				}
			}
		} catch (Exception e) {
			e.printStackTrace();
		}
	}


	public static String SHA_Encode(String str) throws NoSuchAlgorithmException {
		if (str == null) {
			return str;
		}
		str = secretKey + str;

	    String output = "";
	    StringBuffer sb = new StringBuffer();
	    MessageDigest md = MessageDigest.getInstance("SHA-512");
	    md.update(str.getBytes());
	    byte[] msgb = md.digest();

	    for (int i = 0; i < msgb.length; i++) {
	        byte temp = msgb[i];
	        String hex = Integer.toHexString(temp & 0xFF);
	        while (hex.length() < 2) {
	        	hex = "0" + hex;
	        }
	        hex = hex.substring(hex.length() - 2);
	        sb.append(hex);
	    }
	    output = sb.toString();

	    return output;
	}
	*/
}
