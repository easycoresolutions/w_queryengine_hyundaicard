package co.kr.coresolutions.facades.Impl;

import co.kr.coresolutions.commons.CustomResponseDto;
import co.kr.coresolutions.enums.QueueName;
import co.kr.coresolutions.facades.IQueryLazyCommandFacade;
import co.kr.coresolutions.facades.IQuerySocketFacade;
import co.kr.coresolutions.model.dtos.LazyCommandDto;
import co.kr.coresolutions.model.dtos.SocketBodyDto;
import co.kr.coresolutions.service.QueryService;
import co.kr.coresolutions.util.CoreEntry;
import com.fasterxml.jackson.annotation.JsonInclude;
import com.fasterxml.jackson.databind.ObjectMapper;
import lombok.RequiredArgsConstructor;
import lombok.SneakyThrows;
import lombok.extern.slf4j.Slf4j;
import org.json.JSONArray;
import org.json.JSONObject;
import org.springframework.scheduling.TaskScheduler;
import org.springframework.scheduling.concurrent.ConcurrentTaskScheduler;
import org.springframework.stereotype.Component;

import javax.annotation.PostConstruct;
import java.io.IOException;
import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.Arrays;
import java.util.Date;
import java.util.LinkedList;
import java.util.List;
import java.util.Queue;
import java.util.stream.Collectors;

import static co.kr.coresolutions.enums.QueueName.LONGMIDDLEQ;
import static co.kr.coresolutions.enums.QueueName.LONGQ;
import static co.kr.coresolutions.enums.QueueName.LOWQ;
import static co.kr.coresolutions.enums.QueueName.MIDDLELOWQ;
import static co.kr.coresolutions.enums.QueueName.MIDDLEQ;

@Component
@RequiredArgsConstructor
@Slf4j
public class QueryLazyCommandFacade implements IQueryLazyCommandFacade {

    public static final Queue<CoreEntry> coreEntriesLONGQ = new LinkedList<>();
    public static final Queue<CoreEntry> coreEntriesLONGMIDDLEQ = new LinkedList<>();
    public static final Queue<CoreEntry> coreEntriesMIDDLEQ = new LinkedList<>();
    public static final Queue<CoreEntry> coreEntriesMIDDLELOWQ = new LinkedList<>();
    public static final Queue<CoreEntry> coreEntriesLOWQ = new LinkedList<>();
    private final QueryService queryService;
    private final IQuerySocketFacade querySocketFacade;
    private final ObjectMapper objectMapper;

    @PostConstruct
    @SneakyThrows
    public void init() {
        TaskScheduler threadPoolTaskExecutorLONGQ = new ConcurrentTaskScheduler();
        threadPoolTaskExecutorLONGQ.scheduleAtFixedRate(() -> runForEveryQueue(coreEntriesLONGQ), 1000);
        TaskScheduler threadPoolTaskExecutorLONGMIDDLEQ = new ConcurrentTaskScheduler();
        threadPoolTaskExecutorLONGMIDDLEQ.scheduleAtFixedRate(() -> runForEveryQueue(coreEntriesLONGMIDDLEQ), 1000);
        TaskScheduler threadPoolTaskExecutorMIDDLEQ = new ConcurrentTaskScheduler();
        threadPoolTaskExecutorMIDDLEQ.scheduleAtFixedRate(() -> runForEveryQueue(coreEntriesMIDDLEQ), 1000);
        TaskScheduler threadPoolTaskExecutorMIDDLELOWQ = new ConcurrentTaskScheduler();
        threadPoolTaskExecutorMIDDLELOWQ.scheduleAtFixedRate(() -> runForEveryQueue(coreEntriesMIDDLELOWQ), 1000);
        TaskScheduler threadPoolTaskExecutorLOWQ = new ConcurrentTaskScheduler();
        threadPoolTaskExecutorLOWQ.scheduleAtFixedRate(() -> runForEveryQueue(coreEntriesLOWQ), 1000);
    }

    public void runForEveryQueue(Queue<CoreEntry> coreEntries) {
        CoreEntry coreEntry = coreEntries.poll();
            if (coreEntry != null) {
                CoreEntry subCoreEntry = (CoreEntry) coreEntry.getValue();
                String commandID = (String) subCoreEntry.getKey();
                LazyCommandDto command = (LazyCommandDto) subCoreEntry.getValue();
                boolean isCommandFileExists = queryService.isCommandFileExists(command.getCommand(), true);
                String commandAfterKeyReplace;
                try {
                    if (!isCommandFileExists) {
                        queryService.addLogging("POST", "lazyCommand executed by scheduler, command file\t" + command.getCommand() + " (information) doesn't exist." + "\n");
                        querySocketFacade.send(SocketBodyDto.builder().userId(command.getUserId()).key("COMMAND-LOG").message("command\noutput message:\n--------------\n\t\t" +
                                "lazyCommand executed by scheduler, command file\t" + command.getCommand() + " (information) doesn't exist." + "\n" + "\nBODY:\n--------------\n\t\t/command/\n\tPOST\n\t\"" + objectMapper.setSerializationInclusion(JsonInclude.Include.NON_NULL).writeValueAsString(command) + "\"\n").build());
                        return;
                    }
                    commandAfterKeyReplace = queryService.getFormattedCommand(command.getCommand(), command.getParameter(), true);
                    if (commandAfterKeyReplace.startsWith("Error") || commandAfterKeyReplace.length() == 0) {
                        queryService.addLogging("POST", "lazyCommand executed by scheduler, Error\t" + commandAfterKeyReplace + "\n");
                        querySocketFacade.send(SocketBodyDto.builder().userId(command.getUserId()).key("COMMAND-LOG").message("command\noutput message:\n--------------\n\t\t" +
                                "lazyCommand executed by scheduler, Error\t" + commandAfterKeyReplace + "\n" + "\nBODY:\n--------------\n\t\t/command/\n\tPOST\n\t\"" +
                                objectMapper.setSerializationInclusion(JsonInclude.Include.NON_NULL).writeValueAsString(command) + "\"\n").build());
                        return;
                    }
                    queryService.addLogging("POST", "lazyCommand/send executed by scheduler, Started\n");
                    querySocketFacade.send(SocketBodyDto.builder().userId(command.getUserId()).key("COMMAND-LOG").message("POST /lazycommand/send endpoint is STARTED, commandID\t" + commandID + "\n").build());
//                    String resultExecute = queryService.executeCommand("lazyCommand_" + command.getCommand(), commandAfterKeyReplace, commandID, command.getOwner(), null);
                    String resultExecute = queryService.executeCommand("lazyCommand_" + command.getCommand(), commandAfterKeyReplace, commandID, command.getOwner(), null, "INTERNAL");
                    queryService.addLogging("POST", "lazyCommand\noutput message:\n--------------\n\t\t" + resultExecute + "\nBODY:\n--------------\n\t" +
                            "\t/lazycommand/send\n\tPOST\n\t\"" + objectMapper.setSerializationInclusion(JsonInclude.Include.NON_NULL).writeValueAsString(command) + "\"\n");
                    querySocketFacade.send(SocketBodyDto.builder().userId(command.getUserId()).key("COMMAND-LOG").message("POST /lazycommand/send endpoint is ENDED, result:\t"
                            + (resultExecute.isEmpty() ? "Empty result returned by the process from CommandID\t" + commandID : resultExecute) + "\n").build());

                } catch (IOException e) {
                    queryService.addLogging("POST", "lazyCommand executed by scheduler ENDED, Error Execution is (" + e.getMessage() + "),\tcommandId is\t" +
                            commandID + "\n");
                }
            }
    }

    @Override
    public CustomResponseDto send(LazyCommandDto lazyCommandDto) {
        queryService.addLogging("POST", "lazycommand/send api triggered with command\t" + lazyCommandDto.getCommand() + "\n");
        querySocketFacade.send(SocketBodyDto.builder().userId(lazyCommandDto.getUserId()).key("COMMAND-LOG").message("POST /lazycommand/send triggered, command\t" + lazyCommandDto.getCommand() + "\n").build());
        QueueName queueName;
        try {
            queueName = QueueName.valueOf(lazyCommandDto.getQueueName());
        } catch (IllegalArgumentException e) {
            return CustomResponseDto.builder().ErrorCode(10).ErrorMessage("Invalid Queue Name provided!").build();
        }
        Date execDateTime = null;
        if (!lazyCommandDto.getExecDateTime().isEmpty()) {
            List<String> formats = Arrays.asList("yyyyMMddHHmmss", "yyyyMMdd:HH:mm:ss");
            for (String format : formats) {
                try {
                    execDateTime = new SimpleDateFormat(format).parse(lazyCommandDto.getExecDateTime());
                } catch (ParseException e) {
                }
            }
            if (execDateTime == null) {
                return CustomResponseDto.builder().ErrorCode(11).ErrorMessage("Invalid datetime format provided.").build();
            }
        }

        String requestedDate = new SimpleDateFormat("yyyyMMddHHmmss").format(new Date());
        String commandID = "C" + lazyCommandDto.getUserId() + requestedDate;
        lazyCommandDto.setRequestedDateTime(requestedDate);
        CoreEntry coreEntry = CoreEntry.builder().key(queueName).value(CoreEntry.builder().key(commandID).value(lazyCommandDto).build()).build();
        switch (queueName) {
            case LONGMIDDLEQ:
                coreEntriesLONGMIDDLEQ.add(coreEntry);
                break;
            case LONGQ:
                coreEntriesLONGQ.add(coreEntry);
                break;
            case LOWQ:
                coreEntriesLOWQ.add(coreEntry);
                break;
            case MIDDLELOWQ:
                coreEntriesMIDDLELOWQ.add(coreEntry);
                break;
            case MIDDLEQ:
                coreEntriesMIDDLEQ.add(coreEntry);
                break;
        }
        queryService.addLogging("POST", "lazycommand/send api triggered with command\t" + lazyCommandDto.getCommand() + "\tEnded, queue saved with queue name\t" +
                queueName.getName() + "\n");
        querySocketFacade.send(SocketBodyDto.builder().userId(lazyCommandDto.getUserId()).key("COMMAND-LOG").message("POST /lazycommand/send triggered, command\t" + lazyCommandDto.getCommand() + "\tEnded, queue saved" +
                " with queue name\t" + queueName.getName() + "\n").build());
        return CustomResponseDto.ok();
    }

    @Override
    public JSONArray getAll(String queueName) {
//        queryService.addLogging("GET", "lazycommand/{" + queueName + "} api triggered" + "\n");
//        querySocketFacade.send(SocketBodyDto.builder().userId("").key("COMMAND-LOG").message("GET /lazycommand/{" + queueName + "} api triggered" + "\n").build());
        JSONArray jsonArray = new JSONArray();
        QueueName queue;
        try {
            queue = QueueName.valueOf(queueName);
        } catch (IllegalArgumentException e) {
            return jsonArray;
        }

        switch (queue) {
            case LONGMIDDLEQ:
                jsonArray = getForQueueName(coreEntriesLONGMIDDLEQ, LONGMIDDLEQ);
                break;
            case LONGQ:
                jsonArray = getForQueueName(coreEntriesLONGQ, LONGQ);
                break;
            case LOWQ:
                jsonArray = getForQueueName(coreEntriesLOWQ, LOWQ);
                break;
            case MIDDLELOWQ:
                jsonArray = getForQueueName(coreEntriesMIDDLELOWQ, MIDDLELOWQ);
                break;
            case MIDDLEQ:
                jsonArray = getForQueueName(coreEntriesMIDDLEQ, MIDDLEQ);
                break;
        }
//        queryService.addLogging("GET", "lazycommand/{" + queueName + "} api ENDED execution, result:\t" + jsonArray.toString() + "\n");
//        querySocketFacade.send(SocketBodyDto.builder().userId("").key("COMMAND-LOG").message("GET /lazycommand/{" + queueName + "} api ENDED execution, result:\t" + jsonArray.toString() + "\n").build());
        return jsonArray;
    }

    private JSONArray getForQueueName(Queue<CoreEntry> coreEntries, QueueName queueName) {
        JSONArray jsonArray = new JSONArray();
        coreEntries.stream().filter(coreEntry -> coreEntry.getKey().toString().equals(queueName.getName())).forEach(coreEntry -> {
            CoreEntry subCoreEntry = (CoreEntry) coreEntry.getValue();
            String commandID = (String) subCoreEntry.getKey();
            LazyCommandDto lazyCommandDto = (LazyCommandDto) subCoreEntry.getValue();
            JSONObject jsonObject = new JSONObject();
            jsonObject.put("commandid", commandID);
            jsonObject.put("requested", lazyCommandDto.getRequestedDateTime());
            jsonObject.put("exec_datetime", lazyCommandDto.getExecDateTime());
            jsonObject.put("userId", lazyCommandDto.getUserId());
            jsonObject.put("commandName", lazyCommandDto.getCommand());
            jsonObject.put("alert", lazyCommandDto.isAlert());
            jsonArray.put(jsonObject);
        });
        return jsonArray;
    }

    @Override
    @SneakyThrows
    public CustomResponseDto remove(String queueName, String commandID) {
        queryService.addLogging("DELETE", "lazycommand/" + queueName + "/" + commandID + ",\tapi triggered" + "\n");
        querySocketFacade.send(SocketBodyDto.builder().userId("").key("COMMAND-LOG").message("DELETE /lazycommand/{" + queueName + "/" + commandID + ",\tapi triggered" + "\n").build());

        QueueName queue;
        try {
            queue = QueueName.valueOf(queueName);
        } catch (IllegalArgumentException e) {
            return CustomResponseDto.builder().ErrorCode(10).ErrorMessage("Invalid Queue Name provided!").build();
        }
        CustomResponseDto customResponseDto = CustomResponseDto.builder().ErrorCode(12).ErrorMessage(commandID + "\tdoesn't exist!").build();

        switch (queue) {
            case LONGMIDDLEQ:
                customResponseDto = removeForQueueName(coreEntriesLONGMIDDLEQ, LONGMIDDLEQ, commandID);
                break;
            case LONGQ:
                customResponseDto = removeForQueueName(coreEntriesLONGQ, LONGQ, commandID);
                break;
            case LOWQ:
                customResponseDto = removeForQueueName(coreEntriesLOWQ, LOWQ, commandID);
                break;
            case MIDDLELOWQ:
                customResponseDto = removeForQueueName(coreEntriesMIDDLELOWQ, MIDDLELOWQ, commandID);
                break;
            case MIDDLEQ:
                customResponseDto = removeForQueueName(coreEntriesMIDDLEQ, MIDDLEQ, commandID);
                break;
        }
        queryService.addLogging("DELETE", "lazycommand/" +
                queueName + "/" + commandID + ",\tapi ENDED, result:\t" + objectMapper.setSerializationInclusion(JsonInclude.Include.NON_NULL).writeValueAsString(customResponseDto) + "\n");
        querySocketFacade.send(SocketBodyDto.builder().userId("").key("COMMAND-LOG").
                message("DELETE /lazycommand/{" + queueName + "/" + commandID + ",\tapi ENDED, result:\t" + objectMapper.setSerializationInclusion(JsonInclude.Include.NON_NULL).writeValueAsString(customResponseDto) + "\n").build());
        return customResponseDto;
    }

    private CustomResponseDto removeForQueueName(Queue<CoreEntry> coreEntries, QueueName queueName, String commandID) {
        coreEntries.stream().filter(coreEntry -> coreEntry.getKey().toString().equals(queueName.getName())).filter(coreEntry -> {
            String commandIDFound = (String) ((CoreEntry) coreEntry.getValue()).getKey();
            return commandID.equals(commandIDFound);
        }).collect(Collectors.toList()).forEach(coreEntries::remove);
        return CustomResponseDto.ok();
    }

}
