package co.kr.coresolutions.facades.Impl;

import co.kr.coresolutions.commons.CustomResponseDto;
import co.kr.coresolutions.commons.ErrorCode;
import co.kr.coresolutions.dao.NativeQuery;
import co.kr.coresolutions.facades.IQueryDateFacade;
import co.kr.coresolutions.model.Connection;
import co.kr.coresolutions.model.dtos.CriteriaDto;
import co.kr.coresolutions.model.dtos.DelayDto;
import co.kr.coresolutions.service.QueryService;
import co.kr.coresolutions.service.QueryServiceDate;
import lombok.RequiredArgsConstructor;
import lombok.extern.slf4j.Slf4j;
import org.json.JSONArray;
import org.springframework.stereotype.Component;

import java.lang.reflect.Field;
import java.text.DateFormat;
import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.Arrays;
import java.util.Date;
import java.util.Locale;

@Component
@RequiredArgsConstructor
@Slf4j
public class QueryDateFacade implements IQueryDateFacade {
    private final QueryService queryService;
    private final QueryServiceDate queryServiceDate;
    private final NativeQuery nativeQuery;

    @Override
    public CustomResponseDto validate(String connectionID, String holidayTableName, String date, CriteriaDto criteriaDto) {
        boolean isConnectionFileExists = queryService.isConnectionIdFileExists(connectionID);
        if (!isConnectionFileExists) {
            queryService.addLogging("POST", "validate/date triggered, fail due to connection file with name\t" + connectionID + "\tdoesn't exist");
            return CustomResponseDto.builder().ErrorCode(ErrorCode.THREE.getCode()).ErrorMessage("fail due to connection file with name\t" + connectionID + "\tdoesn't exist").build();
        }
        if (nativeQuery.isTableExist(connectionID, holidayTableName).startsWith("no")) {
            queryService.addLogging("POST", "validate/date triggered, fail due to " + holidayTableName + " doesn't exist");
            return CustomResponseDto.builder().ErrorCode(ErrorCode.TWO.getCode()).ErrorMessage(holidayTableName + " doesn't exist").build();
        }

        Date parsedDate;
        try {
            DateFormat dateFormat = new SimpleDateFormat("yyyyMMdd");
            dateFormat.setLenient(false);
            parsedDate = dateFormat.parse(date);
        } catch (ParseException e) {
            queryService.addLogging("POST", "validate/date triggered, fail due to " + date + " is not valid");
            return CustomResponseDto.builder().ErrorCode(ErrorCode.FOUR.getCode()).ErrorMessage(date + " is not valid").build();
        }

        String monthInLowerCaseFormat = new SimpleDateFormat("MMM", Locale.US).format(parsedDate).toLowerCase();
        CriteriaDto.Month month = criteriaDto.getMonth();
        if (!isValidWeekOrMonth(monthInLowerCaseFormat, month)) {
            queryService.addLogging("POST", "validate/date triggered, fail due to A given date(" + date + ") is not allowed in '" + monthInLowerCaseFormat + "'");
            return CustomResponseDto.builder().ErrorCode(ErrorCode.ONE.getCode()).ErrorMessage("A given date(" + date + ") is not allowed in '" + monthInLowerCaseFormat + "'").build();
        }

        String weekInLowerCaseFormat = new SimpleDateFormat("EEE", Locale.US).format(parsedDate).toLowerCase();
        CriteriaDto.Week week = criteriaDto.getWeek();
        if (!isValidWeekOrMonth(weekInLowerCaseFormat, week)) {
            queryService.addLogging("POST", "validate/date triggered, fail due to A given date(" + date + ") is not allowed in '" + weekInLowerCaseFormat + "'");
            return CustomResponseDto.builder().ErrorCode(ErrorCode.ONE.getCode()).ErrorMessage("A given date(" + date + ") is not allowed in '" + weekInLowerCaseFormat + "'").build();
        }

        if (!criteriaDto.getValidateHoliday()) {
            return CustomResponseDto.builder().Success(true).build();
        }

        String resultQuery;
        Connection infoConnection = queryService.getInfoConnection(connectionID);
        resultQuery = queryService.checkValidity("SELECT HOLI_NAME FROM " + infoConnection.getSCHEME() + holidayTableName + " WHERE HOLI_YEAR = '" + date.substring(0, 4)
                        + "' AND HOLI_MONTH = '" + date.substring(4, 6) + "' AND HOLI_DAY = '" + date.substring(6, 8) + "'"
                , connectionID, "", "cache", false);

        if (resultQuery.startsWith("Error") || resultQuery.startsWith("error") || resultQuery.startsWith("maxRunningTimeOut") || resultQuery.startsWith("MaxRows")) {
            queryService.addLogging("POST", "validate/date triggered, fail due\t" + resultQuery);
            return CustomResponseDto.builder().ErrorCode(5).ErrorMessage("validate/date triggered, fail due\t" + resultQuery).build();
        }
        if (resultQuery.isEmpty() || resultQuery.equalsIgnoreCase("[]")) {
            return CustomResponseDto.builder().Success(true).build();
        }
        Object holyNameFoundObject = new JSONArray(resultQuery).getJSONObject(0).get("HOLI_NAME");
        queryService.addLogging("POST", "validate/date triggered, fail due to A given date(" + date + ") is not allowed in '" + holyNameFoundObject + "'");
        return CustomResponseDto.builder().ErrorCode(ErrorCode.ONE.getCode()).ErrorMessage("A given date(" + date + ") is not allowed in '" + holyNameFoundObject + "'").build();
    }

    @Override
    public CustomResponseDto calculate(DelayDto delayDto) {
        switch (delayDto.getDelayType()) {
            case D: {
                return queryServiceDate.convertDays(delayDto);
            }
            case S: {
                return queryServiceDate.convertFixTime(delayDto);
            }
            default: {
                return queryServiceDate.convertFixDateAndTime(delayDto);
            }
        }
    }

    @Override
    public boolean isValidWeekOrMonth(String inLowerCaseFormat, Object object) {
        Field[] fields = object.getClass().getDeclaredFields();
        Field field = Arrays.stream(fields)
                .filter(m -> m.getName().equalsIgnoreCase(inLowerCaseFormat))
                .findFirst().orElse(null);
        if (field != null) {
            try {
                return (Boolean) field.get(object);
            } catch (IllegalAccessException e) {
                return false;
            }
        }
        return false;
    }
}

