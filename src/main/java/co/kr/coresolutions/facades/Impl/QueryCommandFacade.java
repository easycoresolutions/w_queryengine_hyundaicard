package co.kr.coresolutions.facades.Impl;

import co.kr.coresolutions.commons.CustomResponseDto;
import co.kr.coresolutions.commons.ResponseCodes;
import co.kr.coresolutions.commons.ResponseDto;
import co.kr.coresolutions.enums.OutputType;
import co.kr.coresolutions.facades.IQueryCommandFacade;
import co.kr.coresolutions.facades.IQueryETLFacade;
import co.kr.coresolutions.facades.IQuerySocketFacade;
import co.kr.coresolutions.model.Command;
import co.kr.coresolutions.model.Connection;
import co.kr.coresolutions.model.dtos.CommandDto;
import co.kr.coresolutions.model.dtos.CommandTemplate;
import co.kr.coresolutions.model.dtos.FileDto;
import co.kr.coresolutions.model.dtos.FileResponseDto;
import co.kr.coresolutions.model.dtos.SocketBodyDto;
import co.kr.coresolutions.service.Constants;
import co.kr.coresolutions.service.QueryService;
import co.kr.coresolutions.service.QueryServiceCSV;
import co.kr.coresolutions.service.QueryServiceCommand;
import co.kr.coresolutions.service.QueryServiceJSON;
import co.kr.coresolutions.service.RunCommands;
import com.fasterxml.jackson.annotation.JsonInclude;
import com.fasterxml.jackson.core.JsonProcessingException;
import com.fasterxml.jackson.databind.ObjectMapper;
import lombok.RequiredArgsConstructor;
import lombok.SneakyThrows;
import lombok.extern.slf4j.Slf4j;
import org.springframework.http.HttpStatus;
import org.springframework.security.core.context.SecurityContextHolder;
import org.springframework.security.core.userdetails.UserDetails;
import org.springframework.stereotype.Component;

import javax.annotation.PostConstruct;
import javax.servlet.http.HttpServletResponse;
import java.io.File;
import java.io.IOException;
import java.nio.charset.StandardCharsets;
import java.nio.file.Files;
import java.nio.file.Path;
import java.nio.file.Paths;
import java.nio.file.StandardOpenOption;
import java.nio.file.attribute.BasicFileAttributes;
import java.text.SimpleDateFormat;
import java.time.LocalDateTime;
import java.time.ZoneId;
import java.time.ZoneOffset;
import java.util.ArrayList;
import java.util.Date;
import java.util.List;
import java.util.concurrent.atomic.AtomicInteger;
import java.util.stream.Stream;

@Component
@RequiredArgsConstructor
@Slf4j
public class QueryCommandFacade implements IQueryCommandFacade {
    private final Constants constants;
    private String commandDir;
    private final QueryServiceCommand queryServiceCommand;
    private final QueryServiceJSON queryServiceJSON;
    private final QueryService queryService;
    private final ObjectMapper objectMapper;
    private final QueryServiceCSV queryServiceCSV;
    private final IQuerySocketFacade querySocketFacade;
    private final IQueryETLFacade queryETLFacade;
    private String scriptEllaJarDir;
    private String scriptEllaDir;

    @PostConstruct
    public void init() {
        scriptEllaJarDir = constants.getRootDir();
        scriptEllaDir = constants.getScriptEllaDir();
    }

    @Override
    @SneakyThrows
    public ResponseDto fileUpload(FileDto fileDto) {
        commandDir = constants.getCommandDir();
        try {
            if (!Files.isDirectory(Paths.get(commandDir + File.separator + fileDto.getUserId())))
                Files.createDirectory(Paths.get(commandDir + File.separator + fileDto.getUserId()));
            Files.write(Paths
                            .get(commandDir + fileDto.getUserId() + File.separator + fileDto.getFileName() + "." + fileDto.getExt()),
                    fileDto.getContent().getBytes(StandardCharsets.UTF_8), StandardOpenOption.CREATE);
            queryService.addLogging("POST", "file Upload successfully");

        } catch (IOException e) {
            queryService.addLogging("POST", "error upload file");
            return ResponseDto.builder().errorCode(HttpStatus.BAD_REQUEST.value()).message(e.getMessage()).build();
        }

        return ResponseDto.ok();
    }

    @Override
    @SneakyThrows
    public ResponseDto fileDelete(String userId, String fileNameExt) {
        commandDir = constants.getCommandDir();
        try {
            Files.delete(Paths.get(commandDir + userId + File.separator + fileNameExt));
            queryService.addLogging("DELETE", "file deleted successfully with name\t" + fileNameExt);
        } catch (IOException e) {
            queryService.addLogging("DELETE", "file deleted error with name\t" + fileNameExt);
            return ResponseDto.builder().errorCode(HttpStatus.NOT_FOUND.value()).build();
        }

        return ResponseDto.ok();
    }

    @Override
    public String findAll(String userId) throws JsonProcessingException {
        commandDir = constants.getCommandDir();
        List<FileResponseDto> fileResponseDtoList = new ArrayList<>();
        try (Stream<Path> paths = Files.walk(Paths.get(commandDir + userId))) {
            paths
                    .filter(Files::isRegularFile)
                    .forEach(path1 -> {
                        try {
                            fileResponseDtoList.add(FileResponseDto
                                    .builder()
                                    .fileName(path1.getFileName().toString())
                                    .size(Files.size(path1))
                                    .update(Files
                                            .readAttributes(path1, BasicFileAttributes.class)
                                            .lastModifiedTime()
                                            .toInstant()
                                            .atZone(ZoneId.systemDefault())
                                            .toLocalDateTime()
                                            .toString())
                                    .build());
                        } catch (IOException ignored) {
                        }
                    });
            queryService.addLogging("GET", "list files triggered for userId\t" + userId);
        } catch (IOException e) {
            queryService.addLogging("GET", "list files triggered for userId\t" + userId + "and return error");
            return "";
        }
        return fileResponseDtoList.isEmpty() ? "" : queryServiceCSV.structureCSV(objectMapper.writeValueAsString(fileResponseDtoList));
    }

    @Override
    public String getOwner() {
        Object principal = SecurityContextHolder.getContext().getAuthentication().getPrincipal();
        String owner;
        if (principal instanceof UserDetails) {
            owner = ((UserDetails) principal).getUsername();
        } else {
            owner = principal.toString();
        }
        return owner;
    }

    @Override
    public CustomResponseDto executeETL(String directory, String xml, CommandTemplate commandTemplate, HttpServletResponse response) {
        String commandID = commandTemplate.getCommandID();
        String startDttm = new SimpleDateFormat("yyyyMMddHHmmss").format(new Date());
        long startTime = LocalDateTime.now().toEpochSecond(ZoneOffset.UTC);
        AtomicInteger querySeq = new AtomicInteger();
        String partMessage = "scriptella/{" + directory + "}/{" + xml + "} triggered, ";
        String connectionID = "quadmax";
        Connection infoConnectionInput = queryService.getInfoConnection(connectionID);
        String inputSCHEMA = infoConnectionInput.getSCHEME();
        try {
            Path path = Files.write(Files.createTempFile(Paths.get(scriptEllaDir + directory + File.separator), commandID.concat(xml), ".xml"),
                    queryServiceCommand.getFormattedCommandEtl(directory, xml.concat(".xml"), commandTemplate).getBytes());
            String command = "java -jar " + scriptEllaJarDir + "scriptella.jar -nostat -nojmx " + path.toString();
            String responseExecution = queryService.executeCommand("scriptella/{" + directory + "}/{" + xml + "}", command, commandID, commandTemplate.getOwner(), response);
            queryService.addLogging("POST", "scriptella/{" + directory + "}/{" + xml + "} triggered, output is\t" + responseExecution);
            Files.deleteIfExists(path);
            final boolean[] success = {true};
            if (commandTemplate.getErrorWord() != null) {
                commandTemplate.getErrorWord().stream().filter(s -> !s.isEmpty()).forEach(s -> {
                    if (responseExecution.contains(s)) {
                        success[0] = false;
                    }
                });
            }

            queryETLFacade.logToDBTriggered(startDttm, null, querySeq.incrementAndGet(), partMessage, connectionID, null,
                    inputSCHEMA, null, null, (success[0] ? "success" : "fail"),
                    responseExecution, LocalDateTime.now().toEpochSecond(ZoneOffset.UTC) - startTime);
            return CustomResponseDto.builder().Success(success[0]).message(responseExecution).build();
        } catch (IOException e) {
            queryService.addLogging("POST", "scriptella/{" + directory + "}/{" + xml + "} triggered, error is\t" + e.getMessage());
            queryETLFacade.logToDBTriggered(startDttm, null, querySeq.incrementAndGet(), partMessage, connectionID, null,
                    inputSCHEMA, null, null, "fail",
                    e.getMessage(), LocalDateTime.now().toEpochSecond(ZoneOffset.UTC) - startTime);
            return CustomResponseDto.builder().Success(true).message(e.getMessage()).build();
        }
    }

    @Override
    public CustomResponseDto executeETLFromDBMS(String connectionID, String projectID, String scriptID, CommandTemplate commandTemplate, HttpServletResponse response) {
        boolean isConnectionFileExists = queryService.isConnectionIdFileExists(connectionID);
        if (!isConnectionFileExists) {
            queryService.addLogging("POST", "scriptella/dbtable/{" + connectionID + "}/{" + projectID + "}/{" + scriptID
                    + "} triggered, error connection file\t" + connectionID + " doesn't exist");
            return CustomResponseDto.builder().Success(true).message("error connection file\t" + connectionID + " doesn't exist").build();
        }

        String commandID = commandTemplate.getCommandID();
        String startDttm = new SimpleDateFormat("yyyyMMddHHmmss").format(new Date());
        long startTime = LocalDateTime.now().toEpochSecond(ZoneOffset.UTC);
        AtomicInteger querySeq = new AtomicInteger();
        String partMessage = "scriptella/dbtable/{" + connectionID + "}/{" + projectID + "}/{" + scriptID + "} triggered, ";
        Connection infoConnectionInput = queryService.getInfoConnection("quadmax");
        String inputSCHEMA = infoConnectionInput.getSCHEME();
        try {
            String xmlContent = queryServiceCommand.getFormattedCommandEtlFromDBMS(connectionID, projectID, scriptID, commandTemplate);
            if (xmlContent.isEmpty()) {
                return CustomResponseDto.builder().Success(false).message("etl-script (" + projectID + " . " + scriptID + ") doesn't exist\"").build();
            }
            Path path = Files.write(Files.createTempFile(Paths.get(scriptEllaDir), commandID.concat(scriptID), ".xml"), xmlContent.replace("{PROJECT_ID}", projectID)
                    .replace("{USERID}", commandTemplate.getOwner()).replace("{SCRIPT_ID}", scriptID).getBytes());
            String command = "java -jar " + scriptEllaJarDir + "scriptella.jar -nostat -nojmx " + path.toString();
            String responseExecution = queryService.executeCommand("scriptella/dbtable", command, commandID, commandTemplate.getOwner(), response);
            queryService.addLogging("POST", "scriptella/dbtable/{" + connectionID + "}/{"
                    + projectID + "}/{" + scriptID + "} triggered, output is\t" + responseExecution);
            Files.deleteIfExists(path);
            final boolean[] success = {true};
            if (commandTemplate.getErrorWord() != null) {
                commandTemplate.getErrorWord().stream().filter(s -> !s.isEmpty()).forEach(s -> {
                    if (responseExecution.contains(s)) {
                        success[0] = false;
                    }
                });
            }

            queryETLFacade.logToDBTriggered(startDttm, null, querySeq.incrementAndGet(), partMessage, "quadmax", connectionID,
                    inputSCHEMA, projectID, scriptID, (success[0] ? "success" : "fail"),
                    responseExecution, LocalDateTime.now().toEpochSecond(ZoneOffset.UTC) - startTime);
            return CustomResponseDto.builder().Success(success[0]).message(responseExecution).build();
        } catch (IOException e) {
            queryService.addLogging("POST", "scriptella/dbtable/{" + connectionID + "}/{"
                    + projectID + "}/{" + scriptID + "} triggered, error is\t" + e.getMessage());
            queryETLFacade.logToDBTriggered(startDttm, null, querySeq.incrementAndGet(), partMessage, "quadmax", connectionID,
                    inputSCHEMA, projectID, scriptID, "fail",
                    e.getMessage(), LocalDateTime.now().toEpochSecond(ZoneOffset.UTC) - startTime);
            return CustomResponseDto.builder().Success(false).message(e.getMessage()).build();
        }
    }

    @Override
    @SneakyThrows
    public ResponseDto execute(CommandDto commandDto, String userId, HttpServletResponse response) {
        String fileName = commandDto.getCommand();
        if (RunCommands.isUnix()) fileName = fileName.concat(".sh");
        else if (RunCommands.isWindows()) fileName = fileName.concat(".bat");
        String owner = getOwner();
        String commandID = "";
        if (commandDto.getCommandId() == null || commandDto.getCommandId().isEmpty()) {
            commandID = owner + new SimpleDateFormat("yyyyMMdd:HHmmss").format(new Date());
        } else {
            commandID = commandDto.getCommandId();
        }
        String startDateTime = new SimpleDateFormat("yyyyMMddHHmmss").format(new Date());
        Command.CommandDetails commandDetails = Command.CommandDetails.builder()
                .commadName(commandDto.getCommand()).CommandID(commandID).owner(owner).startdatetime(startDateTime).build();
        if (queryServiceCommand.isCommandFileExists(userId, fileName)) {
            queryService.addLogging("POST", Constants.commandDirName + "/{" + userId + "}, Started, BODY\t" + objectMapper.setSerializationInclusion(JsonInclude.Include.NON_NULL).writeValueAsString(commandDto) + "\n");
            querySocketFacade.send(SocketBodyDto.builder().userId(userId).key("COMMAND").message("POST " + Constants.commandDirName + "/{" + userId + "}, Started, BODY\t" + objectMapper.setSerializationInclusion(JsonInclude.Include.NON_NULL).writeValueAsString(commandDto) + "\n").build());
            try {
                String responseExecution = queryService.executeCommand("command/{" + userId + "}_" + fileName, queryServiceCommand.formatCommand(userId, fileName, commandDto.getParameter()), commandID, owner, response);
                queryService.addLogging("POST", Constants.commandDirName + "\noutput message:\n--------------\n\t\t" + responseExecution + "\nBODY:\n--------------\n\t" +
                        "\t/" + Constants.commandDirName + "/{" + userId + "}\n\tPOST\n\t\"" + objectMapper.setSerializationInclusion(JsonInclude.Include.NON_NULL).writeValueAsString(commandDto) + "\"\n");
                querySocketFacade.send(SocketBodyDto.builder().userId(userId).key("COMMAND").message(Constants.commandDirName + "\noutput message:\n--------------\n\t\t" + responseExecution + "\nBODY:\n--------------\n\t" +
                        "\t/" + Constants.commandDirName + "/{" + userId + "}\n\tPOST\n\t\"" + objectMapper.setSerializationInclusion(JsonInclude.Include.NON_NULL).writeValueAsString(commandDto) + "\"\n").build());
                OutputType outputType = commandDto.getOutput();
                if (outputType.getName().equalsIgnoreCase(OutputType.csv.getName())) {
                    queryService.addLogging("POST", "execute command\t" + commandDetails + "\tsuccessfully for userId\t" + userId + "\n\n");
                    return ResponseDto
                            .builder()
                            .successCode(ResponseCodes.OK_RESPONSE)
                            .message(responseExecution)
                            .build();
                }

                String structuredJson = queryServiceJSON.structureJsonFromCSV(responseExecution);
                if (structuredJson.length() > 0) {
                    String resultJson = queryServiceJSON.
                            structureJsonForCommandStructure(structuredJson, commandDto.getChars(), null);
                    if (resultJson.length() > 0) {
                        return ResponseDto
                                .builder()
                                .successCode(ResponseCodes.OK_RESPONSE)
                                .message(outputType.getName().equalsIgnoreCase(OutputType.json.getName())
                                        ? resultJson : responseExecution.replaceAll("\\\\t", " ").replaceAll(",", "\t"))
                                .build();
                    }
                }
            } catch (Exception e) {
                queryService.addLogging("POST", Constants.commandDirName + "\noutput message:\n--------------\n\t\t" + e.getMessage() + "\nBODY:\n--------------\n\t" +
                        "\t/" + Constants.commandDirName + "/{" + userId + "}\n\tPOST\n\t\"" + objectMapper.setSerializationInclusion(JsonInclude.Include.NON_NULL).writeValueAsString(commandDto) + "\"\n");
                querySocketFacade.send(SocketBodyDto.builder().userId(userId).key("COMMAND").message(Constants.commandDirName + "\noutput message:\n--------------\n\t\t" + e.getMessage() + "\nBODY:\n--------------\n\t" +
                        "\t/" + Constants.commandDirName + "/{" + userId + "}\n\tPOST\n\t\"" + objectMapper.setSerializationInclusion(JsonInclude.Include.NON_NULL).writeValueAsString(commandDto) + "\"\n").build());
                return ResponseDto
                        .builder()
                        .errorCode(HttpStatus.NOT_FOUND.value())
                        .message("commandFile \t" + fileName + "\tdoesn't executed properly for reason not valid CSV output.")
                        .build();
            }
        }
        queryService.addLogging("POST", "execute command user\t" + userId + "\tdoesn't found");
        return ResponseDto.ERROR_PARSE;
    }

    @Override
    public List<Command.CommandDetails> monitor() {
        return queryServiceCommand.getCacheCommands();
    }

    @Override
    public List monitorWaiting() {
        return queryService.getCacheCommandsWaiting();
    }

    @Override
    public ResponseDto deleteCommand(String commandID) {
        queryService.addLogging("POST", "/" + Constants.commandDirName + " + BODY request is terminated by \"DELETE\" command .(" + queryServiceCommand.getCacheCommandByKey(commandID) + ")");
        queryServiceCommand.stopCommand(commandID);
        if (queryServiceCommand.removeCacheCommand(commandID)) {
            return ResponseDto.builder().message("Command with id\t" + commandID + "\tdeleted success").successCode(ResponseCodes.OK_RESPONSE).build();
        }
        return ResponseDto.NOT_EXISTS;
    }

}
