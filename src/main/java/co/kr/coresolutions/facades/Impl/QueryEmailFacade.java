package co.kr.coresolutions.facades.Impl;

import co.kr.coresolutions.commons.ResponseDto;
import co.kr.coresolutions.enums.EmailType;
import co.kr.coresolutions.facades.IQueryEmailFacade;
import co.kr.coresolutions.model.dtos.EmailDto;
import co.kr.coresolutions.model.dtos.SubEmailDto;
import co.kr.coresolutions.service.Constants;
import com.fasterxml.jackson.core.JsonParser;
import com.fasterxml.jackson.databind.ObjectMapper;
import lombok.RequiredArgsConstructor;
import org.apache.commons.lang3.StringUtils;
import org.apache.commons.mail.EmailAttachment;
import org.apache.commons.mail.EmailException;
import org.apache.commons.mail.HtmlEmail;
import org.apache.commons.mail.MultiPartEmail;
import org.springframework.http.HttpStatus;
import org.springframework.stereotype.Component;

import javax.annotation.PostConstruct;
import javax.validation.Valid;
import java.io.IOException;
import java.net.MalformedURLException;
import java.net.URL;
import java.nio.charset.StandardCharsets;
import java.nio.file.Files;
import java.nio.file.Paths;

@Component
@RequiredArgsConstructor
public class QueryEmailFacade implements IQueryEmailFacade {
    private final Constants constants;
    private String CONNECTION_DIR;
    private final ObjectMapper objectMapper;

    private SubEmailDto readFile(String connectionId) throws IOException {
        return objectMapper.configure(JsonParser.Feature.ALLOW_MISSING_VALUES, false)
                .readValue(new String(Files.readAllBytes(Paths.get(CONNECTION_DIR + connectionId + ".txt")), StandardCharsets.UTF_8), SubEmailDto.class);
    }

    @PostConstruct
    public void init() {
        CONNECTION_DIR = constants.getConnectionDir();
    }

    @Override
    public ResponseDto sendSimpleEmail(@Valid EmailDto emailDto) {
        try {
            SubEmailDto subEmailDto = readFile(emailDto.getConnectionId());

            MultiPartEmail email;

            String type = emailDto.getEmailType();

            if (type.equalsIgnoreCase(EmailType.html.getName())) {
                email = new HtmlEmail();
                email.setCharset("utf-8");
                ((HtmlEmail) email).setHtmlMsg(emailDto.getData());
            } else {
                email = new MultiPartEmail();
                email.setCharset("utf-8");
                email.setMsg(emailDto.getData());
            }

            emailDto.getAttachment().forEach(oneAttachmentUrl -> {
                EmailAttachment attachment = new EmailAttachment();
                try {
                    if (StringUtils.startsWithIgnoreCase(oneAttachmentUrl, "http")) {
                        attachment.setURL(new URL(oneAttachmentUrl));
                    } else {
                        attachment.setPath(oneAttachmentUrl);
                    }
                    attachment.setDisposition(EmailAttachment.ATTACHMENT);
                    email.attach(attachment);
                } catch (MalformedURLException | EmailException ignored) {
                }
            });

            if (!emailDto.getTo().isEmpty()) {
                email.addTo(StringUtils.join(emailDto.getTo(), ",").split(","));
            }

            if (!emailDto.getCc().isEmpty()) {
                email.addCc(StringUtils.join(emailDto.getCc(), ",").split(","));
            }

            email.setFrom(emailDto.getFrom(), "CoreSolution");
            email.setSubject(emailDto.getSubject());
            email.setHostName(subEmailDto.getSmtp());
            email.setAuthentication(subEmailDto.getId(), subEmailDto.getPassword());
            email.setSmtpPort(subEmailDto.getPort());
            email.send();

            return ResponseDto.ok();

        } catch (EmailException | IOException e) {
            return ResponseDto.builder().errorCode(HttpStatus.BAD_REQUEST.value()).message(e.getMessage()).build();
        }

    }
}
