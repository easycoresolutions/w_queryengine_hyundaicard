package co.kr.coresolutions.facades.Impl;

import co.kr.coresolutions.commons.CustomResponseDto;
import co.kr.coresolutions.facades.IQueryProjectFacade;
import co.kr.coresolutions.service.QueryService;
import co.kr.coresolutions.service.QueryServiceJSON;
import co.kr.coresolutions.service.QueryServiceProjectAuth;
import com.fasterxml.jackson.databind.JsonNode;
import com.fasterxml.jackson.databind.ObjectMapper;
import lombok.RequiredArgsConstructor;
import lombok.SneakyThrows;
import lombok.extern.slf4j.Slf4j;
import org.json.JSONArray;
import org.json.JSONObject;
import org.springframework.stereotype.Component;

import java.nio.charset.StandardCharsets;
import java.util.stream.StreamSupport;

@Component
@RequiredArgsConstructor
@Slf4j
public class QueryProjectFacade implements IQueryProjectFacade {
    private final QueryService queryService;
    private final ObjectMapper objectMapper;
    private final QueryServiceJSON queryServiceJSON;
    private final QueryServiceProjectAuth queryServiceProjectAuth;

    @Override
    @SneakyThrows
    public CustomResponseDto saveProject(JsonNode node, String connectionID, String tableName, String projectID) {
        JSONObject jsonNode = new JSONObject(objectMapper.writeValueAsString(node));
        if (!jsonNode.has("projectID") || !jsonNode.get("projectID").toString().equals(projectID)) {
            queryService.addLogging("POST", "/project/{" + connectionID + "}/{" + tableName + "}/{" + projectID + "} triggered, fail due to projectID path not match projectID in body");
            return CustomResponseDto.builder().ErrorCode(121).ErrorMessage("projectID path not match projectID in body").build();
        }
        if (!jsonNode.has("projectowner") || !jsonNode.has("projectVersion") || !jsonNode.has("projectjson")) {
            queryService.addLogging("POST", "/project/{" + connectionID + "}/{" + tableName + "}/{" + projectID + "} triggered, fail due to missing required parameters in body");
            return CustomResponseDto.builder().ErrorCode(121).ErrorMessage("missing required [projectowner, projectVersion, projectjson, projectID] parameter in body ").build();
        }

        String projectowner = jsonNode.get("projectowner").toString();
        boolean isConnectionFileExists = queryService.isConnectionIdFileExists(connectionID);
        if (!isConnectionFileExists) {
            queryService.addLogging("POST", "/project/{" + connectionID + "}/{" + tableName + "}/{" + projectID + "} triggered, fail due to connection file with name\t" + connectionID + "\tdoesn't exist");
            return CustomResponseDto.builder().ErrorCode(120).ErrorMessage("Connection file with name\t" + connectionID + "\tdoesn't exist").build();
        }
        final String[] projectVersion = {"0"};
        String resultSelect = queryService
                .checkValidity("select projectversion from " + tableName + "  where projectid = '"
                                + projectID + "' and projectowner = '" + projectowner + "'",
                        connectionID, "", "", false);
        queryServiceProjectAuth.getProjectVersion(connectionID, projectVersion, resultSelect);

        resultSelect = queryService
                .checkValidityUpdateClauses("DELETE FROM " + tableName + " where projectid = '" + projectID
                        + "' and projectowner = '" + projectowner + "'", connectionID);

        if (!resultSelect.startsWith("success")) {
            queryService.addLogging("POST", "/project/{" + connectionID + "}/{" + tableName + "}/{" + projectID + "} triggered, fail due to Error is\t" + resultSelect);
            return CustomResponseDto.builder().ErrorCode(122)
                    .ErrorMessage("/project/{" + connectionID + "}/{" + tableName + "}/{" + projectID + "} triggered, fail due to Error is\t" + resultSelect).build();
        }
        StringBuilder stringBuilderKeys = new StringBuilder();
        StringBuilder stringBuilderValues = new StringBuilder();

        stringBuilderKeys.append("projectversion,");
        stringBuilderValues.append("'" + (Integer.parseInt(projectVersion[0]) + 1) + "',");
        JSONObject finalJsonNode = jsonNode;
        jsonNode.keySet().stream().filter(s -> !s.equalsIgnoreCase("projectversion")).forEachOrdered(s -> {
            stringBuilderKeys.append(s + ",");
            stringBuilderValues.append("'" + finalJsonNode.get(s).toString() + "',");
        });

        String resultInsert = queryService
                .checkValidityUpdateClauses("insert into " + tableName + "(" + stringBuilderKeys.deleteCharAt(stringBuilderKeys.length() - 1).toString() + ") values (" + stringBuilderValues.deleteCharAt(stringBuilderValues.length() - 1).toString() + ")", connectionID);

        if (!resultInsert.startsWith("success")) {
            queryService.addLogging("POST", "/project/{" + connectionID + "}/{" + tableName + "}/{" + projectID + "} triggered, fail due to Error is\t" + resultInsert);
            return CustomResponseDto.builder().ErrorCode(122)
                    .ErrorMessage("/project/{" + connectionID + "}/{" + tableName + "}/{" + projectID + "} triggered, fail due to Error is\t" + resultInsert).build();
        }
        queryService.addLogging("POST", "/project/{" + connectionID + "}/{" + tableName + "}/{" + projectID + "} triggered, Success insert");
        return CustomResponseDto.builder().Success(true).DetailedMessage("/project/{" + connectionID + "}/{" + tableName + "}/{" + projectID + "} triggered, Success insert").build();
    }

    @Override
    @SneakyThrows
    public CustomResponseDto deleteRow(JsonNode node, String connectionID, String tableName, String projectID) {
        JSONObject jsonNode = new JSONObject(objectMapper.writeValueAsString(node));
        boolean isConnectionFileExists = queryService.isConnectionIdFileExists(connectionID);
        if (!isConnectionFileExists) {
            queryService.addLogging("DELETE", "/project/{" + connectionID + "}/{" + tableName + "}/{" + projectID + "} triggered, fail due to connection file with name\t" + connectionID
                    + "\tdoesn't exist");
            return CustomResponseDto.builder().ErrorCode(120).ErrorMessage("Connection file with name\t" + connectionID + "\tdoesn't exist").build();
        }
        if (!jsonNode.has("projectowner")) {
            queryService.addLogging("DELETE", "/project/{" + connectionID + "}/{" + tableName + "}/{" + projectID + "} triggered, fail due to missing required parameters projectowner in body");
            return CustomResponseDto.builder().ErrorCode(121).ErrorMessage("missing required [projectowner] parameter in body ").build();
        }

        String projectowner = jsonNode.get("projectowner").toString();

        String resultInsert = queryService
                .checkValidityUpdateClauses("DELETE FROM " + tableName + " where projectid = '" + projectID
                        + "' and projectowner = '" + projectowner + "'", connectionID);
        if (!resultInsert.startsWith("success")) {
            queryService.addLogging("DELETE", "/project/{" + connectionID + "}/{" + tableName + "}/{" + projectID + "} triggered, fail due to Error is\t" + resultInsert);
            return CustomResponseDto.builder().ErrorCode(122)
                    .ErrorMessage("/project/{" + connectionID + "}/{" + tableName + "}/{" + projectID + "} triggered, fail due to Error is\t" + resultInsert).build();
        }
        queryService.addLogging("DELETE", "/project/{" + connectionID + "}/{" + tableName + "}/{" + projectID + "} triggered, Success delete");
        return CustomResponseDto.builder().Success(true).DetailedMessage("/project/{" + projectID + "}/{" + tableName + "}/{" + projectID + "} triggered, Success delete").build();
    }

    @Override
    public CustomResponseDto getRow(String connectionID, String tableName, String projectID) {
        boolean isConnectionFileExists = queryService.isConnectionIdFileExists(connectionID);
        if (!isConnectionFileExists) {
            queryService.addLogging("GET", "/project/{" + connectionID + "}/{" + tableName + "}/{" + projectID + "} triggered, fail due to connection file with name\t" + connectionID
                    + "\tdoesn't exist");
            return CustomResponseDto.builder().ErrorCode(120).ErrorMessage("Connection file with name\t" + connectionID + "\tdoesn't exist").build();
        }
        String resultSelect = queryService
                .checkValidity("select * from " + tableName + " where projectid = '" + projectID + "'", connectionID, "", "", false);

//        System.out.println("resultSelect\t" + resultSelect);
        if (!(resultSelect.equalsIgnoreCase(connectionID) || resultSelect.startsWith("Error") || resultSelect.startsWith("error") ||
                resultSelect.startsWith("maxRunningTimeOut") || resultSelect.startsWith("MaxRows"))) {

            String structuredJson = new String(queryServiceJSON.structureJson(resultSelect, false,
                    queryService.getQueryDataTypes(connectionID, "select * from " + tableName + " where projectid = '" + projectID + "'", false), "cache")
                    .getBytes(), StandardCharsets.UTF_8);

            if (structuredJson.startsWith("Error") || structuredJson.startsWith("error")) {
                queryService.addLogging("GET", "/project/{" + connectionID + "}/{" + tableName + "}/{" + projectID + "} triggered, fail due\t" + structuredJson);
                return CustomResponseDto.builder().ErrorCode(122).ErrorMessage(structuredJson).build();
            }

            if (structuredJson.isEmpty()) {
                queryService.addLogging("GET", "/project/{" + connectionID + "}/{" + tableName + "}/{" + projectID + "} triggered, fail due\t" + structuredJson);
                return CustomResponseDto.builder().ErrorCode(123).ErrorMessage("empty value returned").build();
            }

            queryService.addLogging("GET", "/project/{" + connectionID + "}/{" + tableName + "}/{" + projectID + "} triggered, Successfully");
            return CustomResponseDto.builder().Success(true).DetailedMessage(getWithoutFieldsAndWrapProjectJson(structuredJson)).build();
        }
        queryService.addLogging("GET", "/project/{" + connectionID + "}/{" + tableName + "}/{" + projectID + "} triggered, fail due\t" + resultSelect);
        return CustomResponseDto.builder().ErrorCode(122).ErrorMessage(resultSelect).build();
    }

    private String getWithoutFieldsAndWrapProjectJson(String structuredJson) {
        JSONObject jsonObjectGlobal = new JSONObject(structuredJson);
        if (jsonObjectGlobal.has("Input")) {
            String returnJson = jsonObjectGlobal.get("Input").toString();
            JSONArray jsonArray = new JSONArray(returnJson);
            JSONArray newJsonArray = new JSONArray();
            jsonArray.forEach(o -> {
                JSONObject jsonObject = new JSONObject(o.toString());
                if (jsonObject.has("projectjson")) {
                    JSONObject projectJson = new JSONObject(jsonObject.get("projectjson").toString());
                    jsonObject.remove("projectjson");
                    jsonObject.put("projectjson", projectJson);
                    newJsonArray.put(jsonObject);
                }
            });
            if (!newJsonArray.isEmpty()) {
                return newJsonArray.toString();
            }
        }
        return jsonObjectGlobal.toString();
    }

    private String getWithoutFields(String structuredJson) {
        JSONObject jsonObjectGlobal = new JSONObject(structuredJson);
        if (jsonObjectGlobal.has("Input")) {
            return jsonObjectGlobal.get("Input").toString();
        }
        return jsonObjectGlobal.toString();
    }

    private String getWithoutFieldName(String structuredJson, String fieldName) {
        JSONObject jsonObjectGlobal = new JSONObject(structuredJson);
        if (jsonObjectGlobal.has("Input")) {
            JSONArray jsonArray = jsonObjectGlobal.getJSONArray("Input");
            JSONArray newJsonArray = new JSONArray();
            StreamSupport.stream(jsonArray.spliterator(), false).forEach(o -> {
                JSONObject jsonObject = new JSONObject(o.toString());
                jsonObject.remove(fieldName);
                newJsonArray.put(jsonObject);
            });
            jsonObjectGlobal.put("Input", newJsonArray);
        }
        if (jsonObjectGlobal.has("Fields")) {
            JSONArray jsonArray = jsonObjectGlobal.getJSONArray("Fields");
            JSONArray newJsonArray = new JSONArray();
            StreamSupport.stream(jsonArray.spliterator(), false).forEach(o -> {
                JSONObject jsonObject = new JSONObject(o.toString());
                if (!jsonObject.get("FieldName").toString().equalsIgnoreCase(fieldName)) {
                    newJsonArray.put(jsonObject);
                }
            });
            jsonObjectGlobal.put("Fields", newJsonArray);
        }
        return jsonObjectGlobal.toString();
    }

    @Override
    @SneakyThrows
    public CustomResponseDto getRows(String connectionID, String tableName, String userID) {
        boolean isConnectionFileExists = queryService.isConnectionIdFileExists(connectionID);
        if (!isConnectionFileExists) {
            queryService.addLogging("GET", "/project/projects/{" + connectionID + "}/{" + tableName + "}/{" + userID + "} triggered, fail due to connection file with name\t" + connectionID
                    + "\tdoesn't exist");
            return CustomResponseDto.builder().ErrorCode(120).ErrorMessage("Connection file with name\t" + connectionID + "\tdoesn't exist").build();
        }
        String sql = "select * from " + tableName + " where projectowner = '" + userID + "'";
        String resultSelect = queryService
                .checkValidity(sql, connectionID, "", "", false);

        if (!(resultSelect.equalsIgnoreCase(connectionID) || resultSelect.startsWith("Error") || resultSelect.startsWith("error") ||
                resultSelect.startsWith("maxRunningTimeOut") || resultSelect.startsWith("MaxRows"))) {

            String structuredJson = new String(queryServiceJSON.structureJson(resultSelect, false,
                    queryService.getQueryDataTypes(connectionID, sql, false), "cache")
                    .getBytes(), StandardCharsets.UTF_8);

            if (structuredJson.startsWith("Error") || structuredJson.startsWith("error")) {
                queryService.addLogging("GET", "/project/projects/{" + connectionID + "}/{" + tableName + "}/{" + userID + "} triggered, fail due\t" + structuredJson);
                return CustomResponseDto.builder().ErrorCode(122).ErrorMessage(structuredJson).build();
            }

            if (structuredJson.isEmpty()) {
                queryService.addLogging("GET", "/project/projects/{" + connectionID + "}/{" + tableName + "}/{" + userID + "} triggered, fail due\t" + structuredJson);
                return CustomResponseDto.builder().ErrorCode(123).ErrorMessage("empty value returned").build();
            }
            String withoutProjectJson = getWithoutFieldName(structuredJson, "projectjson");
            queryService.addLogging("GET", "/project/projects/{" + connectionID + "}/{" + tableName + "}/{" + userID + "} triggered, Successfully");
            return CustomResponseDto.builder().Success(true).DetailedMessage(getWithoutFields(withoutProjectJson)).build();
        }

        queryService.addLogging("GET", "/project/projects/{" + connectionID + "}/{" + tableName + "}/{" + userID + "} triggered, fail due\t" + resultSelect);
        return CustomResponseDto.builder().ErrorCode(122).ErrorMessage(resultSelect).build();
    }
}
