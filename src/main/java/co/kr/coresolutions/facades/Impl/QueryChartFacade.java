package co.kr.coresolutions.facades.Impl;

import co.kr.coresolutions.commons.CustomResponseDto;
import co.kr.coresolutions.commons.ErrorCode;
import co.kr.coresolutions.facades.IQueryChartFacade;
import co.kr.coresolutions.facades.IQueryPythonFacade;
import co.kr.coresolutions.facades.IQueryRFacade;
import co.kr.coresolutions.facades.IQuerySocketFacade;
import co.kr.coresolutions.model.dtos.Chart2Dto;
import co.kr.coresolutions.model.dtos.Chart3Dto;
import co.kr.coresolutions.model.dtos.ChartDto;
import co.kr.coresolutions.model.dtos.SocketBodyDto;
import co.kr.coresolutions.model.dtos.SocketDto;
import co.kr.coresolutions.service.QueryService;
import co.kr.coresolutions.service.QueryServiceChart;
import co.kr.coresolutions.service.QueryServiceJSON;
import co.kr.coresolutions.service.QueryServiceSQLite;

import com.fasterxml.jackson.core.JsonProcessingException;
import com.fasterxml.jackson.databind.JsonNode;
import com.fasterxml.jackson.databind.ObjectMapper;

import lombok.RequiredArgsConstructor;
import org.json.JSONObject;
import org.springframework.stereotype.Component;

import java.io.File;
import java.nio.charset.StandardCharsets;
import java.util.regex.Pattern;

@Component
@RequiredArgsConstructor
public class QueryChartFacade implements IQueryChartFacade {
    private final QueryServiceJSON queryServiceJSON;
    private final QueryService queryService;
    private final QueryServiceSQLite queryServiceSQLite;
    private final IQueryRFacade queryRFacade;
    private final IQueryPythonFacade queryPythonFacade;
    private final ObjectMapper objectMapper;
    public static String[] message = {""};
    private final IQuerySocketFacade querySocketFacade;
    private final QueryServiceChart queryServiceChart;
    private static final String CHART_FILE_REGEX = "(.*)(``SQL)(.*)(``ENDSQL)(.*)(``HTML)(.*)(``ENDHTML)(.*)";

    @Override
    public CustomResponseDto getChart(String connectionId, ChartDto chartDto) {
        boolean isConnectionFileExists = queryService.isConnectionIdFileExists(connectionId);
        if (!isConnectionFileExists) {
            queryService.addLogging("POST", "select_chart triggered with sql\t" + chartDto.getSql() + "\t, fail due to connection file with name\t" + connectionId
                    + "\tdoesn't exist");
            return CustomResponseDto.builder().ErrorCode(ErrorCode.ONE.getCode()).ErrorMessage("Connection file with name\t" + connectionId + "\tdoesn't exist").build();
        }
        String fileChartBody = queryService.getFileFromDir(chartDto.getChartFile(), "charts");
        if (fileChartBody.isEmpty()) {
            queryService.addLogging("POST", "select_chart triggered with sql\t" + chartDto.getSql() + "\t, fail due to Chart file with name\t" + chartDto.getChartFile() + "\tdoesn't exist");
            return CustomResponseDto.builder().ErrorCode(ErrorCode.TWO.getCode()).ErrorMessage("Chart file with name\t" + chartDto.getChartFile() + "\tdoesn't exist").build();
        }

        String resultValidity = queryService.checkValidityWithLabel(queryService.convertGlobalDateTime(chartDto.getSql()), connectionId, chartDto.getUserId(), "cache", "", false);
        if (resultValidity.startsWith("Error") || resultValidity.startsWith("error") || resultValidity.startsWith("maxRunningTimeOut") || resultValidity.startsWith("MaxRows")) {
            queryService.addLogging("POST", "select_chart triggered with sql\t" + chartDto.getSql() + "\t, fail due\t" + resultValidity);
            return CustomResponseDto.builder().ErrorCode(ErrorCode.THREE.getCode()).ErrorMessage(resultValidity).build();
        }

        String structuredJson = new String(queryServiceJSON.structureJson(resultValidity, false, queryService.getQueryDataTypes(connectionId, chartDto.getSql(), true), "cache")
                .getBytes(), StandardCharsets.UTF_8);

        if (structuredJson.startsWith("Error") || structuredJson.startsWith("error")) {
            queryService.addLogging("POST", "select_chart triggered with sql\t" + chartDto.getSql() + "\t, fail due\t" + structuredJson);
            return CustomResponseDto.builder().ErrorCode(ErrorCode.FOUR.getCode()).ErrorMessage(structuredJson).build();
        }

        if (structuredJson.isEmpty()) {
            queryService.addLogging("POST", "select_chart triggered with sql\t" + chartDto.getSql() + "\t, fail due Empty value returned from query\t" + chartDto.getSql());
            return CustomResponseDto.builder().ErrorCode(ErrorCode.FIVE.getCode()).ErrorMessage("Empty value returned from query\t" + chartDto.getSql()).build();
        }

        JSONObject jsonObjectGlobal = new JSONObject(structuredJson);
        if (!jsonObjectGlobal.has("Input")) {
            queryService.addLogging("POST", "select_chart triggered with sql\t" + chartDto.getSql() + "\t, fail due can't transform result as valid json");
            return CustomResponseDto.builder().ErrorCode(ErrorCode.SIX.getCode()).ErrorMessage("can't transform result as valid json").build();
        }

        queryService.addLogging("POST", "select_chart triggered with sql\t" + chartDto.getSql() + "\t, Successfully");
        return CustomResponseDto.builder().Success(true).DetailedMessage(queryServiceChart.constructDetails(jsonObjectGlobal, chartDto, fileChartBody)).build();
    }

    @Override
    public CustomResponseDto execChart(String connectionId, Object object, boolean isExecData) {
        QueryChartFacade.message[0] = "";
        boolean isConnectionFileExists = queryService.isConnectionIdFileExists(connectionId);
        String execMessage = "exec_chart";

        if (isExecData) {
            execMessage = "exec_data";
        }
        String partMessage = "";
        Chart2Dto chartDto;
        if (object instanceof Chart2Dto) {
            chartDto = (Chart2Dto) object;
        } else {
            chartDto = (Chart3Dto) object;
            execMessage = "query_json/" + connectionId;
            partMessage = execMessage + " triggered, ";
        }

        String userID = chartDto.getUserId();
        String socketKey = "QUERY_JSON";
        if (!isConnectionFileExists) {
            QueryChartFacade.message[0] = "fail due to connection file with name\t" + connectionId + "\tdoesn't exist";
            if (!partMessage.isEmpty()) {
                querySocketFacade.send(SocketBodyDto.builder().userId(userID).key(socketKey).message(partMessage + QueryChartFacade.message[0]).build());
                queryService.addLogging("POST", partMessage + QueryChartFacade.message[0]);
            } else {
                queryService.addLogging("POST", execMessage + " triggered, " + QueryChartFacade.message[0]);
            }
            return CustomResponseDto.builder().ErrorCode(ErrorCode.ONE.getCode()).ErrorMessage(QueryChartFacade.message[0]).build();
        }
        String fileChartBody = "";

        if (object instanceof Chart3Dto) {
            String dir = chartDto.getDir();
            if (chartDto.getChartFile() != null) {
                fileChartBody = queryService.getFileFromDir(
                        chartDto.getChartFile(), "charts" + (dir != null && !dir.isEmpty() ? File.separator + dir : ""));
            } else {
                fileChartBody = queryService.getFileFromDir(
                        chartDto.getQueryFile(), "business_query" + (dir != null && !dir.isEmpty() ? File.separator + dir : ""));
            }
        } else {
            fileChartBody = queryService.getFileFromDir(chartDto.getChartFile(), "charts");
        }

        fileChartBody = fileChartBody.replaceAll("\\s", "\t");

        if (fileChartBody.isEmpty()) {
            QueryChartFacade.message[0] = "fail due to Chart file with name\t" + chartDto.getChartFile() + "\tdoesn't exist";
            if (!partMessage.isEmpty()) {
                querySocketFacade.send(SocketBodyDto.builder().userId(userID).key(socketKey).message(partMessage + QueryChartFacade.message[0]).build());
                queryService.addLogging("POST", partMessage + QueryChartFacade.message[0]);
            } else {
                queryService.addLogging("POST", execMessage + " triggered, " + QueryChartFacade.message[0]);
            }
            return CustomResponseDto.builder().ErrorCode(ErrorCode.TWO.getCode()).ErrorMessage(QueryChartFacade.message[0]).build();
        }

        if (!Pattern.compile(CHART_FILE_REGEX).matcher(fileChartBody).matches()) {
            QueryChartFacade.message[0] = "fail due chart file contain invalid structure";
            if (!partMessage.isEmpty()) {
                querySocketFacade.send(SocketBodyDto.builder().userId(userID).key(socketKey).message(partMessage + QueryChartFacade.message[0]).build());
                queryService.addLogging("POST", partMessage + QueryChartFacade.message[0]);
            } else {
                queryService.addLogging("POST", execMessage + " triggered, " + QueryChartFacade.message[0]);
            }
            return CustomResponseDto.builder().ErrorCode(ErrorCode.FIVE.getCode()).ErrorMessage(QueryChartFacade.message[0]).build();
        }

        String sql;
        try {
            sql = fileChartBody.substring(fileChartBody.indexOf("``SQL") + 5, fileChartBody.indexOf("``ENDSQL")).replaceAll("\t", " ");
        } catch (Exception e) {
            QueryChartFacade.message[0] = "fail due chart file contain invalid structure";
            if (!partMessage.isEmpty()) {
                querySocketFacade.send(SocketBodyDto.builder().userId(userID).key(socketKey).message(partMessage + QueryChartFacade.message[0]).build());
                queryService.addLogging("POST", partMessage + QueryChartFacade.message[0]);
            } else {
                queryService.addLogging("POST", execMessage + " triggered, " + QueryChartFacade.message[0]);
            }
            return CustomResponseDto.builder().ErrorCode(ErrorCode.FIVE.getCode()).ErrorMessage(QueryChartFacade.message[0]).build();
        }

        String formattedQuery = queryService.convertGlobalDateTime(queryService.getFormattedQuery(sql, chartDto.getPrompt()));

        String html;
        try {
            html = fileChartBody.substring(fileChartBody.indexOf("``HTML") + 6, fileChartBody.indexOf("``ENDHTML"));
        } catch (Exception e) {
            QueryChartFacade.message[0] = "fail due chart file contain invalid structure";
            if (!partMessage.isEmpty()) {
                querySocketFacade.send(SocketBodyDto.builder().userId(userID).key(socketKey).message(partMessage + QueryChartFacade.message[0]).build());
                queryService.addLogging("POST", partMessage + QueryChartFacade.message[0]);
            } else {
                queryService.addLogging("POST", execMessage + " triggered, " + QueryChartFacade.message[0]);
            }
            return CustomResponseDto.builder().ErrorCode(ErrorCode.FIVE.getCode()).ErrorMessage(QueryChartFacade.message[0]).build();
        }

        String dbFile = null;
        String dbms = chartDto.getDbms();
        if (dbms != null && dbms.equals("sqlite")) {
            dbFile = queryServiceSQLite.getDBFileRepo(userID);
            if (dbFile.equals(QueryServiceSQLite.DBFILE + " doesn't exist!") || dbFile.equals("There is no active dbfile defined!") ||
                    dbFile.startsWith("error : {")) {
                querySocketFacade.send(SocketBodyDto.builder().userId(userID).key(socketKey).message(partMessage + "fail due: " + dbFile).build());
                queryService.addLogging("POST", partMessage + "fail due: " + dbFile);
                return CustomResponseDto.builder().ErrorCode(ErrorCode.FIVE.getCode()).ErrorMessage("fail due: " + dbFile).build();
            } else {
                dbFile = dbFile.concat(".db");
            }
            dbms = null;
        } else if (dbms != null && !(dbms.equalsIgnoreCase("R") || dbms.equalsIgnoreCase("PY"))) {
            dbms = null;

        }

        CustomResponseDto customResponseDto;
        if (dbms != null) {
            SocketDto socketDto = SocketDto.builder().script(formattedQuery).owner(chartDto.getOwner()).format("text").build();
            Object evaluation = null;
            if (dbms.equalsIgnoreCase("R")) {
                evaluation = queryRFacade.evaluate(socketDto, connectionId);
                querySocketFacade.send(SocketBodyDto.builder().userId(userID).key(socketKey).message("Successfully").build());
            } else if (dbms.equalsIgnoreCase("PY")) {
                evaluation = queryPythonFacade.evaluate(socketDto, connectionId);
                querySocketFacade.send(SocketBodyDto.builder().userId(userID).key(socketKey).message("Successfully").build());
            }
            if (!(evaluation instanceof CustomResponseDto)) {
                try {
                    objectMapper.readTree(evaluation.toString());
                    customResponseDto = CustomResponseDto.builder().Success(true).message(evaluation.toString()).build();
                } catch (JsonProcessingException e) {
                    customResponseDto = CustomResponseDto.builder().message(evaluation.toString()).build();
                }
            } else {
                customResponseDto = (CustomResponseDto) evaluation;
                try {
                    if (customResponseDto.getSuccess()) {
                        objectMapper.readTree(customResponseDto.getMessage());
                    }
                } catch (JsonProcessingException e) {
                    customResponseDto.setSuccess(false);
                    customResponseDto.setMessage(e.getMessage());
                }
            }
            return customResponseDto;
        } else {

            String resultValidity = queryService.checkValidityWithLabel(formattedQuery, connectionId, userID, "cache", dbFile, false);
            if (resultValidity.startsWith("Error") || resultValidity.startsWith("error") || resultValidity.startsWith("maxRunningTimeOut") || resultValidity.startsWith("MaxRows")) {
                QueryChartFacade.message[0] = resultValidity;
                if (!partMessage.isEmpty()) {
                    querySocketFacade.send(SocketBodyDto.builder().userId(userID).key(socketKey).message(partMessage + QueryChartFacade.message[0]).build());
                    queryService.addLogging("POST", partMessage + QueryChartFacade.message[0]);
                } else {
                    queryService.addLogging("POST", execMessage + " triggered, " + QueryChartFacade.message[0]);
                }
                return CustomResponseDto.builder().ErrorCode(ErrorCode.THREE.getCode()).ErrorMessage(QueryChartFacade.message[0]).build();
            }

            String structuredJson = new String(queryServiceJSON.structureJson(resultValidity, false, queryService.getQueryDataTypes(connectionId, formattedQuery, true), "cache")
                    .getBytes(), StandardCharsets.UTF_8);

            if (structuredJson.startsWith("Error") || structuredJson.startsWith("error")) {
                QueryChartFacade.message[0] = structuredJson;
                if (!partMessage.isEmpty()) {
                    querySocketFacade.send(SocketBodyDto.builder().userId(userID).key(socketKey).message(partMessage + QueryChartFacade.message[0]).build());
                    queryService.addLogging("POST", partMessage + QueryChartFacade.message[0]);
                } else {
                    queryService.addLogging("POST", execMessage + " triggered, " + QueryChartFacade.message[0]);
                }
                return CustomResponseDto.builder().ErrorCode(ErrorCode.FOUR.getCode()).ErrorMessage(structuredJson).build();
            }

            if (structuredJson.isEmpty()) {
                queryService.addLogging("POST", execMessage + " triggered with sql\t" + formattedQuery + "\t, fail due Empty value returned from query\t" + formattedQuery);
                QueryChartFacade.message[0] = "Empty value returned from query\t" + formattedQuery;
                if (!partMessage.isEmpty()) {
                    querySocketFacade.send(SocketBodyDto.builder().userId(userID).key(socketKey).message(partMessage + QueryChartFacade.message[0]).build());
                    queryService.addLogging("POST", partMessage + QueryChartFacade.message[0]);
                } else {
                    queryService.addLogging("POST", execMessage + " triggered, " + QueryChartFacade.message[0]);
                }
                return CustomResponseDto.builder().ErrorCode(ErrorCode.FIVE.getCode()).ErrorMessage(QueryChartFacade.message[0]).build();
            }

            JSONObject jsonObjectGlobal = new JSONObject(structuredJson);
            if (!jsonObjectGlobal.has("Input")) {
                QueryChartFacade.message[0] = "can't transform result as valid json";
                if (!partMessage.isEmpty()) {
                    querySocketFacade.send(SocketBodyDto.builder().userId(userID).key(socketKey).message(partMessage + QueryChartFacade.message[0]).build());
                    queryService.addLogging("POST", partMessage + QueryChartFacade.message[0]);
                } else {
                    queryService.addLogging("POST", execMessage + " triggered, " + QueryChartFacade.message[0]);
                }
                return CustomResponseDto.builder().ErrorCode(ErrorCode.SIX.getCode()).ErrorMessage(QueryChartFacade.message[0]).build();
            }
            queryService.addLogging("POST", execMessage + " triggered, Successfully");

            if (object instanceof Chart3Dto) {
                final String[] resultMixedJson = {queryServiceChart.getMixedResultJson(jsonObjectGlobal, html)};
                JsonNode parameter = chartDto.getParameter();
                if (parameter != null) {
                    parameter.fieldNames().forEachRemaining(s -> resultMixedJson[0] = resultMixedJson[0].replaceAll(s, parameter.get(s).asText()));
                }
                querySocketFacade.send(SocketBodyDto.builder().userId(userID).key(socketKey).message(partMessage + resultMixedJson[0]).build());
                return CustomResponseDto.builder().Success(true).message(resultMixedJson[0]).build();
            }

            ChartDto chartDtoBuilder = ChartDto.builder().dataParameter(chartDto.getDataParameter()).parameter(chartDto.getParameter()).build();
            if (isExecData) {
                return CustomResponseDto.builder().Success(true).DetailedMessage(queryServiceChart.constructDetailsData(jsonObjectGlobal, chartDtoBuilder, html)).build();
            }
            return CustomResponseDto.builder().Success(true).DetailedMessage(queryServiceChart.constructDetails(jsonObjectGlobal, chartDtoBuilder, html)).build();
        }
    }

    @Override
    public CustomResponseDto refreshDateTime() {
        return queryService.loadListDateTimeFunctions();
    }
}
