package co.kr.coresolutions.facades.Impl;

import co.kr.coresolutions.commons.CustomResponseDto;
import co.kr.coresolutions.commons.ResponseCodes;
import co.kr.coresolutions.commons.ResponseDto;
import co.kr.coresolutions.facades.IQueryControlFacade;
import co.kr.coresolutions.facades.IQuerySocketFacade;
import co.kr.coresolutions.model.Connection;
import co.kr.coresolutions.model.dtos.ExcelTemplateDto;
import co.kr.coresolutions.model.dtos.FileControlDto;
import co.kr.coresolutions.model.dtos.FileReportControlDto;
import co.kr.coresolutions.model.dtos.SocketBodyDto;
import co.kr.coresolutions.service.Constants;
import co.kr.coresolutions.service.QueryService;
import co.kr.coresolutions.service.QueryServiceControl;
import com.fasterxml.jackson.databind.ObjectMapper;
import lombok.RequiredArgsConstructor;
import lombok.SneakyThrows;
import lombok.extern.slf4j.Slf4j;
import lombok.val;
import org.apache.poi.hssf.usermodel.HSSFWorkbook;
import org.apache.poi.openxml4j.opc.OPCPackage;
import org.apache.poi.ss.usermodel.Cell;
import org.apache.poi.ss.usermodel.Row;
import org.apache.poi.ss.usermodel.Sheet;
import org.apache.poi.ss.usermodel.Workbook;
import org.apache.poi.xssf.usermodel.XSSFWorkbook;
import org.json.JSONArray;
import org.json.JSONObject;
import org.springframework.http.HttpStatus;
import org.springframework.stereotype.Component;
import org.springframework.util.Assert;
import org.springframework.util.StringUtils;

import javax.annotation.PostConstruct;
import javax.servlet.http.HttpServletResponse;
import java.io.File;
import java.io.FileOutputStream;
import java.io.IOException;
import java.nio.charset.StandardCharsets;
import java.nio.file.Files;
import java.nio.file.NoSuchFileException;
import java.nio.file.Path;
import java.nio.file.Paths;
import java.nio.file.StandardOpenOption;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.Comparator;
import java.util.Date;
import java.util.List;
import java.util.concurrent.Callable;
import java.util.concurrent.CompletableFuture;
import java.util.concurrent.ExecutorService;
import java.util.concurrent.Executors;
import java.util.stream.IntStream;

@Component
@RequiredArgsConstructor
@Slf4j
public class QueryControlFacade implements IQueryControlFacade {
    private final Constants constants;
    private String BATCH_REPORT_DIR;
    private final IQuerySocketFacade querySocketFacade;
    private final QueryService queryService;
    private final ObjectMapper objectMapper;
    private final QueryServiceControl queryServiceControl;
    private static volatile CompletableFuture completableFuture;
    private String EXCEL_DIR;

    @PostConstruct
    public void init() {
        BATCH_REPORT_DIR = constants.getBatchQueryDir();
        EXCEL_DIR = constants.getExcelDir();
    }

    @Override
    @SneakyThrows
    public ResponseDto createFile(FileControlDto fileControlDto) {
        String pathRoot = BATCH_REPORT_DIR + fileControlDto.getOwner() + File.separator + fileControlDto.getReportDir() + File.separator;
        if (!Paths.get(pathRoot).toFile().isDirectory())
            Files.createDirectories(Paths.get(pathRoot));
        Path filepath = Paths.get(pathRoot + fileControlDto.getControlFilename());
        File file = filepath.toFile();
        if (file.exists()) {
            file.delete();
        }
        file = Files.write(filepath, fileControlDto.getFile().getBytes(StandardCharsets.UTF_8), StandardOpenOption.CREATE).toFile();
        file.setExecutable(true, false);

        return ResponseDto.ok();
    }

    @Override
    @SneakyThrows
    public ResponseDto deleteDir(FileControlDto fileControlDto) {
        val pathRoot = BATCH_REPORT_DIR + fileControlDto.getOwner() + File.separator + fileControlDto.getReportDir();
        try (val pathStream = Files.walk(Paths.get(pathRoot))) {
            pathStream
                    .sorted(Comparator.reverseOrder())
                    .map(Path::toFile)
                    .forEach(File::delete);
            Assert.isTrue(!Files.isDirectory(Paths.get(pathRoot)), "[Assertion failed] - the directory doesn't deleted");
            queryService.addLogging("DELETE", "deleteDir success with path\t" + fileControlDto.getReportDir());
        } catch (Exception e) {
            queryService.addLogging("DELETE", "deleteDir fail with name\t" + fileControlDto.getReportDir());
            return ResponseDto.builder().errorCode(HttpStatus.BAD_REQUEST.value()).message("fail\t" + e.getMessage()).build();
        }
        return ResponseDto.ok();
    }

    @Override
    @SneakyThrows
    public ResponseDto reportSubmit(FileControlDto fileControlDto, HttpServletResponse response) {
        val pathRoot = BATCH_REPORT_DIR + fileControlDto.getOwner() + File.separator + fileControlDto.getReportDir();
        if (!Files.isDirectory(Paths.get(pathRoot))) {
            queryService.addLogging("POST", "directory doesn't exist");
            return ResponseDto.builder().errorCode(HttpStatus.NOT_FOUND.value()).message("directory doesn't exists").build();
        }
        val deleteOnExists = new ArrayList<String>();
        try {
            val fileReportControlDto = objectMapper.
                    readValue(new String(Files.readAllBytes(Paths.get(pathRoot + File.separator + fileControlDto.getControlFilename())), StandardCharsets.UTF_8),
                            FileReportControlDto.class);
            ExecutorService executorService = java.util.concurrent.Executors.newCachedThreadPool();

            if (!fileReportControlDto.getGetData().isNull()) {
                Callable<ResponseDto> callable = () -> queryServiceControl.callRestServer(fileReportControlDto.getGetData(), pathRoot);
                ResponseDto responseDto = executorService.submit(callable).get();
                if (responseDto.getSuccessCode() != ResponseCodes.OK_RESPONSE) {
                    executorService.shutdown();
                    return responseDto;
                }
            }

            if (!fileReportControlDto.getCheckExists1().isNull()) {
                Callable<ResponseDto> callable = () -> queryServiceControl.checkExist(fileReportControlDto.getCheckExists1());
                ResponseDto responseDto = executorService.submit(callable).get();
                if (responseDto == null || responseDto.getSuccessCode() != ResponseCodes.OK_RESPONSE) {
                    executorService.shutdown();
                    return responseDto == null ? ResponseDto.ERROR_PARSE : responseDto;
                } else {
                    deleteOnExists.addAll(Arrays.asList(StringUtils.delimitedListToStringArray(responseDto.getMessage(), ",")));
                }
            }

            if (!fileReportControlDto.getCreateReport().isNull()) {
                Runnable runnable = () -> queryServiceControl.createReport(fileReportControlDto.getCreateReport(), fileControlDto.getOwner(), response);
                executorService.submit(runnable).get();
            }

            if (!fileReportControlDto.getCheckExists2().isNull()) {
                Callable<ResponseDto> callable = () -> queryServiceControl.checkExist(fileReportControlDto.getCheckExists2());
                ResponseDto responseDto = executorService.submit(callable).get();
                if (responseDto == null || responseDto.getSuccessCode() != ResponseCodes.OK_RESPONSE) {
                    executorService.shutdown();
                    return responseDto == null ? ResponseDto.ERROR_PARSE : responseDto;
                } else {
                    deleteOnExists.addAll(Arrays.asList(StringUtils.delimitedListToStringArray(responseDto.getMessage(), ",")));
                }
            }

            if (!fileReportControlDto.getSendEmail().isNull()) {
                Callable<ResponseDto> callable = () -> queryServiceControl.sendEmail(fileReportControlDto.getSendEmail());
                ResponseDto responseDto = executorService.submit(callable).get();
                if (responseDto.getSuccessCode() != ResponseCodes.OK_RESPONSE) {
                    executorService.shutdown();
                    return responseDto;
                }
            }

            if (!fileReportControlDto.getCheckExists3().isNull()) {
                Callable<ResponseDto> callable = () -> queryServiceControl.checkExist(fileReportControlDto.getCheckExists3());
                ResponseDto responseDto = executorService.submit(callable).get();
                if (responseDto == null || responseDto.getSuccessCode() != ResponseCodes.OK_RESPONSE) {
                    executorService.shutdown();
                    return responseDto == null ? ResponseDto.ERROR_PARSE : responseDto;
                } else {
                    deleteOnExists.addAll(Arrays.asList(StringUtils.delimitedListToStringArray(responseDto.getMessage(), ",")));
                }
            }

            if (!deleteOnExists.isEmpty()) {
                queryServiceControl.deleteBulkFiles(deleteOnExists);
            }

        } catch (Exception e) {
            String msg;
            if (e instanceof NoSuchFileException) {
                msg = "no such file exists: " + fileControlDto.getControlFilename();
            } else {
                msg = "something went wrong with file name: " + fileControlDto.getControlFilename();
            }
            e.printStackTrace();
            queryService.addLogging("POST", msg);
            return ResponseDto
                    .builder()
                    .errorCode(HttpStatus.INTERNAL_SERVER_ERROR.value())
                    .message(msg)
                    .build();
        }
        return ResponseDto.ok();
    }

    @Override
    public CustomResponseDto reportExcel(ExcelTemplateDto excelTemplateDto) {
        String connectionID = excelTemplateDto.getConnectionID();
        String partMessage = "reports/excel triggered, ";
        final Long[] fileSize = {0L};
        String xlID = excelTemplateDto.getXlID();
        if (xlID == null || xlID.isEmpty()) {
            CustomResponseDto customResponseDto = CustomResponseDto.builder().build();
            customResponseDto.setMessage("xl_id should be provided!");
            customResponseDto.setSuccess(false);
            querySocketFacade.send(SocketBodyDto.builder().userId(excelTemplateDto.getOwner()).key("SQL_EXCEL").message(partMessage + customResponseDto.getMessage()).build());
            queryService.addLogging("POST", partMessage + customResponseDto.getMessage());
            return customResponseDto;
        } else if (xlID.length() > 16) {
            CustomResponseDto customResponseDto = CustomResponseDto.builder().build();
            customResponseDto.setMessage("The length of xl_id should be less than 16 characters!");
            customResponseDto.setSuccess(false);
            querySocketFacade.send(SocketBodyDto.builder().userId(excelTemplateDto.getOwner()).key("SQL_EXCEL").message(partMessage + customResponseDto.getMessage()).build());
            queryService.addLogging("POST", partMessage + customResponseDto.getMessage());
            return customResponseDto;
        }
        String sql = queryService.convertGlobalDateTime(excelTemplateDto.getSql());
        String outName = queryService.convertGlobalDateTime(excelTemplateDto.getOutName());
        CustomResponseDto customResponseDto = queryService.getResultQuery(connectionID, sql);
        if (customResponseDto.getSuccess()) {
            JSONArray jsonArray = new JSONArray(customResponseDto.getMessage());
            String internalConnectionID = "quadmax";
            Connection infoConnection = queryService.getInfoConnection(internalConnectionID);
            if (excelTemplateDto.getMaxRecords() != null && jsonArray.length() > excelTemplateDto.getMaxRecords()) {
                customResponseDto.setMessage("Too much records are retrieved from query , it's stopped!");
                customResponseDto.setSuccess(false);
                logAndPush(excelTemplateDto, false, jsonArray.length(), fileSize[0], xlID, connectionID, sql, outName, partMessage, customResponseDto.getMessage(), internalConnectionID, infoConnection);
                return customResponseDto;
            }

            String excelExtension = excelTemplateDto.getExtension();
            List<String> permittedExtensions = Arrays.asList("xls", "xlsm", "xlm", "xlsx");
            if (!permittedExtensions.contains(excelExtension)) {
                customResponseDto.setMessage("Unknow exec extention !");
                customResponseDto.setSuccess(false);
                logAndPush(excelTemplateDto, false, jsonArray.length(), fileSize[0], xlID, connectionID, sql, outName, partMessage, customResponseDto.getMessage(), internalConnectionID, infoConnection);
                return customResponseDto;
            }
            try {
                if (!Paths.get(EXCEL_DIR + excelTemplateDto.getTemplate()).toFile().exists()) {
                    customResponseDto.setMessage("No template file defined!");
                    customResponseDto.setSuccess(false);
                    logAndPush(excelTemplateDto, false, jsonArray.length(), fileSize[0], xlID, connectionID, sql, outName, partMessage, customResponseDto.getMessage(), internalConnectionID, infoConnection);
                    return customResponseDto;
                }

                if (!Paths.get(EXCEL_DIR + excelTemplateDto.getOwner()).toFile().exists()) {
                    Files.createDirectories(Paths.get(EXCEL_DIR + excelTemplateDto.getOwner()));
                }
            } catch (IOException e) {
                e.printStackTrace();
            }
            Workbook workbook = new XSSFWorkbook();
            Path pathTemplate = null;
            byte[] bytes = null;
            if (excelExtension.equals("xls")) {
                workbook = new HSSFWorkbook();
            } else if (excelExtension.equals("xlsm") || excelExtension.equals("xlm")) {
                try {
                    pathTemplate = Paths.get(EXCEL_DIR + excelTemplateDto.getTemplate());
                    bytes = Files.readAllBytes(pathTemplate);
                    workbook = new XSSFWorkbook(OPCPackage.open(pathTemplate.toFile()));
                } catch (Exception e) {
                    customResponseDto.setErrorMessage(e.getMessage());
                    customResponseDto.setMessage(null);
                    logAndPush(excelTemplateDto, false, jsonArray.length(), fileSize[0], xlID, connectionID, sql, outName, partMessage, customResponseDto.getErrorMessage(), internalConnectionID, infoConnection);
                    return customResponseDto;
                }
            }
            Workbook finalWorkbook = workbook;
            int indexSheet = finalWorkbook.getSheetIndex(excelTemplateDto.getDataSheet());
            if (indexSheet > -1) {
                finalWorkbook.removeSheetAt(indexSheet);
            }
            Sheet sheet = finalWorkbook.createSheet(excelTemplateDto.getDataSheet());
            Row rowHeader = sheet.createRow(0);
            final int[] columnCountHeader = {0};
            jsonArray.getJSONObject(0).keySet().forEach(key -> {
                Cell cell = rowHeader.createCell(columnCountHeader[0]++);
                cell.setCellValue(key);
            });

            String pathOutput = EXCEL_DIR + excelTemplateDto.getOwner() + File.separator + outName + "." + excelTemplateDto.getExtension();

            if (!jsonArray.isEmpty()) {
                completableFuture = CompletableFuture.runAsync(() -> IntStream
                        .range(0, jsonArray.length())
                        .forEach(rowCount -> {
                            JSONObject jsonObject = jsonArray.getJSONObject(rowCount);
                            final int[] columnCount = {0};
                            Row row = sheet.createRow(++rowCount);
                            jsonObject.keySet().forEach(key -> {
                                Cell cell = row.createCell(columnCount[0]++);
                                cell.setCellValue(jsonObject.get(key).toString());
                            });

                            if (columnCount[0] > 0 && columnCount[0] % 1000 == 0) {
                                ExecutorService executorService = Executors.newSingleThreadExecutor();
                                executorService.submit(() -> {
                                    try (FileOutputStream outputStream = new FileOutputStream(pathOutput)) {
                                        finalWorkbook.write(outputStream);
                                    } catch (Exception e) {
                                        customResponseDto.setErrorMessage("fail, " + e.getMessage());
                                    }
                                });
                                executorService.shutdown();
                            }
                        }));

                completableFuture.thenRunAsync(() -> {
                    try (FileOutputStream outputStream = new FileOutputStream(pathOutput)) {
                        finalWorkbook.write(outputStream);
                        fileSize[0] = Files.size(Paths.get(pathOutput));
                        finalWorkbook.close();
                    } catch (Exception e) {
                        customResponseDto.setErrorMessage("fail, " + e.getMessage());
                    }
                });
            } else {
                customResponseDto.setErrorMessage("can't format result to valid json");
            }

            if (pathTemplate != null) {
                ExecutorService executorService = Executors.newSingleThreadExecutor();
                Path finalPathTemplate = pathTemplate;
                byte[] finalBytes = bytes;
                executorService.submit(() -> {
                    try {
                        Files.deleteIfExists(finalPathTemplate);
                        Files.write(finalPathTemplate, finalBytes, StandardOpenOption.CREATE_NEW);
                    } catch (IOException e) {
                        e.printStackTrace();
                    }
                });
                executorService.shutdown();
            }

            completableFuture.thenRunAsync(() -> logToDBTriggered(excelTemplateDto.getOwner(), customResponseDto.getSuccess(), excelTemplateDto.getDesc(),
                    (customResponseDto.getSuccess() ? "success" : customResponseDto.getErrorMessage()), connectionID, sql,
                    excelTemplateDto.getTemplate(), outName, excelTemplateDto.getExtension(),
                    excelTemplateDto.getMaxRecords(), jsonArray.length(), fileSize[0], xlID, internalConnectionID, infoConnection));
        }

        if (customResponseDto.getSuccess() || customResponseDto.getErrorMessage() != null) {
            customResponseDto.setMessage(null);
        }
        querySocketFacade.send(SocketBodyDto.builder().userId(excelTemplateDto.getOwner()).key("SQL_EXCEL").message(partMessage +
                (customResponseDto.getSuccess() ? "success" : customResponseDto.getErrorMessage())).build());
        queryService.addLogging("POST", partMessage + (customResponseDto.getSuccess() ? "success" : customResponseDto.getErrorMessage()));
        return customResponseDto;
    }

    private void logAndPush(ExcelTemplateDto excelTemplateDto, boolean status, int length, Long fileSize, String xlID, String connectionID, String sql, String outName, String partMessage, String message, String internalConnectionID, Connection infoConnection) {
        ExecutorService executorService = Executors.newSingleThreadExecutor();
        executorService.submit(() -> {
            querySocketFacade.send(SocketBodyDto.builder().userId(excelTemplateDto.getOwner()).key("SQL_EXCEL").message(partMessage + message).build());
            queryService.addLogging("POST", partMessage + message);
            logToDBTriggered(excelTemplateDto.getOwner(), status, excelTemplateDto.getDesc(), message, connectionID, sql,
                    excelTemplateDto.getTemplate(), outName, excelTemplateDto.getExtension(),
                    excelTemplateDto.getMaxRecords(), length, fileSize, xlID, internalConnectionID, infoConnection);
        });
        executorService.shutdown();
    }

    private void logToDBTriggered(String ownerID, Boolean status, String desc, String message, String internalConnectionID, String sql, String template, String outName,
                                  String ext, Long maxRecords, Integer records, Long fileSize, String xlID, String connectionID, Connection infoConnection) {
        ExecutorService executorService = Executors.newSingleThreadExecutor();
        executorService.submit(() -> {
            try {
                queryService.checkValidityUpdateClauses("INSERT INTO " + infoConnection.getSCHEME() +
                        "T_SQL_EXCEL_TRIGGERED (XL_ID, OWNER_ID, XL_STATUS, XL_DESC, XL_MESSAGE, XL_CONNECTIONID, XL_SQL, XL_TEMPLATE, XL_OUTNAME, XL_EXT, XL_MAXRECORDS, " +
                        "XL_RECORDS, XL_FILESIZE, LOAD_DTTM) VALUES ('"
                        + xlID + "', '"
                        + ownerID + "', '"
                        + (status ? "success" : "fail") + "', "
                        + (desc != null ? "'" + desc.replace("'", "") + "'" : null) + ", '"
                        + message.replace("'", "") + "', '"
                        + internalConnectionID + "', '"
                        + sql.replace("'", "") + "', '"
                        + template + "', '"
                        + outName + "', '"
                        + ext + "', "
                        + maxRecords + ", "
                        + records + ", "
                        + fileSize + ", '"
                        + new SimpleDateFormat("yyyyMMddHHmmss").format(new Date()) + "')", connectionID);
            } catch (Exception e) {
                queryService.addLogging("POST", "reports/excel, fail when inserting data to T_SQL_EXCEL_TRIGGERED\t" + e.getMessage());
            }
        });
        executorService.shutdown();

        ExecutorService executorService2 = Executors.newSingleThreadExecutor();
        executorService2.submit(() -> {
            try {
                String resultDelete = queryService.checkValidityUpdateClauses("DELETE FROM " + infoConnection.getSCHEME() + "T_SQL_EXCEL WHERE XL_ID = '" + xlID + "'", connectionID);
                if (resultDelete.startsWith("success")) {
                    queryService.checkValidityUpdateClauses("INSERT INTO " + infoConnection.getSCHEME() +
                            "T_SQL_EXCEL (XL_ID, OWNER_ID, XL_STATUS, XL_DESC, XL_MESSAGE, XL_CONNECTIONID, XL_SQL, XL_TEMPLATE, XL_OUTNAME, XL_EXT, XL_MAXRECORDS, " +
                            "XL_RECORDS, XL_FILESIZE, LOAD_DTTM) VALUES ('"
                            + xlID + "', '"
                            + ownerID + "', '"
                            + (status ? "success" : "fail") + "', "
                            + (desc != null ? "'" + desc.replace("'", "") + "'" : null) + ", '"
                            + message.replace("'", "") + "', '"
                            + internalConnectionID + "', '"
                            + sql.replace("'", "") + "', '"
                            + template + "', '"
                            + outName + "', '"
                            + ext + "', "
                            + maxRecords + ", "
                            + records + ", "
                            + fileSize + ", '"
                            + new SimpleDateFormat("yyyyMMddHHmmss").format(new Date()) + "')", connectionID);
                } else {
                    queryService.addLogging("POST", "reports/excel, fail when delete from T_SQL_EXCEL, error is\t" + resultDelete);
                }
            } catch (Exception e) {
                queryService.addLogging("POST", "reports/excel, fail when inserting data to T_SQL_EXCEL_TRIGGERED\t" + e.getMessage());
            }
        });
        executorService2.shutdown();
    }
}
