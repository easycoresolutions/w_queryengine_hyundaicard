package co.kr.coresolutions.facades.Impl;

import co.kr.coresolutions.commons.CustomResponseDto;
import co.kr.coresolutions.commons.QueryChunkPublisher;
import co.kr.coresolutions.facades.IQuerySocketFacade;
import co.kr.coresolutions.facades.IQueryTargetFacade;
import co.kr.coresolutions.model.Connection;
import co.kr.coresolutions.model.dtos.SocketBodyDto;
import co.kr.coresolutions.model.dtos.Target2Dto;
import co.kr.coresolutions.model.dtos.TargetDto;
import co.kr.coresolutions.service.QueryService;
import co.kr.coresolutions.service.QueryServicePolling;
import com.fasterxml.jackson.databind.ObjectMapper;
import lombok.RequiredArgsConstructor;
import lombok.extern.slf4j.Slf4j;
import lombok.val;
import org.apache.commons.lang.StringEscapeUtils;
import org.json.JSONArray;
import org.json.JSONObject;
import org.springframework.stereotype.Component;

import java.time.LocalDateTime;
import java.time.format.DateTimeFormatter;
import java.util.Arrays;

@Component
@RequiredArgsConstructor
public class QueryTargetFacade implements IQueryTargetFacade {
    private final QueryService queryService;
    private final QueryServicePolling queryServicePolling;
    private final ObjectMapper objectMapper;
    public static String[] message = {""};
    public static String[] messageUpload = {""};
    private final IQuerySocketFacade querySocketFacade;
    private final QueryChunkPublisher queryChunkPublisher;

    @Override
    //fixme rollback cannot be performed for some db
    public CustomResponseDto targetList(TargetDto targetDto) {
        val connectionID = targetDto.getConnection();
        String partMessage = "target/targetlist triggered, ";
        boolean isConnectionFileExists = queryService.isConnectionIdFileExists(connectionID);
        if (!isConnectionFileExists) {
            QueryTargetFacade.message[0] = "fail due to connection file with name\t" + connectionID + "\tdoesn't exist";
            querySocketFacade.send(SocketBodyDto.builder().userId(targetDto.getUserID()).key("TARGETLIST").message(partMessage + QueryTargetFacade.message[0]).build());
            queryService.addLogging("POST", partMessage + QueryTargetFacade.message[0]);
            return CustomResponseDto.builder().ErrorMessage(QueryTargetFacade.message[0]).build();
        }
        String inputConnection = targetDto.getInputConnection();
        isConnectionFileExists = queryService.isConnectionIdFileExists(inputConnection);
        if (!isConnectionFileExists) {
            QueryTargetFacade.message[0] = "fail due to input_connection  file with name\t" + inputConnection + "\tdoesn't exist";
            querySocketFacade.send(SocketBodyDto.builder().userId(targetDto.getUserID()).key("TARGETLIST").message(partMessage + QueryTargetFacade.message[0]).build());
            queryService.addLogging("POST", partMessage + QueryTargetFacade.message[0]);
            return CustomResponseDto.builder().ErrorMessage(QueryTargetFacade.message[0]).build();
        }

        String tID = targetDto.getParameters().getTId();
        Connection infoConnectionOutput = queryService.getInfoConnection(connectionID);
        String outputSCHEMA = infoConnectionOutput.getSCHEME();
        //first delete from connection output
        String sql = "DELETE FROM " + outputSCHEMA + "T_TARGETLIST_CUST WHERE T_ID = '" + tID + "'";
        String resultValidity = queryService.checkValidityUpdateClauses(sql, connectionID);
        if (handleFail(targetDto, partMessage, sql, resultValidity)) {
            return CustomResponseDto.builder().ErrorMessage(QueryTargetFacade.message[0]).build();
        }

        //second read all records from input_connection sql, and insert in T_TARGETLIST_CUST
        String inSql = queryService.convertGlobalDateTime(queryService.convertGlobalVariable(queryService.getFormattedString(targetDto.getSql())));

        queryChunkPublisher.attach(queryServicePolling, targetDto, connectionID,
                outputSCHEMA + "T_TARGETLIST_CUST", null, true, infoConnectionOutput.getDBMS(), partMessage);
        CustomResponseDto customResponseDto = queryService.loadInChunk(null, inputConnection, inSql, "", "", false, 2000);
        queryChunkPublisher.detach(queryServicePolling);

        if (QueryTargetFacade.message[0].startsWith("error")) {
            querySocketFacade.send(SocketBodyDto.builder().userId(targetDto.getUserID()).key("TARGETLIST").message(partMessage +
                    QueryTargetFacade.message[0]).build());
            queryService.addLogging("POST", partMessage + QueryTargetFacade.message[0]);
//            queryService.rollBack(listRandomIds);
            return CustomResponseDto.builder().Success(false).ErrorMessage(QueryTargetFacade.message[0]).build();
        }

        int querySize;
        if (customResponseDto.getSuccess()) {
            querySize = Integer.parseInt(customResponseDto.getMessage());
            if (querySize <= 0) {
                querySocketFacade.send(SocketBodyDto.builder().userId(targetDto.getUserID()).key("TARGETLIST").message(partMessage +
                        "fail due empty records found").build());
                queryService.addLogging("POST", partMessage + "fail due empty records found");
                return CustomResponseDto.builder().Success(false).ErrorMessage("fail due empty records found").build();
            }

            //delete from output connection T_TARGETLIST
            sql = "DELETE FROM " + outputSCHEMA + "T_TARGETLIST WHERE T_ID = '" + tID + "'";
            resultValidity = queryService.checkValidityUpdateClauses(sql, connectionID);
            if (handleFail(targetDto, partMessage, sql, resultValidity)) {
//                queryService.rollBack(listRandomIds);
                return CustomResponseDto.builder().ErrorMessage(QueryTargetFacade.message[0]).build();
            }

            LocalDateTime localDateTime = LocalDateTime.now();
            String dateNow = localDateTime.format(DateTimeFormatter.ofPattern("yyyyMMdd"));

            sql = "INSERT INTO " + outputSCHEMA + "T_TARGETLIST (T_ID, T_NAME, T_PASS, T_TYPE, T_STATUS, DIR_ID, CUST_NO, PRR, RUN_DATE, " +
                    "VALID_DATE, SOURCE_TYPE, SOURCE_INFO, SOURCE_JSON, REG_EMP_ID, REG_DATE) VALUES ('" + tID + "', '" + targetDto.getParameters().getTName() + "', '', " +
                    "'M', 'ACTIVE', null, " + querySize + ", 0, '" + dateNow + "', '" +
                    localDateTime.plusDays(30).format(DateTimeFormatter.ofPattern("yyyyMMdd")) + "', 'DASH', '" + StringEscapeUtils.escapeSql(inSql) + "', null, '" + targetDto.getUserID()
                    + "', '" + dateNow + "')";
            resultValidity = queryService.checkValidityUpdateClauses(sql, connectionID);
            if (handleFail(targetDto, partMessage, sql, resultValidity)) {
//                queryService.rollBack(listRandomIds);
                return CustomResponseDto.builder().ErrorMessage(QueryTargetFacade.message[0]).build();
            }

//            listRandomIds.add(randomInsert);
            querySocketFacade.send(SocketBodyDto.builder().userId(targetDto.getUserID()).key("TARGETLIST").message(partMessage + "SUCCESS").build());
            queryService.addLogging("POST", partMessage + "SUCCESS");
            return CustomResponseDto.builder().Success(true).count(querySize).build();
        } else {
//            queryService.rollBack(listRandomIds);
            querySocketFacade.send(SocketBodyDto.builder().userId(targetDto.getUserID()).key("TARGETLIST").message(partMessage +
                    customResponseDto.getMessage()).build());
            queryService.addLogging("POST", partMessage + customResponseDto.getMessage());
            return CustomResponseDto.builder().Success(false).ErrorMessage(customResponseDto.getMessage()).build();
        }
    }

    private boolean handleFail(Object object, String partMessage, String sql, String resultValidity) {
        if (!resultValidity.startsWith("success")) {
            if (object instanceof TargetDto) {
                TargetDto targetDto = (TargetDto) object;
                QueryTargetFacade.message[0] = "fail due\t" + resultValidity + "\t, sql is\t" + sql;
                querySocketFacade.send(SocketBodyDto.builder().userId(targetDto.getUserID()).key("TARGETLIST").message(partMessage + QueryTargetFacade.message[0]).build());
                queryService.addLogging("POST", partMessage + QueryTargetFacade.message[0]);
                return true;
            } else {
                Target2Dto targetDto = (Target2Dto) object;
                QueryTargetFacade.messageUpload[0] = "fail due\t" + resultValidity + "\t, sql is\t" + sql;
                querySocketFacade.send(SocketBodyDto.builder().userId(targetDto.getUserID()).key("TARGETLIST_UPLOAD").message(partMessage + QueryTargetFacade.messageUpload[0]).build());
                queryService.addLogging("POST", partMessage + QueryTargetFacade.messageUpload[0]);
                return true;
            }
        }
        return false;
    }

    @Override
    public Object upload(Target2Dto target2Dto) {
        val connectionID = target2Dto.getConnection();
        String partMessage = "targetlist/upload triggered, ";
        boolean isConnectionFileExists = queryService.isConnectionIdFileExists(connectionID);
        if (!isConnectionFileExists) {
            QueryTargetFacade.messageUpload[0] = "fail due to connection file with name\t" + connectionID + "\tdoesn't exist";
            querySocketFacade.send(SocketBodyDto.builder().userId(target2Dto.getUserID()).key("TARGETLIST_UPLOAD").message(partMessage + QueryTargetFacade.messageUpload[0]).build());
            queryService.addLogging("POST", partMessage + QueryTargetFacade.messageUpload[0]);
            return CustomResponseDto.builder().ErrorMessage(QueryTargetFacade.messageUpload[0]).build();
        }
        val tID = target2Dto.getParameters().getTId();
        JSONArray outJsonArray = new JSONArray();
        Arrays.asList(target2Dto.getFile().split("\n")).forEach(value -> {
            JSONObject jsonObject = new JSONObject();
            jsonObject.put("CUST_ID", value);
            jsonObject.put("T_ID", tID);
            outJsonArray.put(jsonObject);
        });
        Connection infoConnectionOutput = queryService.getInfoConnection(connectionID);
        val outputSCHEMA = infoConnectionOutput.getSCHEME();
        String sql = "DELETE FROM " + outputSCHEMA + "T_TARGETLIST_CUST WHERE T_ID = '" + tID + "'";
        String resultValidity = queryService.checkValidityUpdateClauses(sql, connectionID);
        if (handleFail(target2Dto, partMessage, sql, resultValidity)) {
            return CustomResponseDto.builder().ErrorMessage(QueryTargetFacade.messageUpload[0]).build();
        }
        //delete from output connection T_TARGETLIST
        sql = "DELETE FROM " + outputSCHEMA + "T_TARGETLIST WHERE T_ID = '" + tID + "'";
        resultValidity = queryService.checkValidityUpdateClauses(sql, connectionID);

        if (handleFail(target2Dto, partMessage, sql, resultValidity)) {
            return CustomResponseDto.builder().ErrorMessage(QueryTargetFacade.messageUpload[0]).build();
        }
        LocalDateTime localDateTime = LocalDateTime.now();
        val dateNow = localDateTime.format(DateTimeFormatter.ofPattern("yyyyMMdd"));

        sql = "INSERT INTO " + outputSCHEMA + "T_TARGETLIST (T_ID, T_NAME, T_PASS, T_TYPE, T_STATUS, DIR_ID, CUST_NO, PRR, RUN_DATE, " +
                "VALID_DATE, SOURCE_TYPE, SOURCE_INFO, SOURCE_JSON, REG_EMP_ID, REG_DATE) VALUES ('" + tID + "', '" + target2Dto.getParameters().getTName() + "', '', " +
                "'M', 'ACTIVE', null, " + outJsonArray.length() + ", 0, '" + dateNow + "', '" +
                localDateTime.plusDays(30).format(DateTimeFormatter.ofPattern("yyyyMMdd")) + "', 'DASH', null, null, '" + target2Dto.getUserID() + "', '" + dateNow + "')";
        resultValidity = queryService.checkValidityUpdateClauses(sql, connectionID);

        if (handleFail(target2Dto, partMessage, sql, resultValidity)) {
            return CustomResponseDto.builder().ErrorMessage(QueryTargetFacade.messageUpload[0]).build();
        }

        //insert records in connection output T_TARGETLIST_CUST
        boolean success = queryServicePolling.insert(
                objectMapper.valueToTree(outJsonArray.toList()), connectionID, outputSCHEMA + "T_TARGETLIST_CUST", null, true, infoConnectionOutput.getDBMS());
        if (!success) {
            querySocketFacade.send(SocketBodyDto.builder().userId(target2Dto.getUserID()).key("TARGETLIST_UPLOAD").message(partMessage + QueryTargetFacade.messageUpload[0]).build());
            queryService.addLogging("POST", partMessage + QueryTargetFacade.messageUpload[0]);
            return CustomResponseDto.builder().ErrorMessage("fail due\t" + QueryPollingFacade.message[0]).build();
        } else {
            QueryTargetFacade.messageUpload[0] = "SUCCESS";
            querySocketFacade.send(SocketBodyDto.builder().userId(target2Dto.getUserID()).key("TARGETLIST_UPLOAD").message(partMessage + QueryTargetFacade.messageUpload[0]).build());
            queryService.addLogging("POST", partMessage + QueryTargetFacade.messageUpload[0]);
            return CustomResponseDto.builder().Success(true).count(outJsonArray.length()).build();
        }
    }
}

