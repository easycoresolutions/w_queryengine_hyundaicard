package co.kr.coresolutions.facades.Impl;

import co.kr.coresolutions.commons.CustomResponseDto;
import co.kr.coresolutions.facades.IQueryQuadMaxWorkFacade;
import co.kr.coresolutions.model.Connection;
import co.kr.coresolutions.model.dtos.QuadMaxWorkDto;
import co.kr.coresolutions.service.QueryService;
import co.kr.coresolutions.util.Util;
import com.fasterxml.jackson.databind.JsonNode;
import lombok.RequiredArgsConstructor;
import lombok.extern.slf4j.Slf4j;
import org.json.JSONArray;
import org.json.JSONObject;
import org.springframework.jdbc.BadSqlGrammarException;
import org.springframework.stereotype.Component;

@Component
@RequiredArgsConstructor
@Slf4j
public class QueryQuadMaxFacade implements IQueryQuadMaxWorkFacade {
    private final QueryService queryService;

    @Override
    public CustomResponseDto countRows(String connectionID, String tableName) {
        String partMessage = "/{" + connectionID + "}/{" + tableName + "} triggered, ";
        boolean isConnectionFileExists = queryService.isConnectionIdFileExists(connectionID);
        if (!isConnectionFileExists) {
            queryService.addLogging("GET", partMessage + "fail due to connection file with name\t" + connectionID + "\tdoesn't exist");
            return CustomResponseDto.builder().message("fail due to connection file with name\t" + connectionID + "\tdoesn't exist").build();
        }
        Connection connectionInfo = queryService.getInfoConnection(connectionID);
        if (!connectionInfo.getDBMS().isEmpty() && !connectionInfo.getID().isEmpty() && !connectionInfo.getPW().isEmpty()
                && !connectionInfo.getDRIVER().isEmpty() && !connectionInfo.getURL().isEmpty()) {
        	
            String count = queryService.checkValidity(("select count(*) CNT from " + connectionInfo.getSCHEME() + tableName).toUpperCase(), connectionID, "", "cache", false);
            
            if (!count.equals("") && !count.toLowerCase().contains("error")) {
                queryService.setRunStop("cache");
                queryService.addLogging("GET", partMessage + "success");
                return CustomResponseDto.builder().Success(true).message(String.valueOf(new JSONArray(count).getJSONObject(0).getInt("CNT"))).build();
            } else {
                queryService.addLogging("GET", partMessage + "fail due to an error\t" + count);
                return CustomResponseDto.builder().message("fail due to an error\t" + count).build();
            }
        } else {
            queryService.addLogging("GET", partMessage + "fail due missing some infos from connection " + connectionID + " file");
            return CustomResponseDto.builder().message("missing some infos from connection " + connectionID + " file").build();
        }
    }

    @Override
    public Object countRows(String connectionID, QuadMaxWorkDto quadMaxWorkDto) {
        String partMessage = "/{" + connectionID + "}/getcount triggered, ";
        boolean isConnectionFileExists = queryService.isConnectionIdFileExists(connectionID);
        if (!isConnectionFileExists) {
            queryService.addLogging("POST", partMessage + "fail due to connection file with name\t" + connectionID + "\tdoesn't exist");
            return CustomResponseDto.builder().message("fail due to connection file with name\t" + connectionID + "\tdoesn't exist").build();
        }
        JSONObject response = new JSONObject();
        quadMaxWorkDto.getNodes().fields().forEachRemaining(stringJsonNodeEntry -> {
            String key = stringJsonNodeEntry.getKey();
            JsonNode entryValue = stringJsonNodeEntry.getValue();	// 여기서 "WHERE CUST_TYPE_CD = 'T'" 
            if (entryValue.isTextual()) {
                String value = entryValue.asText();
                CustomResponseDto customResponseDto = queryService.getResultQuery
                        (connectionID,
                                ("select count(*) CNT from " + queryService.getInfoConnection(connectionID).getSCHEME() +
                                        quadMaxWorkDto.getProjectID() + "_" + key + " ")
                                        .toUpperCase().concat(value.isEmpty() ? "" : value));
                				//결과 예) select count(*) CNT from CMPWORK.A20201126000000857_N0009 WHERE CUST_TYPE_CD = 'T'
                if (customResponseDto.getSuccess()) {
                    response.put(key, new JSONArray(customResponseDto.getMessage()).getJSONObject(0).getInt("CNT"));
                } else {
                    response.put(key, customResponseDto.getErrorMessage());
                }
            } else {
                response.put(key, "not valid json value!");
            }
        });
        queryService.addLogging("POST", partMessage + "success");
        return CustomResponseDto.builder().Success(true).message(response.toString()).build();
    }
    
    @Override
    public Object submitCountRows(String connectionID, QuadMaxWorkDto quadMaxWorkDto) {
        String partMessage = "/{" + connectionID + "}/getcount triggered, ";
        boolean isConnectionFileExists = queryService.isConnectionIdFileExists(connectionID);
        if (!isConnectionFileExists) {
            queryService.addLogging("POST", partMessage + "fail due to connection file with name\t" + connectionID + "\tdoesn't exist");
            return CustomResponseDto.builder().message("fail due to connection file with name\t" + connectionID + "\tdoesn't exist").build();
        }
        JSONObject response = new JSONObject();
        quadMaxWorkDto.getNodes().fields().forEachRemaining(stringJsonNodeEntry -> {
            String key = stringJsonNodeEntry.getKey();
            JsonNode entryValue = stringJsonNodeEntry.getValue();	// 여기서 "WHERE CUST_TYPE_CD = 'T'" 
            if (entryValue.isTextual()) {
                String custTypeCd = Util.filterQueryStr(entryValue.asText());
                
                CustomResponseDto customResponseDto = queryService.getResultQuery(
                		connectionID,
                                ( "select count(*) CNT from " + queryService.getInfoConnection(connectionID).getSCHEME() + quadMaxWorkDto.getProjectID() + "_" + key )
                                	.toUpperCase().concat( (custTypeCd.equals("''")) ? "" : " WHERE CUST_TYPE_CD = " +  custTypeCd ) );
                
                				//결과 예) select count(*) CNT from CMPWORK.A20201126000000857_N0009 WHERE CUST_TYPE_CD = 'T'
                
                if (customResponseDto.getSuccess()) {
                    response.put(key, new JSONArray(customResponseDto.getMessage()).getJSONObject(0).getInt("CNT"));
                } else {
                    response.put(key, customResponseDto.getErrorMessage());
                }
            } else {
                response.put(key, "not valid json value!");
            }
        });
        queryService.addLogging("POST", partMessage + "success");
        return CustomResponseDto.builder().Success(true).message(response.toString()).build();
    }

    @Override
    public CustomResponseDto dropTable(String connectionID, String tableName) {
        String partMessage = "/{" + connectionID + "}/{" + tableName + "} triggered, ";
        boolean isConnectionFileExists = queryService.isConnectionIdFileExists(connectionID);
        if (!isConnectionFileExists) {
            queryService.addLogging("DELETE", partMessage + "fail due to connection file with name\t" + connectionID + "\tdoesn't exist");
            return CustomResponseDto.builder().message("fail due to connection file with name\t" + connectionID + "\tdoesn't exist")
                    .build();
        }
        try {
        	String sql = "				BEGIN\r\n" + 
        			"				   EXECUTE IMMEDIATE 'DROP TABLE "+tableName+"';\r\n" + 
        			"				EXCEPTION\r\n" + 
        			"				   WHEN OTHERS THEN\r\n" + 
        			"				      IF SQLCODE != -942 THEN\r\n" + 
        			"				         RAISE;\r\n" + 
        			"				      END IF;\r\n" + 
        			"				END;";
        	String resultValidity = queryService.checkValidityUpdateClauses(sql, connectionID);
        	if (!resultValidity.startsWith("success")) {
        		queryService.addLogging("DELETE", partMessage + "fail due " + resultValidity);
        		return CustomResponseDto.builder().message("fail due\t" + resultValidity).build();
        	}
        } catch(BadSqlGrammarException e) {
        	
        }
        queryService.addLogging("DELETE", partMessage + "success");
        return CustomResponseDto.builder().Success(true).message("success").build();
    }
}

