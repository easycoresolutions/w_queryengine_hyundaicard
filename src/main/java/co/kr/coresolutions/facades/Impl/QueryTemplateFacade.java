package co.kr.coresolutions.facades.Impl;

import co.kr.coresolutions.commons.CustomResponseDto;
import co.kr.coresolutions.commons.Monitor;
import co.kr.coresolutions.facades.IQueryChartMonitorFacade;
import co.kr.coresolutions.facades.IQueryPythonFacade;
import co.kr.coresolutions.facades.IQueryRFacade;
import co.kr.coresolutions.facades.IQuerySocketFacade;
import co.kr.coresolutions.facades.IQueryTemplateFacade;
import co.kr.coresolutions.model.dtos.Chart3Dto;
import co.kr.coresolutions.model.dtos.Chart4Dto;
import co.kr.coresolutions.model.dtos.CommandTemplateLiquid;
import co.kr.coresolutions.model.dtos.SocketBodyDto;
import co.kr.coresolutions.model.dtos.SocketDto;
import co.kr.coresolutions.service.QueryService;
import co.kr.coresolutions.service.QueryServiceChart;
import co.kr.coresolutions.service.QueryServiceSQLite;

import com.fasterxml.jackson.core.JsonProcessingException;
import com.fasterxml.jackson.databind.JsonNode;
import com.fasterxml.jackson.databind.ObjectMapper;
import liqp.Template;
import lombok.RequiredArgsConstructor;
import lombok.extern.slf4j.Slf4j;
import org.apache.commons.collections4.map.MultiKeyMap;
import org.json.JSONObject;
import org.springframework.stereotype.Component;

import javax.servlet.http.HttpServletRequest;
import java.io.File;
import java.io.IOException;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;
import java.util.Map;

@Component
@RequiredArgsConstructor
@Slf4j

public class QueryTemplateFacade implements IQueryTemplateFacade, IQueryChartMonitorFacade {
    private final QueryService queryService;
    public static String[] message = {""};
    private final ObjectMapper objectMapper;
    private final IQuerySocketFacade querySocketFacade;
    private final IQueryRFacade queryRFacade;
    private final IQueryPythonFacade queryPythonFacade;
    private final QueryServiceChart queryServiceChart;
    private final QueryServiceSQLite queryServiceSQLite;

    @Override
    public Object execChartInternal(Chart3Dto chartDto, HttpServletRequest httpServletRequest) {
        QueryTemplateFacade.message[0] = "";
        final String[] format = {""};
        String dir = chartDto.getDir();
        String fileChartBody;
        String queryID = chartDto.getQueryID();
        if (queryID != null) {
            queryID = queryService.makeQueryId(queryID);
        } else {
            queryID = "cache";
        }

        String api = "template/query_json";
        String partMessage = api + " triggered, queryid : " + queryID + " , ";
        String userID = chartDto.getUserId();
        String socketKey = api.toUpperCase();
        queryService
                .saveCacheQueryID(queryID, Monitor.builder().api(api).ID(Monitor.ID.IDBuilder().queryID(queryID).owner(chartDto.getOwner()).build()).build());

        if (chartDto.getChartFile() != null) {
            fileChartBody = queryService.getFileFromDir(chartDto.getChartFile(), "charts" + (dir != null && !dir.isEmpty() ? File.separator + dir : ""));
        } else {
            fileChartBody = queryService.getFileFromDir(chartDto.getQueryFile(), "business_query" + (dir != null && !dir.isEmpty() ? File.separator + dir : ""));
        }
        if (fileChartBody.isEmpty()) {
            QueryTemplateFacade.message[0] = "chartfile/queryfile has no contents !";
            querySocketFacade.send(SocketBodyDto.builder().userId(userID).key(socketKey).message(partMessage + QueryTemplateFacade.message[0]).build());
            queryService.addLogging("POST", partMessage + QueryTemplateFacade.message[0]);
            queryService.removeCacheQueryID(queryID);
            queryService.setRunStop(queryID);
            return CustomResponseDto.builder().ErrorMessage(QueryTemplateFacade.message[0]).format(format[0]).build();
        }

        if (!(fileChartBody.contains("``HTML") && fileChartBody.contains("``ENDHTML"))) {
            QueryTemplateFacade.message[0] = "HTML ..ENDHTML section is required !";
            querySocketFacade.send(SocketBodyDto.builder().userId(userID).key(socketKey).message(partMessage + QueryTemplateFacade.message[0]).build());
            queryService.addLogging("POST", partMessage + QueryTemplateFacade.message[0]);
            queryService.removeCacheQueryID(queryID);
            queryService.setRunStop(queryID);
            return CustomResponseDto.builder().ErrorMessage(QueryTemplateFacade.message[0]).format(format[0]).build();
        }

        final boolean[] success = {true};
        final String[] html = {fileChartBody.substring(fileChartBody.indexOf("``HTML") + 6, fileChartBody.indexOf("``ENDHTML"))};
        Arrays.stream(html[0].split("\n")).limit(1).filter(s -> s.trim().matches("\\/(.*?)")).findFirst().ifPresent(s -> {
            if (fileChartBody.contains("``HTML" + s)) {
                format[0] = s.substring(1).trim();
                html[0] = html[0].substring(html[0].indexOf(s) + s.length());
            }
        });

        final String[] originalHtml = {html[0]};
        Template template = Template.parse(html[0]);
        Map map;
        try {
            map = objectMapper.readValue(chartDto.getTemplate().toString(), Map.class);
        } catch (IOException e) {
            queryService.addLogging("POST", partMessage + "fail due: " + e.getMessage());
            querySocketFacade.send(SocketBodyDto.builder().userId(userID).key(socketKey).message(partMessage + e.getMessage()).build());
            queryService.removeCacheQueryID(queryID);
            queryService.setRunStop(queryID);
            return CustomResponseDto.builder().format(format[0]).ErrorMessage(e.getMessage()).build();
        }
        List<Map> savedTemplatesObjectResult = new ArrayList<>();

        html[0] = template.render(map);

        if (!QueryService.isCacheQueryIDExist(queryID)) {
            queryService.removeCacheQueryID(queryID);
            queryService.setRunStop(queryID);
            queryService.addLogging("POST", partMessage + "fail due: killed by request!");
            querySocketFacade.send(SocketBodyDto.builder().userId(userID).key(socketKey).message(partMessage + "fail due: killed by request!").build());
            return CustomResponseDto.builder().format(format[0]).ErrorMessage("fail due: killed by request!").build();
        }

        MultiKeyMap<String, List<String>> multiMap = queryServiceChart.getMatrixForConversion(html[0], fileChartBody);
        Map finalMap = map;
        Map finalMap1 = map;
        String finalQueryID = queryID;
        String baseUrl = queryService.getBaseUrl(httpServletRequest, false);

        String finalQueryID1 = queryID;
        multiMap.forEach((keySet, htmlList) -> {
            if (!success[0]) {
                return;
            }
            boolean isApi = false;
            List<String> keys = Arrays.asList(keySet.getKeys());
            String dataSetName = keys.get(0).trim();
            Template templateSql;
            if (keys.get(0).equals("api")) {
                isApi = true;
            }
            if (keys.size() > 3) { //keySet = datasetname, connectionID, templateObject, sql || //keySet = api, datasetName, url, body
                templateSql = Template.parse(keys.get(3));
            } else {
                templateSql = Template.parse(keys.get(2));
            }

            if (!QueryService.isCacheQueryIDExist(finalQueryID)) {
                success[0] = false;
                html[0] = "fail due: killed by request!";
                return;
            }

            String resultSqlConversion;

            if (!savedTemplatesObjectResult.isEmpty()) {
                resultSqlConversion = templateSql.render(finalMap1);
            } else {
                resultSqlConversion = templateSql.render(finalMap);
            }

            if (!QueryService.isCacheQueryIDExist(finalQueryID)) {
                success[0] = false;
                html[0] = "fail due: killed by request!";
                return;
            }
            CustomResponseDto customResponseDto;
            if (isApi) {
                //we can't stop rest api when it start, the only solution to add queryID as global to all apis
                customResponseDto = queryServiceChart
                        .postForObject(baseUrl + "/" + keys.get(2).trim(),
                                resultSqlConversion, chartDto.getPrompt(), userID, partMessage
                                        + "sub_api " + keys.get(2) + " triggered, ", socketKey);
            } else {
                String dbFile = null;
                String dbms = chartDto.getDbms();
                if (dbms != null && dbms.equalsIgnoreCase("sqlite")) {
                    dbFile = queryServiceSQLite.getDBFileRepo(userID);
                    if (dbFile.equals(QueryServiceSQLite.DBFILE + " doesn't exist!") || dbFile.equals("There is no active dbfile defined!") ||
                            dbFile.startsWith("error : {")) {
                        queryService.removeCacheQueryID(finalQueryID1);
                        querySocketFacade.send(SocketBodyDto.builder().userId(userID).key(socketKey).message(partMessage + "fail due: " + dbFile).build());
                        queryService.addLogging("POST", partMessage + "fail due: " + dbFile);
                        success[0] = false;
                        html[0] = "fail due: " + dbFile;
                        return;
                    } else {
                        dbFile = dbFile.concat(".db");
                    }
                } else if (dbms != null && !(dbms.equalsIgnoreCase("R") || dbms.equalsIgnoreCase("PY"))) {
                    dbms = null;
                }

                String connectionId = keys.get(1).trim();

                if (dbms != null) {
                    SocketDto socketDto = SocketDto.builder().script(resultSqlConversion).owner(chartDto.getOwner()).format("text").build();
                    Object evaluation = null;
                    if (dbms.equalsIgnoreCase("R")) {
                        evaluation = queryRFacade.evaluate(socketDto, connectionId);
                        querySocketFacade.send(SocketBodyDto.builder().userId(userID).key(socketKey).message("Successfully").build());
                    } else if (dbms.equalsIgnoreCase("PY")) {
                        evaluation = queryPythonFacade.evaluate(socketDto, connectionId);
                        querySocketFacade.send(SocketBodyDto.builder().userId(userID).key(socketKey).message("Successfully").build());
                    }
                    if (!(evaluation instanceof CustomResponseDto)) {
                        try {
                            objectMapper.readTree(evaluation.toString());
                            customResponseDto = CustomResponseDto.builder().Success(true).message(evaluation.toString()).build();
                        } catch (JsonProcessingException e) {
                            customResponseDto = CustomResponseDto.builder().message(evaluation.toString()).build();
                        }
                    } else {
                        customResponseDto = (CustomResponseDto) evaluation;
                        try {
                            if (customResponseDto.getSuccess()) {
                                objectMapper.readTree(customResponseDto.getMessage());
                            }
                        } catch (JsonProcessingException e) {
                            customResponseDto.setSuccess(false);
                            customResponseDto.setMessage(e.getMessage());
                        }
                    }
                } else {
                    customResponseDto = queryServiceChart
                            .getResultFetchQuery(finalQueryID, dataSetName, userID, partMessage, connectionId,
                                    resultSqlConversion, chartDto.getPrompt(), socketKey, dbFile);
                }
            }

            if (!QueryService.isCacheQueryIDExist(finalQueryID)) {
                success[0] = false;
                html[0] = "fail due: killed by request!";
                return;
            }

            if (isApi) {
                html[0] = html[0].replace("%%" + keys.get(1) + "%%.RESPONSE", customResponseDto.getMessage());
            } else {
                if (customResponseDto.getSuccess()) {
                    if (keys.size() > 3 && keys.get(2).trim().equalsIgnoreCase("templateObject")) {
                        JsonNode jsonNode;
                        try {
                            jsonNode = objectMapper.readTree(customResponseDto.getMessage());
                            JSONObject jsonObject = new JSONObject();
                            jsonObject.put(keys.get(0).trim(), jsonNode);
                            savedTemplatesObjectResult.add(jsonObject.toMap());
                            Template template2Html = Template.parse(originalHtml[0]);
                            savedTemplatesObjectResult.forEach(finalMap1::putAll);
                            html[0] = template2Html.render(finalMap1);
                        } catch (IOException e) {
                            success[0] = false;
                            html[0] = e.getMessage();
                        }
                    }
                    html[0] = queryServiceChart.getMixedResultJson2(keys.get(0).trim(), customResponseDto.getMessage(), htmlList, html[0]);
                } else {
                    success[0] = customResponseDto.getErrorMessage().equalsIgnoreCase("{}");
                    if (customResponseDto.getErrorMessage().equalsIgnoreCase("{}")) {
                        JSONObject jsonArrayInput = new JSONObject("{\"Input\" : []}");
                        html[0] = queryServiceChart.getMixedResultJson2(keys.get(0).trim(), jsonArrayInput.toString(), htmlList, html[0]);
                        html[0] = html[0].replace("{\"Input\":[]}", "{}");
                    } else {
                        html[0] = customResponseDto.getErrorMessage();
                    }
                }
            }
        });
        if (success[0]) {
            querySocketFacade.send(SocketBodyDto.builder().userId(userID).key(socketKey).message("Successfully").build());
            queryService.addLogging("POST", partMessage + "Successfully");
            queryService.removeCacheQueryID(queryID);
            queryService.setRunStop(queryID);
            return CustomResponseDto.builder().format(format[0]).Success(true).message(html[0]).build();
        } else {
            querySocketFacade.send(SocketBodyDto.builder().userId(userID).key(socketKey).message(html[0]).build());
            queryService.addLogging("POST", partMessage + html[0]);
            queryService.removeCacheQueryID(queryID);
            queryService.setRunStop(queryID);
            return CustomResponseDto.builder().format(format[0]).message(html[0]).build();
        }
    }

    @Override
    public Object execCommandTemplate(HttpServletRequest httpServletRequest, CommandTemplateLiquid commandTemplateLiquid) {
        String queryID = commandTemplateLiquid.getQueryID();
        if (queryID != null) {
            queryID = queryService.makeQueryId(queryID);
        } else {
            queryID = "cache";
        }

        String api = "template/command_output";
        String userID = commandTemplateLiquid.getUserID();
        String socketKey = api.toUpperCase();
        String partMessage = api + " triggered, queryid : " + queryID + " , ";
        queryService.saveCacheQueryID(queryID, Monitor.builder().api(api).ID(Monitor.ID.IDBuilder().queryID(queryID).owner(userID).build()).build());

        String fileCommandBody = queryService.getFileFromDir(commandTemplateLiquid.getCommandFile(), "command_script");
        if (fileCommandBody.isEmpty()) {
            QueryTemplateFacade.message[0] = "fail due command_script file with name\t" + commandTemplateLiquid.getCommandFile() + "\tdoesn't exist";
            querySocketFacade.send(SocketBodyDto.builder().userId(userID).key(socketKey).message(partMessage + QueryTemplateFacade.message[0]).build());
            queryService.removeCacheQueryID(queryID);
            queryService.setRunStop(queryID);
            queryService.addLogging("POST", partMessage + QueryTemplateFacade.message[0]);
            return CustomResponseDto.builder().message(QueryTemplateFacade.message[0]).build();
        }

        final boolean[] success = {true};
        final String[] html = {fileCommandBody.substring(fileCommandBody.indexOf("``HTML") + 6, fileCommandBody.indexOf("``ENDHTML"))};
        Template template = Template.parse(html[0]);

        Map map;
        try {
            map = objectMapper.readValue(commandTemplateLiquid.getTemplate().toString(), Map.class);
        } catch (IOException e) {
            queryService.addLogging("POST", partMessage + "fail due: " + e.getMessage());
            querySocketFacade.send(SocketBodyDto.builder().userId(userID).key(socketKey).message(partMessage + e.getMessage()).build());
            queryService.removeCacheQueryID(queryID);
            queryService.setRunStop(queryID);
            return CustomResponseDto.builder().message(e.getMessage()).build();
        }

        String url = queryService.getBaseUrl(httpServletRequest, true) + "/command";

        html[0] = template.render(map);

        if (!QueryService.isCacheQueryIDExist(queryID)) {
            queryService.addLogging("POST", partMessage + "fail due: killed by request!");
            querySocketFacade.send(SocketBodyDto.builder().userId(userID).key(socketKey).message(partMessage + "fail due: killed by request!").build());
            return CustomResponseDto.builder().message("fail due: killed by request!").build();
        }

        MultiKeyMap<String, List<String>> multiMap = queryServiceChart.getMatrixForConversionCommand(html[0], fileCommandBody);

        String finalQueryID = queryID;
        multiMap.forEach((keySet, htmlList) -> {

            if (!success[0]) {
                return;
            }

            //datasetname, commandProgram, script_file_name, option, command
            List<String> keys = Arrays.asList(keySet.getKeys());

            Template templateCommand = Template.parse(keys.get(4));

            if (!QueryService.isCacheQueryIDExist(finalQueryID)) {
                success[0] = false;
                html[0] = "fail due: killed by request!";
                return;
            }

            String resultCommandConversion = templateCommand.render(map);

            if (!QueryService.isCacheQueryIDExist(finalQueryID)) {
                success[0] = false;
                html[0] = "fail due: killed by request!";
                return;
            }

            CustomResponseDto customResponseDto = queryServiceChart
                    .getResultFetchCommand(finalQueryID, url, keys, userID, partMessage, resultCommandConversion, commandTemplateLiquid.getPrompt(), socketKey);

            if (!QueryService.isCacheQueryIDExist(finalQueryID)) {
                success[0] = false;
                html[0] = "fail due: killed by request!";
                return;
            }

            if (customResponseDto.getSuccess()) {
                html[0] = html[0].replace("%%" + keys.get(0) + "%%", customResponseDto.getMessage());
            } else {
                html[0] = customResponseDto.getErrorMessage();
                success[0] = false;
            }
        });

        if (success[0]) {
            querySocketFacade.send(SocketBodyDto.builder().userId(userID).key(socketKey).message("Successfully").build());
            queryService.addLogging("POST", partMessage + "Successfully");
            queryService.removeCacheQueryID(queryID);
            queryService.setRunStop(queryID);
            return CustomResponseDto.builder().Success(true).message(html[0]).build();
        } else {
            querySocketFacade.send(SocketBodyDto.builder().userId(userID).key(socketKey).message(html[0]).build());
            queryService.addLogging("POST", partMessage + html[0]);
            queryService.removeCacheQueryID(queryID);
            queryService.setRunStop(queryID);
            return CustomResponseDto.builder().message(html[0]).build();
        }
    }

    @Override
    public Object execChartInternalSelectJson(Chart4Dto chartDto, String connectionId) {
        String queryID = chartDto.getQueryID();
        if (queryID != null) {
            queryID = queryService.makeQueryId(queryID);
        } else {
            queryID = "cache";
        }
        String api = "template/select_json/{" + connectionId + "}";
        String userID = chartDto.getUserId();
        String socketKey = api.toUpperCase();
        String partMessage = api + " triggered, " + (queryID != null && !queryID.isEmpty() ? "queryid : " + queryID + " , " : "");
        queryService.saveCacheQueryID(queryID, Monitor.builder().api(api).ID(Monitor.ID.IDBuilder().queryID(queryID).owner(userID).build()).build());

        boolean isConnectionFileExists = queryService.isConnectionIdFileExists(connectionId);
        if (!isConnectionFileExists) {
            QueryTemplateFacade.message[0] = "fail due to connection file with name\t" + connectionId + "\tdoesn't exist";
            querySocketFacade.send(SocketBodyDto.builder().userId(userID).key(socketKey).message(partMessage + QueryTemplateFacade.message[0]).build());
            queryService.addLogging("POST", partMessage + QueryTemplateFacade.message[0]);
            queryService.removeCacheQueryID(queryID);
            queryService.setRunStop(queryID);
            return CustomResponseDto.builder().message(QueryTemplateFacade.message[0]).build();
        }

        Template template = Template.parse(chartDto.getSql().replace("@@USERID@@", userID));

        if (!QueryService.isCacheQueryIDExist(queryID)) {
            queryService.removeCacheQueryID(queryID);
            queryService.setRunStop(queryID);
            querySocketFacade.send(SocketBodyDto.builder().userId(userID).key(socketKey).message(partMessage + "fail due: killed by request!").build());
            queryService.addLogging("POST", partMessage + "fail due: killed by request!");
            return CustomResponseDto.builder().message("fail due: killed by request!").build();
        }

        Map map;
        try {
            map = objectMapper.readValue(chartDto.getTemplate().toString(), Map.class);
        } catch (IOException e) {
            querySocketFacade.send(SocketBodyDto.builder().userId(userID).key(socketKey).message(partMessage + e.getMessage()).build());
            queryService.addLogging("POST", partMessage + "fail due: " + e.getMessage());
            queryService.removeCacheQueryID(queryID);
            queryService.setRunStop(queryID);
            return CustomResponseDto.builder().message(e.getMessage()).build();
        }

        String query = template.render(map);

        if (!QueryService.isCacheQueryIDExist(queryID)) {
            queryService.removeCacheQueryID(queryID);
            queryService.setRunStop(queryID);
            querySocketFacade.send(SocketBodyDto.builder().userId(userID).key(socketKey).message(partMessage + "fail due: killed by request!").build());
            queryService.addLogging("POST", partMessage + "fail due: killed by request!");
            return CustomResponseDto.builder().message("fail due: killed by request!").build();
        }

        String dbms = chartDto.getDbms();

        if (dbms != null) {
            SocketDto socketDto = SocketDto.builder().script(query).owner(chartDto.getOwner()).format("text").build();
            Object evaluation;
            if (dbms.equals("R")) {
                evaluation = queryRFacade.evaluate(socketDto, connectionId);
                querySocketFacade.send(SocketBodyDto.builder().userId(userID).key(socketKey).message("Successfully").build());
            } else if (dbms.equals("PY")) {
                evaluation = queryPythonFacade.evaluate(socketDto, connectionId);
                querySocketFacade.send(SocketBodyDto.builder().userId(userID).key(socketKey).message("Successfully").build());
            } else {
                evaluation = CustomResponseDto.builder().message("dbms " + dbms + " not available!").build();
                querySocketFacade.send(SocketBodyDto.builder().userId(userID).key(socketKey)
                        .message(partMessage + "fail due: dbms " + dbms + " not available!").build());
            }
            queryService.removeCacheQueryID(queryID);
            if (!(evaluation instanceof CustomResponseDto)) {
                try {
                    objectMapper.readTree(evaluation.toString());
                    queryService.removeCacheQueryID(queryID);
                    queryService.setRunStop(queryID);
                    return CustomResponseDto.builder().Success(true).message(evaluation.toString()).build();
                } catch (JsonProcessingException e) {
                    queryService.removeCacheQueryID(queryID);
                	queryService.setRunStop(queryID);
                    return CustomResponseDto.builder().message(evaluation.toString()).build();
                }
            } else {
                CustomResponseDto customResponseDto = (CustomResponseDto) evaluation;
                try {
                    objectMapper.readTree(customResponseDto.getMessage());
                } catch (JsonProcessingException e) {
                    customResponseDto.setSuccess(false);
                }
                queryService.removeCacheQueryID(queryID);
                queryService.setRunStop(queryID);
                return customResponseDto;
            }
        } else {
            CustomResponseDto customResponseDto = queryServiceChart.getResultFetchQuery(queryID, "", userID, partMessage, connectionId, query, chartDto.getPrompt() == null
                    ? objectMapper.createObjectNode() : chartDto.getPrompt(), socketKey, chartDto.getDbFile());

            if (!QueryService.isCacheQueryIDExist(queryID)) {
                queryService.removeCacheQueryID(queryID);
                queryService.setRunStop(queryID);
                querySocketFacade.send(SocketBodyDto.builder().userId(userID).key(socketKey).message(partMessage + "fail due: killed by request!").build());
                queryService.addLogging("POST", partMessage + "fail due: killed by request!");
                return CustomResponseDto.builder().message("fail due: killed by request!").build();
            }

            if (customResponseDto.getSuccess()) {
                query = customResponseDto.getMessage();
            } else if (customResponseDto.getErrorMessage().equalsIgnoreCase("{}")) {
                query = customResponseDto.getErrorMessage();
            } else {
                queryService.removeCacheQueryID(queryID);
                queryService.setRunStop(queryID);
                querySocketFacade.send(SocketBodyDto.builder().userId(userID).key(socketKey).message(customResponseDto.getErrorMessage()).build());
                queryService.addLogging("POST", partMessage + customResponseDto.getErrorMessage());
                return CustomResponseDto.builder().message(customResponseDto.getErrorMessage()).build();
            }

            queryService.removeCacheQueryID(queryID);
            queryService.setRunStop(queryID);
            querySocketFacade.send(SocketBodyDto.builder().userId(userID).key(socketKey).message("Successfully").build());
            queryService.addLogging("POST", partMessage + "Successfully");
            return CustomResponseDto.builder().Success(true).message(query).build();
        }
    }

    @Override
    public Object listTemplates() {
        String partMessage = "template/monitorQuery triggered, ";
        queryService.addLogging("GET", partMessage);
        return queryServiceChart.getCacheQueryIDS();
    }

    @Override
    public Object deleteTemplate(String queryID) {
        String partMessage = "template/releaseQuery/{" + queryID + "} triggered, ";
        if (queryService.removeCacheQueryID(queryID)) {
            queryService.addLogging("DELETE", partMessage + "success");
            return CustomResponseDto.builder().message("template with queryid " + queryID + " deleted success").Success(true).build();
        }
        queryService.addLogging("DELETE", partMessage + "fail queryid does not exist");
        return CustomResponseDto.builder().message("queryid does not exist!").build();
    }
}
