package co.kr.coresolutions.facades.Impl;

import co.kr.coresolutions.commons.CustomResponseDto;
import co.kr.coresolutions.facades.IQueryCommentFacade;
import co.kr.coresolutions.model.dtos.CommentDto;
import co.kr.coresolutions.service.QueryService;
import co.kr.coresolutions.service.QueryServiceCommentTable;
import com.google.common.collect.Lists;
import lombok.RequiredArgsConstructor;
import org.json.JSONArray;
import org.springframework.stereotype.Component;

import java.util.LinkedList;
import java.util.List;
import java.util.UUID;
import java.util.stream.Collectors;

@Component
@RequiredArgsConstructor
public class QueryCommentTableFacade implements IQueryCommentFacade {
    private static final String mainTable = "t_ssbi_comments_index";
    private static final String authTable = "t_ssbi_comments_auth";
    private final QueryService queryService;
    private final QueryServiceCommentTable queryServiceCommentTable;

    @Override
    public Object addComment(String connectionID, String projectID, CommentDto commentDto) {
        String partMessage = "commentTable/{" + connectionID + "}/{" + projectID + "} triggered, ";
        boolean isConnectionFileExists = queryService.isConnectionIdFileExists(connectionID);
        if (!isConnectionFileExists) {
            queryService.addLogging("POST", partMessage + "fail due to connection file with name\t" + connectionID + "\t doesn't exist");
            return CustomResponseDto.builder().message("fail due to connection file with name\t" + connectionID + "\t doesn't exist").build();
        }
        if (!commentDto.getProjectID().equals(projectID)) {
            return CustomResponseDto.builder().message("fail due mismatch projectID from url and body!").build();
        }

        List<String> listRandomIds = new LinkedList<>();
        CustomResponseDto customResponseDto = queryServiceCommentTable.insertForMain(listRandomIds, connectionID, mainTable, projectID, commentDto);
        if (!customResponseDto.getSuccess()) {
            queryService.addLogging("POST", partMessage + customResponseDto.getMessage());
            queryService.rollBack(listRandomIds);
            return customResponseDto;
        }

        customResponseDto = queryServiceCommentTable
                .deleteAndInsertAuxiliaryTable(authTable,
                        commentDto.getAuth().stream().map(CommentDto.CommentAuth::getAuthID).collect(Collectors.toList()), listRandomIds, projectID, connectionID);
        if (!customResponseDto.getSuccess()) {
            queryService.addLogging("POST", partMessage + customResponseDto.getMessage());
            queryService.rollBack(listRandomIds);
            return customResponseDto;
        }

        queryService.addLogging("POST", partMessage + "Success insert");
        customResponseDto.setMessage("Success insert");
        queryService.commit(listRandomIds);

        return customResponseDto;
    }

    @Override
    public Object listComments(String connectionID, String projectID) {
        String partMessage = "commentTable/{" + connectionID + "}/{" + projectID + "} triggered, ";
        boolean isConnectionFileExists = queryService.isConnectionIdFileExists(connectionID);
        if (!isConnectionFileExists) {
            queryService.addLogging("GET", partMessage + "fail due to connection file with name\t" + connectionID + "\t doesn't exist");
            return CustomResponseDto.builder().message("fail due to connection file with name\t" + connectionID + "\t doesn't exist").build();
        }

        String resultValidity = queryService
                .checkValidity("SELECT CMT_CONTENTS FROM " + mainTable + " WHERE projectID = '" + projectID + "'",
                        connectionID, "", "", false);
        if (resultValidity.equalsIgnoreCase(connectionID) || resultValidity.startsWith("Error") || resultValidity.startsWith("error")) {
            queryService.addLogging("GET", partMessage + "fail due error is " + resultValidity);
            return CustomResponseDto.builder().message(resultValidity).build();
        }

        JSONArray jsonArray = new JSONArray(resultValidity);
        if (jsonArray.isEmpty()) {
            queryService.addLogging("GET", partMessage + "fail due projectID " + projectID + " doesn't exist");
            return CustomResponseDto.builder().message("projectID " + projectID + " doesn't exist").build();
        }
        queryService.addLogging("GET", partMessage + "success");
        return CustomResponseDto.builder().Success(true).message(jsonArray.getJSONObject(0).get("CMT_CONTENTS").toString()).build();
    }

    @Override
    public Object deleteComment(String connectionID, String projectID) {
        String partMessage = "commentTable/{" + connectionID + "}/{" + projectID + "} triggered, ";
        boolean isConnectionFileExists = queryService.isConnectionIdFileExists(connectionID);
        if (!isConnectionFileExists) {
            queryService.addLogging("DELETE", partMessage + "fail due to connection file with name\t" + connectionID + "\t doesn't exist");
            return CustomResponseDto.builder().message("fail due to connection file with name\t" + connectionID + "\t doesn't exist").build();
        }

        List<String> listRandomIds = new LinkedList<>();
        CustomResponseDto customResponseDto = CustomResponseDto.builder().build();

        String randomDeleteMain = UUID.randomUUID().toString();
        String resultDeleteMain = queryService
                .updateClausesWithRollback(
                        Lists.newArrayList("DELETE FROM " + mainTable + " WHERE projectID = '" + projectID + "'"), connectionID, randomDeleteMain);
        if (!resultDeleteMain.startsWith("success")) {
            queryService.addLogging("DELETE", partMessage + resultDeleteMain);
            customResponseDto.setMessage(resultDeleteMain);
            return customResponseDto;
        }
        listRandomIds.add(randomDeleteMain);

        String randomDeleteAuxiliary = UUID.randomUUID().toString();
        String resultDeleteAuxiliary = queryService
                .updateClausesWithRollback(
                        Lists.newArrayList("DELETE FROM " + authTable + " WHERE projectID = '" + projectID + "'"), connectionID, randomDeleteAuxiliary);
        if (!resultDeleteAuxiliary.startsWith("success")) {
            queryService.rollBack(listRandomIds);
            queryService.addLogging("DELETE", partMessage + resultDeleteAuxiliary);
            customResponseDto.setMessage(resultDeleteAuxiliary);
            return customResponseDto;
        }
        listRandomIds.add(randomDeleteAuxiliary);

        queryService.commit(listRandomIds);
        queryService.addLogging("DELETE", partMessage + "Success delete");
        customResponseDto.setMessage("Success delete");
        customResponseDto.setSuccess(true);
        return customResponseDto;
    }
}
