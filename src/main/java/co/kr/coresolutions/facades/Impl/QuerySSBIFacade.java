package co.kr.coresolutions.facades.Impl;

import co.kr.coresolutions.commons.CustomResponseDto;
import co.kr.coresolutions.facades.IQuerySSBIFacade;
import co.kr.coresolutions.model.Connection;
import co.kr.coresolutions.model.dtos.SSBIDto;
import co.kr.coresolutions.service.QueryService;
import lombok.RequiredArgsConstructor;
import lombok.extern.slf4j.Slf4j;
import org.apache.commons.lang.StringEscapeUtils;
import org.springframework.stereotype.Component;

import java.text.SimpleDateFormat;
import java.util.Date;

@Component
@RequiredArgsConstructor
@Slf4j
public class QuerySSBIFacade implements IQuerySSBIFacade {
    private final QueryService queryService;

    @Override
    public Object logToDB(SSBIDto ssbiDto, String owner, String ssbiID) {
        String partMessage = "ssbisql/{" + ssbiID + "}/{" + owner + "}\ttriggered, ";
        queryService.addLogging("POST", partMessage);
        String connectionID = "quadmax";
        Connection infoConnectionInput = queryService.getInfoConnection(connectionID);
        try {
            String resultQuery = queryService
                    .checkValidityUpdateClauses("DELETE FROM CMPMETA.T_SSBI_SQL WHERE OWNER = '" + owner + "' and SSBI_ID = '" + ssbiID + "'", connectionID);
            if (resultQuery.startsWith("success")) {
                resultQuery = queryService.checkValidityUpdateClauses("INSERT INTO " + infoConnectionInput.getSCHEME() +
                        "T_SSBI_SQL (SSBI_ID, OWNER, CONNECTIONID, `DESC`, `SQL`, LOAD_DTTM) VALUES ('"
                        + ssbiID + "', '"
                        + owner + "', '"
                        + ssbiDto.getConnectionID() + "', "
                        + (ssbiDto.getDesc() != null ? "'" + ssbiDto.getDesc().replace("'", "") + "'" : null) + ", '"
                        + StringEscapeUtils.escapeSql(ssbiDto.getSql()) + "', '"
                        + new SimpleDateFormat("yyyyMMddHHmmss").format(new Date()) + "')", connectionID);
                if (resultQuery.startsWith("success")) {
                    return CustomResponseDto.ok();
                }
            }
            queryService.addLogging("POST", partMessage + "fail when inserting data to T_SSBI_SQL\t" + resultQuery);
            return CustomResponseDto.builder().message(resultQuery).build();
        } catch (Exception e) {
            queryService.addLogging("POST", partMessage + "fail when inserting data to T_SSBI_SQL\t" + e.getMessage());
            return CustomResponseDto.builder().message(e.getMessage()).build();
        }
    }

}

