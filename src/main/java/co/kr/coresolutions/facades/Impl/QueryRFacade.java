package co.kr.coresolutions.facades.Impl;

import co.kr.coresolutions.commons.CustomResponseDto;
import co.kr.coresolutions.facades.IQueryRFacade;
import co.kr.coresolutions.model.dtos.SocketDto;
import co.kr.coresolutions.model.dtos.SocketLoginDto;
import co.kr.coresolutions.service.QueryService;
import co.kr.coresolutions.service.QueryServiceRSocketClient;
import lombok.RequiredArgsConstructor;
import lombok.extern.slf4j.Slf4j;
import org.springframework.stereotype.Component;

import java.text.SimpleDateFormat;
import java.util.Date;

@Component
@RequiredArgsConstructor
@Slf4j
public class QueryRFacade implements IQueryRFacade {
    private final QueryService queryService;
    private final QueryServiceRSocketClient queryServiceRSocketClient;
    private static final SimpleDateFormat simpleDateFormat = new SimpleDateFormat("yyyyMMdd:HHmmss");


    @Override
    public Object evaluate(SocketDto socketDto, String connectionID) {
        String partMessage = "/R/{" + connectionID + "}/evaluate triggered, ";
        boolean isConnectionFileExists = queryService.isConnectionIdFileExists(connectionID);
        if (!isConnectionFileExists) {
            queryService.addLogging("POST", partMessage + "fail due to connection file with name " + connectionID + " doesn't exist");
            return CustomResponseDto.builder().message("information (" + connectionID + ") doesn't exist").build();
        }
        QueryServiceRSocketClient.sessionRSocketRunningUsers.put(socketDto.getOwner(), simpleDateFormat.format(new Date()));
        return queryServiceRSocketClient.requestResponse("evaluate", queryService.getInfoConnection(connectionID), socketDto);
    }

    @Override
    public Object open(String owner, String connectionID) {
        String partMessage = "/R/{" + connectionID + "}/ropen/{" + owner + "} triggered, ";
        boolean isConnectionFileExists = queryService.isConnectionIdFileExists(connectionID);
        if (!isConnectionFileExists) {
            queryService.addLogging("POST", partMessage + "fail due to connection file with name " + connectionID + " doesn't exist");
            return CustomResponseDto.builder().message("information (" + connectionID + ") doesn't exist").build();
        }
        return queryServiceRSocketClient.requestResponse("openSession", queryService.getInfoConnection(connectionID), SocketDto.builder().owner(owner).build());
    }

    @Override
    public Object close(String owner, String connectionID) {
        String partMessage = "/R/{" + connectionID + "}/rclose/{" + owner + "} triggered, ";
        boolean isConnectionFileExists = queryService.isConnectionIdFileExists(connectionID);
        if (!isConnectionFileExists) {
            queryService.addLogging("POST", partMessage + "fail due to connection file with name " + connectionID + " doesn't exist");
            return CustomResponseDto.builder().message("information (" + connectionID + ") doesn't exist").build();
        }
        return queryServiceRSocketClient.requestResponse("closeSession", queryService.getInfoConnection(connectionID), SocketDto.builder().owner(owner).build());
    }

    @Override
    public Object listUsers() {
        String partMessage = "/R/rusers triggered, ";
        queryService.addLogging("GET", partMessage + "success");
        return QueryServiceRSocketClient.sessionRSocketRequester.keySet();
    }

    @Override
    public Object listUsersRunning(String connectionID) {
        String partMessage = "R/{" + connectionID + "}/running triggered, ";
        boolean isConnectionFileExists = queryService.isConnectionIdFileExists(connectionID);
        if (!isConnectionFileExists) {
            queryService.addLogging("GET", partMessage + "fail due to connection file with name " + connectionID + " doesn't exist");
            return CustomResponseDto.builder().message("information (" + connectionID + ") doesn't exist").build();
        }

        queryService.addLogging("GET", partMessage + "success");
        return queryServiceRSocketClient.requestResponse("listUsersRunning", queryService.getInfoConnection(connectionID), SocketDto.builder().build());
    }

    @Override
    public Object deleteUser(SocketLoginDto socketLoginDto, String connectionID, String userID) {
        String partMessage = "R/{" + connectionID + "}/ruser/{" + userID + "} triggered, ";
        boolean isConnectionFileExists = queryService.isConnectionIdFileExists(connectionID);
        if (!isConnectionFileExists) {
            queryService.addLogging("DELETE", partMessage + "fail due to connection file with name " + connectionID + " doesn't exist");
            return CustomResponseDto.builder().message("information (" + connectionID + ") doesn't exist").build();
        }

        queryService.addLogging("DELETE", partMessage + "success");
        return queryServiceRSocketClient.fireAndForget("deleteRunningUser", queryService.getInfoConnection(connectionID), socketLoginDto, userID);
    }
}
