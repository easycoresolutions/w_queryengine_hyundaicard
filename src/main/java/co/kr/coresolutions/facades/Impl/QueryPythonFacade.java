package co.kr.coresolutions.facades.Impl;

import co.kr.coresolutions.commons.CustomResponseDto;
import co.kr.coresolutions.facades.IQueryPythonFacade;
import co.kr.coresolutions.model.dtos.SocketDto;
import co.kr.coresolutions.model.dtos.SocketLoginDto;
import co.kr.coresolutions.service.QueryService;
import co.kr.coresolutions.service.QueryServicePythonSocketClient;
import lombok.RequiredArgsConstructor;
import lombok.extern.slf4j.Slf4j;
import org.springframework.stereotype.Component;

import java.text.SimpleDateFormat;
import java.util.Date;

@Component
@RequiredArgsConstructor
@Slf4j
public class QueryPythonFacade implements IQueryPythonFacade, IQueryPythonFacade.IAdmin, IQueryPythonFacade.IUser {
    private static final SimpleDateFormat simpleDateFormat = new SimpleDateFormat("yyyyMMdd:HHmmss");
    private final QueryService queryService;
    private final QueryServicePythonSocketClient queryServicePythonSocketClient;

    @Override
    public Object open(String owner, String connectionID) {
        String partMessage = "/python/{" + connectionID + "}/open/{" + owner + "} triggered, ";
        boolean isConnectionFileExists = queryService.isConnectionIdFileExists(connectionID);
        if (!isConnectionFileExists) {
            queryService.addLogging("POST", partMessage + "fail due to connection file with name " + connectionID + " doesn't exist");
            return CustomResponseDto.builder().message("information (" + connectionID + ") doesn't exist").build();
        }
        return queryServicePythonSocketClient
                .requestResponse("openSession", queryService.getInfoConnection(connectionID), SocketDto.builder().owner(owner).build());
    }

    @Override
    public Object close(String owner, String connectionID) {
        String partMessage = "/python/{" + connectionID + "}/close/{" + owner + "} triggered, ";
        boolean isConnectionFileExists = queryService.isConnectionIdFileExists(connectionID);
        if (!isConnectionFileExists) {
            queryService.addLogging("POST", partMessage + "fail due to connection file with name " + connectionID + " doesn't exist");
            return CustomResponseDto.builder().message("information (" + connectionID + ") doesn't exist").build();
        }
        return queryServicePythonSocketClient.requestResponse("closeSession"
                , queryService.getInfoConnection(connectionID), SocketDto.builder().owner(owner).build());
    }

    @Override
    public Object evaluate(SocketDto socketDto, String connectionID) {
        String partMessage = "/python/{" + connectionID + "}/evaluate triggered, ";
        boolean isConnectionFileExists = queryService.isConnectionIdFileExists(connectionID);
        if (!isConnectionFileExists) {
            queryService.addLogging("POST", partMessage + "fail due to connection file with name " + connectionID + " doesn't exist");
            return CustomResponseDto.builder().message("information (" + connectionID + ") doesn't exist").build();
        }
        QueryServicePythonSocketClient.sessionPythonSocketRunningUsers.put(socketDto.getOwner(), simpleDateFormat.format(new Date()));
        return queryServicePythonSocketClient.requestResponse("evaluate", queryService.getInfoConnection(connectionID), socketDto);
    }

    @Override
    public Object listUsers() {
        String partMessage = "/python/users triggered, ";
        queryService.addLogging("GET", partMessage + "success");
        return QueryServicePythonSocketClient.sessionPythonSocketRequester.keySet();
    }

    @Override
    public Object listUsersRunning(String connectionID) {
        String partMessage = "/python/{" + connectionID + "}/running triggered, ";
        boolean isConnectionFileExists = queryService.isConnectionIdFileExists(connectionID);
        if (!isConnectionFileExists) {
            queryService.addLogging("GET", partMessage + "fail due to connection file with name " + connectionID + " doesn't exist");
            return CustomResponseDto.builder().message("information (" + connectionID + ") doesn't exist").build();
        }

        queryService.addLogging("GET", partMessage + "success");
        return queryServicePythonSocketClient.requestResponse("listUsersRunning", queryService.getInfoConnection(connectionID), SocketDto.builder().build());
    }

    @Override
    public Object deleteUser(SocketLoginDto socketLoginDto, String connectionID, String userID) {
        String partMessage = "/python/{" + connectionID + "}/user/{" + userID + "} triggered, ";
        boolean isConnectionFileExists = queryService.isConnectionIdFileExists(connectionID);
        if (!isConnectionFileExists) {
            queryService.addLogging("DELETE", partMessage + "fail due to connection file with name " + connectionID + " doesn't exist");
            return CustomResponseDto.builder().message("information (" + connectionID + ") doesn't exist").build();
        }

        queryService.addLogging("DELETE", partMessage + "success");
        return queryServicePythonSocketClient.fireAndForget("deleteRunningUser", queryService.getInfoConnection(connectionID), socketLoginDto, userID);
    }
}
