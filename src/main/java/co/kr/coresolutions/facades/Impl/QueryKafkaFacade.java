package co.kr.coresolutions.facades.Impl;

import co.kr.coresolutions.commons.CustomResponseDto;
import co.kr.coresolutions.facades.IQueryKafkaFacade;
import co.kr.coresolutions.model.dtos.Kafka2Dto;
import co.kr.coresolutions.model.dtos.Kafka3Dto;
import co.kr.coresolutions.model.dtos.SocketBodyDto;
import co.kr.coresolutions.service.QueryService;
import co.kr.coresolutions.util.QueryPollingHelper;
import com.fasterxml.jackson.databind.JsonNode;
import com.fasterxml.jackson.databind.ObjectMapper;
import lombok.RequiredArgsConstructor;
import org.apache.kafka.clients.admin.AdminClient;
import org.apache.kafka.clients.admin.KafkaAdminClient;
import org.apache.kafka.clients.producer.KafkaProducer;
import org.apache.kafka.clients.producer.Producer;
import org.apache.kafka.clients.producer.ProducerRecord;
import org.springframework.stereotype.Component;

import java.io.IOException;
import java.util.Properties;
import java.util.concurrent.ExecutionException;

@Component
@RequiredArgsConstructor
public class QueryKafkaFacade implements IQueryKafkaFacade {
    private final QuerySocketFacade querySocketFacade;
    private final QueryPollingHelper queryPollingHelper;
    private final QueryService queryService;
    private final ObjectMapper objectMapper;

    @Override
    public Object send(String connectionID, Kafka3Dto kafka3Dto) {
        String partMessage = "send/{" + connectionID + "}, triggered, ";
        boolean isConnectionFileExists = queryService.isConnectionIdFileExists(connectionID);
        if (!isConnectionFileExists) {
            querySocketFacade.send(SocketBodyDto.builder().userId(kafka3Dto.getOwner()).key("KAFKA").message(partMessage + "fail due to kafka connection file with name " +
                    connectionID + " doesn't exist").build());
            return CustomResponseDto.builder().message("fail due to kafka connection file with name " +
                    connectionID + " doesn't exist").build();
        }

        Properties properties;
        try {
            properties = queryPollingHelper.getProperties(queryService.getConnectionFile(connectionID));
            AdminClient adminClient = KafkaAdminClient.create(properties);
            String topic = properties.getProperty("topic");
            Producer producer = new KafkaProducer<>(properties);
            adminClient.listTopics().names().get().stream().filter(s -> s.equals(topic)).findFirst().ifPresent(s -> producer.send(new ProducerRecord<String, String>(topic, kafka3Dto.getData().toString())));
            producer.close();
            adminClient.close();
        } catch (IOException | InterruptedException | ExecutionException e) {
            querySocketFacade.send(SocketBodyDto.builder().userId(kafka3Dto.getOwner()).key("KAFKA").message(partMessage + "fail due " + e.getMessage()).build());
            return CustomResponseDto.builder().message(e.getMessage()).build();
        }
        querySocketFacade.send(SocketBodyDto.builder().userId(kafka3Dto.getOwner()).key("KAFKA").message(partMessage + "success").build());
        return CustomResponseDto.ok();
    }

    @Override
    public Object sendRecords(String connectionID, Kafka2Dto kafka2Dto) {
        String partMessage = "sendrecords/{" + connectionID + "}, triggered, ";
        boolean isConnectionFileExists = queryService.isConnectionIdFileExists(connectionID);
        if (!isConnectionFileExists) {
            queryService.addLogging("POST", partMessage + "fail due to kafka connection file with name " + connectionID + " doesn't exist");
            querySocketFacade.send(SocketBodyDto.builder().userId(kafka2Dto.getOwner()).key("KAFKA").message(partMessage + "fail due to kafka connection file with name " +
                    connectionID + " doesn't exist").build());
            return CustomResponseDto.builder().message("fail due to kafka connection file with name " +
                    connectionID + " doesn't exist").build();
        }
        isConnectionFileExists = queryService.isConnectionIdFileExists(kafka2Dto.getConnectionID());
        if (!isConnectionFileExists) {
            queryService.addLogging("POST", partMessage + "fail due to connection file with name " + kafka2Dto.getConnectionID() + " doesn't exist");
            querySocketFacade.send(SocketBodyDto.builder().userId(kafka2Dto.getOwner()).key("KAFKA").message(partMessage + "fail due to connection file with name " +
                    kafka2Dto.getConnectionID() + " doesn't exist").build());
            return CustomResponseDto.builder().message("fail due to connection file with name " +
                    kafka2Dto.getConnectionID() + " doesn't exist").build();
        }
        CustomResponseDto customResponseDto = queryService.getResultQuery(kafka2Dto.getConnectionID(), kafka2Dto.getSql());
        if (customResponseDto.getSuccess()) {

            Properties properties;
            try {
                JsonNode jsonNode = objectMapper.readTree(customResponseDto.getMessage());
                properties = queryPollingHelper.getProperties(queryService.getConnectionFile(connectionID));
                AdminClient adminClient = KafkaAdminClient.create(properties);
                String topic = properties.getProperty("topic");
                Producer producer = new KafkaProducer<>(properties);
                adminClient.listTopics().names().get().stream().filter(s -> s.equals(topic)).findFirst().ifPresent(s -> {
                    queryPollingHelper.getOutData(jsonNode, "json").forEach(oneLine -> producer.send(new ProducerRecord<String, String>(topic, oneLine)));
                });
                producer.close();
                adminClient.close();
                queryService.addLogging("POST", partMessage + "success, recordsCount = " + jsonNode.size());
                querySocketFacade.send(SocketBodyDto.builder().userId(kafka2Dto.getOwner()).key("KAFKA").message(partMessage + "success, recordsCount = " + jsonNode.size()).build());
                return CustomResponseDto.ok();
            } catch (IOException | InterruptedException | ExecutionException e) {
                queryService.addLogging("POST", partMessage + "fail due " + e.getMessage());
                querySocketFacade.send(SocketBodyDto.builder().userId(kafka2Dto.getOwner()).key("KAFKA").message(partMessage + "fail due " + e.getMessage()).build());
                return CustomResponseDto.builder().message(e.getMessage()).build();
            }
        }
        queryService.addLogging("POST", partMessage + customResponseDto.getErrorMessage());
        querySocketFacade.send(SocketBodyDto.builder().userId(kafka2Dto.getOwner()).key("KAFKA").message(partMessage + customResponseDto.getErrorMessage()).build());
        return customResponseDto;
    }
}
