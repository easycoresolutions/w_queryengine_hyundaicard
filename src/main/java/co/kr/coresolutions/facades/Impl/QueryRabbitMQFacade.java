package co.kr.coresolutions.facades.Impl;

import co.kr.coresolutions.caches.QueryRabbitMQCache;
import co.kr.coresolutions.commons.ResponseCodes;
import co.kr.coresolutions.commons.ResponseDto;
import co.kr.coresolutions.facades.IQueryRabbitMQFacade;
import co.kr.coresolutions.model.dtos.JsonDataDto;
import co.kr.coresolutions.service.QueryRabbitMQHttp;
import co.kr.coresolutions.service.QueryService;
import com.fasterxml.jackson.databind.JsonNode;
import com.fasterxml.jackson.databind.ObjectMapper;
import com.fasterxml.jackson.databind.node.ObjectNode;
import com.rabbitmq.client.Channel;
import com.rabbitmq.client.Connection;
import lombok.RequiredArgsConstructor;
import lombok.SneakyThrows;
import lombok.extern.slf4j.Slf4j;
import org.springframework.stereotype.Component;
import org.springframework.web.context.annotation.RequestScope;

@Component
@RequiredArgsConstructor
@RequestScope
@Slf4j
public class QueryRabbitMQFacade implements IQueryRabbitMQFacade {

    private final QueryService queryService;
    private final QueryRabbitMQCache queryRabbitMQCache;
    private final QueryRabbitMQHttp queryRabbitMQHttp;
    private final ObjectMapper objectMapper;

    @Override
    @SneakyThrows
    public ResponseDto publish(JsonDataDto jsonDataDto, String connectionId, String queueName) {
        co.kr.coresolutions.model.Connection connectionInfo = queryService.getInfoConnection(connectionId);

        if (!connectionInfo.getUrlHttp().isEmpty() && !connectionInfo.getID().isEmpty() && !connectionInfo.getPW().isEmpty()) {
            boolean isExists = queryRabbitMQHttp.checkQueue(queueName, connectionInfo.getUrlHttp(), connectionInfo.getID(), connectionInfo.getPW());

            if (!isExists)
                return ResponseDto.builder().errorCode(ResponseCodes.NOT_FOUND_ERROR).message("Queue doesn't exists with name\t" + queueName).build();

            if (!connectionInfo.getURL().isEmpty()) {

                Connection connection = queryRabbitMQCache.getConnection(connectionInfo.getURL());

                Channel channel = connection.createChannel();
                channel.basicPublish("", queueName, null, objectMapper.writeValueAsBytes(jsonDataDto.getData()));
                channel.close();
                connection.close();
                return ResponseDto.ok();
            }
        }

        return ResponseDto.NOT_EXISTS;
    }

    @Override
    @SneakyThrows
    public ResponseDto findAllQueues(String connectionId) {
        co.kr.coresolutions.model.Connection connectionInfo = queryService.getInfoConnection(connectionId);

        if (!connectionInfo.getUrlHttp().isEmpty() && !connectionInfo.getID().isEmpty() && !connectionInfo.getPW().isEmpty()) {

            ObjectNode[] nodes = queryRabbitMQHttp.getListQueue(connectionInfo.getUrlHttp(), connectionInfo.getID(), connectionInfo.getPW());

            if (nodes == null)
                return ResponseDto.builder().errorCode(ResponseCodes.NULL_RESPONSE_ERROR).message("found empty results").build();

            return ResponseDto
                    .builder()
                    .successCode(ResponseCodes.OK_RESPONSE)
                    .message(objectMapper.writeValueAsString(nodes))
                    .build();
        }

        return ResponseDto.NOT_EXISTS;
    }

    @Override
    @SneakyThrows
    public ResponseDto findAllExchanges(String connectionId, String queueName) {
        co.kr.coresolutions.model.Connection connectionInfo = queryService.getInfoConnection(connectionId);

        if (!connectionInfo.getUrlHttp().isEmpty() && !connectionInfo.getID().isEmpty() && !connectionInfo.getPW().isEmpty()) {

            JsonNode node =
                    queryRabbitMQHttp
                            .getExchanges(connectionInfo.getUrlHttp(), connectionInfo.getID(), connectionInfo.getPW(), queueName);

            if (node == null)
                return ResponseDto.builder().errorCode(ResponseCodes.NULL_RESPONSE_ERROR).message("found empty results").build();

            return ResponseDto
                    .builder()
                    .successCode(ResponseCodes.OK_RESPONSE)
                    .message(objectMapper.writeValueAsString(node))
                    .build();
        }

        return ResponseDto.NOT_EXISTS;
    }
}
