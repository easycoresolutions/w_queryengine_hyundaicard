package co.kr.coresolutions.facades.Impl;

import co.kr.coresolutions.commons.CustomResponseDto;
import co.kr.coresolutions.commons.ResponseCodes;
import co.kr.coresolutions.commons.ResponseDto;
import co.kr.coresolutions.facades.IQueryLoadFacade;
import co.kr.coresolutions.facades.IQuerySocketFacade;
import co.kr.coresolutions.model.UnLoad;
import co.kr.coresolutions.model.dtos.SocketBodyDto;
import co.kr.coresolutions.service.QueryService;
import co.kr.coresolutions.service.QueryServiceClickHouse;
import co.kr.coresolutions.service.QueryServiceSQLite;
import com.fasterxml.jackson.databind.JsonNode;
import lombok.RequiredArgsConstructor;
import org.springframework.stereotype.Component;

import java.nio.charset.StandardCharsets;
import java.util.Date;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.UUID;
import java.util.concurrent.ConcurrentHashMap;

@Component
@RequiredArgsConstructor
public class QueryLoadFacade implements IQueryLoadFacade {
    public static Map<String, UnLoad.UnloadDetails> sessionUnload = new HashMap<>();
    public static Map<String, String> sessionUnloadError = new HashMap<>();
    public static Map<Object, Object> sessionClickHouseImport = new ConcurrentHashMap<>();
    private final QueryService queryService;
    private final IQuerySocketFacade querySocketFacade;
    private final QueryServiceClickHouse queryServiceClickHouse;

    @Override
    public Object unload(UnLoad unLoad, String connectionID) {
        String unloadID = UUID.randomUUID().toString();
        String partMessage = "unload/{" + connectionID + "} triggered, sql statement : {" + unLoad.getSql() + "}, unload_id : " + unloadID;
        String format = unLoad.getFormat().name().toLowerCase();
        String owner = unLoad.getOwner();
        UnLoad.UnloadDetails unloadDetails = UnLoad.UnloadDetails.builder().unloadID(unloadID).fileName(unLoad.getFilename()).owner(owner)
                .startdatetime(QueryServiceSQLite.SIMPLE_DATE_FORMAT.format(new Date())).build();
        unLoad.setUnloadDetails(unloadDetails);
        JsonNode jsonNode = unLoad.getOutfields();
        CustomResponseDto customResponseDto = CustomResponseDto.builder().build();
        if (jsonNode.isEmpty()) {
            customResponseDto.setSuccess(true);
            return customResponseDto;
        }

        QueryLoadFacade.sessionUnload.put(unloadID, unloadDetails);

        boolean isConnectionFileExist = queryService.isConnectionIdFileExists(connectionID);
        if (!isConnectionFileExist) {
            queryService.addLogging("POST", partMessage + connectionID + " (information) doesn't exist.");
            querySocketFacade.send(SocketBodyDto.builder().userId(owner).key("UNLOAD").message(partMessage + connectionID + " (information) doesn't exist.").build());
            customResponseDto.setMessage(connectionID + " (information) doesn't exist.");
        } else {
            String fileName = queryService.convertGlobalVariable(unLoad.getFilename());
            String queryAfterConversion = queryService.convertGlobalDateTime(queryService.convertGlobalVariable(queryService.getFormattedString(unLoad.getSql())));
            if (queryAfterConversion.contains("NOT RESOLVED")) {
                queryService.addLogging("POST", partMessage + "error can't convert properly date time");
                querySocketFacade.send(SocketBodyDto.builder().userId(owner).key("UNLOAD").message(partMessage + "error can't convert properly date time").build());
                customResponseDto.setMessage(new String(queryAfterConversion.getBytes(), StandardCharsets.UTF_8));
            } else {
                synchronized (QueryServiceSQLite.DBFILE) {
                    synchronized (QueryServiceSQLite.SQLITE_USERID) {
                        QueryServiceSQLite.SQLITE_USERID = owner;
                        if (unLoad.getDbFile() != null && !unLoad.getDbFile().isEmpty()) {
                            QueryServiceSQLite.DBFILE = unLoad.getDbFile() + ".db";
                        }
                    }
                }
                customResponseDto = queryService.loadInChunk(unLoad, connectionID, queryAfterConversion, fileName, format, true, 0);
                querySocketFacade.send(SocketBodyDto.builder().userId(owner).key("UNLOAD").message(partMessage + customResponseDto.getMessage()).build());
                queryService.addLogging("POST", partMessage + customResponseDto.getMessage());
                synchronized (QueryServiceSQLite.DBFILE) {
                    synchronized (QueryServiceSQLite.SQLITE_USERID) {
                        QueryServiceSQLite.SQLITE_USERID = "mss";
                        QueryServiceSQLite.DBFILE = "mydbfile.db";
                    }
                }
            }
        }

        QueryLoadFacade.sessionUnload.remove(unloadID);
        if (QueryLoadFacade.sessionUnloadError.containsKey(unloadID)) {
            customResponseDto.setSuccess(false);
            customResponseDto.setMessage(QueryLoadFacade.sessionUnloadError.get(unloadID));
        }
        QueryLoadFacade.sessionUnloadError.remove(unloadID);
        return customResponseDto;
    }

    @Override
    public Object unloadClickHouse(UnLoad unLoad, String connectionID) {
        String owner = unLoad.getOwner();
        String unloadID = UUID.randomUUID().toString();
        String partMessage = "unload2/{" + connectionID + "} triggered, sql statement : {" + unLoad.getSql() + "}, unload2_id : " + unloadID;
        UnLoad.UnloadDetails unloadDetails = UnLoad.UnloadDetails.builder().unloadID(unloadID).fileName(unLoad.getFilename()).owner(owner)
                .startdatetime(QueryServiceSQLite.SIMPLE_DATE_FORMAT.format(new Date())).build();
        unLoad.setUnloadDetails(unloadDetails);
        QueryLoadFacade.sessionUnload.put(unloadID, unloadDetails);
        String format = unLoad.getFormat().name().toLowerCase();
        CustomResponseDto customResponseDto = CustomResponseDto.builder().build();
        boolean isConnectionFileExist = queryService.isConnectionIdFileExists(connectionID);
        if (!isConnectionFileExist) {
            querySocketFacade.send(SocketBodyDto.builder().userId(owner).key("UNLOAD2").message(partMessage + connectionID + " (information) doesn't exist.").build());
            queryService.addLogging("POST", partMessage + connectionID + " (information) doesn't exist.");
            customResponseDto.setMessage(connectionID + " (information) doesn't exist.");
        } else {
            String fileName = queryService.convertGlobalVariable(unLoad.getFilename());
            String queryAfterConversion = queryService.convertGlobalDateTime(queryService.convertGlobalVariable(queryService.getFormattedString(unLoad.getSql())));
            if (queryAfterConversion.contains("NOT RESOLVED")) {
                querySocketFacade.send(SocketBodyDto.builder().userId(owner).key("UNLOAD2").message(partMessage + "error can't convert properly date time").build());
                queryService.addLogging("POST", partMessage + "error can't convert properly date time");
                customResponseDto.setMessage(new String(queryAfterConversion.getBytes(), StandardCharsets.UTF_8));
            } else {
                synchronized (QueryServiceSQLite.DBFILE) {
                    synchronized (QueryServiceSQLite.SQLITE_USERID) {
                        QueryServiceSQLite.SQLITE_USERID = owner;
                        if (unLoad.getDbFile() != null && !unLoad.getDbFile().isEmpty()) {
                            QueryServiceSQLite.DBFILE = unLoad.getDbFile() + ".db";
                        }
                    }
                }
                customResponseDto = queryService.loadInChunk(unLoad, connectionID, queryAfterConversion, fileName, format, false, 0);
                querySocketFacade.send(SocketBodyDto.builder().userId(owner).key("UNLOAD2").message(partMessage + customResponseDto.getMessage()).build());
                queryService.addLogging("POST", partMessage + customResponseDto.getMessage());
                synchronized (QueryServiceSQLite.DBFILE) {
                    synchronized (QueryServiceSQLite.SQLITE_USERID) {
                        QueryServiceSQLite.SQLITE_USERID = "mss";
                        QueryServiceSQLite.DBFILE = "mydbfile.db";
                    }
                }
            }
        }
        QueryLoadFacade.sessionUnload.remove(unloadID);
        if (QueryLoadFacade.sessionUnloadError.containsKey(unloadID)) {
            customResponseDto.setSuccess(false);
            customResponseDto.setMessage(QueryLoadFacade.sessionUnloadError.get(unloadID));
        }
        QueryLoadFacade.sessionUnloadError.remove(unloadID);
        return customResponseDto;
    }

    @Override
    public List<UnLoad.UnloadDetails> monitor() {
        String partMessage = "unload2/monitor triggered, ";
        querySocketFacade.send(SocketBodyDto.builder().userId("").key("UNLOAD2").message(partMessage).build());
        queryService.addLogging("GET", partMessage);
        return queryServiceClickHouse.getCacheUnLoad();
    }

    @Override
    public ResponseDto deleteUnload(String unloadID) {
        String partMessage = "unload2/monitor/{" + unloadID + "} triggered, ";
        if (queryServiceClickHouse.removeCacheUnload(unloadID)) {
            querySocketFacade.send(SocketBodyDto.builder().userId("").key("UNLOAD2").message(partMessage + "success").build());
            queryService.addLogging("DELETE", partMessage + "success");
            return ResponseDto.builder().message("Unload with id\t" + unloadID + "\tdeleted success").successCode(ResponseCodes.OK_RESPONSE).build();
        }
        querySocketFacade.send(SocketBodyDto.builder().userId("").key("UNLOAD2").message(partMessage + "fail not exist").build());
        queryService.addLogging("DELETE", partMessage + "fail not exist");
        return ResponseDto.NOT_EXISTS;
    }

}
