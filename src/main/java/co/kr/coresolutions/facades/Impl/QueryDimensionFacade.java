package co.kr.coresolutions.facades.Impl;

import co.kr.coresolutions.commons.CustomResponseDto;
import co.kr.coresolutions.facades.IQueryDimensionFacade;
import co.kr.coresolutions.facades.IQuerySocketFacade;
import co.kr.coresolutions.model.Connection;
import co.kr.coresolutions.model.dtos.SocketBodyDto;
import co.kr.coresolutions.service.QueryService;
import co.kr.coresolutions.service.QueryServiceJSON;
import com.fasterxml.jackson.databind.JsonNode;
import lombok.RequiredArgsConstructor;
import lombok.extern.slf4j.Slf4j;
import org.springframework.stereotype.Component;

import java.nio.charset.StandardCharsets;

@Component
@RequiredArgsConstructor
@Slf4j
public class QueryDimensionFacade implements IQueryDimensionFacade {
    private final QueryService queryService;
    private final QueryServiceJSON queryServiceJSON;
    public static String[] message = {""};
    private final IQuerySocketFacade querySocketFacade;

    @Override
    public CustomResponseDto queryWithDimension(String connectionID, String langType, String dimensionName, String sql, JsonNode jsonNode) {
        boolean isConnectionFileExists = queryService.isConnectionIdFileExists(connectionID);
        String methodType = jsonNode == null ? "GET" : "POST";
        String partMessage = "{" + connectionID + "}/dimension/{" + langType + "}/{" + dimensionName + "}, triggered, ";

        if (!isConnectionFileExists) {
            QueryDimensionFacade.message[0] = "fail due to connection file with name\t" + connectionID + "\tdoesn't exist";
            querySocketFacade.send(SocketBodyDto.builder().userId("").key("DIMENSION").message(partMessage + QueryDimensionFacade.message[0]).build());
            queryService.addLogging(methodType, partMessage + QueryDimensionFacade.message[0]);
            return CustomResponseDto.builder().ErrorCode(2).ErrorMessage(QueryDimensionFacade.message[0]).build();
        }

        String query;
        if (!sql.isEmpty()) {
            final String[] replacedSql = {sql};
            if (jsonNode != null) {
                jsonNode.fields().forEachRemaining(stringJsonNodeEntry -> replacedSql[0] = replacedSql[0].replace(stringJsonNodeEntry.getKey(), stringJsonNodeEntry.getValue().asText()));
            }
            query = replacedSql[0];
        } else {
            Connection infoConnection = queryService.getInfoConnection(connectionID);
            query = "SELECT LOOKUP_NAME ,LOOKUP_DESC, VAL1 , VAL3 FROM " + infoConnection.getSCHEME() + "T_LOOKUP_VALUES WHERE LOOKUP_TYPE = 'DIMENSION_LIST' AND LOOKUP_CODE = '"
                    + dimensionName + "' AND VAL2 = 'DS' AND LANG = '" + langType + "'";
        }

        String resultValidity = queryService.checkValidity(query, connectionID, "", "cache", false);
        if (resultValidity.startsWith("Error") || resultValidity.startsWith("error")) {
            queryService.setRunStop("cache");
            QueryDimensionFacade.message[0] = "fail\t" + resultValidity;
            querySocketFacade.send(SocketBodyDto.builder().userId("").key("DIMENSION").message(partMessage + QueryDimensionFacade.message[0]).build());
            queryService.addLogging(methodType, partMessage + QueryDimensionFacade.message[0]);
            return CustomResponseDto.builder().ErrorCode(3).ErrorMessage(QueryDimensionFacade.message[0]).build();
        }

        String structuredJson = new String(queryServiceJSON.structureJson(resultValidity, false,
                queryService.getQueryDataTypes(connectionID, query, false), "cache").getBytes(), StandardCharsets.UTF_8);
        if (structuredJson.startsWith("Error") || structuredJson.startsWith("error")) {
            queryService.setRunStop("cache");
            QueryDimensionFacade.message[0] = "fail can't format to json\t" + structuredJson;
            querySocketFacade.send(SocketBodyDto.builder().userId("").key("DIMENSION").message(partMessage + QueryDimensionFacade.message[0]).build());
            queryService.addLogging(methodType, partMessage + QueryDimensionFacade.message[0]);
            return CustomResponseDto.builder().ErrorCode(4).ErrorMessage(QueryDimensionFacade.message[0]).build();
        }
        if (!sql.isEmpty()) {
            QueryDimensionFacade.message[0] = "success";
            querySocketFacade.send(SocketBodyDto.builder().userId("").key("DIMENSION").message(partMessage + QueryDimensionFacade.message[0]).build());
            queryService.addLogging(methodType, partMessage + QueryDimensionFacade.message[0]);
            return CustomResponseDto.builder().Success(true).DetailedMessage(structuredJson).build();
        }

        org.json.JSONObject jsonObjectGlobal = new org.json.JSONObject(structuredJson);
        if (jsonObjectGlobal.has("Input")) {
            org.json.JSONArray jsonArrayInput = jsonObjectGlobal.getJSONArray("Input");
            if (!jsonArrayInput.isEmpty()) {
                org.json.JSONObject jsonObject = jsonArrayInput.getJSONObject(0);
                if (jsonObject.has("VAL1") && jsonObject.has("VAL3")) {
                    String val1 = jsonObject.get("VAL1").toString();
                    String val3 = jsonObject.get("VAL3").toString();
                    return queryWithDimension(val1, "", "", val3, jsonNode);
                }
                QueryDimensionFacade.message[0] = "VAL1 and VAL3 missing from the result json Input";
                querySocketFacade.send(SocketBodyDto.builder().userId("").key("DIMENSION").message(partMessage + QueryDimensionFacade.message[0]).build());
                queryService.addLogging(methodType, partMessage + QueryDimensionFacade.message[0]);
                return CustomResponseDto.builder().ErrorCode(6).ErrorMessage(QueryDimensionFacade.message[0]).build();
            }
            QueryDimensionFacade.message[0] = "empty result returned from Input json array";
            querySocketFacade.send(SocketBodyDto.builder().userId("").key("DIMENSION").message(partMessage + QueryDimensionFacade.message[0]).build());
            queryService.addLogging(methodType, partMessage + QueryDimensionFacade.message[0]);
            return CustomResponseDto.builder().ErrorCode(6).ErrorMessage(QueryDimensionFacade.message[0]).build();
        }
        QueryDimensionFacade.message[0] = "fail, empty json or can't format to valid query json\t" + structuredJson;
        querySocketFacade.send(SocketBodyDto.builder().userId("").key("DIMENSION").message(partMessage + QueryDimensionFacade.message[0]).build());
        queryService.addLogging(methodType, partMessage + QueryDimensionFacade.message[0]);
        return CustomResponseDto.builder().ErrorCode(5).ErrorMessage(QueryDimensionFacade.message[0]).build();
    }
}

