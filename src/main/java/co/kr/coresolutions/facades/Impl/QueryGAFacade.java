package co.kr.coresolutions.facades.Impl;

import co.kr.coresolutions.commons.CustomResponseDto;
import co.kr.coresolutions.facades.IQueryGAFacade;
import co.kr.coresolutions.model.GAConnection;
import co.kr.coresolutions.model.dtos.GATemplateDto;
import co.kr.coresolutions.service.Constants;
import co.kr.coresolutions.service.QueryService;
import co.kr.coresolutions.service.QueryServiceGA;
import com.fasterxml.jackson.databind.ObjectMapper;
import com.google.api.services.analyticsreporting.v4.AnalyticsReporting;
import com.google.api.services.analyticsreporting.v4.model.GetReportsResponse;
import lombok.RequiredArgsConstructor;
import lombok.extern.slf4j.Slf4j;
import org.springframework.stereotype.Component;

import javax.annotation.PostConstruct;
import java.io.IOException;
import java.nio.file.Files;
import java.nio.file.Path;
import java.nio.file.Paths;

@Component
@RequiredArgsConstructor
@Slf4j
public class QueryGAFacade implements IQueryGAFacade {
    private static String GA_DIR;
    private final QueryService queryService;
    private final QueryServiceGA queryServiceGA;
    private final Constants constants;
    private final ObjectMapper objectMapper;

    @PostConstruct
    public void init() throws IOException {
        GA_DIR = constants.getGADir();
        if (!Files.exists(Paths.get(GA_DIR))) {
            Files.createDirectories(Paths.get(GA_DIR));
        }
    }

    @Override
    public Object getReports(String connectionFile, GATemplateDto gaTemplateDto) {
        String partMessage = "ga/{" + connectionFile + "} triggered, ";
        Path pathConnectionFile = Paths.get(GA_DIR + connectionFile + ".txt");
        if (!pathConnectionFile.toFile().exists()) {
            queryService.addLogging("POST", partMessage + "fail due connection file " + connectionFile + " doesn't exist");
            return CustomResponseDto.builder().message("fail due connection file " + connectionFile + " doesn't exist").build();
        }
        try {
            GAConnection gaConnection = objectMapper.readValue(Files.readAllBytes(pathConnectionFile), GAConnection.class);
            AnalyticsReporting analyticsReporting = queryServiceGA.initializeAnalyticsReporting(gaConnection);
            GetReportsResponse response = queryServiceGA.getResponse(analyticsReporting, gaConnection, gaTemplateDto.getQueryGA());
            String result = queryServiceGA.saveResponse(response.getReports(), gaTemplateDto.getOutput());
            queryService.addLogging("POST", partMessage + "success");
            if (result != null) {
                return result;
            }
            return CustomResponseDto.builder().Success(true).build();
        } catch (Exception e) {
            queryService.addLogging("POST", partMessage + "fail due\t" + e.getMessage());
            return CustomResponseDto.builder().message("fail due\t" + e.getMessage()).build();
        }
    }

}
