package co.kr.coresolutions.facades;

import co.kr.coresolutions.model.dtos.SSBIDto;

@FunctionalInterface
public interface IQuerySSBIFacade {
    Object logToDB(SSBIDto ssbiDto, String owner, String ssbiID);
}
