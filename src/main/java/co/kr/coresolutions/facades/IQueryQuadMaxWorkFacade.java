package co.kr.coresolutions.facades;

import co.kr.coresolutions.commons.CustomResponseDto;
import co.kr.coresolutions.model.dtos.QuadMaxWorkDto;

public interface IQueryQuadMaxWorkFacade {
    CustomResponseDto countRows(String connectionID, String tableName);

    Object countRows(String connectionID, QuadMaxWorkDto quadMaxWorkDto);
    
    // Object countRows(String connectionID, QuadMaxWorkDto quadMaxWorkDto) 의  SQL 인젝션 이슈 fix 버전
    Object submitCountRows(String connectionID, QuadMaxWorkDto quadMaxWorkDto);

    CustomResponseDto dropTable(String connectionID, String tableName);
}
