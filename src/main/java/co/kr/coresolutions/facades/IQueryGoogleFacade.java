package co.kr.coresolutions.facades;

import co.kr.coresolutions.model.dtos.GoogleTemplateDTO;

@FunctionalInterface
public interface IQueryGoogleFacade {
    Object getAndConvertSheet(String connectionID, GoogleTemplateDTO googleTemplateDTO);
}
