package co.kr.coresolutions.facades;

import co.kr.coresolutions.model.dtos.SocketDto;
import co.kr.coresolutions.model.dtos.SocketLoginDto;

public interface IQueryRFacade {
    Object evaluate(SocketDto socketDto, String connectionID);

    Object open(String owner, String connectionID);

    Object close(String owner, String connectionID);

    Object listUsers();

    Object listUsersRunning(String connectionID);

    Object deleteUser(SocketLoginDto socketLoginDto, String connectionID, String userID);
}
