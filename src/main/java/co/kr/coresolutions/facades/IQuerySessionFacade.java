package co.kr.coresolutions.facades;

import co.kr.coresolutions.commons.ResponseDto;
import co.kr.coresolutions.model.dtos.SessionDto;

public interface IQuerySessionFacade {
    SessionDto saveOrUpdateSession(String ownerId);

    ResponseDto listSessions();

    ResponseDto deleteSession(String ownerId);
}
