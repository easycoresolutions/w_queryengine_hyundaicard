package co.kr.coresolutions.facades;

import co.kr.coresolutions.commons.ResponseDto;
import co.kr.coresolutions.model.dtos.EmailDto;

@FunctionalInterface
public interface IQueryEmailFacade {
    ResponseDto sendSimpleEmail(EmailDto emailDto);
}
