package co.kr.coresolutions.facades;

import co.kr.coresolutions.model.dtos.GATemplateDto;

public interface IQueryGAFacade {
    Object getReports(String connectionFile, GATemplateDto gaTemplateDto);
}
