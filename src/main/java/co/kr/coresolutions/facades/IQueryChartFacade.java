package co.kr.coresolutions.facades;

import co.kr.coresolutions.model.dtos.ChartDto;

public interface IQueryChartFacade {
    Object getChart(String connectionId, ChartDto chartDto);

    Object execChart(String connectionId, Object chartDto, boolean isExecData);

    Object refreshDateTime();
}
