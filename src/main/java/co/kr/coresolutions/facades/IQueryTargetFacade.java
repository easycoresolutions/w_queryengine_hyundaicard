package co.kr.coresolutions.facades;

import co.kr.coresolutions.commons.CustomResponseDto;
import co.kr.coresolutions.model.dtos.Target2Dto;
import co.kr.coresolutions.model.dtos.TargetDto;

public interface IQueryTargetFacade {
    CustomResponseDto targetList(TargetDto targetDto);

    Object upload(Target2Dto target2Dto);
}
