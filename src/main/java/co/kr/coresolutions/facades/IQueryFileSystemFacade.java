package co.kr.coresolutions.facades;

import co.kr.coresolutions.commons.CustomResponseDto;
import co.kr.coresolutions.model.dtos.FileSystemDto;

public interface IQueryFileSystemFacade {
    CustomResponseDto getFiles(FileSystemDto fileSystemDto);

    CustomResponseDto getContents(FileSystemDto fileSystemDto);

    CustomResponseDto write(FileSystemDto fileSystemDto);

    CustomResponseDto delete(FileSystemDto fileSystemDto);
}
