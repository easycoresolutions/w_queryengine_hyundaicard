package co.kr.coresolutions.facades;

import co.kr.coresolutions.model.dtos.SocketBodyDto;

@FunctionalInterface
public interface IQuerySocketFacade {
    SocketBodyDto send(SocketBodyDto socketBody);
}
