package co.kr.coresolutions.facades;

import co.kr.coresolutions.commons.CustomResponseDto;
import com.fasterxml.jackson.databind.JsonNode;

@FunctionalInterface
public interface IQueryDimensionFacade {
    CustomResponseDto queryWithDimension(String connectionID, String langType, String dimensionName, String sql, JsonNode node);
}
