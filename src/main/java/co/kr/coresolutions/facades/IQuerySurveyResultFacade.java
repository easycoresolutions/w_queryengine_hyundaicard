package co.kr.coresolutions.facades;

import co.kr.coresolutions.commons.ResponseDto;
import co.kr.coresolutions.model.dtos.SurveyResultDto;

public interface IQuerySurveyResultFacade {
    ResponseDto saveSurveyResult(SurveyResultDto surveyResultDto, String userID, String projectID);

    ResponseDto deleteSurveyResult(String userID, String projectID);

    ResponseDto getSurveyResult(String userID, String projectID);

    ResponseDto getSurveyResults(String userID);
}
