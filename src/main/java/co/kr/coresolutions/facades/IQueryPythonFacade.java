package co.kr.coresolutions.facades;

import co.kr.coresolutions.model.dtos.SocketDto;
import co.kr.coresolutions.model.dtos.SocketLoginDto;

@FunctionalInterface
public interface IQueryPythonFacade {
    Object evaluate(SocketDto socketDto, String connectionID);

    interface IAdmin {
        Object open(String owner, String connectionID);

        Object close(String owner, String connectionID);
    }

    interface IUser {
        Object listUsers();

        Object listUsersRunning(String connectionID);

        Object deleteUser(SocketLoginDto socketLoginDto, String connectionID, String userID);
    }
}
