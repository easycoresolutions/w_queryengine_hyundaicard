package co.kr.coresolutions.facades;

import co.kr.coresolutions.commons.CustomResponseDto;
import co.kr.coresolutions.model.dtos.CriteriaDto;
import co.kr.coresolutions.model.dtos.DelayDto;

public interface IQueryDateFacade {
    CustomResponseDto validate(String connectionID, String holidayTableName, String yyyyMMdd, CriteriaDto criteriaDto);

    CustomResponseDto calculate(DelayDto delayDto);

    boolean isValidWeekOrMonth(String inLowerCaseFormat, Object object);
}
