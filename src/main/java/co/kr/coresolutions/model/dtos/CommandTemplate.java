package co.kr.coresolutions.model.dtos;

import com.fasterxml.jackson.annotation.JsonInclude;
import com.fasterxml.jackson.annotation.JsonProperty;
import com.fasterxml.jackson.databind.JsonNode;
import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.Data;
import lombok.NoArgsConstructor;
import org.hibernate.validator.constraints.NotBlank;

import java.util.List;

@AllArgsConstructor
@NoArgsConstructor
@Data
@Builder
@JsonInclude(JsonInclude.Include.NON_EMPTY)
public class CommandTemplate {
    @JsonProperty(value = "commandid", required = true)
    @NotBlank
    private String commandID;

    @JsonProperty(required = true)
    @NotBlank
    private String owner;

    @JsonProperty
    private List<String> errorWord;

    @JsonProperty
    private JsonNode prompts;
}
