package co.kr.coresolutions.model.dtos;

import com.fasterxml.jackson.annotation.JsonInclude;
import com.fasterxml.jackson.annotation.JsonProperty;
import com.fasterxml.jackson.databind.node.ObjectNode;
import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;

@Data
@AllArgsConstructor
@NoArgsConstructor
@JsonInclude(JsonInclude.Include.NON_NULL)
public class QueryTemplateDto {
    @JsonProperty
    private ObjectNode replace;
    @JsonProperty(value = "TEMPLATE")
    private ObjectNode template;
    @JsonProperty(required = true, value = "queryid")
    private String queryID;
    @JsonProperty(value = "connectionid", required = true)
    private String connectionID;
    @JsonProperty(required = true)
    private String file;
    @JsonProperty(value = "userid")
    private String userID;
}
