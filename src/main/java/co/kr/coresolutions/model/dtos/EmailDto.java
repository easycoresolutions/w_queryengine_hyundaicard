package co.kr.coresolutions.model.dtos;

import co.kr.coresolutions.commons.Conditional;
import com.fasterxml.jackson.annotation.JsonInclude;
import com.fasterxml.jackson.annotation.JsonProperty;
import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.Data;
import lombok.NoArgsConstructor;
import org.hibernate.validator.constraints.NotEmpty;

import javax.validation.constraints.NotNull;
import java.util.List;

@AllArgsConstructor
@NoArgsConstructor
@Data
@Builder
@JsonInclude(JsonInclude.Include.NON_EMPTY)
@Conditional(selected = "emailType")
public class EmailDto {

    @JsonProperty(value = "connectionid", required = true)
    private String connectionId;

    @NotNull
    private List<String> to;

    @NotEmpty
    private String from;

    @NotNull
    private List<String> cc;

    @NotEmpty
    private String subject;

    private String emailType;

    @NotEmpty
    private String data;

    @NotNull
    private List<String> attachment;
}

