package co.kr.coresolutions.model.dtos;

import com.fasterxml.jackson.annotation.JsonCreator;
import com.fasterxml.jackson.annotation.JsonInclude;
import com.fasterxml.jackson.annotation.JsonProperty;
import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;
import org.hibernate.validator.constraints.NotEmpty;

import javax.validation.Valid;

@Data
@AllArgsConstructor
@NoArgsConstructor
@JsonInclude(JsonInclude.Include.NON_ABSENT)
public class TargetDto {
    @JsonProperty(required = true)
    @NotEmpty(message = "sql should not be empty")
    private String sql;
    @JsonProperty(value = "userid", required = true)
    @NotEmpty(message = "userid should not be empty")
    private String userID;
    @JsonProperty
    private String owner;
    @JsonProperty(value = "input_connection", required = true)
    @NotEmpty(message = "input_connection should not be empty")
    private String inputConnection;
    @JsonProperty(required = true)
    @NotEmpty(message = "connection should not be empty")
    private String connection;
    @JsonProperty(required = true)
    @Valid
    private Parameter parameters;

    @Data
    public static class Parameter {
        @NotEmpty(message = "t_id should not be empty")
        public String tId;
        @NotEmpty(message = "t_name should not be empty")
        public String tName;

        @JsonCreator
        public Parameter(@JsonProperty(value = "t_id", required = true) String tId, @JsonProperty(value = "t_name", required = true) String tName) {
            this.tId = tId;
            this.tName = tName;
        }
    }
}
