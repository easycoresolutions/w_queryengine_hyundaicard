package co.kr.coresolutions.model.dtos;

import com.fasterxml.jackson.annotation.JsonCreator;
import com.fasterxml.jackson.annotation.JsonInclude;
import com.fasterxml.jackson.annotation.JsonProperty;
import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;
import org.hibernate.validator.constraints.NotEmpty;
import org.json.JSONObject;

import javax.validation.Valid;
import java.util.List;

@Data
@AllArgsConstructor
@NoArgsConstructor
@JsonInclude(JsonInclude.Include.NON_EMPTY)
public class GATemplateDto {
    @JsonProperty(required = true, value = "query")
    @Valid
    private QueryGA queryGA;
    @JsonProperty(required = true)
    @NotEmpty
    private String output;

    @Data
    @JsonInclude(JsonInclude.Include.NON_NULL)
    public static class QueryGA {
        @NotEmpty
        String startDate;
        @NotEmpty
        String endDate;
        List<String> dimensions;
        List<String> metrics;
        JSONObject filters;
        Integer startIndex;
        Integer maxResults;

        @JsonCreator
        public QueryGA(@JsonProperty(value = "start-date", required = true) String startDate, @JsonProperty(value = "end-date", required = true) String endDate,
                       @JsonProperty("dimensions") List<String> dimensions, @JsonProperty(value = "metrics", required = true) List<String> metrics,
                       @JsonProperty(value = "filters", required = true) JSONObject filters, @JsonProperty(value = "start-index") Integer startIndex,
                       @JsonProperty(value = "max-results", required = true) Integer maxResults) {
            this.startDate = startDate;
            this.endDate = endDate;
            this.dimensions = dimensions;
            this.metrics = metrics;
            this.filters = filters;
            this.startIndex = startIndex;
            this.maxResults = maxResults;
        }
    }
}
