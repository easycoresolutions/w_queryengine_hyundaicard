package co.kr.coresolutions.model.dtos;

import com.fasterxml.jackson.annotation.JsonInclude;
import com.fasterxml.jackson.annotation.JsonProperty;
import com.fasterxml.jackson.databind.JsonNode;
import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.Data;
import lombok.NoArgsConstructor;

import java.util.List;

@Data
@AllArgsConstructor
@NoArgsConstructor
@Builder
@JsonInclude(JsonInclude.Include.NON_ABSENT)
public class MustacheTemplateDTO {
    @JsonProperty(required = true)
    private List<String> ids;
    @JsonProperty(required = true)
    private String separator;
    @JsonProperty
    private String owner;
    @JsonProperty(required = true)
    private JsonNode parameters;
}
