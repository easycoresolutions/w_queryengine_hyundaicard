package co.kr.coresolutions.model.dtos;

import com.fasterxml.jackson.annotation.JsonInclude;
import com.fasterxml.jackson.annotation.JsonProperty;
import com.fasterxml.jackson.databind.JsonNode;
import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.Data;
import lombok.NoArgsConstructor;

@Data
@AllArgsConstructor
@NoArgsConstructor
@Builder
@JsonInclude(JsonInclude.Include.NON_EMPTY)
public class NodeCommand {
    @JsonProperty(required = true)
    private Boolean submit;
    @JsonProperty("batchsubmit")
    private Boolean batchSubmit;
    @JsonProperty("retargeting")
    private Boolean retargeting;
    private String url;
    private String stepID;
    private JsonNode body;
    private JsonNode batch_body;
    @JsonProperty(required = true)
    private String nodeId;
}
