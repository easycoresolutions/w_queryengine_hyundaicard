package co.kr.coresolutions.model.dtos;

import com.fasterxml.jackson.annotation.JsonCreator;
import com.fasterxml.jackson.annotation.JsonInclude;
import com.fasterxml.jackson.annotation.JsonProperty;
import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;

import javax.validation.Valid;
import javax.validation.constraints.Size;
import java.util.List;

@Data
@AllArgsConstructor
@NoArgsConstructor
@JsonInclude(JsonInclude.Include.NON_EMPTY)
public class CommentDto {
    @JsonProperty(required = true)
    private String projectID;
    @JsonProperty(required = true, value = "CMT_SOURCE")
    private String cmtSource;
    @JsonProperty(required = true, value = "CMT_TITLE")
    private String cmtTitle;
    @JsonProperty(required = true, value = "CMT_SHARED")
    private String cmtShared;
    @JsonProperty(required = true, value = "CMT_REGDATE")
    private String cmtRegDate;
    @JsonProperty(required = true, value = "CMT_OWNER")
    private String cmtOwner;
    @JsonProperty(required = true, value = "CMT_CONTENTS")
    private String cmtContents;
    @JsonProperty(required = true, value = "CMT_SEQUENCE")
    private String cmtSequence;
    @JsonProperty(required = true, value = "DEL_F")
    private String delF;
    @JsonProperty(required = true)
    @Valid
    @Size(min = 1)
    private List<CommentAuth> auth;

    @Data
    public static class CommentAuth {
        String authID;

        @JsonCreator
        public CommentAuth(@JsonProperty("auth_id") String authID) {
            this.authID = authID;
        }
    }
}
