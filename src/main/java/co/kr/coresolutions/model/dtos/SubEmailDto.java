package co.kr.coresolutions.model.dtos;

import co.kr.coresolutions.commons.Conditional;
import com.fasterxml.jackson.annotation.JsonInclude;
import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.Data;
import lombok.NoArgsConstructor;
import org.hibernate.validator.constraints.NotEmpty;

import javax.validation.constraints.NotNull;

@AllArgsConstructor
@NoArgsConstructor
@Data
@Builder
@JsonInclude(JsonInclude.Include.NON_EMPTY)
@Conditional(selected = "emailType")
public class SubEmailDto {

    @NotEmpty
    private String id;

    @NotEmpty
    private String password;

    @NotEmpty
    private String smtp;

    @NotNull
    private int port;
}

