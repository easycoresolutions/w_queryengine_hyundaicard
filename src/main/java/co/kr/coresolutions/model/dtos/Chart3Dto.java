package co.kr.coresolutions.model.dtos;

import co.kr.coresolutions.commons.ConditionalOneExist;
import com.fasterxml.jackson.annotation.JsonInclude;
import com.fasterxml.jackson.annotation.JsonProperty;
import com.fasterxml.jackson.databind.JsonNode;
import com.fasterxml.jackson.databind.node.ObjectNode;
import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.EqualsAndHashCode;
import lombok.NoArgsConstructor;

@Data
@EqualsAndHashCode(callSuper = true)
@AllArgsConstructor
@NoArgsConstructor
@JsonInclude(JsonInclude.Include.NON_EMPTY)
@ConditionalOneExist(firstSelected = "chartFile", secondSelected = "queryFile")
public class Chart3Dto extends Chart2Dto {
    @JsonProperty(required = true)
    private ObjectNode prompt;
    @JsonProperty(required = true)
    private JsonNode template;
    @JsonProperty(required = true, value = "userid")
    private String userId;
    @JsonProperty(required = true)
    private String owner;
    @JsonProperty
    private String chartFile;
    @JsonProperty
    private String queryFile;
    @JsonProperty
    private String dir;
}
