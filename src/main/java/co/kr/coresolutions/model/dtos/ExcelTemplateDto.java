package co.kr.coresolutions.model.dtos;

import com.fasterxml.jackson.annotation.JsonInclude;
import com.fasterxml.jackson.annotation.JsonProperty;
import lombok.Data;

@Data
@JsonInclude(JsonInclude.Include.NON_EMPTY)
public class ExcelTemplateDto {
    @JsonProperty(value = "xl_id")
    private String xlID;
    @JsonProperty(required = true)
    private String sql;
    @JsonProperty(required = true, value = "connectionid")
    private String connectionID;
    @JsonProperty(required = true)
    private String owner;
    @JsonProperty(required = true, value = "datasheet")
    private String dataSheet;
    @JsonProperty(required = true)
    private String template;
    @JsonProperty(required = true, value = "outname")
    private String outName;
    @JsonProperty(required = true, value = "ext")
    private String extension;
    @JsonProperty
    private String desc;
    @JsonProperty
    private Long maxRecords;
}
