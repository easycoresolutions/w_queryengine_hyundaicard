package co.kr.coresolutions.model.dtos;

import com.fasterxml.jackson.annotation.JsonInclude;
import com.fasterxml.jackson.annotation.JsonProperty;
import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.Data;
import lombok.NoArgsConstructor;
import org.hibernate.validator.constraints.NotEmpty;

import java.util.List;

@AllArgsConstructor
@NoArgsConstructor
@Data
@Builder
@JsonInclude(JsonInclude.Include.NON_EMPTY)
//@ConditionalPassword(selected = "password", target = "userId", message = "A PW can't have same string as userid. ")
public class LoginDto {
    @NotEmpty
    @JsonProperty(value = "UserId", required = true)
    private String userId;
    @NotEmpty
    @JsonProperty(value = "Password", required = true)
//    @Size(min = 10, max = 16, message = "A PW should have a length of 10~16 bytes.")
//    @Pattern(regexp = "(?=.*\\d)(?=.*[a-z])(?=.*[A-Z])(?=.*[@#$-_~^&*()+={}%])", message = "A PW should have a combination of lower-case , upper-case ,number and special character. ")
    private String password;
    @JsonProperty(value = "NewPassword")
    private String newPassword;
    @JsonProperty(value = "InternalAuths")
    private List<String> internalAuths;
    @JsonProperty(value = "UserName")
    private String userName;
    @JsonProperty(value = "AccessLevel")
    private Integer accessLevel;
    @JsonProperty(value = "DeptId")
    private String deptId;
    @JsonProperty(value = "UserTel")
    private String userTel;
    @JsonProperty(value = "UserIp")
    private String userIp;
    @JsonProperty(value = "UserMobile")
    private String userMobile;
    @JsonProperty(value = "UserEmail")
    private String userEmail;
    @JsonProperty(value = "UserLang")
    private String userLang;
}
