package co.kr.coresolutions.model.dtos;

import co.kr.coresolutions.commons.ConditionalOneExist;
import com.fasterxml.jackson.annotation.JsonInclude;
import com.fasterxml.jackson.annotation.JsonProperty;
import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.Data;
import lombok.NoArgsConstructor;

@Data
@AllArgsConstructor
@NoArgsConstructor
@Builder
@JsonInclude(JsonInclude.Include.NON_EMPTY)
@ConditionalOneExist(firstSelected = "script", secondSelected = "file")
public class SocketDto {
    @JsonProperty(required = true)
    private String owner;
    @JsonProperty
    private String script;
    @JsonProperty
    private String file;
    private String format;
}
