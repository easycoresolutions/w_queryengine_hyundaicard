package co.kr.coresolutions.model.dtos;

import com.fasterxml.jackson.annotation.JsonCreator;
import com.fasterxml.jackson.annotation.JsonInclude;
import com.fasterxml.jackson.annotation.JsonProperty;
import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.Data;
import lombok.NoArgsConstructor;

@Data
@AllArgsConstructor
@NoArgsConstructor
@Builder
@JsonInclude(JsonInclude.Include.NON_EMPTY)
public class NodeDto {
    @JsonProperty(value = "connectionid", required = true)
    private String connectionID;
    @JsonProperty(value = "table", required = true)
    private String table;
    @JsonProperty(value = "user", required = true)
    private String user;
    @JsonProperty(value = "datevalidation", required = true)
    private DateCriteriaDto dateCriteria;
    @JsonProperty(value = "conditions", required = true)
    private Conditions conditions;
    @JsonProperty
    private String source;

    @Data
    @JsonInclude(JsonInclude.Include.NON_NULL)
    public static class Conditions {
        Boolean submit;
        Boolean batchsubmit;
        Boolean retargeting;

        @JsonCreator
        public Conditions(@JsonProperty(value = "batchsubmit") Boolean batchsubmit,
                          @JsonProperty(value = "retargeting") Boolean retargeting,
                          @JsonProperty(value = "submit") Boolean submit) {
            this.batchsubmit = batchsubmit;
            this.retargeting = retargeting;
            this.submit = submit;
        }
    }
}
