package co.kr.coresolutions.model.dtos;


import com.fasterxml.jackson.annotation.JsonInclude;
import com.fasterxml.jackson.annotation.JsonProperty;
import com.fasterxml.jackson.databind.JsonNode;
import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;

import javax.validation.Valid;

@Data
@AllArgsConstructor
@NoArgsConstructor
@JsonInclude(JsonInclude.Include.NON_EMPTY)
public class QuadMaxWorkDto {
    @JsonProperty(required = true, value = "projectid")
    private String projectID;
    @Valid
    @JsonProperty(required = true)
    private JsonNode nodes;
}
