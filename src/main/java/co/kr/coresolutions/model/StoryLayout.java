package co.kr.coresolutions.model;

import com.fasterxml.jackson.annotation.JsonInclude;
import com.fasterxml.jackson.databind.node.ObjectNode;
import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;
import lombok.ToString;
import org.hibernate.validator.constraints.NotBlank;

@NoArgsConstructor
@AllArgsConstructor
@Getter
@Setter
@ToString
@Builder
@JsonInclude(JsonInclude.Include.NON_EMPTY)
public class StoryLayout {
    @NotBlank
    private String storylayoutID;
    @NotBlank
    private String storylayoutName;
    @NotBlank
    private String storylayoutOwner;
    @NotBlank
    private String storylayoutCreated;
    @NotBlank
    private String storylayoutModified;
    @NotBlank
    private String storylayoutDesc;
    @NotBlank
    private String storylayoutUserid;
    @NotBlank
    private String storylayoutCategory;
    @NotBlank
    private String storylayoutVersion;
    private ObjectNode storylayoutjson;
}
