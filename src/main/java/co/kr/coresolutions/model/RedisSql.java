package co.kr.coresolutions.model;

import com.fasterxml.jackson.annotation.JsonInclude;
import com.fasterxml.jackson.annotation.JsonProperty;
import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;
import lombok.ToString;
import org.hibernate.validator.constraints.NotBlank;

@NoArgsConstructor
@AllArgsConstructor
@Getter
@Setter
@ToString
@Builder
@JsonInclude(JsonInclude.Include.NON_EMPTY)
public class RedisSql {
    @NotBlank
    @JsonProperty("connectionid")
    private String connectionid;
    @NotBlank
    @JsonProperty("sql")
    private String sql;
    @NotBlank
    @JsonProperty("KEY_NAME")
    private String KEY_NAME;
}

