package co.kr.coresolutions.model;

import com.fasterxml.jackson.annotation.JsonInclude;
import com.fasterxml.jackson.annotation.JsonProperty;
import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.Data;
import lombok.NoArgsConstructor;

@NoArgsConstructor
@AllArgsConstructor
@Data
@Builder
@JsonInclude(JsonInclude.Include.NON_EMPTY)
public class TableLoad {
    @JsonProperty(value = "userid", required = true)
    private String userid;
    @JsonProperty(value = "db_file", required = true)
    private String db_file;
    @JsonProperty(value = "table_name", required = true)
    private String table_name;
    @JsonProperty(value = "input_directory", required = true)
    private String input_directory;
    @JsonProperty(value = "file_name", required = true)
    private String file_name;
}

