package co.kr.coresolutions.model;

import com.fasterxml.jackson.annotation.JsonInclude;
import com.fasterxml.jackson.annotation.JsonProperty;
import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.Data;
import lombok.NoArgsConstructor;

@NoArgsConstructor
@AllArgsConstructor
@Data
@Builder
@JsonInclude(JsonInclude.Include.NON_EMPTY)
public class Connection {
    @JsonProperty
    private String CONNNAME;
    @JsonProperty
    private String DBMS;
    @JsonProperty
    private String DRIVER;
    @JsonProperty
    private String URL;
    @JsonProperty
    private String ID;
    @JsonProperty
    private String PW;
    @JsonProperty
    private String OWNER;
    @JsonProperty
    private String ETC;
    @JsonProperty(value = "SCHEMA")
    private String SCHEME;
    @JsonProperty
    private String Host;
    @JsonProperty
    private String Port;
    @JsonProperty
    private String UrlHttp;
    @JsonProperty(value = "PW_CONNECTION")
    private String pwConnection;
}
