package co.kr.coresolutions.model;

import com.fasterxml.jackson.annotation.JsonCreator;
import com.fasterxml.jackson.annotation.JsonInclude;
import com.fasterxml.jackson.annotation.JsonProperty;
import com.fasterxml.jackson.databind.node.ObjectNode;
import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.Data;
import lombok.NoArgsConstructor;
import org.hibernate.validator.constraints.NotBlank;

import java.io.Serializable;
import java.util.List;

import javax.persistence.Column;

@AllArgsConstructor
@NoArgsConstructor
@Data
@Builder
@JsonInclude(JsonInclude.Include.NON_EMPTY)
public class Command implements Serializable {
    @NotBlank
    @Column(name="Command")
    @JsonProperty(value = "Command")
    private String Command;
    @NotBlank
    @Column(name="Output")
    @JsonProperty(value = "Output")
    private String Output;
    @JsonProperty(value = "commandId")
    private String CommandId;
    private List<String> CHAR;
    private List<String> NUM;
    private ObjectNode parameter;
    private CommandDetails commandDetails;
    @JsonProperty(value = "Alert")
    private boolean alert;
    private String owner;
    @JsonProperty(value = "userid")
    private String userId;
    @JsonProperty(value = "runMode")
    private String runMode;

    @Data
    @Builder
    @NoArgsConstructor
    public static class CommandDetails implements Serializable {
        String CommandID;
        String commadName;
        String startdatetime;
        String owner;

        @JsonCreator
        public CommandDetails(@JsonProperty("CommandID") String commandID, @JsonProperty("commadName") String commandName,
                              @JsonProperty("startdatetime") String startDT, @JsonProperty("owner") String owner) {
            this.CommandID = commandID;
            this.commadName = commandName;
            this.startdatetime = startDT;
            this.owner = owner;
        }
    }

}