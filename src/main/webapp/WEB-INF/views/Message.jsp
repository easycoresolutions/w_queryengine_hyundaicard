<%@include file="taglibs.jsp" %>
<html>
<head>
    <link rel="stylesheet" href="${pageContext.request.contextPath}/assets/css/all.css" type="text/css"/>
    <meta name="viewport" content="width=device-width, initial-scale=1" charset="UTF-8">
    <title>Messaging WebSocket</title>
    <script src="${pageContext.request.contextPath}/assets/js/jquery-3.1.1.js"></script>
    <script src="${pageContext.request.contextPath}/assets/js/sockjs.min.js"></script>
    <script src="${pageContext.request.contextPath}/assets/js/stomp.js"></script>
    <script type="text/javascript">
        var stompClient = null;

        function connect() {
            var socket = new SockJS('${pageContext.request.contextPath}/socket/send');
            stompClient = Stomp.over(socket);
            stompClient.connect({}, function (frame) {
                console.log('Connected: ' + frame);

                stompClient.subscribe('/topic/messages', function (messageOutput) {
                    showMessageOutput(JSON.parse(messageOutput.body));
                }, function (Error) {
                    console.log(Error);
                });
            });
        }

        function disconnect() {
            if (stompClient != null) {
                stompClient.disconnect();
            }
            console.log("Disconnected");
        }

        function showMessageOutput(messageOutput) {
            $('#key').val(messageOutput.Key);
            $('#message').text(messageOutput.Message);
            $("#userid").val(messageOutput.UserId)
        }
    </script>
</head>
<body onload="connect()" onended="disconnect()">
<div class="form-group input-group" style="left:10%; width:20%;">
    <h2 class="form-heading">Socket Listener</h2>
    <div class="form-group">
        <label for="key">Key: </label>
        <input type="text" id="key" class="form-control"/>
    </div>
    <div class="form-group">
        <label for="message">Message: </label>
        <textarea class="form-control" id="message"></textarea>
    </div>
    <div class="form-group">
        <label for="userid">UserID: </label>
        <textarea class="form-control" id="userid"></textarea>
    </div>
</div>
</body>
</html>