<%--
  Created by IntelliJ IDEA.
  User: yassine
  Date: 2017-10-11
  Time: 04:01 PM
  To change this template use File | Settings | File Templates.
--%>
<%@include file="taglibs.jsp" %>
<html>
<head>
    <title>Query Server</title>
    <link rel="stylesheet" href="${pageContext.request.contextPath}/assets/css/all.css" type="text/css"/>
    <meta name="viewport" content="width=device-width, initial-scale=1" charset="UTF-8">
    <script type="text/javascript" src="${pageContext.request.contextPath}/assets/js/angular.min.js"></script>
    <script type="text/javascript" src="${pageContext.request.contextPath}/assets/js/angular-resource.js"></script>
    <script type="text/javascript" src="${pageContext.request.contextPath}/assets/js/jquery-3.1.1.js"></script>
    <script type="text/javascript" src="${pageContext.request.contextPath}/assets/js/bootstrap_3.3.7.min.js"></script>
    <script>var ctx = "${pageContext.request.contextPath}"</script>

    <script type="text/javascript" src="${pageContext.request.contextPath}/assets/jsscripts/queryServer.js"></script>
</head>
<body ng-app="queryApp">
<div ng-controller="queryCTRL" class="form-group input-group" style="left:10%; width:40%;">
    <h4><label for="buttonGetCsv" class="col-md-3 control-label">Get CSV</label></h4>
    <div class="col-md-6">
        <input type="button" id="buttonGetCsv" class="form-control" value="Submit"
               ng-click="getforCSV('mssql1','q0001')"/>
    </div>
    <br><br>
    <h4><label for="buttonGetJson" class="col-md-3 control-label">Get Json</label></h4>
    <div class="col-md-6">
        <input type="button" id="buttonGetJson" class="form-control" value="Submit"
               ng-click="getforJson('mssql1','q0001')"/>
    </div>
    <br><br>

    <h4><label for="buttonPost" class="col-md-3 control-label">Post Query</label></h4>
    <div class="col-md-6">
        <input type="button" id="buttonPost" class="form-control" value="Submit" ng-click="postQuery('mssql1')"/>
    </div>


</div>

</body>
</html>
