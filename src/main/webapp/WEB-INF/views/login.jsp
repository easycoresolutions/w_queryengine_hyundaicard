<%@include file="taglibs.jsp" %>
<!DOCTYPE html>
<html lang="en">

<head>
    <script>var ctx = "${pageContext.request.contextPath}"</script>
    <link rel="stylesheet" href="${pageContext.request.contextPath}/assets/css/all.css" type="text/css"/>
    <meta name="viewport" content="width=device-width, initial-scale=1" charset="UTF-8">
    <script type="text/javascript" src="${pageContext.request.contextPath}/assets/js/angular.min.js"></script>
    <script type="text/javascript" src="${pageContext.request.contextPath}/assets/js/angular-resource.js"></script>
    <script type="text/javascript" src="${pageContext.request.contextPath}/assets/js/jquery-3.1.1.js"></script>
    <script type="text/javascript" src="${pageContext.request.contextPath}/assets/js/bootstrap_3.3.7.min.js"></script>
    <script type="text/javascript" src="${pageContext.request.contextPath}/assets/jsscripts/queryServer.js"></script>
    <title>Login</title>
</head>


<body ng-app="queryApp">
<div ng-controller="authCTRL" class="form-group input-group" style="left:10%; width:20%;">
    <form class="form-signin">
        <h2 class="form-heading">Log in</h2>
        <div class="form-group">
            <input id="UserId" type="text" class="form-control" placeholder="UserId" autofocus="true"/>
            <input id="Password" type="password" class="form-control" placeholder="Password"/>
            <input class="btn btn-md btn-primary" ng-click="login()" value="Log In" id="submit" type="submit"/>
        </div>
    </form>
    <div id="result" hidden>

    </div>
</div>
</body>
</html>